# TensorPhylo
## By Mike and Xavier

Fast implementation of SSE-type models likelihood.

## Preprint

* [Definition of the Generalized State-Dependent Birth-Death-Sampling process](doc/preprint/preprint.pdf)

## Other pages
* [How to install TensorPhylo](doc/Install.md)
* [Custom input files](doc/InputFiles.md)
* [Benchmarks](doc/Benchmark.md)
* [Todo list](doc/TODO.md)
