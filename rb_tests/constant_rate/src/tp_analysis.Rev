# do a revbayes analysis under a constant-rate model with
# TENSORPHYLO
# verdict: WORKS

loadPlugin("TensorPhylo")

# read the tree
tree = readTrees("data/tree.nex")[1]
taxa = tree.taxa()

# set parameters
age <- tree.rootAge()

lambda_param ~ dnExponential(1)
moves[1] = mvScale(lambda_param)

epsilon ~ dnBeta(1,1)
moves[2] = mvBetaProbability(epsilon)

mu_param := lambda_param * epsilon

# make the tensors
k = 2

lambda := rep(lambda_param, k)
mu     := rep(mu_param, k)

# mass-event parameters
rho <- 0.25

# anagenetic events
eta <- fnFreeSymmetricRateMatrix(rep(0.2, choose(k,2) ), rescaled=FALSE)

# root frequency
root_freq <- simplex(rep(1, k))

# condition
condition <- "survival"

# distribution object
psi ~ dnGeneralizedLineageHeterogeneousBirthDeathProcess(
		rootAge      = age,
		lambda       = lambda,
		mu           = mu,
		rho          = rho,
		eta          = eta,
		pi           = root_freq,
		condition    = condition,
		taxa         = taxa
)
psi.clamp(tree)

# model
my_model = model(psi)

# monitors
monitors[1] = mnScreen(printgen=100)
monitors[2] = mnModel(printgen=1, filename="output/tp.log")

# analysis
my_mcmc = mcmc(my_model, monitors, moves)
my_mcmc.burnin(1000, 100)
my_mcmc.run(100000)


q()
