# calculate the likelihood with TENSORPHYLO
# verdict: WORKS

loadPlugin("TensorPhylo")

k = 2

# read the tree
tree = readTrees("data/example_yule_tree.nex")[1]
taxa = tree.taxa()
data = readDiscreteCharacterData("data/example_yule_tree.nex")

# set parameters
age <- tree.rootAge()

lambda_param ~ dnExponential(1)
lambda_param.setValue(1)

epsilon ~ dnBeta(1,1)
epsilon.setValue(0)

mu_param := lambda_param * epsilon

# make the tensors
lambda := rep(lambda_param, k)
mu     := rep(mu_param, k)

# mass events
frac <- 0.25

# anagenetic events
eta <- fnFreeSymmetricRateMatrix(rep(0.2, choose(k,2) ), rescaled=FALSE)

# root frequency
root_freq <- simplex(rep(1, k))

# condition
condition <- "survival"

# distribution object
psi ~ dnGLHBDSP(
		rootAge      = age,
		lambda       = lambda,
		mu           = mu,
		rho          = frac,
		eta          = eta,
		pi           = root_freq,
		condition    = condition,
		taxa         = taxa,
		nStates      = k
)
psi.clamp(tree)
psi.clampCharData(data)
# psi.clamp(tree)
"original likelihood: " + psi.lnProbability()

# iterate over parameters
lambdas  = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
epsilons = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]

for(l in lambdas) {
	for(e in epsilons) {
		lambda_param.setValue(l)
		epsilon.setValue(e)
		"lambda:\t" + lambda_param + "\tmu:\t" + mu_param + "\tlik:\t" + psi.lnProbability()
	}
}

q()
