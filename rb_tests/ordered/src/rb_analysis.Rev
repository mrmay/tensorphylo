# read the tree
tree = readTrees("data/tree.nex")[1]
taxa = tree.taxa()
age  <- tree.rootAge()
frac <- 0.25

# variables
k    = 40
mvi  = 0
mni  = 0

# set parameters
lambda_mean ~ dnExponential(10)
moves[++mvi] = mvScale(lambda_mean)

lambda_sd ~ dnExponential(2)
moves[++mvi] = mvScale(lambda_sd)

lambda := fnDiscretizeDistribution( dnLognormal( ln(lambda_mean) - 0.5 * lambda_sd * lambda_sd, lambda_sd ), k )

moves[++mvi] = mvUpDownScale()
moves[mvi].addVariable(lambda_mean, up=true)
moves[mvi].addVariable(lambda_sd, up=false)

epsilon ~ dnBeta(1,1)
moves[++mvi] = mvBetaProbability(epsilon)

mu := epsilon * lambda

frac   <- 0.25

# transition rates
delta ~ dnExponential(10)
moves[++mvi] = mvScale(delta)

Q := fnOrderedRateMatrix(k, delta, delta)

# root frequency
root_freq <- simplex(rep(1, k))

# condition
condition <- "time"

# distribution
psi ~ dnCDBDP(rootAge = age, lambda = lambda, mu = mu, Q = Q, delta = 1.0, pi = root_freq, rho = frac, condition = condition)
psi.clamp(tree)

# model
my_model = model(psi)

# monitors
monitors[1] = mnScreen(printgen=100, delta)
monitors[2] = mnModel(printgen=10, filename="output/rb.log")

# analysis
my_mcmc = mcmc(my_model, monitors, moves)
my_mcmc.burnin(1000, 100)
my_mcmc.run(10000)


q()
