# calculate the likelihood with standard implementation
# verdict: WORKS

loadPlugin("TensorPhylo", "/home/mike/repos/tensorphyloprototype/build_local")

# read the tree
tree = readTrees("data/example_fbd_tree.nex")[1]
taxa = tree.taxa()

# set parameters
k = 2
age <- tree.rootAge()
frac <- 0.2

# rate parameters
lambda ~ dnExponential(1)
lambda.setValue(0.9)

mu ~ dnExponential(1)
mu.setValue(0.5)

phi ~ dnExponential(1)
phi.setValue(0.1)

lambda_tens := rep(lambda, k)
mu_tens     := rep(mu, k)
phi_tens    := rep(phi, k)

# anagenetic events
eta <- fnFreeSymmetricRateMatrix(rep(0.2, choose(k,2) ), rescaled=FALSE)

# root frequency
root_freq <- simplex(rep(1, k))

# condition
condition <- "time"

# make the distribution
xi ~ dnSerialSampledBirthDeath(rootAge = age, lambda = lambda, mu = mu, psi = phi, rho = frac, condition = condition, taxa = taxa)
xi.clamp(tree)

tsi ~ dnGLHBDSP(
		rootAge      = age,
		lambda       = lambda_tens,
		mu           = mu_tens,
		phi          = phi_tens,
		rho          = frac,
		eta          = eta,
		pi           = root_freq,
		condition    = condition,
		taxa         = taxa
)
tsi.clamp(tree)

# now with CDBDP implementation
cd ~ dnCDBDP(rootAge = age, lambda = lambda_tens, mu = mu_tens, phi = phi_tens, Q = eta, delta = 1.0, pi = root_freq, rho = frac, condition = condition)
cd.clamp(tree)

tsi.lnProbability() + " -- " + cd.lnProbability()
"the correct answer is: -17.21199"

lambdas  = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
epsilons = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
phis     = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]

for(l in lambdas) {
	for(e in epsilons) {
		for(p in phis) {
			lambda.setValue(l)
			mu.setValue(l * e)
			phi.setValue(p)
			tp_vs_an = (tsi.lnProbability() - ln(l) - xi.lnProbability())
			rb_vs_an = (cd.lnProbability()          - xi.lnProbability())
			if ( abs(tp_vs_an) < abs(rb_vs_an) ) {
				"diff:\t" + tp_vs_an + " \t " + rb_vs_an + " \t TP wins" 
			} else {
				"diff:\t" + tp_vs_an + " \t " + rb_vs_an + " \t RB wins" 
			}
		}
	}
}

q()
