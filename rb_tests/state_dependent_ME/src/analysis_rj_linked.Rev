loadPlugin("TensorPhylo", "/home/mike/repos/tensorphyloprototype/build_local")

# helpers
mvi = 0
mni = 0

# read the data
tree = readTrees("data/tree.nex")[1]
age  = tree.rootAge()
taxa = tree.taxa()

data = readDiscreteCharacterData("data/data.nex")
k    = data.getStateDescriptions().size()

# diversification parameters
lambda ~ dnExponential(1)
moves[++mvi] = mvScale(lambda)

mu ~ dnExponential(1)
moves[++mvi] = mvScale(mu)

phi ~ dnExponential(1)
moves[++mvi] = mvScale(phi)

eta ~ dnExponential(1)
moves[++mvi] = mvScale(eta)

rho <- 1.0

# mass-extinction events
expected_num_events <- ln(2)
rate <- expected_num_events / age

gamma_times_model ~ dnMarkovTimes(rate, age)
moves[++mvi] = mvOrderedEventBirthDeath(gamma_times_model)
moves[++mvi] = mvOrderedEventTimeSlide(gamma_times_model)

gamma_num_events := gamma_times_model.getNumberOfEvents()

gamma_magnitude_dist = dnBeta(1,1)
gamma_event_model ~ dnMarkovEvents(gamma_times_model, gamma_magnitude_dist)
moves[++mvi] = mvOrderedEventSlideProbability(gamma_event_model)

gamma_event_model_rep := rep(gamma_event_model, 2)

# root frequency
root_freq <- simplex(rep(1, k))

# condition
condition <- "time"

# distribution
psi ~ dnGLHBDSP(
		rootAge      = age,
		lambda       = rep(lambda, k),
		mu           = rep(mu, k),
		phi          = rep(phi, k),
		rho          = rho,
		eta          = eta,
		gamma        = gamma_event_model_rep.getEvents(),
		gammaTimes   = gamma_times_model.getTimes(),
		pi           = root_freq,
		condition    = condition,
		taxa         = taxa,
		nStates      = k
)
psi.clamp(tree)
psi.clampCharData(data)

psi.lnProbability()

# model
my_model = model(psi)

# monitors
monitors[1] = mnScreen(printgen=10, gamma_num_events)
monitors[2] = mnModel(printgen=10, filename="output/sd_mass_extinction_rj_linked.log")
monitors[3] = mnFile(printgen=10, filename="output/sd_mass_extinction_rj_events_linked.log")
monitors[3].addVariable(gamma_times_model)
monitors[3].addVariable(gamma_event_model)

# analysis
my_mcmc = mcmc(my_model, monitors, moves)
my_mcmc.burnin(1000, 100)
my_mcmc.operatorSummary()
my_mcmc.run(10000)


q()














q()
