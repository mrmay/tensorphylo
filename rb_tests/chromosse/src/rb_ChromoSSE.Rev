loadPlugin("TensorPhylo", "/home/mike/repos/tensorphyloprototype/build_local")

seed(1)

# read the data
tree = readTrees("data/aristolochia-bd.tree")[1]
age  = tree.rootAge()
taxa = tree.taxa()

k    = 26
data = readCharacterDataDelimited("data/aristolochia_chromosome_counts.tsv", stateLabels=k + 1, type="NaturalNumbers", delimiter="\t", headers=FALSE)

mvi = 0

# rate matrix
gamma ~ dnExponential(10)
moves[++mvi] = mvScale(gamma)

delta ~ dnExponential(10)
moves[++mvi] = mvScale(delta)

rho ~ dnExponential(10)
moves[++mvi] = mvScale(rho)

Q_chromo := fnChromosomes(k, gamma, delta, rho)

# cladogenetic stuff
clado_no_change ~ dnExponential(10)
moves[++mvi] = mvScale(clado_no_change)

clado_fission ~ dnExponential(10)
moves[++mvi] = mvScale(clado_fission)

clado_fusion ~ dnExponential(10)
moves[++mvi] = mvScale(clado_fusion)

clado_polyploid ~ dnExponential(10)
moves[++mvi] = mvScale(clado_polyploid)

clado_demipoly  <- abs(0.0)

# set a vector to hold the speciation rates
speciation_rates := [clado_no_change, clado_fission, clado_fusion, clado_polyploid, clado_demipoly]
total_speciation := sum(speciation_rates)

# map the speciation rates to chromosome cladogenetic events
clado_matrix := fnChromosomesCladoEventsBD(speciation_rates, k)

# diversification parameters
mu               <- rep(0.2, k + 1)
frac             <- 1.0
root_frequencies <- simplex(rep(1, k + 1))
condition        <- "time"

# the distribution
x ~ dnCDBDP(rootAge            = age,
            cladoEventMap      = clado_matrix,
            extinctionRates    = mu, 
            Q                  = Q_chromo,
            pi                 = root_frequencies,
            rho                = frac,
			condition          = condition)
x.clamp(tree)
x.clampCharData(data)
x.lnProbability()

# model
my_model = model(x)

# monitors
monitors[1] = mnScreen(printgen=1)
monitors[2] = mnModel(printgen=10, file="output/rb.log")

# analysis
my_mcmc = mcmc(my_model, monitors, moves)
my_mcmc.burnin(1000, tuningInterval=100)
my_mcmc.run(10000)


q()
