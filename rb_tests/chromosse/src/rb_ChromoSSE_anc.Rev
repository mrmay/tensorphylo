loadPlugin("TensorPhylo", "/home/mike/repos/tensorphyloprototype/build_local")

seed(1)

# read the data
tree = readTrees("data/aristolochia-bd.tree")[1]
age  = tree.rootAge()
taxa = tree.taxa()

k    = 26
data = readCharacterDataDelimited("data/aristolochia_chromosome_counts.tsv", stateLabels=k + 1, type="NaturalNumbers", delimiter="\t", headers=FALSE)

mvi = 0

# rate matrix
gamma ~ dnExponential(10)
gamma.setValue(0.03)
moves[++mvi] = mvScale(gamma, weight=0)

delta ~ dnExponential(10)
delta.setValue(0.06)
moves[++mvi] = mvScale(delta, weight=0)

rho ~ dnExponential(10)
rho.setValue(0.025)
moves[++mvi] = mvScale(rho, weight=0)

Q_chromo := fnChromosomes(k, gamma, delta, rho)

# cladogenetic stuff
clado_no_change ~ dnExponential(10)
clado_no_change.setValue(0.4)
moves[++mvi] = mvScale(clado_no_change, weight=0)

clado_fission ~ dnExponential(10)
clado_fission.setValue(0.02)
moves[++mvi] = mvScale(clado_fission, weight=0)

clado_fusion ~ dnExponential(10)
clado_fusion.setValue(0.04)
moves[++mvi] = mvScale(clado_fusion, weight=0)

clado_polyploid ~ dnExponential(10)
clado_polyploid.setValue(0.02)
moves[++mvi] = mvScale(clado_polyploid, weight=0)

clado_demipoly  <- abs(0.0)

# set a vector to hold the speciation rates
speciation_rates := [clado_no_change, clado_fission, clado_fusion, clado_polyploid, clado_demipoly]
total_speciation := sum(speciation_rates)

# map the speciation rates to chromosome cladogenetic events
clado_matrix := fnChromosomesCladoEventsBD(speciation_rates, k)

# diversification parameters
mu               <- rep(0.2, k + 1)
frac             <- 1.0
root_frequencies <- simplex(rep(1, k + 1))
condition        <- "time"

# the distribution
x ~ dnCDBDP(rootAge            = age,
            cladoEventMap      = clado_matrix,
            extinctionRates    = mu, 
            Q                  = Q_chromo,
            pi                 = root_frequencies,
            rho                = frac,
			condition          = condition,
			nTimeSlices        = 2000)
x.clamp(tree)
x.clampCharData(data)
x.lnProbability()

# model
my_model = model(x)

# monitors
monitors[1] = mnScreen(printgen=10)
monitors[2] = mnModel(printgen=10, file="output/rb.log")
monitors[3] = mnJointConditionalAncestralState(printgen=1, file="output/rb_anc.log", tree=tree, cdbdp=x, withStartStates=true, type="NaturalNumbers")

# analysis
my_mcmc = mcmc(my_model, monitors, moves)
# my_mcmc.burnin(200, tuningInterval=50)
my_mcmc.run(20000)

# ancestral state summary
treetrace = readAncestralStateTrace("output/rb_anc.log")
anc_state_tree = ancestralStateTree(tree, treetrace, "output/rb_anc_states.tree", burnin=0, reconstruction="marginal")

q()
