loadPlugin("TensorPhylo", "/home/mike/repos/tensorphyloprototype/build_local")

seed(1)

# read the data
tree = readTrees("data/aristolochia-bd.tree")[1]
age  = tree.rootAge()
taxa = tree.taxa()

k    = 26
data = readCharacterDataDelimited("data/aristolochia_chromosome_counts.tsv", stateLabels=k + 1, type="NaturalNumbers", delimiter="\t", headers=FALSE)

mvi = 0

# rate matrix
gamma ~ dnExponential(10)
moves[++mvi] = mvScale(gamma)

delta ~ dnExponential(10)
moves[++mvi] = mvScale(delta)

rho ~ dnExponential(10)
moves[++mvi] = mvScale(rho)

Q_chromo := fnChromosomes(k, gamma, delta, rho)

# cladogenetic stuff
clado_no_change ~ dnExponential(10)
moves[++mvi] = mvScale(clado_no_change)

clado_fission ~ dnExponential(10)
moves[++mvi] = mvScale(clado_fission)

clado_fusion ~ dnExponential(10)
moves[++mvi] = mvScale(clado_fusion)

clado_polyploid ~ dnExponential(10)
moves[++mvi] = mvScale(clado_polyploid)

clado_demipoly  <- abs(0.0)


ne = 0
clado_events[++ne] <- [1,1,1]
clado_probs[ne]    := Probability(1.0)

# also specify the speciation rates
lambda[1] := 0

# right now no demipolyploidy
for(i in 1:k) { # this index is the number of chromosomes
	
	idx = i + 1 # because tensorphylo will drop the indexing by one
		
	if (i == 1) {

		# compute the total rate
		lambda[idx] := abs(clado_no_change + clado_fission + clado_polyploid)
		
		# no change
		clado_events[++ne] <- [idx, idx, idx]
		clado_probs[ne]    := Probability(clado_no_change / lambda[idx])

		# increase by one
		clado_events[++ne] <- [idx, idx + 1, idx]
		clado_probs[ne]    := Probability(0.5 * (clado_fission + clado_polyploid) / lambda[idx])
		clado_events[++ne] <- [idx, idx, idx + 1]
		clado_probs[ne]    := Probability(0.5 * (clado_fission + clado_polyploid) / lambda[idx])
		
	} else if ( i + i <= k ) { # polyploidization allowed
		
		# compute the total rate
		lambda[idx] := abs(clado_no_change + clado_fission + clado_fusion + clado_polyploid)
		
		# no change
		clado_events[++ne] <- [idx, idx, idx]
		clado_probs[ne]    := Probability(clado_no_change / lambda[idx])
		
		# increase by one
		clado_events[++ne] <- [idx, idx + 1, idx]
		clado_probs[ne]    := Probability(0.5 * clado_fission / lambda[idx])
		clado_events[++ne] <- [idx, idx, idx + 1]
		clado_probs[ne]    := Probability(0.5 * clado_fission / lambda[idx])
		
		# decrease by one
		clado_events[++ne] <- [idx, idx - 1, idx]
		clado_probs[ne]    := Probability(0.5 * clado_fusion / lambda[idx])
		clado_events[++ne] <- [idx, idx, idx - 1]
		clado_probs[ne]    := Probability(0.5 * clado_fusion / lambda[idx])
		
		# polyploidization
		clado_events[++ne] <- [idx, i + i + 1, idx]
		clado_probs[ne]    := Probability(0.5 * clado_polyploid / lambda[idx])
		clado_events[++ne] <- [idx, idx, i + i + 1]
		clado_probs[ne]    := Probability(0.5 * clado_polyploid / lambda[idx])

	} else if ( i < k ) { # fissuion but no polyploidy
		
		# compute the total rate
		lambda[idx] := abs(clado_no_change + clado_fission + clado_fusion)

		# no change
		clado_events[++ne] <- [idx, idx, idx]
		clado_probs[ne]    := Probability(clado_no_change / lambda[idx])

		# increase by one
		clado_events[++ne] <- [idx, idx + 1, idx]
		clado_probs[ne]    := Probability(0.5 * clado_fission / lambda[idx])
		clado_events[++ne] <- [idx, idx, idx + 1]
		clado_probs[ne]    := Probability(0.5 * clado_fission / lambda[idx])
		
		# decrease by one
		clado_events[++ne] <- [idx, idx - 1, idx]
		clado_probs[ne]    := Probability(0.5 * clado_fusion / lambda[idx])
		clado_events[++ne] <- [idx, idx, idx - 1]
		clado_probs[ne]    := Probability(0.5 * clado_fusion / lambda[idx])		

	} else { # maximum state -- no fission or polyploidy

		# compute the total rate
		lambda[idx] := abs(clado_no_change + clado_fusion)

		# no change
		clado_events[++ne] <- [idx, idx, idx]
		clado_probs[ne]    := Probability(clado_no_change / lambda[idx])
		
		# decrease by one
		clado_events[++ne] <- [idx, idx - 1, idx]
		clado_probs[ne]    := Probability(0.5 * clado_fusion / lambda[idx])
		clado_events[++ne] <- [idx, idx, idx - 1]
		clado_probs[ne]    := Probability(0.5 * clado_fusion / lambda[idx])		
		
	}

}

omega := fnCladogeneticProbabilityMatrix(clado_events, clado_probs, k + 1)

# diversification parameters
mu               <- rep(0.2, k + 1)
frac             <- 1.0
root_frequencies <- simplex(rep(1, k + 1))
condition        <- "time"

# the distribution
x ~ dnGLHBDSP(rootAge   = age,
			  lambda    = lambda,
			  mu        = mu,
			  eta       = Q_chromo,
			  omega     = omega,
			  pi        = root_frequencies,
			  rho       = frac,
			  condition = condition,
			  taxa      = taxa,
			  nStates   = k + 1)
x.clamp(tree)
x.clampCharData(data)
x.lnProbability()

# model
my_model = model(x)

# monitors
monitors[1] = mnScreen(printgen=1)
monitors[2] = mnModel(printgen=10, file="output/tp.log")

# analysis
my_mcmc = mcmc(my_model, monitors, moves)
# my_mcmc.burnin(1000, tuningInterval=100)
my_mcmc.run(10000)


q()
