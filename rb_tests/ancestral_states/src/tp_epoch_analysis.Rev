loadPlugin("TensorPhylo", "/home/mike/repos/tensorphyloprototype/build_local")

# read the tree
tree = readTrees("data/tree.nex")[1]
taxa = tree.taxa()
# data = readDiscreteCharacterData("data/data.nex")
data = readCharacterDataDelimited("data/data.tsv", stateLabels=2, headers=FALSE, delimiter="\t")

age  <- tree.rootAge()
frac <- 0.25

# variables
k    = 2
mvi  = 0
mni  = 0

# set parameters
lambda_mean ~ dnExponential(1)
lambda_mean.setValue(1.0)
moves[++mvi] = mvScale(lambda_mean, weight=0)

lambda := rep(lambda_mean, k)

mu <- rep(0.0, k)

# transition rates
epoch_times <- [2.0]
Q[1] := fnJC(k)
Q[2] := fnJC(k)
rates[1] <- 0.2
rates[2] <- 0.1
Q_epoch[1] := fnFreeSymmetricRateMatrix(rep(abs(rates[1] / (k - 1)), choose(k, 2)), rescaled=FALSE)
Q_epoch[2] := fnFreeSymmetricRateMatrix(rep(abs(rates[2] / (k - 1)), choose(k, 2)), rescaled=FALSE)

# root frequency
root_freq <- simplex(rep(1, k))

# condition
condition <- "time"

# distribution
psi ~ dnGLHBDSP(
		rootAge      = age,
		lambda       = lambda,
		mu           = mu,
		rho          = frac,
		eta          = Q_epoch,
		etaTimes     = epoch_times,
		pi           = root_freq,
		condition    = condition,
		taxa         = taxa,
		nStates      = k,
		nProc        = 1
)
psi.clamp(tree)
psi.clampCharData(data)

# model
my_model = model(psi)

# monitors
monitors[1] = mnScreen(printgen=100)
monitors[2] = mnModel(printgen=10, filename="output/tp_epoch.log")
monitors[3] = mnJointConditionalAncestralState(printgen=1, filename="output/tp_epoch_states.log", glhbdsp=psi, tree=psi, withTips=false, withStartStates=false, type="NaturalNumbers")
monitors[4] = mnStochasticCharacterMap(printgen=1, filename="output/tp_epoch_maps.log", glhbdsp=psi)

# analysis
my_mcmc = mcmc(my_model, monitors, moves)
my_mcmc.burnin(1000, 100)
my_mcmc.run(1000)

q()
