loadPlugin("TensorPhylo", "/home/mike/repos/tensorphyloprototype/build_local")

# read the tree
tree = readTrees("data/tree.nex")[1]
taxa = tree.taxa()
# data = readDiscreteCharacterData("data/data.nex")
data = readCharacterDataDelimited("data/data.tsv", stateLabels=8, headers=FALSE, delimiter="\t")

age  <- tree.rootAge()
frac <- 0.25

# variables
k    = 8
mvi  = 0
mni  = 0

# set parameters
lambda_mean ~ dnExponential(1)
lambda_mean.setValue(1.0)
moves[++mvi] = mvScale(lambda_mean, weight=0)

lambda := rep(lambda_mean, k)

mu <- rep(0.0, k)

# transition rates
num_events = 50
eta <- num_events / tree.treeLength()
Q := fnFreeSymmetricRateMatrix(rep(abs(eta / (k - 1)), choose(k,2)), rescaled=FALSE)

# root frequency
root_freq <- simplex(rep(1, k))

# condition
condition <- "time"

# distribution
psi ~ dnCDBDP(rootAge = age,
			  lambda = lambda,
			  mu = mu,
			  Q = Q,
			  delta = 1.0,
			  pi = root_freq,
			  rho = frac,
			  condition = condition,
			  nTimeSlices=500)
psi.clamp(tree)
psi.clampCharData(data)

# model
my_model = model(psi)

# monitors
monitors[1] = mnScreen(printgen=100)
monitors[2] = mnModel(printgen=10, filename="output/rb.log")
monitors[3] = mnJointConditionalAncestralState(printgen=1, filename="output/rb_states.log", cdbdp=psi, tree=psi, withTips=false, withStartStates=false, type="NaturalNumbers")

# analysis
my_mcmc = mcmc(my_model, monitors, moves)
my_mcmc.burnin(1000, 100)
my_mcmc.run(100000)


q()
