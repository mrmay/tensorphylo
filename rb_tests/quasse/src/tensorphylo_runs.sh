#!/bin/bash

# prepare the parameters
rb src/prepare_rb_params.Rev;

# loop over the powers
for i in {2..11};
	do echo $i;	
	TensorPhylo --param data/params_p_${i}.dat --nexus data/small_tree.nex --tsv data/probs_p_${i}.tsv --legacy 0 --verbosity 1 > output/tp_p_${i}.txt;
done;
