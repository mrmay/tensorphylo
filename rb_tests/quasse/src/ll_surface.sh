#!/bin/bash

# simulate a dataset
Rscript src/simulate_for_surface.R;

# run rb
rb src/comparison.Rev;

# make figures
Rscript src/ll_surface.R;
