library(diversitree)

# settings
p = 2:11

# read diversitree output
dt = as.numeric(read.table("rb_tests/quasse/output/diversitree.csv", header=TRUE, sep=","))
dt = dt[2:11-1]

# read tensorphylo output
tp = numeric(length(p))
for(i in 1:length(p)) {
  log = readLines(paste0("rb_tests/quasse/output/tp_p_",p[i],".txt"))
  if ( length(log) != 0 ) {
    tp[i] = as.numeric(strsplit(log," ")[[1]][3])
  } else {
    tp[i] = NA
  }
}

ylim = range(c(dt[dt > -Inf], tp[tp > -Inf]), na.rm=TRUE)

ll = -533.5546

pdf("rb_tests/quasse/output/convergence.pdf", height=4)
par(mar=c(5,4,0.1,0.1))
plot(p, dt, type="b", col="blue", pch=3, xlab="log(k,2)", ylab="log-likelihood")
lines(p, tp, type="b", col="orange", pch=4)
abline(h=ll, lty=2)
legend("bottomright", legend=c("diversitree","tensorphylo"), col=c("blue","orange"), lty=1)
dev.off()

pdf("rb_tests/quasse/output/convergence_delta.pdf", height=4)
par(mar=c(5,4,0.1,0.1))
plot(p, dt / ll, type="b", col="blue", pch=3, xlab="log(k,2)", ylab="log-likelihood")
lines(p, tp / ll, type="b", col="orange", pch=4)
abline(h=1, lty=2)
legend("topleft", legend=c("diversitree","tensorphylo"), col=c("blue","orange"), lty=1)
dev.off()










# read tree and data
tree = read.tree("rb_tests/quasse/data/small_tree_rescaled.nex")
data = read.table("rb_tests/quasse/data/small_data.tsv", sep="\t")
traits = data[,2]
names(traits) = data[,1]


pics = pic(traits, tree, scaled=FALSE, var.contrasts = TRUE)

sum(dnorm(pics[,1], 0, sqrt(pics[,2]), log=TRUE))


