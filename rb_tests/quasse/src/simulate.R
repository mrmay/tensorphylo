library(diversitree)
source("validation/utils.R")

# diversification rates
sigma  = 0.5
sd     = 0.01
lambda = function(x) constant.x(x, 1.0)
mu     = function(x) constant.x(x, 0.1)
bm     = make.brownian.with.drift(0.0, sigma * sigma)
rho    = 1

# simulate a tree
sim = tree.quasse( c(lambda, mu, bm), max.tax=25, x0=0, verbose=TRUE )

# write the tree
write.nexus(sim, file="rb_tests/quasse/data/small_tree.nex")

# rescale the tree
rate = sigma^2
# rate = 0.1

bm_tree = sim
bm_tree$edge.length = bm_tree$edge.length * rate

# add the variance at the tips
var = sd * sd
# var = sd
bm_tree$edge.length[bm_tree$edge[,2] <= length(bm_tree$tip.label)] = bm_tree$edge.length[bm_tree$edge[,2] <= length(bm_tree$tip.label)] + var

write.tree(bm_tree, "rb_tests/quasse/data/small_tree_rescaled.nex")

# write the data
mat = cbind(bm_tree$tip.label, sim$tip.state)

write.table(mat, file="rb_tests/quasse/data/small_data.tsv", sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)

# write data in nexus
writeContinuousData(t(t(sim$tip.state)), "rb_tests/quasse/data/small_data.nex")

# write the tree topology
topo_tree = sim
topo_tree$edge.length[] = 1
write.tree(topo_tree, "rb_tests/quasse/data/small_tree_topology.nex")

