/*
 * main.cpp
 *
 *  Created on: Aug 22, 2019
 *      Author: Xavier Meyer
 */
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/assign/list_of.hpp>
#include <cstdlib>
#include <iostream>
#include <string>

#include "Data/Reader/IncPhyloReader.h"
#include "Data/Structure/IncTreeStructure.h"
#include "Tensor/IncTensor.h"
#include "Likelihood/Scheduler/IncScheduler.h"
#include "Parameters/Container.h"
#include "SynchronousEvents/IncSynchronousEvents.h"
#include "Likelihood/Approximator/IncLikelihoodApproximator.h"
#include "Utils/MemoryPool/EigenCPU.h"
#include "Likelihood/ConditionTypes/ConditionType.h"
#include "Likelihood/Kernels/CPU/EigenUtilsMatrix.h"
#include "Utils/Output/OutputManager.h"
#include "Test/Utils.h"
#include "Utils/Profiling/CustomProfiling.h"


void runBenchmark(std::vector< std::string > &params) {
	Phylogeny::NexusReader::NexusParserSharedPtr nexusParser(Test::InitData::nexusFromFile(params[1], params[2]));

	if(params[6] == "1") {
		Parameters::ContainerSharedPtr parameters(new Parameters::Container);

		if(!params[0].empty()) {
			parameters->readFromFile(params[0]);
		} else {
			parameters = Test::InitData::parametersBD1(nexusParser->getNState(), 1.2, 0.8, 1.0, 0.0);
			parameters->intLikApproximator = 0;
			parameters->nThreads = std::atoi(params[5].c_str());
			assert(parameters->nThreads >= 1);
		}

		Likelihood::Integrator::integrationScheme_t intScheme = static_cast<Likelihood::Integrator::integrationScheme_t >(parameters->intScheme);

		size_t nLikApproximation = std::atoi(params[3].c_str());
		Test::SequentialCPU::runBenchmarkLegacy(nLikApproximation, intScheme, nexusParser, parameters, params[4]);
	} else {
		size_t nLikApproximation = std::atoi(params[3].c_str());
		Phylogeny::NexusReader::NexusParserSharedPtr nexusParser(Test::InitData::nexusFromFile(params[1], params[2]));
		Test::SequentialCPU::runBenchmarkNew(nLikApproximation, nexusParser, params[0], params[4]);

	}


}

std::string getHelpMessage() {
	std::stringstream ss;

	ss << ">> ./TensorPhylo --param paramFile --nexus nexusFile --tsv tsvFile --benchmark N --log myLogFile" << std::endl;
	ss << "  OR  " << std::endl;
	ss << ">> ./TensorPhylo --nThread 6 --nexus nexusFile --tsv tsvFile --benchmark N --log myLogFile" << std::endl;
	ss << "Parameters:" << std::endl;
	ss << "\t--param\t\t Parameters file (mandatory or replaced by nThread)"<< std::endl;
	ss << "\t--nThread\t Maximum number of threads -- will use a default model (mandatory or replaced by param)."<< std::endl;
	ss << "\t--nexus\t\t Nexus file (one of nexusFile or singleBranchFile must be specified)"<< std::endl;
	ss << "\t--tsv\t\t TSV file"<< std::endl;
	ss << "\t--benchmark\t Compute N times the likelihood (--benchmark N)."<< std::endl;
	ss << "\t--log\t Output log file."<< std::endl;
	ss << "\t--legacy\t Whether to use legacy parameter files, default is 1 - (0=FALSE to 3=TRUE)."<< std::endl;
	ss << "\t-h\t\t Print this message." << std::endl;

	return ss.str();
}

void parseParameters(int argc, char** argv, std::vector< std::string > &params) {

	std::vector<std::string> keys = boost::assign::list_of("--param")("--nexus")("--tsv")("--benchmark")("--log")("--nThread")("--legacy");
	std::vector< std::string > values(keys.size());
	std::vector<bool> found(keys.size(), false);

	if((argc-1) % 2 != 0) {
		throw std::string("Incorrect number of parameters (call -h for help).\n");
	}

	for(size_t i=1; i<(size_t)argc; i+=2) {

		std::string key(argv[i]);
		std::string val(argv[i+1]);

		std::vector<std::string>::iterator itFind = std::find(keys.begin(), keys.end(), key);
		if(itFind == keys.end()) {
			std::stringstream ss;
			ss << "Command : '" << key << "' is not defined (call -h for help). " << std::endl;
			throw ss.str();
		}

		size_t index = std::distance(keys.begin(), itFind);
		values[index] = val;
		found[index] = true;

	}

	size_t nFound = 0;
	for(size_t iF=0; iF<found.size(); ++iF) {
		if(found[iF]) nFound++;
	}

	assert(nFound==6 && (found[0] ^ found[5]) && "Call -h for help.");

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	params = values;
}

void benchmarkQuasseConvolution() {
	std::vector<size_t> vecK;

	size_t startK = 5;
	size_t endK = 12;

	for(size_t K = (size_t)(1 << startK); K <= (size_t)(1 << endK); K = K << 1) {
		vecK.push_back(K);
	}
	vecK.push_back(100);
	vecK.push_back(123);
	vecK.push_back(1000);
	vecK.push_back(1234);

	Test::SequentialCPU::benchmarkConvolution(1000, vecK);
	Test::SequentialCPU::benchmarkConvolution2(1000, vecK);
}



int main(int argc, char** argv) {


	/* Parameters - input files */
	/* Parameters - log file */
	/* Parameter - number of iteration */

	Utils::Parallel::Manager::getInstance()->setNThread(1);
	Utils::Parallel::Manager::getInstance()->setMaxNThread(1);

	//benchmarkQuasseConvolution();
	//return 1;

	if(argc == 2 && std::string(argv[1]) == "-h") {
		std::cout << getHelpMessage();
		return 0;
	} else if(argc != 13){
		std::cout << getHelpMessage();
		return 0;
	}

	std::vector<std::string> params;
	try {
		parseParameters(argc, argv, params);
	} catch(std::string const& e) {
		std::cout << getHelpMessage();
		std::cout << "----------------------------------------------------------------------" << std::endl;
		std::cerr << e << std::endl;
		return 1;
	}

	runBenchmark(params);

	return 0;
}
