/*
 * main.cpp
 *
 *  Created on: Aug 22, 2019
 *      Author: Xavier Meyer
 */
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/assign/list_of.hpp>
#include <iostream>
#include <string>

#include "Data/Reader/IncPhyloReader.h"
#include "Data/Structure/IncTreeStructure.h"
#include "Tensor/IncTensor.h"
#include "Likelihood/Scheduler/IncScheduler.h"
#include "Parameters/Container.h"
#include "SynchronousEvents/IncSynchronousEvents.h"
#include "Likelihood/Approximator/IncLikelihoodApproximator.h"
#include "Utils/MemoryPool/EigenCPU.h"
#include "Likelihood/ConditionTypes/ConditionType.h"
#include "Utils/Output/OutputManager.h"
#include "Test/Utils.h"

void singleBranchHack(std::vector< std::string > &params) {

	Phylogeny::NexusReader::NexusParserSharedPtr nexusParser(Test::InitData::nexusFromSingleBranchFile(params[1]));

	Parameters::ContainerSharedPtr parameters(new Parameters::Container);
	parameters->readFromFile(params[0]);

	Likelihood::Integrator::integrationScheme_t intScheme = static_cast<Likelihood::Integrator::integrationScheme_t >(parameters->intScheme);

	size_t nLikApproximation = std::atoi(params[3].c_str());

	Test::SequentialCPU::computeLogLik(nLikApproximation, intScheme, nexusParser, parameters);

}


void computeLikForData(std::vector< std::string > &params) {
	Phylogeny::NexusReader::NexusParserSharedPtr nexusParser(Test::InitData::nexusFromFile(params[1]));

	Parameters::ContainerSharedPtr parameters(new Parameters::Container);
	parameters->readFromFile(params[0]);

	Likelihood::Integrator::integrationScheme_t intScheme = static_cast<Likelihood::Integrator::integrationScheme_t >(parameters->intScheme);

	size_t nLikApproximation = std::atoi(params[3].c_str());

	Test::SequentialCPU::computeLogLik(nLikApproximation, intScheme, nexusParser, parameters);
}

void computeLikForDataTSV(std::vector< std::string > &params) {
	Phylogeny::NexusReader::NexusParserSharedPtr nexusParser(Test::InitData::nexusFromFile(params[1], params[2]));

	Parameters::ContainerSharedPtr parameters(new Parameters::Container);
	parameters->readFromFile(params[0]);

	Likelihood::Integrator::integrationScheme_t intScheme = static_cast<Likelihood::Integrator::integrationScheme_t >(parameters->intScheme);

	size_t nLikApproximation = std::atoi(params[3].c_str());

	Test::SequentialCPU::computeLogLik(nLikApproximation, intScheme, nexusParser, parameters);
}

void computeLikForDataTSVNew(std::vector< std::string > &params) {
	Phylogeny::NexusReader::NexusParserSharedPtr nexusParser(Test::InitData::nexusFromFile(params[1], params[2]));
	Test::SequentialCPU::computeLikForInterface(nexusParser, params[0]);
}

std::string getHelpMessage() {
	std::stringstream ss;

	ss << ">> ./TensorPhylo --param paramFile --nexus nexusFile --verbosity 1" << std::endl;
	ss << "OR" << std::endl;
	ss << ">> ./TensorPhylo --param paramFile --nexus nexusFile --tsv tsvFile --verbosity 1" << std::endl;
	ss << "OR" << std::endl;
	ss << ">> ./TensorPhylo --param paramFile --singleBranch nexusFile --verbosity 1" << std::endl;
	ss << "Parameters:" << std::endl;
	ss << "\t--param\t\t Parameters file (Mandatory)"<< std::endl;
	ss << "\t--nexus\t\t Nexus file (one of nexusFile or singleBranchFile must be specified)"<< std::endl;
	ss << "\t--tsv\t\t TSV file"<< std::endl;
	ss << "\t--benchmark\t Compute N times the likelihood (--benchmark N)."<< std::endl;
	ss << "\t--verbosity\t Verbosity level, default is high - (0=SILENT to 3=HIGH, 4=HIGH+DUMP_PROBES)."<< std::endl;
	ss << "\t--legacy\t Whether to use legacy parameter files, default is 1 - (0=FALSE to 3=TRUE)."<< std::endl;
	ss << "\t--dmpFile\t Output dump file path (monitoring probes + dump)."<< std::endl;
	ss << "\t-h\t\t Print this message." << std::endl;

	return ss.str();
}

enum commandType_t {NONE, FROM_NEXUS, FROM_NEXUS_AND_TSV, FROM_NEXUS_AND_TSV_NEW, FROM_SINGLE_BRANCH};
commandType_t  parseParameters(int argc, char** argv, std::vector< std::string > &params) {

	std::vector<std::string> keys = boost::assign::list_of("--param")("--nexus")("--singleBranch")("--verbosity")("-v")("--tsv")("--benchmark")("--legacy")("--dmpFile");
	std::vector< std::string > values(keys.size());
	std::vector<bool> found(keys.size(), false);

	if((argc-1) % 2 != 0) {
		throw std::string("Incorrect number of parameters (call -h for help).\n");
	}

	for(size_t i=1; i<(size_t)argc; i+=2) {

		std::string key(argv[i]);
		std::string val(argv[i+1]);

		std::vector<std::string>::iterator itFind = std::find(keys.begin(), keys.end(), key);
		if(itFind == keys.end()) {
			std::stringstream ss;
			ss << "Command : '" << key << "' is not defined (call -h for help). " << std::endl;
			throw ss.str();
		}

		size_t index = std::distance(keys.begin(), itFind);
		values[index] = val;
		found[index] = true;

	}

	// COMMAND (input)
	std::string dataFile;
	std::string tsvFile;
	commandType_t cmd = NONE;
	if(found[1] == found[2]) {
		throw std::string("You must specify one of --nexus and --singleBranch (call -h for help).\n");
	} else if(found[1] && found[5]) {
		cmd = FROM_NEXUS_AND_TSV;
		dataFile = values[1];
		tsvFile = values[5];
	} else if(found[1]) {
		cmd = FROM_NEXUS;
		dataFile = values[1];
	} else if(found[2]) {
		cmd = FROM_SINGLE_BRANCH;
		dataFile = values[2];
	}

	// VERBOSITY
	size_t iVerb = 3; // Default to HIGH_VERB
	if(found[3]) {
		iVerb = atoi(values[3].c_str());
	} else if(found[4]) {
		iVerb = atoi(values[4].c_str());
	}
	Utils::Output::verboseLevel_t vLevel = static_cast<Utils::Output::verboseLevel_t>(iVerb);
	Utils::Output::outputManager().setVerbosityThreshold(vLevel);

	// PARAMETERS
	if(!found[0]) throw std::string("You must specify a parameter file (call -h for help).\n");
	std::string paramFile(values[0]);

	std::string benchmarkStr("1");
	if(found[6]) {
		benchmarkStr = values[6];
	}

	// Optional dump file
	if(found[8]) {
		Utils::Output::outputManager().setDumpFileName(values[8]);
	}

	// CHECK PARAM FILE VERSION
	if(cmd == FROM_NEXUS_AND_TSV) {
		if(found[7]) { // if we provided the argument
			if ( values[7] == "0" ) { // and we are told not to use legacy
				cmd = FROM_NEXUS_AND_TSV_NEW;
			}
		}
	}

	params.clear();
	params.push_back(paramFile); // parameter file
	params.push_back(dataFile); // data file
	params.push_back(tsvFile); // data file
	params.push_back(benchmarkStr);

	return cmd;
}

int main(int argc, char** argv) {

	 /*Eigen::initParallel();
	 size_t n = Eigen::nbThreads( );
	 std::cout << n << std::endl;*/

	Utils::Parallel::Manager::getInstance()->setNThread(1);
	Utils::Parallel::Manager::getInstance()->setMaxNThread(1);

	if(argc == 1){
		std::cout << getHelpMessage();
		return 0;
	} else if(argc == 2 && std::string(argv[1]) == "-h") {
		std::cout << getHelpMessage();
		return 0;
	}

	commandType_t cmd = NONE;
	std::vector<std::string> params;
	try {
		cmd =  parseParameters(argc, argv, params);
		} catch(std::string const& e) {
			std::cout << getHelpMessage();
			std::cout << "----------------------------------------------------------------------" << std::endl;
			std::cerr << e << std::endl;
			return 1;
		}

	if(cmd == FROM_NEXUS) {
		computeLikForData(params);
	} else if(cmd == FROM_SINGLE_BRANCH) {
		singleBranchHack(params);
	} else if(cmd == FROM_NEXUS_AND_TSV) {
		computeLikForDataTSV(params);
	} else if (cmd == FROM_NEXUS_AND_TSV_NEW) {
//		std::cout << "HELLO" << std::endl;
		computeLikForDataTSVNew(params);
	}

	return 0;
}
