/*
 * SanityTests.cpp
 *
 *  Created on: April 17, 2020
 *      Author: meyerx
 */

#include "../Likelihood/Approximator/StochasticMapping/TreeStatistics.h"
#include "Test/Catch2/catch.hpp"
#include "Test/Utils.h"


TEST_CASE("Testing synchronous events: mass speciation.",
          "[Sequential][CPU][Scheduler][SynchronousEvents][MassSpeciation][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	std::string directory("./unit_tests/synchronous_events/test_1/");

	// Read the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( directory + "tree_mass_speciation.nex" ) );

	// Read the parameters
	double lambda      = 1.0;
	double mu          = 0.5;
	double phi 	       = 0.1;
	double eta         = 0.2;
	double rho         = 1.0;
	double putativeLik = -3.1655084055;

	// set the parameters
	Parameters::ContainerSharedPtr parameters( Test::InitData::parametersFBD1(nexusReader->getNState(), lambda, mu, phi, eta, rho )  );

	// Create a mass speciation event
	parameters->massSpeciationTimes.push_back(0.2);
	Eigen::VectorXd probs = 0.5*Eigen::VectorXd::Ones(nexusReader->getNState());
	parameters->massSpeciationProb.push_back(probs);


	SECTION("Check for possible mass speciation at time 0.2.") {

		PS::TreeSharedPtr ptrTree( new PS::Tree(nexusReader) );

		SynchronousEvents::ContainerSharedPtr syncEvents( new SynchronousEvents::Container(parameters) );

		// Init scheduler
		using Likelihood::Scheduler::BaseScheduler;
		boost::shared_ptr<BaseScheduler> scheduler( new BaseScheduler(ptrTree) );
		// Set synchronous events in the scheduler
		scheduler->setSynchronousEvents(syncEvents);

		CHECK(scheduler->getEvents()[1]->checkEvent(Likelihood::Scheduler::SYCHRONOUS_SPECIATION_EVENT));

		// Check the event validity
		bool isPossible = true;
		for(size_t iE=0; iE<scheduler->getEvents().size(); ++iE) {
			//std::cout << scheduler->getEvents()[iE]->toString() << std::endl;
			isPossible = isPossible && scheduler->getEvents()[iE]->isEventPossible();
		}
		CHECK(isPossible == true);
		CHECK(scheduler->getEvents()[1]->getNodes().size() == 2);
		//CHECK(scheduler->getActiveEdges(0.2).empty());
	}

	for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

		std::stringstream ss2;
		ss2 << "Check putative likelihood consistency with approximator: " << Likelihood::Approximator::APPROXIMATOR_NAMES[iA];
		parameters->intLikApproximator = iA;

		SECTION( ss2.str() ) {
			double lik = Test::SequentialCPU::computeLikForTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, nexusReader, parameters);
			REQUIRE(  lik == Approx(putativeLik).margin(1.e-5)  );
		}
	}
}


TEST_CASE("Testing synchronous events: mass sampling.",
          "[Sequential][CPU][Scheduler][SynchronousEvents][MassSampling][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	std::string directory("./unit_tests/synchronous_events/test_1/");

	// Read the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( directory + "tree_mass_sampling.nex" ) );

	// Read the parameters
	double lambda  = 1.0;
	double mu      = 0.5;
	double phi 	   = 0.1;
	double eta     = 0.2;
	double rho     = 1.0;
	double putativeLik = -4.69848;

	// set the parameters
	Parameters::ContainerSharedPtr parameters( Test::InitData::parametersFBD1(nexusReader->getNState(), lambda, mu, phi, eta, rho )  );

	// Create a mass speciation event
	parameters->massSamplingTimes.push_back(0.1);
	Eigen::VectorXd probs = 0.5*Eigen::VectorXd::Ones(nexusReader->getNState());
	parameters->massSamplingProb.push_back(probs);

	SECTION("Check for possible mass sampling at time 0.1.") {

		PS::TreeSharedPtr ptrTree( new PS::Tree(nexusReader) );

		SynchronousEvents::ContainerSharedPtr syncEvents( new SynchronousEvents::Container(parameters) );

		// Init scheduler
		using Likelihood::Scheduler::BaseScheduler;
		boost::shared_ptr<BaseScheduler> scheduler( new BaseScheduler(ptrTree) );
		// Set synchronous events in the scheduler
		scheduler->setSynchronousEvents(syncEvents);

		CHECK(scheduler->getEvents()[1]->checkEvent(Likelihood::Scheduler::SYNCHRONOUS_SAMPLING_EVENT));

		// Check the event validity
		bool isPossible = true;
		for(size_t iE=0; iE<scheduler->getEvents().size(); ++iE) {
			//std::cout << scheduler->getEvents()[iE]->toString() << std::endl;
			isPossible = isPossible && scheduler->getEvents()[iE]->isEventPossible();
		}
		CHECK(isPossible == true);
		CHECK(scheduler->getEvents()[1]->getNodes().size() == 2);
		//CHECK(scheduler->getActiveEdges(0.2).empty());

	}

	for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

		std::stringstream ss2;
		ss2 << "Check putative likelihood consistency with approximator: " << Likelihood::Approximator::APPROXIMATOR_NAMES[iA];
		parameters->intLikApproximator = iA;

		SECTION( ss2.str() ) {
			double lik = Test::SequentialCPU::computeLikForTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, nexusReader, parameters);
			REQUIRE(  lik == Approx(putativeLik).margin(1.e-5)  );
		}
	}

}

TEST_CASE("Testing synchronous events: mass destructive sampling.",
          "[Sequential][CPU][Scheduler][SynchronousEvents][MassDestructiveSampling][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	std::string directory("./unit_tests/synchronous_events/test_1/");

	// Read the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( directory + "tree_mass_destructive_sampling.nex" ) );

	// Read the parameters
	double lambda  = 1.0;
	double mu      = 0.5;
	double phi 	   = 0.1;
	double eta     = 0.2;
	double rho     = 1.0;
	double putativeLik = -4.38798;

	// set the parameters
	Parameters::ContainerSharedPtr parameters( Test::InitData::parametersFBD1(nexusReader->getNState(), lambda, mu, phi, eta, rho )  );

	// Create a mass speciation event
	parameters->massDestrSamplingTimes.push_back(0.1);
	Eigen::VectorXd probs = 0.5*Eigen::VectorXd::Ones(nexusReader->getNState());
	parameters->massDestrSamplingProb.push_back(probs);

	SECTION("Check for possible mass sampling at time 0.1.") {

		SynchronousEvents::ContainerSharedPtr syncEvents( new SynchronousEvents::Container(parameters) );

		PS::TreeSharedPtr ptrTree( new PS::Tree(nexusReader) );

		// Init scheduler
		using Likelihood::Scheduler::BaseScheduler;
		boost::shared_ptr<BaseScheduler> scheduler( new BaseScheduler(ptrTree) );
		// Set synchronous events in the scheduler
		scheduler->setSynchronousEvents(syncEvents);

		CHECK(scheduler->getEvents()[1]->checkEvent(Likelihood::Scheduler::SYNCHRONOUS_DESTRUCTIVE_SAMPLING_EVENT));

		// Check the event validity
		bool isPossible = true;
		for(size_t iE=0; iE<scheduler->getEvents().size(); ++iE) {
			//std::cout << scheduler->getEvents()[iE]->toString() << std::endl;
			isPossible = isPossible && scheduler->getEvents()[iE]->isEventPossible();
		}
		CHECK(isPossible == true);
		CHECK(scheduler->getEvents()[1]->getNodes().size() == 2);
		//CHECK(scheduler->getActiveEdges(0.2).empty());
	}

	for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

		std::stringstream ss2;
		ss2 << "Check putative likelihood consistency with approximator: " << Likelihood::Approximator::APPROXIMATOR_NAMES[iA];
		parameters->intLikApproximator = iA;

		SECTION( ss2.str() ) {
			double lik = Test::SequentialCPU::computeLikForTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, nexusReader, parameters);
			REQUIRE(  lik == Approx(putativeLik).margin(1.e-5)  );
		}
	}
}



TEST_CASE("Yule process on a single branch with sequential CPU implementation -- Stochastic Mapping", "[Sequential][CPU][StateIndependent][Yule][SingleBranch][NoBDD][StochasticMapping][SanityCheck]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromSingleBranchFile( "./unit_tests/legacy/test_2/sb_original.dat" ) );

	std::vector<double> lambda = boost::assign::list_of(0.5)(1.0)(1.5);
	double rho = 0.25;
	double eta = 0.2;

	const size_t nReplicas = 5000;
	const double tolerance = 0.1;

	// Now let's try different rates
	for(size_t iL=0; iL<lambda.size(); ++iL) {

		std::stringstream ss;
		ss << "Computing stochastic maps maps with lambda = " << lambda[iL];
		SECTION( ss.str() ) {

			PS::TreeSharedPtr ptrTree( new PS::Tree(nexusReader) );

			// Set parameters
			Parameters::ContainerSharedPtr parameters( Test::InitData::parametersYule1(nexusReader->getNState(), lambda[iL], eta, rho )  );
			TensorPhylo::Interface::vecHistories_t vecHistoriesRejSampling =
					Test::SequentialCPU::computeStochasticMappingLegacy(nReplicas, Likelihood::Approximator::StochasticMapping::REJECTION_SAMPLING_ALGO, nexusReader, parameters, ptrTree);
			Likelihood::Approximator::StochasticMapping::TreeStatistics tStatsRejSampling(nexusReader->getNState(), vecHistoriesRejSampling);

			TensorPhylo::Interface::vecHistories_t vecHistoriesEuler =
					Test::SequentialCPU::computeStochasticMappingLegacy(nReplicas, Likelihood::Approximator::StochasticMapping::DENSE_EULER_ALGO, nexusReader, parameters, ptrTree);
			Likelihood::Approximator::StochasticMapping::TreeStatistics tStatsEuler(nexusReader->getNState(), vecHistoriesEuler);

			TensorPhylo::Interface::vecHistories_t vecHistoriesDOPRI =
					Test::SequentialCPU::computeStochasticMappingLegacy(nReplicas, Likelihood::Approximator::StochasticMapping::DENSE_DOPRI_ALGO, nexusReader, parameters, ptrTree);
			Likelihood::Approximator::StochasticMapping::TreeStatistics tStatsDOPRI(nexusReader->getNState(), vecHistoriesDOPRI);

			CHECK(Likelihood::Approximator::StochasticMapping::areTreeStatisticsSimilar(tStatsRejSampling, tStatsEuler, tolerance));
			CHECK(Likelihood::Approximator::StochasticMapping::areTreeStatisticsSimilar(tStatsRejSampling, tStatsDOPRI, tolerance));
			CHECK(Likelihood::Approximator::StochasticMapping::areTreeStatisticsSimilar(tStatsEuler, tStatsDOPRI, tolerance));

			double avgNbOfTransitionsRS = tStatsRejSampling.getSegmentStatistics(ptrTree->getOldestNode()->getEdgesToChildren().front()->getId()).getAverageNTransitions();
			CHECK(eta  == Approx(avgNbOfTransitionsRS).margin(eta*tolerance));

			double avgNbOfTransitionsEuler = tStatsEuler.getSegmentStatistics(ptrTree->getOldestNode()->getEdgesToChildren().front()->getId()).getAverageNTransitions();
			CHECK(eta  == Approx(avgNbOfTransitionsEuler).margin(eta*tolerance));

			double avgNbOfTransitionsDOPRI = tStatsDOPRI.getSegmentStatistics(ptrTree->getOldestNode()->getEdgesToChildren().front()->getId()).getAverageNTransitions();
			CHECK(eta  == Approx(avgNbOfTransitionsDOPRI).margin(eta*tolerance));

			// JC type matrix
			std::pair<double, double> statsRJ = tStatsRejSampling.getObservedTransitionFrequenciesStats();
			CHECK(statsRJ.second < tolerance);

			std::pair<double, double> statsEuler = tStatsEuler.getObservedTransitionFrequenciesStats();
			CHECK(statsEuler.second < tolerance);

			std::pair<double, double> statsDOPRI = tStatsDOPRI.getObservedTransitionFrequenciesStats();
			CHECK(statsDOPRI.second < tolerance);

			CHECK(Test::SequentialCPU::checkHistoriesEndStatesAgainstDataProbability(ptrTree, nexusReader, vecHistoriesRejSampling, tolerance));
			CHECK(Test::SequentialCPU::checkHistoriesEndStatesAgainstDataProbability(ptrTree, nexusReader, vecHistoriesEuler, tolerance));
			CHECK(Test::SequentialCPU::checkHistoriesEndStatesAgainstDataProbability(ptrTree, nexusReader, vecHistoriesDOPRI, tolerance));
		}
	}
}


TEST_CASE("Birth-death process on a single branch with sequential CPU implementation -- Stochastic Mapping", "[Sequential][CPU][StateIndependent][BDP][SingleBranch][NoBDD][StochasticMapping][SanityCheck]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromSingleBranchFile( "./unit_tests/legacy/test_4/sb_original.dat" ) );

	std::vector<double> lambda = boost::assign::list_of(0.5)(1.0)(1.5);
	std::vector<double> mu     = boost::assign::list_of(0.0)(0.45)(0.95);
	double rho = 0.25;
	double eta = 0.2;

	const size_t nReplicas = 5000;
	const double tolerance = 0.1;

	// iterate over the speciation rates
	for(size_t iL=0; iL < lambda.size(); ++iL) {

		// iterate over extinction rates
		for(size_t iM = 0; iM < mu.size(); ++iM) {

			std::stringstream ss;
			ss << "Computing stochastic maps with lambda = " << lambda[iL] << " and mu = " << mu[iM];

			SECTION( ss.str() ) {

				PS::TreeSharedPtr ptrTree( new PS::Tree(nexusReader) );

				// Set parameters
				Parameters::ContainerSharedPtr parameters( Test::InitData::parametersBD1(nexusReader->getNState(), lambda[iL], mu[iM], eta, rho )  );
				TensorPhylo::Interface::vecHistories_t vecHistoriesRejSampling =
						Test::SequentialCPU::computeStochasticMappingLegacy(nReplicas, Likelihood::Approximator::StochasticMapping::REJECTION_SAMPLING_ALGO, nexusReader, parameters, ptrTree);
				Likelihood::Approximator::StochasticMapping::TreeStatistics tStatsRejSampling(nexusReader->getNState(), vecHistoriesRejSampling);

				TensorPhylo::Interface::vecHistories_t vecHistoriesEuler =
						Test::SequentialCPU::computeStochasticMappingLegacy(nReplicas, Likelihood::Approximator::StochasticMapping::DENSE_EULER_ALGO, nexusReader, parameters, ptrTree);
				Likelihood::Approximator::StochasticMapping::TreeStatistics tStatsEuler(nexusReader->getNState(), vecHistoriesEuler);

				TensorPhylo::Interface::vecHistories_t vecHistoriesDOPRI =
						Test::SequentialCPU::computeStochasticMappingLegacy(nReplicas, Likelihood::Approximator::StochasticMapping::DENSE_DOPRI_ALGO, nexusReader, parameters, ptrTree);
				Likelihood::Approximator::StochasticMapping::TreeStatistics tStatsDOPRI(nexusReader->getNState(), vecHistoriesDOPRI);

				CHECK(Likelihood::Approximator::StochasticMapping::areTreeStatisticsSimilar(tStatsRejSampling, tStatsEuler, tolerance));
				CHECK(Likelihood::Approximator::StochasticMapping::areTreeStatisticsSimilar(tStatsRejSampling, tStatsDOPRI, tolerance));
				CHECK(Likelihood::Approximator::StochasticMapping::areTreeStatisticsSimilar(tStatsEuler, tStatsDOPRI, tolerance));

				// JC type matrix
				std::pair<double, double> statsRJ = tStatsRejSampling.getObservedTransitionFrequenciesStats();
				CHECK(statsRJ.second < tolerance);

				std::pair<double, double> statsEuler = tStatsEuler.getObservedTransitionFrequenciesStats();
				CHECK(statsEuler.second < tolerance);

				std::pair<double, double> statsDOPRI = tStatsDOPRI.getObservedTransitionFrequenciesStats();
				CHECK(statsDOPRI.second < tolerance);

				CHECK(Test::SequentialCPU::checkHistoriesEndStatesAgainstDataProbability(ptrTree, nexusReader, vecHistoriesRejSampling, tolerance));
				CHECK(Test::SequentialCPU::checkHistoriesEndStatesAgainstDataProbability(ptrTree, nexusReader, vecHistoriesEuler, tolerance));
				CHECK(Test::SequentialCPU::checkHistoriesEndStatesAgainstDataProbability(ptrTree, nexusReader, vecHistoriesDOPRI, tolerance));

			}

		} // end loop over mu

	} // end loop over lambda

}

TEST_CASE("Birth-death-fossilization process on a tree (crown age) with sequential CPU implementation -- Stochastic Mapping", "[Sequential][CPU][StateIndependent][FBD][Tree][NoBDD][StochasticMapping][SanityCheck]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "./unit_tests/legacy/test_14/example_fbd_tree_crown.nex" ) );

	double lambda  = 1.0;
	double mu      = 0.5;
	std::vector<double> phi = boost::assign::list_of(0.1)(0.5)(1.0);
	double eta     = 0.2;
	double rho     = 1.0;

	const size_t nReplicas = 5000;
	const double tolerance = 0.1;

	// iterate over extinction rates
	for(size_t iP = 0; iP < phi.size(); ++iP) {

		std::stringstream ss;
		ss << "Computing stochastic maps with phi = " << phi[iP];

		SECTION( ss.str() ) {

			PS::TreeSharedPtr ptrTree( new PS::Tree(nexusReader) );

			// set the parameters
			Parameters::ContainerSharedPtr parameters( Test::InitData::parametersFBD1(nexusReader->getNState(), lambda, mu, phi[iP], eta, rho )  );
			TensorPhylo::Interface::vecHistories_t vecHistoriesRejSampling =
					Test::SequentialCPU::computeStochasticMappingLegacy(nReplicas, Likelihood::Approximator::StochasticMapping::REJECTION_SAMPLING_ALGO, nexusReader, parameters, ptrTree);
			Likelihood::Approximator::StochasticMapping::TreeStatistics tStatsRejSampling(nexusReader->getNState(), vecHistoriesRejSampling);

			TensorPhylo::Interface::vecHistories_t vecHistoriesEuler =
					Test::SequentialCPU::computeStochasticMappingLegacy(nReplicas, Likelihood::Approximator::StochasticMapping::DENSE_EULER_ALGO, nexusReader, parameters, ptrTree);
			Likelihood::Approximator::StochasticMapping::TreeStatistics tStatsEuler(nexusReader->getNState(), vecHistoriesEuler);

			TensorPhylo::Interface::vecHistories_t vecHistoriesDOPRI =
					Test::SequentialCPU::computeStochasticMappingLegacy(nReplicas, Likelihood::Approximator::StochasticMapping::DENSE_DOPRI_ALGO, nexusReader, parameters, ptrTree);
			Likelihood::Approximator::StochasticMapping::TreeStatistics tStatsDOPRI(nexusReader->getNState(), vecHistoriesDOPRI);

			CHECK(Likelihood::Approximator::StochasticMapping::areTreeStatisticsSimilar(tStatsRejSampling, tStatsEuler, tolerance));
			CHECK(Likelihood::Approximator::StochasticMapping::areTreeStatisticsSimilar(tStatsRejSampling, tStatsDOPRI, tolerance));
			CHECK(Likelihood::Approximator::StochasticMapping::areTreeStatisticsSimilar(tStatsEuler, tStatsDOPRI, tolerance));

			// JC type matrix
			std::pair<double, double> statsRJ = tStatsRejSampling.getObservedTransitionFrequenciesStats();
			CHECK(statsRJ.second < tolerance);

			std::pair<double, double> statsEuler = tStatsEuler.getObservedTransitionFrequenciesStats();
			CHECK(statsEuler.second < tolerance);

			std::pair<double, double> statsDOPRI = tStatsDOPRI.getObservedTransitionFrequenciesStats();
			CHECK(statsDOPRI.second < tolerance);

			CHECK(Test::SequentialCPU::checkHistoriesEndStatesAgainstDataProbability(ptrTree, nexusReader, vecHistoriesRejSampling, tolerance));
			CHECK(Test::SequentialCPU::checkHistoriesEndStatesAgainstDataProbability(ptrTree, nexusReader, vecHistoriesEuler, tolerance));
			CHECK(Test::SequentialCPU::checkHistoriesEndStatesAgainstDataProbability(ptrTree, nexusReader, vecHistoriesDOPRI, tolerance));
		}

	} // end loop over phi

}

TEST_CASE("State-dependent birth-death process on a tree (crown age) with sequential CPU implementation -- Stochastic Mapping", "[Sequential][CPU][SSE][Tree][NoBDD][StochasticMapping][SanityCheck]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "./unit_tests/legacy/test_21/example_bisse_tree.nex" ) );

	std::vector<double> lambda = boost::assign::list_of(1.0)(2.0);
	std::vector<double> mu     = boost::assign::list_of(0.5)(1.5);
	double eta                 = 0.2;
	double rho                 = 1.0;

	const size_t nReplicas = 5000;
	const double tolerance = 0.1;

	// set the parameters
	Parameters::ContainerSharedPtr parameters( Test::InitData::parametersBiSSE(nexusReader->getNState(), lambda, mu, eta, rho )  );

	PS::TreeSharedPtr ptrTree( new PS::Tree(nexusReader) );

	// set the parameters
	TensorPhylo::Interface::vecHistories_t vecHistoriesRejSampling =
			Test::SequentialCPU::computeStochasticMappingLegacy(nReplicas, Likelihood::Approximator::StochasticMapping::REJECTION_SAMPLING_ALGO, nexusReader, parameters, ptrTree);
	Likelihood::Approximator::StochasticMapping::TreeStatistics tStatsRejSampling(nexusReader->getNState(), vecHistoriesRejSampling);

	TensorPhylo::Interface::vecHistories_t vecHistoriesEuler =
			Test::SequentialCPU::computeStochasticMappingLegacy(nReplicas, Likelihood::Approximator::StochasticMapping::DENSE_EULER_ALGO, nexusReader, parameters, ptrTree);
	Likelihood::Approximator::StochasticMapping::TreeStatistics tStatsEuler(nexusReader->getNState(), vecHistoriesEuler);

	TensorPhylo::Interface::vecHistories_t vecHistoriesDOPRI =
			Test::SequentialCPU::computeStochasticMappingLegacy(nReplicas, Likelihood::Approximator::StochasticMapping::DENSE_DOPRI_ALGO, nexusReader, parameters, ptrTree);
	Likelihood::Approximator::StochasticMapping::TreeStatistics tStatsDOPRI(nexusReader->getNState(), vecHistoriesDOPRI);

	SECTION( "Checking stochastic maps validity." ) {

		CHECK(Likelihood::Approximator::StochasticMapping::areTreeStatisticsSimilar(tStatsRejSampling, tStatsEuler, tolerance));
		CHECK(Likelihood::Approximator::StochasticMapping::areTreeStatisticsSimilar(tStatsRejSampling, tStatsDOPRI, tolerance));
		CHECK(Likelihood::Approximator::StochasticMapping::areTreeStatisticsSimilar(tStatsEuler, tStatsDOPRI, tolerance));

		CHECK(Test::SequentialCPU::checkHistoriesEndStatesAgainstDataProbability(ptrTree, nexusReader, vecHistoriesRejSampling, tolerance));
		CHECK(Test::SequentialCPU::checkHistoriesEndStatesAgainstDataProbability(ptrTree, nexusReader, vecHistoriesEuler, tolerance));
		CHECK(Test::SequentialCPU::checkHistoriesEndStatesAgainstDataProbability(ptrTree, nexusReader, vecHistoriesDOPRI, tolerance));
	}
}

TEST_CASE("Birth-death mass-extinction process on a tree with sequential CPU implementation with high-precision integration -- Stochastic Mapping",
		"[Sequential][CPU][StateIndependent][BDP][MassExtinction][Tree][NoBDD][StochasticMapping][SanityCheck]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "./unit_tests/legacy/test_12/example_yule_tree.nex" ) );

	double lambda  = 1.0;
	double mu      = 0.5;
	double me_time = 0.5;
	double rho     = 1.0;
	double eta     = 0.2;
	std::vector<double> gamma = boost::assign::list_of(0.25)(0.50)(0.75);

	const size_t nReplicas = 5000;
	const double tolerance = 0.1;

	// iterate over gamma (mass-extinction probability)
	for(size_t iG=0; iG < gamma.size(); ++iG) {

		std::stringstream ss;
		ss << "Computing stochastic maps with gamma = " << gamma[iG];
		SECTION( ss.str() ) {

			// set the parameters
			Parameters::ContainerSharedPtr parameters( Test::InitData::parametersBDME(nexusReader->getNState(), lambda, mu, eta, rho, me_time, gamma[iG])  );

			PS::TreeSharedPtr ptrTree( new PS::Tree(nexusReader) );

			// set the parameters
			TensorPhylo::Interface::vecHistories_t vecHistoriesRejSampling =
					Test::SequentialCPU::computeStochasticMappingLegacy(nReplicas, Likelihood::Approximator::StochasticMapping::REJECTION_SAMPLING_ALGO, nexusReader, parameters, ptrTree);
			Likelihood::Approximator::StochasticMapping::TreeStatistics tStatsRejSampling(nexusReader->getNState(), vecHistoriesRejSampling);

			TensorPhylo::Interface::vecHistories_t vecHistoriesEuler =
					Test::SequentialCPU::computeStochasticMappingLegacy(nReplicas, Likelihood::Approximator::StochasticMapping::DENSE_EULER_ALGO, nexusReader, parameters, ptrTree);
			Likelihood::Approximator::StochasticMapping::TreeStatistics tStatsEuler(nexusReader->getNState(), vecHistoriesEuler);

			TensorPhylo::Interface::vecHistories_t vecHistoriesDOPRI =
					Test::SequentialCPU::computeStochasticMappingLegacy(nReplicas, Likelihood::Approximator::StochasticMapping::DENSE_DOPRI_ALGO, nexusReader, parameters, ptrTree);
			Likelihood::Approximator::StochasticMapping::TreeStatistics tStatsDOPRI(nexusReader->getNState(), vecHistoriesDOPRI);

			SECTION( "Checking stochastic maps validity." ) {

				CHECK(Likelihood::Approximator::StochasticMapping::areTreeStatisticsSimilar(tStatsRejSampling, tStatsEuler, tolerance));
				CHECK(Likelihood::Approximator::StochasticMapping::areTreeStatisticsSimilar(tStatsRejSampling, tStatsDOPRI, tolerance));
				CHECK(Likelihood::Approximator::StochasticMapping::areTreeStatisticsSimilar(tStatsEuler, tStatsDOPRI, tolerance));

				CHECK(Test::SequentialCPU::checkHistoriesEndStatesAgainstDataProbability(ptrTree, nexusReader, vecHistoriesRejSampling, tolerance));
				CHECK(Test::SequentialCPU::checkHistoriesEndStatesAgainstDataProbability(ptrTree, nexusReader, vecHistoriesEuler, tolerance));
				CHECK(Test::SequentialCPU::checkHistoriesEndStatesAgainstDataProbability(ptrTree, nexusReader, vecHistoriesDOPRI, tolerance));
			}

		}

	} // end loop over gamma
}

TEST_CASE("Yule process on a single branch with sequential CPU implementation -- Rejection Sampling stochastic mapping",
		"[Sequential][CPU][StateIndependent][Yule][SingleBranch][NoBDD][StochasticMapping][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromSingleBranchFile( "./unit_tests/legacy/test_2/sb_original.dat" ) );

	std::vector<double> lambda = boost::assign::list_of(0.5)(1.0)(1.5);
	double rho = 0.25;
	double eta = 0.2;

	const size_t nReplicas = 20000;
	const double tolerance = 0.05;

	// Now let's try different rates
	for(size_t iL=0; iL<lambda.size(); ++iL) {

		std::stringstream ss;
		ss << "Computing stochastic maps maps with lambda = " << lambda[iL];
		SECTION( ss.str() ) {

			PS::TreeSharedPtr ptrTree( new PS::Tree(nexusReader) );

			// Set parameters
			Parameters::ContainerSharedPtr parameters( Test::InitData::parametersYule1(nexusReader->getNState(), lambda[iL], eta, rho )  );
			TensorPhylo::Interface::vecHistories_t vecHistoriesRejSampling =
					Test::SequentialCPU::computeStochasticMappingLegacy(nReplicas, Likelihood::Approximator::StochasticMapping::REJECTION_SAMPLING_ALGO, nexusReader, parameters, ptrTree);
			Likelihood::Approximator::StochasticMapping::TreeStatistics tStatsRejSampling(nexusReader->getNState(), vecHistoriesRejSampling);

			double avgNbOfTransitionsRS = tStatsRejSampling.getSegmentStatistics(ptrTree->getOldestNode()->getEdgesToChildren().front()->getId()).getAverageNTransitions();
			CHECK(eta  == Approx(avgNbOfTransitionsRS).margin(eta*tolerance));

			// JC type matrix
			std::pair<double, double> statsRJ = tStatsRejSampling.getObservedTransitionFrequenciesStats();
			CHECK(statsRJ.second < tolerance);

			CHECK(Test::SequentialCPU::checkHistoriesEndStatesAgainstDataProbability(ptrTree, nexusReader, vecHistoriesRejSampling, tolerance));
		}
	}
}

