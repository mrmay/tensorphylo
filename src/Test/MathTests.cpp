/*
 * MathTests.cpp
 *
 *  Created on: Mar 17, 2020
 *      Author: meyerx
 */

#include "Test/Catch2/catch.hpp"
#include "Test/Utils.h"

#include "Tensor/IncTensor.h"
#include "Likelihood/Kernels/CPU/EigenUtils.h"
#include "Likelihood/Kernels/CPU/EigenUtilsMatrix.h"
#include "Likelihood/Kernels/CPU/OpenMP/EigenUtils.h"

TEST_CASE("Tensors contraction (time-heterogenous and sparse tensors)",
		             "[Validation][Math][CPU][Tensor][Time-heterogenous][Sparse][Contraction]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	const size_t N=3;
	std::vector<double> times;
	std::vector< Tensor::rbEventMap_t > events(1);

	for(size_t i=0; i<N; ++i) {
		for(size_t j=0; j<N; ++j) {
			for(size_t k=0; k<N; ++k) {
				std::vector<unsigned int> key = boost::assign::list_of(i)(j)(k);
				double prob = i*0.1 + j*0.2 + k*0.3;
				events[0][key] = prob;
			}
		}
	}

	Eigen::VectorXd x(N), y(N);
	for(size_t i=0; i<N; ++i) {
		x(i) = (1.+i)*(1./N);
		y(i) = 1.-(1.+i)*(1./N);
	}
	x  = x / x.sum();
	y  = y / y.sum();

	//std::cout << x.transpose() << std::endl;
	//std::cout << y.transpose() << std::endl;

	//std::cout << "Creating the tensor: " << std::endl;
	Tensor::BaseTensorSharedPtr ptrTensor( new Tensor::TimeHeterogenousTensor(3, times, events));

	std::vector<Eigen::MatrixXd> denseTensor;
	for(size_t i=0; i<N; ++i) {
		Eigen::MatrixXd tmp = Eigen::MatrixXd(ptrTensor->getSparseTensor(0.)[i]);
		denseTensor.push_back(tmp);
		//std::cout << " -- " << std::endl;
		//std::cout << tmp << std::endl;
	}

	//std::cout << "Checking the vector: " << std::endl;
	Eigen::VectorXd checkVec = Eigen::VectorXd::Zero(N);
	for(size_t i=0; i<N; ++i) {
		for(size_t j=0; j<N; ++j) {
			for(size_t k=0; k<N; ++k) {
				checkVec(i) += denseTensor[i](j,k) * x(j) * y(k);
			}
		}
	}

	SECTION("Sparse tensor - dense vector") {
		Eigen::VectorXd res = Likelihood::Kernels::CPU::computeTensorContractionVector(ptrTensor->getSparseTensor(0.), x, y);
		CHECK( Approx((res-checkVec).norm()).margin(1.e-7) == 0.);
	}

	SECTION("Dense tensor - dense vector") {
		Eigen::VectorXd res = Likelihood::Kernels::CPU::computeTensorContractionVector(denseTensor, x, y);
		CHECK( Approx((res-checkVec).norm()).margin(1.e-7) == 0.);
	}

	SECTION("Sparse tensor - dense vector - opti") {
		double t = 0;
		Eigen::VectorXd res = Likelihood::Kernels::CPU::computeTensorContractionVectorOpti<true>(ptrTensor, x, y, t);
		CHECK( Approx((res-checkVec).norm()).margin(1.e-7) == 0.);
	}

	SECTION("Two-steps - Sparse tensor - dense vector") {
		Eigen::MatrixXd resFirstContractionU(N,N);
		Likelihood::Kernels::CPU::computeFirstStepTensorContraction(ptrTensor->getSparseTensor(0.), y, resFirstContractionU);
		Eigen::MatrixXd res = Eigen::VectorXd::Zero(N);
		Likelihood::Kernels::CPU::computeSecondStepTensorContractionVector(resFirstContractionU, x, res);
		CHECK( Approx((res-checkVec).norm()).margin(1.e-7) == 0.);
	}


	SECTION("Two-steps - Sparse tensor - dense matrix") {
		Eigen::MatrixXd resFirstContractionU(N,N);
		Likelihood::Kernels::CPU::computeFirstStepTensorContraction(ptrTensor->getSparseTensor(0.), y, resFirstContractionU);
		Eigen::MatrixXd res = Eigen::VectorXd::Zero(N);
		Likelihood::Kernels::CPU::computeSecondStepTensorContractionMatrix(resFirstContractionU, x, res);
		CHECK( Approx((res-checkVec).norm()).margin(1.e-7) == 0.);
	}

#if defined(_OPENMP)


	Utils::Parallel::Manager::getInstance()->setNThread(2);
	Utils::Parallel::Manager::getInstance()->setMaxNThread(2);


	SECTION("Parallel: Sparse tensor - dense vector") {
		Eigen::VectorXd res = Likelihood::Kernels::CPU::OpenMP::computeTensorContractionVector(ptrTensor->getSparseTensor(0.), x, y);
		CHECK( Approx((res-checkVec).norm()).margin(1.e-7) == 0.);
	}

	SECTION("Parallel: Dense tensor - dense vector") {
		Eigen::VectorXd res = Likelihood::Kernels::CPU::OpenMP::computeTensorContractionVector(denseTensor, x, y);
		CHECK( Approx((res-checkVec).norm()).margin(1.e-7) == 0.);
	}

	SECTION("Parallel: Sparse tensor - dense vector - opti") {
		double t = 0;
		Eigen::VectorXd res = Likelihood::Kernels::CPU::OpenMP::computeTensorContractionVectorOpti<true>(ptrTensor, x, y, t);
		CHECK( Approx((res-checkVec).norm()).margin(1.e-7) == 0.);
	}

	SECTION("Parallel: Two-steps - Sparse tensor - dense vector") {
		Eigen::MatrixXd resFirstContractionU(N,N);
		Likelihood::Kernels::CPU::OpenMP::computeFirstStepTensorContraction(ptrTensor->getSparseTensor(0.), y, resFirstContractionU);
		Eigen::MatrixXd res = Eigen::VectorXd::Zero(N);
		Likelihood::Kernels::CPU::OpenMP::computeSecondStepTensorContractionVector<true>(resFirstContractionU, x, res);
		CHECK( Approx((res-checkVec).norm()).margin(1.e-7) == 0.);
	}


	SECTION("Parallel: Two-steps - Sparse tensor - dense matrix") {
		Eigen::MatrixXd resFirstContractionU(N,N);
		Likelihood::Kernels::CPU::OpenMP::computeFirstStepTensorContraction(ptrTensor->getSparseTensor(0.), y, resFirstContractionU);
		Eigen::MatrixXd res = Eigen::VectorXd::Zero(N);
		Likelihood::Kernels::CPU::OpenMP::computeSecondStepTensorContractionMatrix(resFirstContractionU, x, res);
		CHECK( Approx((res-checkVec).norm()).margin(1.e-7) == 0.);
	}

#endif

}


TEST_CASE("Tensors contraction with transposed-vector (time-heterogenous and sparse tensors)",
		             "[Validation][Math][CPU][Tensor][Transposed-Vector][Time-heterogenous][Sparse][Contraction]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	const size_t N=3;
	std::vector<double> times;
	std::vector< Tensor::rbEventMap_t > events(1);

	for(size_t i=0; i<N; ++i) {
		for(size_t j=0; j<N; ++j) {
			for(size_t k=0; k<N; ++k) {
				std::vector<unsigned int> key = boost::assign::list_of(i)(j)(k);
				double prob = i*0.1 + j*0.2 + k*0.3;
				events[0][key] = prob;
			}
		}
	}

	Eigen::VectorXd x(N), y(N);
	for(size_t i=0; i<N; ++i) {
		x(i) = (1.+i)*(1./N);
		y(i) = 1.-(1.+i)*(1./N);
	}
	x  = x / x.sum();
	y  = y / y.sum();

	//std::cout << x.transpose() << std::endl;
	//std::cout << y.transpose() << std::endl;

	//std::cout << "Creating the tensor: " << std::endl;
	Tensor::BaseTensorSharedPtr ptrTensor( new Tensor::TimeHeterogenousTensor(3, times, events));

	std::vector<Eigen::MatrixXd> denseTensor;
	for(size_t i=0; i<N; ++i) {
		Eigen::MatrixXd tmp = Eigen::MatrixXd(ptrTensor->getSparseTensor(0.)[i]);
		denseTensor.push_back(tmp);
		//std::cout << " -- " << std::endl;
		//std::cout << tmp << std::endl;
	}

	//std::cout << "Checking the vector: " << std::endl;
	Eigen::VectorXd checkVec = Eigen::VectorXd::Zero(N);
	for(size_t i=0; i<N; ++i) {
		for(size_t j=0; j<N; ++j) {
			for(size_t k=0; k<N; ++k) {
				checkVec(j) += x(i) * denseTensor[i](j,k) * y(k);
			}
		}
	}

	SECTION("Parallel: Sparse tensor - dense vector - opti") {
		double t = 0;
		Eigen::VectorXd res = Likelihood::Kernels::CPU::computeTensorContractionTransposeVectorOpti<true>(ptrTensor, x, y, t);
		CHECK( Approx((res-checkVec).norm()).margin(1.e-7) == 0.);
	}

}

TEST_CASE("Quasse-type matrix-vector multiplication with Eigen and custom convolutions.",
		             "[Validation][Math][CPU][Multiplication][Matrix-Vector][quasse][Band][Sparse]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	const size_t M = 1;

	std::vector<size_t> vecK;
	size_t startK = 5;
	size_t endK = 10;
	for(size_t K = (size_t)(1 << startK); K <= (size_t)(1 << endK); K = K << 1) {
		vecK.push_back(K);
	}
	vecK.push_back(91);
	vecK.push_back(123);
	vecK.push_back(1234);

	for(size_t iK=0; iK<vecK.size(); ++iK) {
		const size_t K = vecK[iK];

		std::stringstream ssK;
		ssK << "Testing 'band' matrix - vector multiplication for size K = " << K;

		// Init the dense matrix
		double alpha = 42.5;
		Eigen::MatrixXd dense = Eigen::MatrixXd::Zero(K,K);

		for(size_t i=0; i<K; ++i) {
			double sum = 0.;

			if(i > 0) {
				dense(i,i-1) = alpha;
				sum += alpha;
			}

			if(i < K-1) {
				dense(i,i+1) = alpha;
				sum += alpha;
			}

			dense(i,i) = -sum;
		}

		// Init the sparse matrix
		Tensor::eigenSparseMatrix_t sparseMat = dense.sparseView();
		sparseMat.makeCompressed();

		// Init vectors
		Eigen::VectorXd vecOnes = Eigen::VectorXd::Ones(K);
		Eigen::VectorXd vecRand = Eigen::VectorXd::Random(K);


		SECTION(ssK.str()) {

			Eigen::VectorXd refOnes, refRand;
			refOnes = dense * vecOnes;
			refRand = dense * vecRand;

			SECTION("Eigen sparse multiplication") {
				Eigen::VectorXd resOnes, resRand;
				for(size_t i=0; i<M; ++i) {
					resOnes = sparseMat * vecOnes;
					resRand = sparseMat * vecRand;
				}

				CHECK( Approx((resOnes-refOnes).norm()).margin(1.e-7) == 0.);
				CHECK( Approx((resRand-refRand).norm()).margin(1.e-7) == 0.);
			}

			SECTION("Convolution-style multiplication with N = 4") {
				Eigen::VectorXd resOnes, resRand;

				for(size_t i=0; i<M; ++i) {
					resOnes = Likelihood::Kernels::CPU::computeQuasseEtaVector<4>(alpha, vecOnes);
					resRand = Likelihood::Kernels::CPU::computeQuasseEtaVector<4>(alpha, vecRand);
				}

				CHECK( Approx((resOnes-refOnes).norm()).margin(1.e-7) == 0.);
				CHECK( Approx((resRand-refRand).norm()).margin(1.e-7) == 0.);
			}

			SECTION("Convolution-style multiplication with N = 8") {
				Eigen::VectorXd resOnes, resRand;

				for(size_t i=0; i<M; ++i) {
					resOnes = Likelihood::Kernels::CPU::computeQuasseEtaVector<8>(alpha, vecOnes);
					resRand = Likelihood::Kernels::CPU::computeQuasseEtaVector<8>(alpha, vecRand);
				}

				CHECK( Approx((resOnes-refOnes).norm()).margin(1.e-7) == 0.);
				CHECK( Approx((resRand-refRand).norm()).margin(1.e-7) == 0.);
			}

			SECTION("Convolution-style multiplication with N = 16") {
				Eigen::VectorXd resOnes, resRand;

				for(size_t i=0; i<M; ++i) {
					resOnes = Likelihood::Kernels::CPU::computeQuasseEtaVector<16>(alpha, vecOnes);
					resRand = Likelihood::Kernels::CPU::computeQuasseEtaVector<16>(alpha, vecRand);
				}

				CHECK( Approx((resOnes-refOnes).norm()).margin(1.e-7) == 0.);
				CHECK( Approx((resRand-refRand).norm()).margin(1.e-7) == 0.);
			}

			if( K > 32 ) {
				SECTION("Convolution-style multiplication with N = 32") {
					Eigen::VectorXd resOnes, resRand;

					for(size_t i=0; i<M; ++i) {
						resOnes = Likelihood::Kernels::CPU::computeQuasseEtaVector<32>(alpha, vecOnes);
						resRand = Likelihood::Kernels::CPU::computeQuasseEtaVector<32>(alpha, vecRand);
					}

					CHECK( Approx((resOnes-refOnes).norm()).margin(1.e-7) == 0.);
					CHECK( Approx((resRand-refRand).norm()).margin(1.e-7) == 0.);
				}
			}
		}
	}
}


TEST_CASE("Quasse-type matrix-matrix multiplication with Eigen and custom convolution.",
		             "[Validation][Math][CPU][Multiplication][Matrix-Matrix][quasse][Band][Sparse]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	const size_t M = 1;

	std::vector<size_t> vecK;
	size_t startK = 5;
	size_t endK = 10;
	for(size_t K = (size_t)(1 << startK); K <= (size_t)(1 << endK); K = K << 1) {
		vecK.push_back(K);
	}
	vecK.push_back(91);
	vecK.push_back(123);
	vecK.push_back(1234);

	for(size_t iK=0; iK<vecK.size(); ++iK) {
		const size_t K = vecK[iK];

		std::stringstream ssK;
		ssK << "Testing 'band' matrix - matrix multiplication for size K = " << K;

		// Init the dense matrix
		double alpha = 42.5;
		Eigen::MatrixXd dense = Eigen::MatrixXd::Zero(K,K);

		for(size_t i=0; i<K; ++i) {
			double sum = 0.;

			if(i > 0) {
				dense(i,i-1) = alpha;
				sum += alpha;
			}

			if(i < K-1) {
				dense(i,i+1) = alpha;
				sum += alpha;
			}

			dense(i,i) = -sum;
		}

		// Init the sparse matrix
		Tensor::eigenSparseMatrix_t sparseMat = dense.sparseView();
		sparseMat.makeCompressed();

		// Init vectors
		Eigen::MatrixXd matI = Eigen::MatrixXd::Identity(K, K);
		Eigen::MatrixXd matRand = Eigen::MatrixXd::Random(K, K);


		SECTION(ssK.str()) {

			Eigen::MatrixXd refI, refRand;
			refI = sparseMat * matI;
			refRand = sparseMat * matRand;


			SECTION("Convolution-style multiplication with inner N = 4 and outer N = 1") {
				Eigen::MatrixXd resI, resRand;

				for(size_t i=0; i<M; ++i) {
					resI = Likelihood::Kernels::CPU::computeQuasseEtaMatrix<4, 1>(alpha, matI);
					resRand = Likelihood::Kernels::CPU::computeQuasseEtaMatrix<4, 1>(alpha, matRand);
				}

				CHECK( Approx((resI-refI).colwise().norm().sum()).margin(1.e-7) == 0.);
				CHECK( Approx((resRand-refRand).colwise().norm().sum()).margin(1.e-7) == 0.);
			}


			SECTION("Convolution-style multiplication with inner N = 4 and outer N = 4") {
				Eigen::MatrixXd resI, resRand;

				for(size_t i=0; i<M; ++i) {
					resI = Likelihood::Kernels::CPU::computeQuasseEtaMatrix<4, 4>(alpha, matI);
					resRand = Likelihood::Kernels::CPU::computeQuasseEtaMatrix<4, 4>(alpha, matRand);
				}

				CHECK( Approx((resI-refI).colwise().norm().sum()).margin(1.e-7) == 0.);
				CHECK( Approx((resRand-refRand).colwise().norm().sum()).margin(1.e-7) == 0.);
			}


			SECTION("Convolution-style multiplication with inner N = 8 and outer N = 4") {
				Eigen::MatrixXd resI, resRand;

				for(size_t i=0; i<M; ++i) {
					resI = Likelihood::Kernels::CPU::computeQuasseEtaMatrix<8, 4>(alpha, matI);
					resRand = Likelihood::Kernels::CPU::computeQuasseEtaMatrix<8, 4>(alpha, matRand);
				}

				CHECK( Approx((resI-refI).colwise().norm().sum()).margin(1.e-7) == 0.);
				CHECK( Approx((resRand-refRand).colwise().norm().sum()).margin(1.e-7) == 0.);
			}

			SECTION("Convolution-style multiplication with inner N = 8 and outer N = 8") {
				Eigen::MatrixXd resI, resRand;

				for(size_t i=0; i<M; ++i) {
					resI = Likelihood::Kernels::CPU::computeQuasseEtaMatrix<8, 8>(alpha, matI);
					resRand = Likelihood::Kernels::CPU::computeQuasseEtaMatrix<8, 8>(alpha, matRand);
				}

				CHECK( Approx((resI-refI).colwise().norm().sum()).margin(1.e-7) == 0.);
				CHECK( Approx((resRand-refRand).colwise().norm().sum()).margin(1.e-7) == 0.);
			}
		}
	}
}
