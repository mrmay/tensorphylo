/*
 * LegacyTests.cpp
 *
 *  Created on: Mar 17, 2020
 *      Author: meyerx
 */

#include "Test/Catch2/catch.hpp"
#include "Test/Utils.h"

TEST_CASE("Yule process on a single branch with sequential CPU implementation", "[Sequential][CPU][StateIndependent][Yule][SingleBranch][NoBDD]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromSingleBranchFile( "./unit_tests/legacy/test_1/sb_original.dat" ) );

	std::vector<double> correctLikelihood = boost::assign::list_of(3.0516886149e-01)(3.3252427962e-01)(3.2025978163e-01);
	std::vector<double> lambda = boost::assign::list_of(0.5)(1.0)(1.5);
	double rho = 0.25;
	double eta = 0.2;

	// Now let's try different rates
	for(size_t iL=0; iL<lambda.size(); ++iL) {

		std::stringstream ss;
		ss << "Computing likelihood with lambda = " << lambda[iL];
		SECTION( ss.str() ) {

			// Set parameters
			Parameters::ContainerSharedPtr parameters( Test::InitData::parametersYule1(nexusReader->getNState(), lambda[iL], eta, rho )  );
			size_t nIntScheme = Test::Definitions::intSchemeNames.size();
			for(size_t iS=0; iS<nIntScheme; ++iS) {

				SECTION( std::string("With integration scheme: ") + Test::Definitions::intSchemeNames[iS] ) {

						Likelihood::Integrator::integrationScheme_t intScheme = static_cast<Likelihood::Integrator::integrationScheme_t >(iS);
						double lik = Test::SequentialCPU::computeLikForTests(intScheme, nexusReader, parameters);
						CHECK( lik == Approx(log(correctLikelihood[iL])).epsilon(1.e-3) ); 		// Check will not end the test, epsilon is the relative error
						REQUIRE(  lik == Approx(log(correctLikelihood[iL])).margin(1.e-7)); 		// Require will end the test, margin is the absolute error

				}
			}
		}
	}
}

TEST_CASE("Yule process on a single branch with sequential CPU implementation with high-precision integration", "[Sequential][CPU][StateIndependent][Yule][SingleBranch][NoBDD][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromSingleBranchFile( "./unit_tests/legacy/test_2/sb_original.dat" ) );

	std::vector<double> correctLikelihood = boost::assign::list_of(3.0516886149e-01)(3.3252427962e-01)(3.2025978163e-01);
	std::vector<double> lambda = boost::assign::list_of(0.5)(1.0)(1.5);
	double rho = 0.25;
	double eta = 0.2;

	// Now let's try different rates
	for(size_t iL=0; iL<lambda.size(); ++iL) {

		std::stringstream ss;
		ss << "Computing likelihood with lambda = " << lambda[iL];
		SECTION( ss.str() ) {

			// Set parameters
			Parameters::ContainerSharedPtr parameters( Test::InitData::parametersYule1(nexusReader->getNState(), lambda[iL], eta, rho )  );

			for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

				std::stringstream ss2;
				ss2 << "With approximator: " << Likelihood::Approximator::APPROXIMATOR_NAMES[iA];
				parameters->intLikApproximator = iA;

				SECTION( ss2.str() ) {

					double lik = Test::SequentialCPU::computeLikForTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, nexusReader, parameters);
					REQUIRE( lik == Approx(log(correctLikelihood[iL])).margin(1.e-7)); 		// Require will end the test, margin is the absolute error
				}
			}
		}
	}
}

TEST_CASE("Birth-death process on a single branch with sequential CPU implementation", "[Sequential][CPU][StateIndependent][BDP][SingleBranch][NoBDD]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromSingleBranchFile( "./unit_tests/legacy/test_3/sb_original.dat" ) );

	std::vector<double> correctLikelihood = boost::assign::list_of(3.0516886149e-01)(2.0649031138e-01)(1.3158368192e-01)(3.3252427962e-01)(2.4375205345e-01)(1.6650591060e-01)(3.2025978163e-01)(2.5818521474e-01)(1.9259773074e-01);
	std::vector<double> lambda = boost::assign::list_of(0.5)(1.0)(1.5);
	std::vector<double> mu     = boost::assign::list_of(0.0)(0.45)(0.95);
	double rho = 0.25;
	double eta = 0.2;

	// iterate over the speciation rates
	for(size_t iL=0; iL < lambda.size(); ++iL) {

		// iterate over extinction rates
		for(size_t iM = 0; iM < mu.size(); ++iM) {

			std::stringstream ss;
			ss << "Computing likelihood with lambda = " << lambda[iL] << " and mu = " << mu[iM];

			SECTION( ss.str() ) {

				// set the parameters
				Parameters::ContainerSharedPtr parameters( Test::InitData::parametersBD1(nexusReader->getNState(), lambda[iL], mu[iM], eta, rho )  );
				size_t nIntScheme = Test::Definitions::intSchemeNames.size();

				// iterate over integration schemes
				for(size_t iS=0; iS<nIntScheme; ++iS) {

					SECTION( std::string("With integration scheme: ") + Test::Definitions::intSchemeNames[iS] ) {

							Likelihood::Integrator::integrationScheme_t intScheme = static_cast<Likelihood::Integrator::integrationScheme_t >(iS);
							double lik = Test::SequentialCPU::computeLikForTests(intScheme, nexusReader, parameters);
							CHECK( lik == Approx(log(correctLikelihood[iL * mu.size() + iM])).epsilon(1.e-3) ); 			// Check will not end the test, epsilon is the relative error
							REQUIRE(  lik == Approx(log(correctLikelihood[iL * mu.size() + iM])).margin(1.e-7)); 		// Require will end the test, margin is the absolute error

					}

				} // end loop over integration schemes

			}

		} // end loop over mu

	} // end loop over lambda


}

TEST_CASE("Birth-death process on a single branch with sequential CPU implementation with high-precision integration", "[Sequential][CPU][StateIndependent][BDP][SingleBranch][NoBDD][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromSingleBranchFile( "./unit_tests/legacy/test_4/sb_original.dat" ) );

	std::vector<double> correctLikelihood = boost::assign::list_of(0.3051688615)(0.2064903114)(0.1315836819)(0.3325242796)(0.2437520535)(0.1665059106)(0.3202597816)(0.2581852147)(0.1925977307);
	std::vector<double> lambda = boost::assign::list_of(0.5)(1.0)(1.5);
	std::vector<double> mu     = boost::assign::list_of(0.0)(0.45)(0.95);
	double rho = 0.25;
	double eta = 0.2;

	// iterate over the speciation rates
	for(size_t iL=0; iL < lambda.size(); ++iL) {

		// iterate over extinction rates
		for(size_t iM = 0; iM < mu.size(); ++iM) {

			std::stringstream ss;
			ss << "Computing likelihood with lambda = " << lambda[iL] << " and mu = " << mu[iM];

			SECTION( ss.str() ) {

				// set the parameters
				Parameters::ContainerSharedPtr parameters( Test::InitData::parametersBD1(nexusReader->getNState(), lambda[iL], mu[iM], eta, rho )  );

				for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

					std::stringstream ss2;
					ss2 << "With approximator: " << Likelihood::Approximator::APPROXIMATOR_NAMES[iA];
					parameters->intLikApproximator = iA;

					SECTION( ss2.str() ) {
						double lik = Test::SequentialCPU::computeLikForTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, nexusReader, parameters);
						CHECK( lik == Approx(log(correctLikelihood[iL * mu.size() + iM])).epsilon(1.e-3) ); 			// Check will not end the test, epsilon is the relative error
						REQUIRE(  lik == Approx(log(correctLikelihood[iL * mu.size() + iM])).margin(1.e-7)); 		// Require will end the test, margin is the absolute error
					}

				}

			}

		} // end loop over mu

	} // end loop over lambda

}

TEST_CASE("Birth-death-mass-extinction process on a single branch with sequential CPU", "[Sequential][CPU][StateIndependent][BDP][SingleBranch][NoBDD][MassExtinction]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromSingleBranchFile( "./unit_tests/legacy/test_5/sb_original.dat" ) );

	std::vector<double> correctLikelihood = boost::assign::list_of(2.7640691610e-01)(2.2068223317e-01)(1.3452817649e-01);
	double lambda  = 1.0;
	double mu      = 0.5;
	double me_time = 0.5;
	double rho     = 1.0;
	double eta     = 0.2;
	std::vector<double> gamma = boost::assign::list_of(0.25)(0.50)(0.75);

	// iterate over gamma (mass-extinction probability)
	for(size_t iG=0; iG < gamma.size(); ++iG) {

		std::stringstream ss;
		ss << "Computing likelihood with gamma = " << gamma[iG];
		SECTION( ss.str() ) {

			// set the parameters
			Parameters::ContainerSharedPtr parameters( Test::InitData::parametersBDME(nexusReader->getNState(), lambda, mu, eta, rho, me_time, gamma[iG])  );

			// iterate over integration schemes
			size_t nIntScheme = Test::Definitions::intSchemeNames.size();
			for(size_t iS=0; iS < nIntScheme; ++iS) {

				SECTION( std::string("With integration scheme: ") + Test::Definitions::intSchemeNames[iS] ) {

						Likelihood::Integrator::integrationScheme_t intScheme = static_cast<Likelihood::Integrator::integrationScheme_t >(iS);
						double lik = Test::SequentialCPU::computeLikForTests(intScheme, nexusReader, parameters);
						//REQUIRE( fabs(lik - correctLikelihood[iL]) < 1.e-7 );
						CHECK( lik == Approx(log(correctLikelihood[iG])).epsilon(1.e-3) ); 		// Check will not end the test, epsilon is the relative error
						REQUIRE(  lik == Approx(log(correctLikelihood[iG])).margin(1.e-7)); 		// Require will end the test, margin is the absolute error

				}

			} // end loop over integration schemes

		}

	} // end loop over gamma

}

TEST_CASE("Birth-death-mass-extinction process on a single branch with sequential CPU with high-precision integration", "[Sequential][CPU][StateIndependent][BDP][SingleBranch][NoBDD][MassExtinction][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromSingleBranchFile( "./unit_tests/legacy/test_6/sb_original.dat" ) );

	std::vector<double> correctLikelihood = boost::assign::list_of(2.7640691610e-01)(2.2068223317e-01)(1.3452817649e-01);
	double lambda  = 1.0;
	double mu      = 0.5;
	double me_time = 0.5;
	double rho     = 1.0;
	double eta     = 0.2;
	std::vector<double> gamma = boost::assign::list_of(0.25)(0.50)(0.75);

	// iterate over gamma (mass-extinction probability)
	for(size_t iG=0; iG < gamma.size(); ++iG) {

		std::stringstream ss;
		ss << "Computing likelihood with gamma = " << gamma[iG];
		SECTION( ss.str() ) {

			// set the parameters
			Parameters::ContainerSharedPtr parameters( Test::InitData::parametersBDME(nexusReader->getNState(), lambda, mu, eta, rho, me_time, gamma[iG])  );

			for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

				std::stringstream ss2;
				ss2 << "With approximator: " << Likelihood::Approximator::APPROXIMATOR_NAMES[iA];
				parameters->intLikApproximator = iA;

				SECTION( ss2.str() ) {
					double lik = Test::SequentialCPU::computeLikForTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, nexusReader, parameters);
					CHECK( lik == Approx(log(correctLikelihood[iG])).epsilon(1.e-3) ); 		// Check will not end the test, epsilon is the relative error
					REQUIRE(  lik == Approx(log(correctLikelihood[iG])).margin(1.e-7)); 		// Require will end the test, margin is the absolute error
				}

			} // end loop over approximators

		}

	} // end loop over gamma

}

TEST_CASE("Yule process on a tree with sequential CPU implementation", "[Sequential][CPU][StateIndependent][Yule][Tree][NoBDD]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "./unit_tests/legacy/test_7/example_yule_tree.nex" ) );

	std::vector<double> correctLikelihood = boost::assign::list_of(-1.8970421828e+01)(-1.2142069983e+01)(-8.4759046580e+00);
	std::vector<double> lambda = boost::assign::list_of(0.5)(1.0)(1.5);
	double rho = 0.25;
	double eta = 0.2;

	// Now let's try different rates
	for(size_t iL=0; iL<lambda.size(); ++iL) {

		std::stringstream ss;
		ss << "Computing likelihood with lambda = " << lambda[iL];
		SECTION( ss.str() ) {

			// Set parameters
			Parameters::ContainerSharedPtr parameters( Test::InitData::parametersYule1(nexusReader->getNState(), lambda[iL], eta, rho )  );

			size_t nIntScheme = Test::Definitions::intSchemeNames.size();
			for(size_t iS=0; iS<nIntScheme; ++iS) {

				SECTION( std::string("With integration scheme: ") + Test::Definitions::intSchemeNames[iS] ) {

						Likelihood::Integrator::integrationScheme_t intScheme = static_cast<Likelihood::Integrator::integrationScheme_t >(iS);
						double lik = Test::SequentialCPU::computeLikForTests(intScheme, nexusReader, parameters);
						CHECK( lik == Approx(correctLikelihood[iL]).epsilon(1.e-3) ); 		// Check will not end the test, epsilon is the relative error
						REQUIRE(  lik == Approx(correctLikelihood[iL]).margin(1.e-7)); 		// Require will end the test, margin is the absolute error

				}

			} // end loop over integration schemes

		}

	} // end loop over lambda

}

TEST_CASE("Yule process on a tree with sequential CPU implementation with high-precision integration", "[Sequential][CPU][StateIndependent][Yule][Tree][NoBDD][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "./unit_tests/legacy/test_8/example_yule_tree.nex" ) );

	std::vector<double> correctLikelihood = boost::assign::list_of(-1.8970421828e+01)(-1.2142069983e+01)(-8.4759046580e+00);
	std::vector<double> lambda = boost::assign::list_of(0.5)(1.0)(1.5);
	double rho = 0.25;
	double eta = 0.2;

	// Now let's try different rates
	for(size_t iL=0; iL<lambda.size(); ++iL) {

		std::stringstream ss;
		ss << "Computing likelihood with lambda = " << lambda[iL];
		SECTION( ss.str() ) {

			// Set parameters
			Parameters::ContainerSharedPtr parameters( Test::InitData::parametersYule1(nexusReader->getNState(), lambda[iL], eta, rho )  );

			for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

				std::stringstream ss2;
				ss2 << "With approximator: " << Likelihood::Approximator::APPROXIMATOR_NAMES[iA];
				parameters->intLikApproximator = iA;

				SECTION( ss2.str() ) {
					double lik = Test::SequentialCPU::computeLikForTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, nexusReader, parameters);
					CHECK( lik == Approx(correctLikelihood[iL]).epsilon(1.e-3) ); 		// Check will not end the test, epsilon is the relative error
					REQUIRE(  lik == Approx(correctLikelihood[iL]).margin(1.e-7)); 		// Require will end the test, margin is the absolute error
				}
			}
		}
	} // end loop over lambda
}

TEST_CASE("Birth-death process on a tree with sequential CPU implementation", "[Sequential][CPU][StateIndependent][BDP][Tree][NoBDD]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "./unit_tests/legacy/test_9/example_yule_tree.nex" ) );

	std::vector<double> correctLikelihood = boost::assign::list_of(-1.8970421828e+01)(-2.1157450252e+01)(-2.3669261848e+01)(-1.2142069983e+01)(-1.3945981369e+01)(-1.6127123071e+01)(-8.4759046580e+00)(-9.8353130603e+00)(-1.1599456213e+01);
	std::vector<double> lambda = boost::assign::list_of(0.5)(1.0)(1.5);
	std::vector<double> mu     = boost::assign::list_of(0.0)(0.45)(0.95);
	double rho = 0.25;
	double eta = 0.2;

	// iterate over the speciation rates
	for(size_t iL=0; iL < lambda.size(); ++iL) {

		// iterate over extinction rates
		for(size_t iM = 0; iM < mu.size(); ++iM) {

			std::stringstream ss;
			ss << "Computing likelihood with lambda = " << lambda[iL] << " and mu = " << mu[iM];

			SECTION( ss.str() ) {

				// set the parameters
				Parameters::ContainerSharedPtr parameters( Test::InitData::parametersBD1(nexusReader->getNState(), lambda[iL], mu[iM], eta, rho )  );
				size_t nIntScheme = Test::Definitions::intSchemeNames.size();

				// iterate over integration schemes
				for(size_t iS=0; iS<nIntScheme; ++iS) {

					SECTION( std::string("With integration scheme: ") + Test::Definitions::intSchemeNames[iS] ) {

							Likelihood::Integrator::integrationScheme_t intScheme = static_cast<Likelihood::Integrator::integrationScheme_t >(iS);
							double lik = Test::SequentialCPU::computeLikForTests(intScheme, nexusReader, parameters);
							CHECK( lik == Approx(correctLikelihood[iL * mu.size() + iM]).epsilon(1.e-3) ); 			// Check will not end the test, epsilon is the relative error
							REQUIRE(  lik == Approx(correctLikelihood[iL * mu.size() + iM]).margin(1.e-7)); 		// Require will end the test, margin is the absolute error

					}

				} // end loop over integration schemes

			}

		} // end loop over mu

	} // end loop over lambda

}

TEST_CASE("Birth-death process on a tree with sequential CPU implementation with high-precision integration", "[Sequential][CPU][StateIndependent][BDP][Tree][NoBDD][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "./unit_tests/legacy/test_10/example_yule_tree.nex" ) );

	std::vector<double> correctLikelihood = boost::assign::list_of(-1.8970421828e+01)(-2.1157450252e+01)(-2.3669261848e+01)(-1.2142069983e+01)(-1.3945981369e+01)(-1.6127123071e+01)(-8.4759046580e+00)(-9.8353130603e+00)(-1.1599456213e+01);
	std::vector<double> lambda = boost::assign::list_of(0.5)(1.0)(1.5);
	std::vector<double> mu     = boost::assign::list_of(0.0)(0.45)(0.95);
	double rho = 0.25;
	double eta = 0.2;

	// iterate over the speciation rates
	for(size_t iL=0; iL < lambda.size(); ++iL) {

		// iterate over extinction rates
		for(size_t iM = 0; iM < mu.size(); ++iM) {

			std::stringstream ss;
			ss << "Computing likelihood with lambda = " << lambda[iL] << " and mu = " << mu[iM];

			SECTION( ss.str() ) {

				// set the parameters
				Parameters::ContainerSharedPtr parameters( Test::InitData::parametersBD1(nexusReader->getNState(), lambda[iL], mu[iM], eta, rho )  );

				for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

					std::stringstream ss2;
					ss2 << "With approximator: " << Likelihood::Approximator::APPROXIMATOR_NAMES[iA];
					parameters->intLikApproximator = iA;

					SECTION( ss2.str() ) {

						double lik = Test::SequentialCPU::computeLikForTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, nexusReader, parameters);
						CHECK( lik == Approx(correctLikelihood[iL * mu.size() + iM]).epsilon(1.e-3) ); 			// Check will not end the test, epsilon is the relative error
						REQUIRE(  lik == Approx(correctLikelihood[iL * mu.size() + iM]).margin(1.e-7)); 		// Require will end the test, margin is the absolute error
					}

				}

			}

		} // end loop over mu

	} // end loop over lambda

}

TEST_CASE("Birth-death mass-extinction process on a tree with sequential CPU implementation", "[Sequential][CPU][StateIndependent][BDP][MassExtinction][Tree][NoBDD]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "./unit_tests/legacy/test_11/example_yule_tree.nex" ) );

	std::vector<double> correctLikelihood = boost::assign::list_of(-7.1986871039e+00)(-8.1541260237e+00)(-1.0172342552e+01);
	double lambda  = 1.0;
	double mu      = 0.5;
	double me_time = 0.5;
	double rho     = 1.0;
	double eta     = 0.2;
	std::vector<double> gamma = boost::assign::list_of(0.25)(0.50)(0.75);

	// iterate over gamma (mass-extinction probability)
	for(size_t iG=0; iG < gamma.size(); ++iG) {

		std::stringstream ss;
		ss << "Computing likelihood with gamma = " << gamma[iG];
		SECTION( ss.str() ) {

			// set the parameters
			Parameters::ContainerSharedPtr parameters( Test::InitData::parametersBDME(nexusReader->getNState(), lambda, mu, eta, rho, me_time, gamma[iG])  );

			// iterate over integration schemes
			size_t nIntScheme = Test::Definitions::intSchemeNames.size();
			for(size_t iS=0; iS < nIntScheme; ++iS) {

				SECTION( std::string("With integration scheme: ") + Test::Definitions::intSchemeNames[iS] ) {

						Likelihood::Integrator::integrationScheme_t intScheme = static_cast<Likelihood::Integrator::integrationScheme_t >(iS);
						double lik = Test::SequentialCPU::computeLikForTests(intScheme, nexusReader, parameters);
						CHECK( lik == Approx(correctLikelihood[iG]).epsilon(1.e-3) ); 		// Check will not end the test, epsilon is the relative error
						REQUIRE(  lik == Approx(correctLikelihood[iG]).margin(1.e-7)); 		// Require will end the test, margin is the absolute error

				}

			} // end loop over integration schemes

		}

	} // end loop over gamma

}

TEST_CASE("Birth-death mass-extinction process on a tree with sequential CPU implementation with high-precision integration", "[Sequential][CPU][StateIndependent][BDP][MassExtinction][Tree][NoBDD][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "./unit_tests/legacy/test_12/example_yule_tree.nex" ) );

	std::vector<double> correctLikelihood = boost::assign::list_of(7.4756664173e-04)(2.8754648505e-04)(3.8212703243e-05);
	double lambda  = 1.0;
	double mu      = 0.5;
	double me_time = 0.5;
	double rho     = 1.0;
	double eta     = 0.2;
	std::vector<double> gamma = boost::assign::list_of(0.25)(0.50)(0.75);

	// iterate over gamma (mass-extinction probability)
	for(size_t iG=0; iG < gamma.size(); ++iG) {

		std::stringstream ss;
		ss << "Computing likelihood with gamma = " << gamma[iG];
		SECTION( ss.str() ) {

			// set the parameters
			Parameters::ContainerSharedPtr parameters( Test::InitData::parametersBDME(nexusReader->getNState(), lambda, mu, eta, rho, me_time, gamma[iG])  );

			for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

				std::stringstream ss2;
				ss2 << "With approximator: " << Likelihood::Approximator::APPROXIMATOR_NAMES[iA];
				parameters->intLikApproximator = iA;

				SECTION( ss2.str() ) {

					double lik = Test::SequentialCPU::computeLikForTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, nexusReader, parameters);
					CHECK( lik == Approx(log(correctLikelihood[iG])).epsilon(1.e-3) ); 			// Check will not end the test, epsilon is the relative error
					REQUIRE(  lik == Approx(log(correctLikelihood[iG])).margin(1.e-7)); 		// Require will end the test, margin is the absolute error

				}

			}

		}

	} // end loop over gamma

}

TEST_CASE("Birth-death-fossilization process on a tree (stem age) with sequential CPU implementation", "[Sequential][CPU][StateIndependent][FBD][Tree][NoBDD][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "./unit_tests/legacy/test_13/example_fbd_tree.nex" ) );

	std::vector<double> correctLikelihood = boost::assign::list_of(-2.3339454987e+01)(-2.3774654984e+01)(-2.7758057768e+01);
	double lambda  = 1.0;
	double mu      = 0.5;
	std::vector<double> phi = boost::assign::list_of(0.1)(0.5)(1.0);
	double eta     = 0.2;
	double rho     = 1.0;

	// iterate over extinction rates
	for(size_t iP = 0; iP < phi.size(); ++iP) {

		std::stringstream ss;
		ss << "Computing likelihood with phi = " << phi[iP];

		SECTION( ss.str() ) {

			// set the parameters
			Parameters::ContainerSharedPtr parameters( Test::InitData::parametersFBD1(nexusReader->getNState(), lambda, mu, phi[iP], eta, rho )  );

			for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

				std::stringstream ss2;
				ss2 << "With approximator: " << Likelihood::Approximator::APPROXIMATOR_NAMES[iA];
				parameters->intLikApproximator = iA;

				SECTION( ss2.str() ) {

					double lik = Test::SequentialCPU::computeLikForTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, nexusReader, parameters);
					CHECK( lik == Approx(correctLikelihood[iP]).epsilon(1.e-3) ); 		// Check will not end the test, epsilon is the relative error
					REQUIRE(  lik == Approx(correctLikelihood[iP]).margin(1.e-7)); 		// Require will end the test, margin is the absolute error
				}

			}

		}

	} // end loop over phi

}

TEST_CASE("Birth-death-fossilization process on a tree (crown age) with sequential CPU implementation", "[Sequential][CPU][StateIndependent][FBD][Tree][NoBDD][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "./unit_tests/legacy/test_14/example_fbd_tree_crown.nex" ) );

	std::vector<double> correctLikelihood = boost::assign::list_of(-2.2312621387e+01)(-2.1969121801e+01)(-2.5138677924e+01);
	double lambda  = 1.0;
	double mu      = 0.5;
	std::vector<double> phi = boost::assign::list_of(0.1)(0.5)(1.0);
	double eta     = 0.2;
	double rho     = 1.0;

	// iterate over extinction rates
	for(size_t iP = 0; iP < phi.size(); ++iP) {

		std::stringstream ss;
		ss << "Computing likelihood with phi = " << phi[iP];

		SECTION( ss.str() ) {

			// set the parameters
			Parameters::ContainerSharedPtr parameters( Test::InitData::parametersFBD1(nexusReader->getNState(), lambda, mu, phi[iP], eta, rho )  );

			for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

				std::stringstream ss2;
				ss2 << "With approximator: " << Likelihood::Approximator::APPROXIMATOR_NAMES[iA];
				parameters->intLikApproximator = iA;

				SECTION( ss2.str() ) {

					double lik = Test::SequentialCPU::computeLikForTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, nexusReader, parameters);
					CHECK( lik == Approx(correctLikelihood[iP]).epsilon(1.e-3) ); 		// Check will not end the test, epsilon is the relative error
					REQUIRE(  lik == Approx(correctLikelihood[iP]).margin(1.e-7)); 		// Require will end the test, margin is the absolute error
				}

			}

		}

	} // end loop over phi

}

TEST_CASE("Conditional birth-death process on a tree starting at the crown with sequential CPU implementation", "[Sequential][CPU][StateIndependent][BDP][Condition][Tree][Crown][NoBDD][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "./unit_tests/legacy/test_15/example_bdp_root.nex" ) );

	std::vector<double> correctLikelihood = boost::assign::list_of(-1.2594997180e+01)(-1.1547026538e+01)(-1.1441666023e+01)(-1.1547026538e+01)(-1.1441666023e+01);
	double lambda  = 0.9;
	double mu      = 0.5;
	double rho     = 1.0;
	double eta     = 0.2;

	// set the parameters
	Parameters::ContainerSharedPtr parameters( Test::InitData::parametersBD1(nexusReader->getNState(), lambda, mu, eta, rho) );

	std::vector<int> conditions = {0, 1, 2, 3, 8};
	for(size_t iC = 0; iC < conditions.size(); ++iC) {

		std::stringstream ss;
		ss << "Computing likelihood with condition: " << Likelihood::Conditions::CONDITION_NAMES[conditions[iC]];

		SECTION( ss.str() ) {

			for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

				SECTION( std::string("With approximator: ") + Likelihood::Approximator::APPROXIMATOR_NAMES[iA] ) {

					// set the condition type
					parameters->condType = conditions[iC];

					// set the approximator type
					parameters->intLikApproximator = iA;

					// calculate the likelihood
					double lik = Test::SequentialCPU::computeLikForTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, nexusReader, parameters);
					REQUIRE( lik == Approx( correctLikelihood[iC] ).margin(1.e-7)); 		// Require will end the test, margin is the absolute error

				}

			} // end loop over approximators

		}

	} // end loop over conditions

}

TEST_CASE("Conditional birth-death process on a tree starting at the stem with sequential CPU implementation", "[Sequential][CPU][StateIndependent][BDP][Condition][Tree][Stem][NoBDD][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "./unit_tests/legacy/test_16/example_bdp_origin.nex" ) );

	std::vector<double> correctLikelihood = boost::assign::list_of(-1.3201928446e+01)(-1.2574116245e+01)(-1.2574116245e+01)(-1.2398851842e+01)(-1.2398851842e+01);
	double lambda  = 0.9;
	double mu      = 0.5;
	double rho     = 1.0;
	double eta     = 0.2;

	// set the parameters
	Parameters::ContainerSharedPtr parameters( Test::InitData::parametersBD1(nexusReader->getNState(), lambda, mu, eta, rho )  );

	std::vector<int> conditions = {0, 4, 5, 6, 7};
	for(size_t iC = 0; iC < conditions.size(); ++iC) {

		std::stringstream ss;
		ss << "Computing likelihood with condition: " << Likelihood::Conditions::CONDITION_NAMES[conditions[iC]];

		SECTION( ss.str() ) {

			for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

				SECTION( std::string("With approximator: ") + Likelihood::Approximator::APPROXIMATOR_NAMES[iA] ) {

					// set the condition type
					parameters->condType = conditions[iC];

					// set the approximator type
					parameters->intLikApproximator = iA;

					// calculate the likelihood
					double lik = Test::SequentialCPU::computeLikForTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, nexusReader, parameters);
					REQUIRE( lik == Approx( correctLikelihood[iC] ).margin(1.e-7));

				}

			} // end loop over approximators

		}

	} // end loop over conditions

}

TEST_CASE("Conditional fossilized birth-death process on a tree starting at the crown with sequential CPU implementation", "[Sequential][CPU][StateIndependent][FBD][Condition][Tree][Crown][NoBDD][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "./unit_tests/legacy/test_17/example_fbd_tree_crown.nex" ) );

	std::vector<double> correctLikelihood = boost::assign::list_of(-1.9054744372e+01)(-1.6667663888e+01)(-1.6562303372e+01)(-1.7381897830e+01)(-1.7276537314e+01);
	double lambda  = 0.9;
	double mu      = 0.5;
	double phi     = 0.1;
	double rho     = 0.2;
	double eta     = 0.2;

	// set the parameters
	Parameters::ContainerSharedPtr parameters( Test::InitData::parametersFBD1(nexusReader->getNState(), lambda, mu, phi, eta, rho )  );

	std::vector<int> conditions = {0, 1, 2, 3, 8};
	for(size_t iC = 0; iC < conditions.size(); ++iC) {

		std::stringstream ss;
		ss << "Computing likelihood with condition: " << Likelihood::Conditions::CONDITION_NAMES[conditions[iC]];

		SECTION( ss.str() ) {

			for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

				SECTION( std::string("With approximator: ") + Likelihood::Approximator::APPROXIMATOR_NAMES[iA] ) {

					// set the condition type
					parameters->condType = conditions[iC];

					// set the approximator type
					parameters->intLikApproximator = iA;

					// calculate the likelihood
					double lik = Test::SequentialCPU::computeLikForTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, nexusReader, parameters);
					REQUIRE( lik == Approx( correctLikelihood[iC] ).margin(1.e-7)); 		// Require will end the test, margin is the absolute error

				}

			} // end loop over approximators

		}

	} // end loop over conditions

}

TEST_CASE("Conditional fossilized birth-death process on a tree starting at the stem with sequential CPU implementation", "[Sequential][CPU][StateIndependent][FBD][Condition][Tree][Stem][NoBDD][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "./unit_tests/legacy/test_18/example_fbd_tree.nex" ) );

	std::vector<double> correctLikelihood = boost::assign::list_of(-1.9736400876e+01)(-1.8678253779e+01)(-1.9023188004e+01)(-1.8170367158e+01)(-1.8654933002e+01);
	double lambda  = 0.9;
	double mu      = 0.5;
	double phi     = 0.1;
	double rho     = 0.2;
	double eta     = 0.2;

	// set the parameters
	Parameters::ContainerSharedPtr parameters( Test::InitData::parametersFBD1(nexusReader->getNState(), lambda, mu, phi, eta, rho )  );

	std::vector<int> conditions = {0, 4, 5, 6, 7};
	for(size_t iC = 0; iC < conditions.size(); ++iC) {

		std::stringstream ss;
		ss << "Computing likelihood with condition: " << Likelihood::Conditions::CONDITION_NAMES[conditions[iC]];

		SECTION( ss.str() ) {

			for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

				SECTION( std::string("With approximator: ") + Likelihood::Approximator::APPROXIMATOR_NAMES[iA] ) {

					// set the condition type
					parameters->condType = conditions[iC];

					// set the approximator type
					parameters->intLikApproximator = iA;

					// calculate the likelihood
					double lik = Test::SequentialCPU::computeLikForTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, nexusReader, parameters);
					REQUIRE( lik == Approx( correctLikelihood[iC] ).margin(1.e-7));

				}

			} // end loop over approximators

		}

	} // end loop over conditions

}

TEST_CASE("Yule process on a tree with extinct tips with sequential CPU implementation", "[Sequential][CPU][StateIndependent][Yule][Tree][Fossils][NoBDD][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "./unit_tests/legacy/test_19/example_fbd_tree.nex" ) );

	double correctLikelihood = log(0.0);
	std::vector<double> lambda = boost::assign::list_of(0.5)(1.0)(1.5);
	double rho = 1.0;
	double eta = 0.2;

	// Now let's try different rates
	for(size_t iL=0; iL<lambda.size(); ++iL) {

		std::stringstream ss;
		ss << "Computing likelihood with lambda = " << lambda[iL];
		SECTION( ss.str() ) {

			// Set parameters
			Parameters::ContainerSharedPtr parameters( Test::InitData::parametersYule1(nexusReader->getNState(), lambda[iL], eta, rho )  );

			size_t nIntScheme = Test::Definitions::intSchemeNames.size();
			for(size_t iS=0; iS<nIntScheme; ++iS) {

				SECTION( std::string("With integration scheme: ") + Test::Definitions::intSchemeNames[iS] ) {

					for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

						std::stringstream ss2;
						ss2 << "With approximator: " << Likelihood::Approximator::APPROXIMATOR_NAMES[iA];
						parameters->intLikApproximator = iA;

						SECTION( ss2.str() ) {

							Likelihood::Integrator::integrationScheme_t intScheme = static_cast<Likelihood::Integrator::integrationScheme_t >(iS);
							double lik = Test::SequentialCPU::computeLikForTests(intScheme, nexusReader, parameters);
							CHECK( lik == Approx(correctLikelihood).epsilon(1.e-3) ); 		// Check will not end the test, epsilon is the relative error
							REQUIRE(  lik == Approx(correctLikelihood).margin(1.e-7)); 		// Require will end the test, margin is the absolute error
						}

					}

				}

			} // end loop over integration schemes

		}
	} // end loop over lambda

}

TEST_CASE("Birth-death process on a tree with extinct tips with sequential CPU implementation", "[Sequential][CPU][StateIndependent][BDP][Tree][Fossils][NoBDD][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "./unit_tests/legacy/test_20/example_fbd_tree.nex" ) );

	double correctLikelihood = log(0.0);
	std::vector<double> lambda = boost::assign::list_of(0.5)(1.0)(1.5);
	std::vector<double> mu     = boost::assign::list_of(0.0)(0.45)(0.95);
	double rho = 1.0;
	double eta = 0.2;

	// iterate over the speciation rates
	for(size_t iL=0; iL < lambda.size(); ++iL) {

		// iterate over extinction rates
		for(size_t iM = 0; iM < mu.size(); ++iM) {

			std::stringstream ss;
			ss << "Computing likelihood with lambda = " << lambda[iL] << " and mu = " << mu[iM];

			SECTION( ss.str() ) {

				// set the parameters
				Parameters::ContainerSharedPtr parameters( Test::InitData::parametersBD1(nexusReader->getNState(), lambda[iL], mu[iM], eta, rho )  );
				size_t nIntScheme = Test::Definitions::intSchemeNames.size();

				// iterate over integration schemes
				for(size_t iS=0; iS<nIntScheme; ++iS) {

					SECTION( std::string("With integration scheme: ") + Test::Definitions::intSchemeNames[iS] ) {

						for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

							std::stringstream ss2;
							ss2 << "With approximator: " << Likelihood::Approximator::APPROXIMATOR_NAMES[iA];
							parameters->intLikApproximator = iA;

							SECTION( ss2.str() ) {

								Likelihood::Integrator::integrationScheme_t intScheme = static_cast<Likelihood::Integrator::integrationScheme_t >(iS);
								double lik = Test::SequentialCPU::computeLikForTests(intScheme, nexusReader, parameters);
								CHECK( lik == Approx(correctLikelihood).epsilon(1.e-3) ); 			// Check will not end the test, epsilon is the relative error
								REQUIRE(  lik == Approx(correctLikelihood).margin(1.e-7)); 		// Require will end the test, margin is the absolute error
							}

						}

					}

				} // end loop over integration schemes

			}

		} // end loop over mu

	} // end loop over lambda

}


TEST_CASE("State-dependent birth-death process on a tree (crown age) with sequential CPU implementation", "[Sequential][CPU][SSE][Tree][NoBDD][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "./unit_tests/legacy/test_21/example_bisse_tree.nex" ) );

	double correctLikelihood   = -1.7265601644e+01;
	std::vector<double> lambda = boost::assign::list_of(1.0)(2.0);
	std::vector<double> mu     = boost::assign::list_of(0.5)(1.5);
	double eta                 = 0.2;
	double rho                 = 1.0;

	std::stringstream ss;
	ss << "Computing likelihood.";

	SECTION( ss.str() ) {

		// set the parameters
		Parameters::ContainerSharedPtr parameters( Test::InitData::parametersBiSSE(nexusReader->getNState(), lambda, mu, eta, rho )  );

		for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

			std::stringstream ss2;
			ss2 << "With approximator: " << Likelihood::Approximator::APPROXIMATOR_NAMES[iA];
			parameters->intLikApproximator = iA;

			SECTION( ss2.str() ) {

				double lik = Test::SequentialCPU::computeLikForTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, nexusReader, parameters);
				REQUIRE(  lik == Approx(correctLikelihood).margin(1.e-7)); 		// Require will end the test, margin is the absolute error

			}

		}

	}

}

TEST_CASE("Interface replication test: State-dependent birth-death process on a tree (crown age) with sequential CPU implementation",
		  "[Interface][Sequential][CPU][SSE][Tree][NoBDD][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	std::string newickStringRB("((t7[&index=10]:0.429661,t9[&index=9]:0.429661)[&index=11]:0.830409,(((((t4[&index=8]:0.206187,(t8[&index=7]:0.174438,(t10[&index=6]:0.044772,t3[&index=5]:0.044772)[&index=12]:0.129667)[&index=13]:0.031749)[&index=14]:0.108421,t5[&index=4]:0.314608)[&index=15]:0.075385,t6[&index=3]:0.389993)[&index=16]:0.119610,t1[&index=2]:0.509603)[&index=17]:0.390282,t2[&index=1]:0.899885)[&index=18]:0.360184)[&index=19]:0.000000;");;
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "./unit_tests/legacy/test_21/example_bisse_tree.nex" ) );

	double correctLikelihood   = -1.7265601644e+01;
	std::vector<double> lambda = boost::assign::list_of(1.0)(2.0);
	std::vector<double> mu     = boost::assign::list_of(0.5)(1.5);
	double eta                 = 0.2;
	double rho                 = 1.0;

	std::stringstream ss;
	ss << "Computing likelihood.";

	SECTION( ss.str() ) {

		// set the parameters
		Parameters::ContainerSharedPtr parameters( Test::InitData::parametersBiSSE(nexusReader->getNState(), lambda, mu, eta, rho )  );

		for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

			std::stringstream ss2;
			ss2 << "With approximator: " << Likelihood::Approximator::APPROXIMATOR_NAMES[iA];
			parameters->intLikApproximator = iA;

			SECTION( ss2.str() ) {

				double lik = Test::SequentialCPU::computeLikForInterfaceTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, newickStringRB, nexusReader, parameters);
				REQUIRE(  lik == Approx(correctLikelihood).margin(1.e-7)); 		// Require will end the test, margin is the absolute error

			}

		}

	}

}

TEST_CASE("Interface replication test: Birth-death mass-extinction process on a tree with sequential CPU implementation with high-precision integration",
		  "[Interface][Sequential][CPU][StateIndependent][BDP][MassExtinction][Tree][NoBDD][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	std::string newickStringRB("((t7[&index=10]:0.429661,t9[&index=9]:0.429661)[&index=11]:0.830409,(((((t4[&index=8]:0.206187,(t8[&index=7]:0.174438,(t10[&index=6]:0.044772,t3[&index=5]:0.044772)[&index=12]:0.129667)[&index=13]:0.031749)[&index=14]:0.108421,t5[&index=4]:0.314608)[&index=15]:0.075385,t6[&index=3]:0.389993)[&index=16]:0.119610,t1[&index=2]:0.509603)[&index=17]:0.390282,t2[&index=1]:0.899885)[&index=18]:0.360184)[&index=19]:0.000000;");;
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "./unit_tests/legacy/test_12/example_yule_tree.nex" ) );

	std::vector<double> correctLikelihood = boost::assign::list_of(7.4756664173e-04)(2.8754648505e-04)(3.8212703243e-05);
	double lambda  = 1.0;
	double mu      = 0.5;
	double me_time = 0.5;
	double rho     = 1.0;
	double eta     = 0.2;
	std::vector<double> gamma = boost::assign::list_of(0.25)(0.50)(0.75);

	// iterate over gamma (mass-extinction probability)
	for(size_t iG=0; iG < gamma.size(); ++iG) {

		std::stringstream ss;
		ss << "Computing likelihood with gamma = " << gamma[iG];
		SECTION( ss.str() ) {

			// set the parameters
			Parameters::ContainerSharedPtr parameters( Test::InitData::parametersBDME(nexusReader->getNState(), lambda, mu, eta, rho, me_time, gamma[iG])  );

			for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

				std::stringstream ss2;
				ss2 << "With approximator: " << Likelihood::Approximator::APPROXIMATOR_NAMES[iA];
				parameters->intLikApproximator = iA;

				SECTION( ss2.str() ) {

					double lik = Test::SequentialCPU::computeLikForInterfaceTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, newickStringRB, nexusReader, parameters);
					CHECK( lik == Approx(log(correctLikelihood[iG])).epsilon(1.e-3) ); 			// Check will not end the test, epsilon is the relative error
					REQUIRE(  lik == Approx(log(correctLikelihood[iG])).margin(1.e-7)); 		// Require will end the test, margin is the absolute error

				}

			}

		}

	} // end loop over gamma

}

TEST_CASE("Interface replication test: Birth-death process on a tree with sequential CPU implementation with high-precision integration",
		             "[Interface][Sequential][CPU][StateIndependent][BDP][Tree][NoBDD][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	std::string newickStringRB("((t7[&index=10]:0.429661,t9[&index=9]:0.429661)[&index=11]:0.830409,(((((t4[&index=8]:0.206187,(t8[&index=7]:0.174438,(t10[&index=6]:0.044772,t3[&index=5]:0.044772)[&index=12]:0.129667)[&index=13]:0.031749)[&index=14]:0.108421,t5[&index=4]:0.314608)[&index=15]:0.075385,t6[&index=3]:0.389993)[&index=16]:0.119610,t1[&index=2]:0.509603)[&index=17]:0.390282,t2[&index=1]:0.899885)[&index=18]:0.360184)[&index=19]:0.000000;");
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "./unit_tests/legacy/test_10/example_yule_tree.nex" ) );

	std::vector<double> correctLikelihood = boost::assign::list_of(-1.8970421828e+01)(-2.1157450252e+01)(-2.3669261848e+01)(-1.2142069983e+01)(-1.3945981369e+01)(-1.6127123071e+01)(-8.4759046580e+00)(-9.8353130603e+00)(-1.1599456213e+01);
	std::vector<double> lambda = boost::assign::list_of(0.5)(1.0)(1.5);
	std::vector<double> mu     = boost::assign::list_of(0.0)(0.45)(0.95);
	double rho = 0.25;
	double eta = 0.2;

	// iterate over the speciation rates
	for(size_t iL=0; iL < lambda.size(); ++iL) {

		// iterate over extinction rates
		for(size_t iM = 0; iM < mu.size(); ++iM) {

			std::stringstream ss;
			ss << "Computing likelihood with lambda = " << lambda[iL] << " and mu = " << mu[iM];

			SECTION( ss.str() ) {

				// set the parameters
				Parameters::ContainerSharedPtr parameters( Test::InitData::parametersBD1(nexusReader->getNState(), lambda[iL], mu[iM], eta, rho )  );

				for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

					std::stringstream ss2;
					ss2 << "With approximator: " << Likelihood::Approximator::APPROXIMATOR_NAMES[iA];
					parameters->intLikApproximator = iA;

					SECTION( ss2.str() ) {

						//double lik = Test::SequentialCPU::computeLikForTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, nexusReader, parameters);
						double lik = Test::SequentialCPU::computeLikForInterfaceTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, newickStringRB, nexusReader, parameters);
						CHECK( lik == Approx(correctLikelihood[iL * mu.size() + iM]).epsilon(1.e-3) ); 			// Check will not end the test, epsilon is the relative error
						REQUIRE(  lik == Approx(correctLikelihood[iL * mu.size() + iM]).margin(1.e-7)); 		// Require will end the test, margin is the absolute error
					}

				}

			}

		} // end loop over mu

	} // end loop over lambda

}




