/*
 * RandomizedTests.cpp
 *
 *  Created on: Mar 27, 2020
 *      Author: michael r may
 */

#include "Test/Catch2/catch.hpp"
#include "Test/Utils.h"

#include <stdio.h>
#include <stdlib.h>

#if !defined(WINDOWS_CYGWIN)

#include <boost/filesystem.hpp>

TEST_CASE("Time-homogeneous, state-homogeneous Yule process on a tree with sequential CPU implementation (randomized)",
          "[Randomized][Sequential][CPU][TimeHomogeneous][StateHomogeneous][Yule][NoBDD]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// set the directory
	chdir("./unit_tests/randomized/test_1/");

	// regenerate the data and parameters
	std::system( "./generate.sh" );

	// loop over datasets
	for(size_t iD = 0; iD < 100; ++iD) {

		// Read the tree
		Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "tree.nex" ) );

		// Read the parameters
		std::string fileParameters( "params.dat" );

		// Read the correct likelihood
		std::ifstream likeFile;
		double correctLikelihood = 0.0;
		likeFile.open( "ll.txt");
		likeFile >> correctLikelihood;
		likeFile.close();

		for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

			std::stringstream ss2;
			ss2 << "With dataset " << iD << " and approximator: " << Likelihood::Approximator::APPROXIMATOR_NAMES[iA];

			SECTION( ss2.str() ) {
				double lik = Test::SequentialCPU::computeLikForInterfaceTests(iA, nexusReader, fileParameters);
				CHECK( lik == Approx(correctLikelihood).epsilon(1.e-3) ); 		// Check will not end the test, epsilon is the relative error
				REQUIRE( lik == Approx(correctLikelihood).margin(1.e-7) ); 		// Require will end the test, margin is the absolute error
			}

		}

	}

	// change back to parent directory
	chdir("../../../");

}

TEST_CASE("Time-homogeneous, state-homogeneous sampled birth-death process on a tree with sequential CPU implementation (randomized)",
          "[Randomized][Sequential][CPU][TimeHomogeneous][StateHomogeneous][BDP][NoBDD]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// set the directory
	chdir("./unit_tests/randomized/test_2/");

	// regenerate the data and parameters
	std::system( "./generate.sh" );

	// loop over datasets
	for(size_t iD = 0; iD < 100; ++iD) {

		// Read the tree
		Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "tree.nex" ) );

		// Read the parameters
		std::string fileParameters( "params.dat" );

		// Read the correct likelihood
		std::ifstream likeFile;
		double correctLikelihood = 0.0;
		likeFile.open( "ll.txt");
		likeFile >> correctLikelihood;
		likeFile.close();

		for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

			std::stringstream ss2;
			ss2 << "With dataset " << iD << " and approximator: " << Likelihood::Approximator::APPROXIMATOR_NAMES[iA];

			SECTION( ss2.str() ) {
				double lik = Test::SequentialCPU::computeLikForInterfaceTests(iA, nexusReader, fileParameters);
				CHECK( lik == Approx(correctLikelihood).epsilon(1.e-3) ); 		// Check will not end the test, epsilon is the relative error
				REQUIRE( lik == Approx(correctLikelihood).margin(1.e-7) ); 		// Require will end the test, margin is the absolute error
			}

		}

	}

	// change back to parent directory
	chdir("../../../");

}

#endif
