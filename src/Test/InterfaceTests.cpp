/*
 * LegacyTests.cpp
 *
 *  Created on: Mar 17, 2020
 *      Author: meyerx
 */

/*
 * InterfaceTests.cpp
 *
 *  Created on: Mar 17, 2020
 *      Author: meyerx
 */

#include "Test/Catch2/catch.hpp"
#include "Test/Utils.h"


TEST_CASE("Time-homogeneous, state-homogeneous Yule process on a tree with sequential CPU implementation",
          "[Interface][Sequential][CPU][TimeHomogeneous][StateHomogeneous][Yule][NoBDD][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	std::string directory("./unit_tests/interface/test_1/");

	// Read the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( directory + "tree.nex" ) );

	// Read the parameters
	std::string fileParameters( directory + "params.dat" );

	// Read the correct likelihood
	std::ifstream likeFile;
	double correctLikelihood = 0.0;
	likeFile.open( directory + "ll.txt");
	likeFile >> correctLikelihood;
	likeFile.close();

	for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

		std::stringstream ss2;
		ss2 << "With approximator: " << Likelihood::Approximator::APPROXIMATOR_NAMES[iA];

		SECTION( ss2.str() ) {

			//double lik = Test::SequentialCPU::computeLikForTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, nexusReader, parameters);
			double lik = Test::SequentialCPU::computeLikForInterfaceTests(iA, nexusReader, fileParameters);
			CHECK( lik == Approx(correctLikelihood).epsilon(1.e-3) ); 		// Check will not end the test, epsilon is the relative error
			REQUIRE( lik == Approx(correctLikelihood).margin(1.e-7) ); 		// Require will end the test, margin is the absolute error
		}

	}

}

TEST_CASE("Time-homogeneous, state-homogeneous birth-death process on a tree with sequential CPU implementation",
          "[Interface][Sequential][CPU][TimeHomogeneous][StateHomogeneous][BDP][NoBDD][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	std::string directory("./unit_tests/interface/test_2/");

	// Read the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( directory + "tree.nex" ) );

	// Read the parameters
	std::string fileParameters( directory + "params.dat" );

	// Read the correct likelihood
	std::ifstream likeFile;
	double correctLikelihood = 0.0;
	likeFile.open( directory + "ll.txt");
	likeFile >> correctLikelihood;
	likeFile.close();

	for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

		std::stringstream ss2;
		ss2 << "With approximator: " << Likelihood::Approximator::APPROXIMATOR_NAMES[iA];

		SECTION( ss2.str() ) {

			//double lik = Test::SequentialCPU::computeLikForTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, nexusReader, parameters);
			double lik = Test::SequentialCPU::computeLikForInterfaceTests(iA, nexusReader, fileParameters);
			CHECK( lik == Approx(correctLikelihood).epsilon(1.e-3) ); 		// Check will not end the test, epsilon is the relative error
			REQUIRE( lik == Approx(correctLikelihood).margin(1.e-7) ); 		// Require will end the test, margin is the absolute error
		}

	}

}


TEST_CASE("Time-heterogeneous birth-death process on a tree with sequential CPU implementation with high-precision integration",
		  "[Interface][Sequential][CPU][StateIndependent][VBDP][Tree][NoBDD][Validation]") {

	Utils::Output::outputManager().setVerbosityThreshold(Utils::Output::SILENT_VERB);

	// This is executed before each section
	// Set initial state for the tree
	Phylogeny::NexusReader::NexusParserSharedPtr nexusReader( Test::InitData::nexusFromFile( "./rb_tests/variable_rate/data/tree.nex" ) );
	std::string fileParameters( "./rb_tests/variable_rate/data/parameters.dat" );

	double correctLikelihood = -450.3046;

	for(size_t iA=0; iA<Likelihood::Approximator::APPROXIMATOR_NAMES.size(); ++iA) {

		std::stringstream ss2;
		ss2 << "With approximator: " << Likelihood::Approximator::APPROXIMATOR_NAMES[iA];

		SECTION( ss2.str() ) {

			//double lik = Test::SequentialCPU::computeLikForTests(Likelihood::Integrator::integrationScheme_t::RUNGE_KUTTA54, nexusReader, parameters);
			double lik = Test::SequentialCPU::computeLikForInterfaceTests(iA, nexusReader, fileParameters);
			CHECK( lik == Approx(correctLikelihood).epsilon(1.e-3) ); 			// Check will not end the test, epsilon is the relative error
			REQUIRE( lik == Approx(correctLikelihood).margin(1.e-7) ); 		// Require will end the test, margin is the absolute error
		}

	}

}
