/*
 * main.cpp
 *
 *  Created on: Aug 22, 2019
 *      Author: Xavier Meyer
 */

// MAIN IS DEFINED HERE
#define CATCH_CONFIG_MAIN

// TESTS ARE IMPLEMENTED THERE
#include "Test/LegacyTests.cpp"
#include "Test/InterfaceTests.cpp"
#include "Test/MathTests.cpp"
#include "Test/RandomizedTests.cpp"
#include "Test/SanityTests.cpp"


