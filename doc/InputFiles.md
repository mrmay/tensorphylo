# InputFiles

## Standard parameters
The general structure is:
* Data name
* Data structure size
* Data (vector/matrix/tensor)
    * vectors are on one line  
    * matrix are row/line format
    * tensor are a "list" of matrix one after the other

A special case is the vector of vectors (e.g., massSamplingProb).
In this case, we have the number of vectors and then the size of each vector and its content.

> N_VECTORS=2
> N_ELEM=2
> X=1 Y=1
> N_ELEM=2
> X=1 Y=1

## Single branch input file
The format is:
* Number of categories
* Number of nodes
* List of nodes:
    * Time and list of state probabilities on one line
    * The last node as no probabilities (so only time)

> N_CAT=2
> N_NODE=3
> TIME=0.0 X=0. Y=0. // Ghost node
> TIME=0.5 X=1.0 Y=1.0 // Sampled ancestor
> TIME=1.0 // God
