# Benchmarks

List of benchmarks:

* [Preliminary against RevBayes](#preliminary-benchmark-vs-revbayes)

## Preliminary benchmark vs RevBayes

This benchmark compares the _synchrounous-time_ algorithm (BLAS3 operations) against the default algorithm in **RevBayes** (i.e., kind of _branch-wise_ algorithm).
The setting is the following:

* Model assumed:

    * Homogeneous in time process (i.e., constant rates)
    * **anagenetic matrix (H)**: Jukes Cantor
    * **cladogenetic matrix (Omega)**: Identity tensor

* Datasets

    * Simulated using TESS (see _benchmark/BenchmarkPhyloDB/createTrees.sh_ and _validation/simulate_trees.r_)
    * **Number of tips (T)**: [50, 100, 250, 500, 750]
    * **Number of states (S)**: [100, 250, 500, 750]

* Procedure

    * **TensorPhylo** likelihoods runtimes are averaged over 100 iterations (including likelihood init. time).
    * **RevBayes** likelihoods runtimes are averaged over 50 iterations - stochastic mapping iterations are not considered (only pure likelihood).

### Results
![Speedup RevBayes/TensorPhylo](/benchmark/BenchmarkPhyloDB/Figures/RevBayesSpeedup.png "Speedup")

### Discussion

**TensorPhylo** is consistently faster than **RevBayes** on this dataset. The speedup tends to get slower with the number of species and might ultimately result in a _slowdown_ if the size of the phylogeny would further grow. This is due to the current algorithm implemented in **TensorPhylo**.

Algorithms complexity order:

* **TensorPhylo**: _synchrounous-time_ algorithm (BLAS3 operations) but complexity order of _O(T^2 * S^3)_
* **TensorPhylo**: _branch-wise_ algorithm (BLAS2 if any BLAS operations) but complexity order of _O(T * S^3)_

The _asynchrounous-time_ (in development) should solve this issue. The projected speedups are expected to be _constant_ with _T_ (since both complexity order should then be equivalent) but still increasing with _S_ given that this new algorithm will exploit BLAS3 operations.
