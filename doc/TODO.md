# Todo list


## Xavier

### Notes
* Dense cladogenetic matrices removed after commit "6296fe4" in templated versions.
* Catch2 tests files (e.g., **src/Test/LegacyTests.cpp**) must follow the following file pattern name: **src/Tests/\*\*\*\*Tests.cpp**. Otherwise the shared library will stop working.

### Boost::odeint integration
boost::odeint is creating a ton of state copies. The integration must be improved:

* Boost odeint memory allocation
    * Boost odeint copy could be avoided by swapping memory matrix (or reduced by rewriting the controlled_RK class). Swap is easy, works with our tests but might be unsafe (for a 1-2% gain in perf).
    * Custom implementation of an integrator should avoid these issues.

### Performance
* Sequential:
    * CPU asynchronous version (i.e., RevBayes style)
* CPU parallel version
    * Check why OpenMP implementation under perform with RevBayes
* GPU
    * Dense
    * Sparse
    * Dynamic kernels (?)
* Benchmark
    * Think about benchmarks for RevBayes, Diversitree, Castor


### Minor/Random

 * Rework the edge length to a lazy evaluation (per Mike suggestion) and the tree scaling function

## Mike

### Test cases

* Time homogeneous
	* one-branch case
		* ~~Pure birth-death~~
		* ~~Mass extinction~~
		* Cladogenetic speciation (?)
	* two-branch case
	    * ~~Pure birth-death~~
	    * ~~Mass extinction~~
	    * Cladogenetic speciation (?)
* Time heterogeneous?
    * Pure birth-death or mass extinction
	* Test to make sure time heterogeneous implementation agrees with time homogeneous implementation when all the epochs have the same rates
* Design test cases for: cladogenetic models, plus others (check what is currently tested)
* ~~Separate tests for most accurate integration schemes against analytical results, mark as [Validation]~~

### Theoretical stuff

* ~~Work out conditional probability cases~~
* Write stochastic mapping math/algorithm

## Both

### Stochastic mapping
* Think how this could be integrated in the current framework.

### Suggestion
* We should probably have test cases for likelihoods that we can't yet validate
    * Asynchrous events (all type)
    * ~~Conditioning (all type)~~
* Safety checks could include
    * Not crashing (yay!)
    * Obtaining the same likelihood as other likelihood approximator
    * 0 likelihood cases? Like lambda=0. ?
