# Installing TensorPhylo using the installer script

The *install.sh* script is designed to automate most of TensorPhylo installation on Linux, MacOS and Windows (i.e., cygwin). This script automate the manual build described in the *build/example* folder.


* [Installing on Linux or MacOS](#how-to-use-the-script-on-linux-or-macos)
* [Installing on Windows](#how-to-use-the-script-on-windows)
* [Installing RevBayes for TensorPhylo](#installing-revbayes-for-tensorphylo)

## How to use the script on Linux or MacOS

### General requirements
To use the script you will need:

* An internet connection (to download dependencies)
* Basic tools that can be installed with your Linux package manager or homebrew for MacOS. That is:
    * *curl*
    * *zip* and *unzip*
    * *automake* and *autoconf*
    * *make*
    * *python*
    * (optional) *scons*

The script will warn you if one of these tools is unavailable.

### Installing
1. Open a terminal and go in the *build/installer*. E.g.,
```bash
      $ cd build/installer
```

2. From the *build/installer* folder, execute the following command:
```bash
      $ bash install.sh
```

3. The installation will proceed and may take several minutes. *TensorPhylo* compilation will use several processor and may slightly impact the responsiveness of your system. Please be patient and do not overload your system (i.e., RevBayes runs, youtube, etc.).

4. Once done, you will have the *TensorPhylo* libraries installed in the *build/installer/lib* folder. If the installation was unsuccessful, the *log.txt* will contain more information - if you can't make sense of it: submit an issue with the *log.txt* file on the git repository.


## How to use the script on Windows

### General requirements
To use the script you will need:
* An internet connection (to download dependencies)
* A functional installation of [cygwin](https://www.cygwin.com/)

### Installing
1. Clone or copy the TensorPhylo repository somewhere in your cygwin home folder (e.g., */home/username/TensorPhylo*)

2. Copy the cygwin *setup-x86_64.exe* executable in the *build/installer/cygwin/* of the TensorPhylo Project.

3. Using a cygwin terminal, go into *build/installer/cygwin/* and to install all packages required for the installation, run
```bash
      $ cd ~/TensorPhylo
      $ cd build/installer/cygwin
      $ bash installPackages.sh
```

4. Move back to *build/installer*, and to launch the installation of *TensorPhylo*, run
```bash
      $ cd ..
      $ bash install.sh
```

5. The installation will proceed and may take a few hours (hang tight). *TensorPhylo* compilation will use several processor and may slightly impact the responsiveness of your system. Please be patient and do not overload your system (i.e., RevBayes runs, youtube, etc.).

6. Once done, you will have the *TensorPhylo* libraries installed in the *build/installer/lib* folder. If the installation was unsuccessful, the *log.txt* will contain more information - if you can't make sense of it: submit an issue with the *log.txt* file on the git repository.

## Installing RevBayes for TensorPhylo

1. Download or clone the *dev_mrm* branches from the RevBayes git repository
> Direct link: https://github.com/revbayes/revbayes/archive/dev_mrm.zip

2. Go in the *projects/installer* of the RevBayes project and follow the instruction in the *projects/installer/README.txt* file.

3. (Optional Linux/MacOS) Copying the *TensorPhylo* in a default location.

     * Locate the *libTensorPhylo.xxx* library, where *xxx* is *.so*, *.dylib* or *.dll* for Linux, MacOS or Windows, respectively.
     * In your home folder (e.g., *~/*), create a *.plugins* folder and copy the library file inside.
     * This will allow you to load the *TensorPhylo* plugin without specifying the path.
