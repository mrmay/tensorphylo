#include <iostream>
#include <exception>
#include <typeinfo>
#include <stdexcept>

#include <boost/dll.hpp>
#include <boost/function.hpp>
#include <boost/dll/import.hpp> // for import_alias

#include "RevBayes/Loader.h"

void testTensorPhyloInterface(const std::string &param) {

	if(Plugin::loader().loadTensorPhylo(param)) {
		assert(Plugin::loader().isTensorPhyloLoaded());

		TensorPhylo::DistributionHandlerSharedPtr ptrDH(Plugin::loader().createTensorPhyloLik());
		std::cout << "Version : " << ptrDH->getVersion() << std::endl;
	} else {
		std::cout << "Not loaded." << std::endl;
	}

}

int main(int argc, char** argv) {

	int nParams = argc;
	assert(nParams == 2);

	std::string exec(argv[0]);
	std::string param(argv[1]);

	std::cout << "Executable: " << exec << std::endl;
	std::cout << "Parameter: " << param << std::endl;

	testTensorPhyloInterface(param);

	return 0;
}
