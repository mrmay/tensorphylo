#!/usr/bin/env Rscript

# check for TESS
has_TESS = require(TESS)
if (has_TESS == FALSE) {
  cat("TESS isn't installed! Installing TESS.\n")
  install.packages("TESS", depend=TRUE)
} else {
  cat("TESS is installed!\n")
}

# get arguments
args = commandArgs(trailingOnly=TRUE)
if ( length(args) != 4 ) {
  stop("Incorrect number of arguments. Please provide these arguments:\n\tArgument 1: output directory\n\tArgument 2: number of tips\n\tArgument 3: number of states\n\tArgument 4: number of simulations\n ")
}

# check the arguments
output_dir = as.character(args[1])
num_tips   = as.numeric(args[2])
num_states = as.numeric(args[3])
num_sims   = as.numeric(args[4])

# create output directory
cat("Creating output directory.\n")
dir.create(output_dir, recursive=TRUE, showWarnings=FALSE)

# simulate character data
cat("Simulating data.\n")
bar = txtProgressBar(style=3, width=40)
for(i in 1:num_sims) {
  
  # simulate a tree
  tree = tess.sim.taxa(1, num_tips, max=1000, lambda=1, mu=0, samplingProbability=1.0)[[1]] 
  tree$edge.length = tree$edge.length / max(branching.times(tree))
  
  # write the tree
  tree_fn = paste0(output_dir,"/tree_",i,".nex")
  write.nexus(tree, file=tree_fn, translate=FALSE)
  
  # create the data
  data_fn = paste0(output_dir,"/data_",i,".tsv")
  data = data.frame(tree$tip.label, "?")
  colnames(data) = c("nStates", as.character(num_states))
  write.table(data, quote=FALSE, sep="\t", row.names=FALSE, file=data_fn)
  
  # update the progress bar
  setTxtProgressBar(bar, i / num_sims)
  
}

cat("\nDone!\n")




