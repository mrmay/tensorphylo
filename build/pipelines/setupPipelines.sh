#!/bin/bash

CACHE_DIR=${HOME}/packages
mkdir -p ${CACHE_DIR}

CURRENT_DIR=`pwd`

if [ -d "${CACHE_DIR}/eigen-3.4.0" ]; then

  echo "Cache is available"

else

  echo "Instaling NCL, eigen and boost 1.73 (headers only)"
  echo "Installing in : ${CACHE_DIR}"

  cd ${CACHE_DIR}
  ############# Install NCL ################
  wget https://github.com/mtholder/ncl/archive/master.zip
  # Unzip
  unzip master.zip
  cd ncl-master
  # Build
  sh bootstrap.sh
  mkdir build
  cd build
  ../configure --prefix=`pwd`/installed
  make -j 4
  make install
  #cp installed/lib/ncl/libncl.a installed/lib/.
  cd ../..  # back to packages
  ############# Install eigen ################
  wget https://gitlab.com/libeigen/eigen/-/archive/3.4.0/eigen-3.4.0.zip
  unzip eigen-3.4.0.zip
  ############# Install last boost ################
  wget https://boostorg.jfrog.io/artifactory/main/release/1.73.0/source/boost_1_73_0.zip
  unzip boost_1_73_0.zip

  cd ${CURRENT_DIR}
fi
