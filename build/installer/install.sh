#!/bin/bash

# keep track of base folder
root_folder=`pwd`
logfile=${root_folder}/log.txt

uname_out="$(uname -s)"
case "${uname_out}" in
    Linux*)     machine=Linux; compiler=g++;;
    Darwin*)    machine=Mac; compiler=clang++;;
    CYGWIN*)    machine=Windows-cygwin; toolchain="x86_64-w64-mingw32"; compiler="${toolchain}-g++";;
    *)          machine="UNKNOWN"
esac

# Checking 1) the OS, 2) the number of processors available for compilation
# and 3) if openmp is installed (macos only)
if [ "${machine}" == "UNKNOWN" ]; then
  echo "This type of OS is not supported. Follow the manual installation instructions."
  echo "[ERROR] ${machine} - ${compiler}" > ${logfile}
  exit
else
  div_proc=2
  if [ "${machine}" == "Mac" ]; then
    n_proc=$((`sysctl -n hw.logicalcpu` / ${div_proc}))
  else
    n_proc=$((`nproc` / ${div_proc}))
  fi

  echo "The installation will proceed for a '${machine}' system and will take several minutes (with ${n_proc} processors)."
  echo "${machine} - ${compiler} - ${n_proc}" > ${logfile}

  if [ "${machine}" == "Mac" ]; then
    checkOpenMP=`brew info libomp|grep "Not installed"`
    if [ "${checkOpenMP}" == "" ]; then
      echo "clang with libomp" >> ${logfile}
    else
      echo "[WARNING] TensorPhylo won't be installed with support for multithreading (no parallelism)."
      echo "[WARNING] Install using the 'brew install libomp' command."
      echo "clang without libomp" >> ${logfile}
      read -r -p "Do you want to continue without multithreading? [y/N] " response
      case "$response" in
          [yY][eE][sS]|[yY])
              echo "Resuming.."
              echo "Continue without openmp" >> ${logfile}
              ;;
          *)
              echo "Aborting.."
              echo "Aborting on user request" >> ${logfile}
              exit
              ;;
      esac
    fi
  fi
fi

echo "########################################"
echo "Checking your installation"

# Prepare folders
tensorphylo_folder=${root_folder}/../..
lib_folder=${root_folder}/lib
build_folder=${root_folder}/build
tmp_folder=${build_folder}/tmp
mkdir -p ${lib_folder}
mkdir -p ${tmp_folder}
mkdir -p ${build_folder}

# Check for mandatory requirements
# Check connection
con_check=`ping -q -c1 google.com &>/dev/null && echo 1 || echo 0`
if [ "${con_check}" == "0" ]; then
  echo "No connection available. Retry whith an internet connection."
  echo "No connection available. Retry whith an internet connection." >> ${logfile} 2>&1
  exit
else
  echo "> connection established successfully"
  echo "> connection established successfully" >> ${logfile} 2>&1
fi

# Check curl
curl_bin=`command -v curl`
if [ "${curl_bin}" == "" ]; then
  echo "'curl' is unavailable on your system and is required."
  echo "Please install curl and retry (e.g., install with homebrew for MacOS or aptitude for Linux)."
  echo "> no curl" >> ${logfile} 2>&1
  exit
else
  echo "> curl found: ${curl_bin}"
  echo "> curl found: ${curl_bin}" >> ${logfile} 2>&1
fi

# Check python
python_bin=`command -v python`
if [ "${python_bin}" == "" ]; then
  echo "'python' is unavailable on your system and is required."
  echo "Please install python and retry (e.g., install with homebrew for MacOS or aptitude for Linux)."
  echo "> no python" >> ${logfile} 2>&1
  exit
else
  echo "> python found: ${python_bin}"
  echo "> python found: ${python_bin}" >> ${logfile} 2>&1
fi

# Check autoconf
autoconf_bin=`command -v autoconf`
if [ "${autoconf_bin}" == "" ]; then
  echo "'autoconf/automake' is unavailable on your system and is required."
  echo "Please install autoconf/automake and retry (e.g., install with homebrew for MacOS or aptitude for Linux)."
  echo "> no autoconf/automake" >> ${logfile} 2>&1
  exit
else
  echo "> autoconf/automake found: ${autoconf_bin}"
  echo "> autoconf/automake found: ${autoconf_bin}" >> ${logfile} 2>&1
fi

# Check make
make_bin=`command -v make`
if [ "${make_bin}" == "" ]; then
  echo "'make' is unavailable on your system and is required."
  echo "Please install make and retry (e.g., install with homebrew for MacOS or aptitude for Linux)."
  echo "> no make" >> ${logfile} 2>&1
  exit
else
  echo "> make found: ${make_bin}"
  echo "> make found: ${make_bin}" >> ${logfile} 2>&1
fi

# Check unzip
unzip_bin=`command -v unzip`
if [ "${unzip_bin}" == "" ]; then
  echo "'unzip' is unavailable on your system and is required."
  echo "Please install make and retry (e.g., install with homebrew for MacOS or aptitude for Linux)."
  echo "> no unzip" >> ${logfile} 2>&1
  exit
else
  echo "> unzip found: ${unzip_bin}"
  echo "> unzip found: ${unzip_bin}" >> ${logfile} 2>&1
fi

# Check perl
perl_bin=`command -v perl`
if [ "${perl_bin}" == "" ]; then
  echo "'perl' is unavailable on your system and is required."
  echo "Please install perl and retry (e.g., install with homebrew for MacOS or aptitude for Linux)."
  echo "> no perl" >> ${logfile} 2>&1
  exit
else
  echo "> perl found: ${perl_bin}"
  echo "> perl found: ${perl_bin}" >> ${logfile} 2>&1
fi

# Check compiler
compiler_bin=`command -v ${compiler}`
if [ "${compiler_bin}" == "" ]; then
  echo "'${compiler}' is unavailable on your system and is requireds."
  echo "Please install ${compiler} and retry (e.g., install with homebrew for MacOS or aptitude for Linux)."
  echo "> no ${compiler}" >> ${logfile} 2>&1
  exit
else
  echo "> compiler found: ${compiler_bin}"
  echo "> compiler found: ${compiler_bin}" >> ${logfile} 2>&1
  # Check if c++11 ready
  isCompatible11=`${compiler_bin} checkCPPVersion.cpp -o checkCPPVersion`
  if [ "${isCompatible11}" == "0" ]; then
    echo "You need a compiler with c++11 enabled (e.g., g++ >=4.8 or Clang/llvm >= 3.3)."
    echo "You need a compiler with c++11 enabled (e.g., g++ >=4.8 or Clang/llvm >= 3.3)." >> ${logfile} 2>&1
    exit
  else
    echo "> compiler is c++11 compatible"
    echo "> compiler is c++11 compatible" >> ${logfile} 2>&1
  fi
fi

# Install scons local if not available
scons_bin=`command -v scons`
if [ "${scons_bin}" == "" ]; then
  # Create a tmp folder for the installation
  scons_folder=${tmp_folder}/scons

  mkdir -p ${scons_folder}

  curl -fsSL http://prdownloads.sourceforge.net/scons/scons-local-3.1.2.tar.gz -o ${scons_folder}/scons.tar.gz
  cd ${scons_folder}
  tar -xf scons.tar.gz
  scons_bin="${scons_folder}/scons.py"
  cd ${root_folder}
  scons_local=1
  echo "> local scons has been installed"
  echo "> local scons has been installed" >> ${logfile} 2>&1
else
  scons_local=0
  echo "> scons is installed"
  echo "> scons is installed" >> ${logfile} 2>&1
fi
echo "########################################"
echo ""

echo "########################################"
echo "Downloading boost locally"
echo ".. will take a few minutes.."
if [ "${machine}" == "Windows-cygwin" ]; then
  boost_folder=/usr/${toolchain}/sys-root/mingw/include
  if [ ! -d ${boost_folder}/boost ]; then
    echo "'boost' is unavailable on your system and is required."
    echo "Please install boost and retry (i.e., install with cygwin - see instructions)."
    echo "> no boost in ${boost_folder}" >> ${logfile} 2>&1
    exit
  fi
else
  boost_folder=${tmp_folder}/boost_1_73_0
  curl -fsSL https://boostorg.jfrog.io/artifactory/main/release/1.73.0/source/boost_1_73_0.tar.gz -o ${tmp_folder}/boost.tar.gz
  cd ${tmp_folder}
  tar -xzf boost.tar.gz
fi
cd ${root_folder}
echo "> boost available in: ${boost_folder}"
echo "> boost available in: ${boost_folder}" >> ${logfile} 2>&1
echo "> done"
echo "########################################"
echo ""

echo "########################################"
echo "Downloading eigen c++ locally"
echo ".. will take a few seconds.."
eigen_folder=${tmp_folder}/eigen-3.4.0
curl -fsSL https://gitlab.com/libeigen/eigen/-/archive/3.4.0/eigen-3.4.0.zip -o ${tmp_folder}/eigen.zip
cd ${tmp_folder}
unzip eigen.zip
cd ${root_folder}
echo "> eigen available in: ${eigen_folder}"
echo "> eigen available in: ${eigen_folder}" >> ${logfile} 2>&1
echo "> done"
echo "########################################"
echo ""

echo "########################################"
echo "Downloading and compiling the Nexus Class Library locally"
echo ".. will take a few minutes.."
ncl_folder=${tmp_folder}/ncl
curl -fsSL https://github.com/meyerx/ncl/archive/master.zip -o ${tmp_folder}/ncl.zip
cd ${tmp_folder}
unzip -q -o ncl.zip
cd ncl-master
sh bootstrap.sh >> ${logfile} 2>&1
if [ "${machine}" == "Windows-cygwin" ]; then
  compiler_def="CC=${toolchain}-gcc CXX=${toolchain}-g++"
  ./configure --prefix=${ncl_folder} ${compiler_def} >> ${logfile} 2>&1
else
  ./configure --prefix=${ncl_folder} >> ${logfile} 2>&1
fi
make -j ${n_proc} CFLAGS='-g -O2 -fpic' CXXFLAGS='-O3 -ffast-math -fpic' LDFLAGS='-fpic' >> ${logfile} 2>&1
make install >> ${logfile} 2>&1
ncl_lib_path=${ncl_folder}/lib/ncl
cd ${root_folder}
echo "> ncl available in: ${ncl_folder}"
echo "> ncl lib available in: ${ncl_lib_path}"
echo "> ncl available in: ${ncl_folder}" >> ${logfile} 2>&1
echo "> ncl lib available in: ${ncl_lib_path}" >> ${logfile} 2>&1

echo "> done"
echo "########################################"
echo ""

# Moving the library
echo "############################"
echo "Compiling TensorPhylo"
echo ".. will take a few minutes.."
# Configure makefile
cp SConstruct ${build_folder}/SConstruct
cp Makefile.stub ${build_folder}/Makefile
perl -i -pe"s#!TP_ROOT#${tensorphylo_folder}#g" ${build_folder}/Makefile
perl -i -pe"s#!COMPILER#${compiler}#g" ${build_folder}/Makefile
perl -i -pe"s#!NCL_INC#${ncl_folder}/include#g" ${build_folder}/Makefile
perl -i -pe"s#!NCL_LIB#${ncl_folder}/lib/ncl#g" ${build_folder}/Makefile
perl -i -pe"s#!EIGEN_INC#${eigen_folder}#g" ${build_folder}/Makefile
perl -i -pe"s#!BOOST_INC#${boost_folder}#g" ${build_folder}/Makefile
perl -i -pe"s#!BUILD_FOLDER#${build_folder}#g" ${build_folder}/Makefile
perl -i -pe"s#!N_PROC#${n_proc}#g" ${build_folder}/Makefile
if [ "${scons_local}" == "1" ]; then
  perl -i -pe"s#!SCONS#${python_bin} ${scons_bin}#g" ${build_folder}/Makefile
else
  perl -i -pe"s#!SCONS#${scons_bin}#g" ${build_folder}/Makefile
fi
# Make
cd ${build_folder}
make >> ${logfile} 2>&1
if [ -f "${build_folder}/libTensorPhylo.a" ] && [ -f "${build_folder}/TensorPhyloTest" ]; then
  if [ "${machine}" == "Windows-cygwin" ] && [ -f "${build_folder}/cygTensorPhylo.dll" ]; then
    mv ${build_folder}/cygTensorPhylo.dll ${lib_folder}/libTensorPhylo.dll
    mv ${build_folder}/libTensorPhylo.a ${lib_folder}/.
  else
    mv ${build_folder}/libTensorPhylo.* ${lib_folder}/.
  fi
  echo "> TensorPhylo library sucessfully installed in: ${lib_folder}"
  echo "> TensorPhylo library sucessfully installed in: ${lib_folder}" >> ${logfile} 2>&1
else
  echo "> [WARNING] TensorPhylo was not sucessfully installed. [WARNING] <"
  echo "> TensorPhylo was not sucessfully installed." >> ${logfile} 2>&1
  exit
fi
echo ".. testing TensorPhylo .."
cp -f TensorPhyloTest ${tensorphylo_folder}/TensorPhyloTest
cd ${tensorphylo_folder}
./TensorPhyloTest [Validation]
echo "> done"
echo "############################"
echo ""

echo "############################"
echo "Cleanup.."
rm TensorPhyloTest
cd ${root_folder}
rm -r build
rm checkCPPVersion
echo "> done"
echo "############################"
echo ""
