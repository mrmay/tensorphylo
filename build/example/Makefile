##########################################################################
## Makefile.
##
## The present Makefile is a pure configuration file, in which
## you can select compilation options. Compilation dependencies
## are managed automatically through the Python library SConstruct.
##########################################################################

# USE: multiple arguments are separated by spaces.
#   For example: projectFiles = file1.cpp file2.cpp
#                optimFlags   = -O -finline-functions

# Leading directory of the TensorPhylo source code
TensorPhylo_Root  = /Users/xaviermeyer/Projets/TensorPhylo
# Name of source files in current directory to compile and link with TensorPhylo
targetNames = TensorPhylo TensorPhyloTest
projectFiles = $(TensorPhylo_Root)/src/main.cpp $(TensorPhylo_Root)/src/test.cpp
# Name of the folder where TensorPhylo is compiled
buildDir = $(TensorPhylo_Root)/build
# Where the library will be installed (if empty they wont)
installLibDir = $(buildDir)/Lib
# Whether a shared library is generated or not
dynamicLibrary = True

# Path to external source files (other than TensorPhylo)
srcPaths =
# Path to external libraries (other than TensorPhylo)
libraryPaths = /usr/local/lib /usr/local/lib/ncl
# Path to inlude directories (other than TensorPhylo)
includePaths = /Users/xaviermeyer/Apps/eigen /usr/local/include/boost
# Dynamic and static libraries (other than TensorPhylo)
libraries    = ncl

# This is the recommended configuration for TensorPhylo.
# Set optimization flags on/off
optimize     = true
# Set debug mode and debug flags on/off
debug        = true
# Set profiling flags on/off
profile      = false

# use omp - install openmp with: brew install libomp
useOMP			 = false


# Compiler to use
serialCXX  = g++
# General compiler flags (e.g. -Wall to turn on all warnings on g++)
compileFlags = -Wall -Wnon-virtual-dtor -Wno-deprecated-declarations -std=c++11 -DEIGEN_DONT_PARALLELIZE
# General linker flags (don't put library includes into this flag) #
linkFlags    =
# Compiler flags to use when optimization mode is on (optional: add -DNDEBUG)
optimFlags   = -O3 -march=native
# Compiler flags to use when debug mode is on
debugFlags   = -g
# Compiler flags to use when profile mode is on
profileFlags = -pg
# Number of processor employed for compilation
nbProc		= 1


##########################################################################
# All code below this line is just about forwarding the options
# to SConstruct. It is recommended not to modify anything there.
##########################################################################

SCons     = scons -j $(nbProc) -f SConstruct

SConsArgs = TensorPhylo_Root=$(TensorPhylo_Root) \
            buildDir=$(buildDir) \
            installLibDir=$(installLibDir) \
            dynamicLibrary=$(dynamicLibrary) \
            targetNames="$(targetNames)" \
            projectFiles="$(projectFiles)" \
            optimize=$(optimize) \
            debug=$(debug) \
            profile=$(profile) \
            useOMP=$(useOMP) \
            serialCXX=$(serialCXX) \
            compileFlags="$(compileFlags)" \
            linkFlags="$(linkFlags)" \
            optimFlags="$(optimFlags)" \
            debugFlags="$(debugFlags)" \
            profileFlags="$(profileFlags)" \
            srcPaths="$(srcPaths)" \
            libraryPaths="$(libraryPaths)" \
            includePaths="$(includePaths)" \
            libraries="$(libraries)"

all:
	$(SCons) $(SConsArgs)

test:
	$(SCons) $(SConsArgs)
	./TensorPhyloTest --success

compile:
	$(SCons) $(SConsArgs)

clean:
	$(SCons) -c $(SConsArgs)
