#!/bin/bash
calc(){ awk "BEGIN { print $* }"; }

# Read the setting file given as argument
SETTING_FILE=${1}
TENSOR_PHYLO_BIN=${2}
SIM_FOLDER=${3}
N_PROC=${4}
RB_EXEC=${5}
TP_LIB_PATH=${6}
WITH_TIP_FIX=${7}

source ../utils/readSettings.sh

# folders required
MY_NAME=TensorPhylo_${WITH_TIP_FIX}F
TEMP_FOLDER=./tmp/${runName}
mkdir -p ${TEMP_FOLDER}
TP_LOCK_FILE=${TEMP_FOLDER}/LOCK_${MY_NAME}_${runName}_${N_PROC}T.txt

PARAM_FOLDER=./data/params${MY_NAME}/${runName}
BASE_RB_CONVERTER_SCRIPT=scripts/basePrepareParamsTP.Rev
mkdir -p ${PARAM_FOLDER}

RESULT_FOLDER=./results/${MY_NAME}/${runName}
mkdir -p ${RESULT_FOLDER}

OUTPUT_FOLDER=./results/${runName}
mkdir -p ${OUTPUT_FOLDER}
outputFile=${OUTPUT_FOLDER}/time${MY_NAME}_${N_PROC}Proc.txt

N_TEST_TIMEOUT=5
TIME_FOR_TEST=$(( 15 + ${N_TEST_TIMEOUT} * ${timeout} ))

# loop through settings
echo "nTips nStates lambdaSim muSim sigmaSim lambdaLL muLL sigmaLL nIter nStepsInt meanTime loglik error" > ${outputFile}

for t in "${nTips[@]}"
do
  for s in "${nStates[@]}"
  do
    for pL in `seq 0 $((${#lambda[@]}-1))`
    do
      for qL in `seq 0 $((${#lambda[@]}-1))`
      do
        for pS in `seq 0 $((${#sigma[@]}-1))`
        do
          for qS in `seq 0 $((${#sigma[@]}-1))`
          do

            if [ -f ${TP_LOCK_FILE} ]
            then
              rm ${TP_LOCK_FILE}
            fi

            name="${runName}_${t}T_${s}S_${pL}LSIM_${qL}LINF_${pS}SSIM_${qS}SINF_${nIter}M"
            echo $name

            treeFile="${SIM_FOLDER}/SIM_${t}T_${pL}LSIM_${pS}SSIM/tree.nex"
            dataFile="${SIM_FOLDER}/SIM_${t}T_${pL}LSIM_${pS}SSIM/data.tsv"

            # Ugly but safer, the params file are created for each analysis
            settingsFile=${PARAM_FOLDER}/${name}_settings.txt
            tipsProbFile=${PARAM_FOLDER}/${name}_tipsProbs.tsv
            Rscript scripts/setupTP.R ${s} ${t} ${lambda[$qL]} ${mu[$qL]} ${sigma[$qS]} ${sigma[$pS]} ${treeFile} ${dataFile} ${settingsFile} ${tipsProbFile} ${WITH_TIP_FIX}

            # Use RB to dump the parameters.dat file
            RB_SCRIPT=${PARAM_FOLDER}/${name}_conv.Rev
            cp ${BASE_RB_CONVERTER_SCRIPT} ${RB_SCRIPT}


            paramsFile=${PARAM_FOLDER}/${name}_params.dat

            sed -i -e "s|TP_LIB_PATH|${TP_LIB_PATH}|g" ${RB_SCRIPT}
            sed -i -e "s|TREE_FILE|${treeFile}|g" ${RB_SCRIPT}
            sed -i -e "s|SETTING_FILE|${settingsFile}|g" ${RB_SCRIPT}
            sed -i -e "s|OUT_PARAM_FILE|${paramsFile}|g" ${RB_SCRIPT}

            echo ${RB_SCRIPT}
            ${RB_EXEC} ${RB_SCRIPT}

            outFile="${RESULT_FOLDER}/${name}.txt"

            if [ ! -f ${TP_LOCK_FILE} ]
            then

              dummyOut=${TEMP_FOLDER}/dummyTP.txt

              # First we try with 5 liks for timeout
              # If the lock is not there after this test we benchmark
              # The lock stay active until when change of tree
              timeout ${TIME_FOR_TEST}s bash <<EOT
                touch ${TP_LOCK_FILE}
                ${TENSOR_PHYLO_BIN} --param ${paramsFile} --nexus ${treeFile} --tsv ${tipsProbFile} --benchmark ${N_TEST_TIMEOUT} --log ${dummyOut} --legacy 0
                rm ${TP_LOCK_FILE}
EOT

            fi

            # If the lock is not there: benchmark
            if [ ! -f ${TP_LOCK_FILE} ]
            then

              ${TENSOR_PHYLO_BIN} --param ${paramsFile} --nexus ${treeFile} --tsv ${tipsProbFile} --benchmark ${nIter} --log ${outFile} --legacy 0

              # approximator timeInit timeComp  lik int steps
              line=`cat ${outFile}`
              words=($line)

              #echo "calc ${words[3]} / ${nIter}"
              timePerLik=`calc ${words[2]} / ${nIter}`
              lastLogLik=${words[3]}
              nSteps=${words[4]}

              echo "\"${t}\" \"${s}\" \"${lambda[$pL]}\" \"${mu[$pL]}\" \"${sigma[$pS]}\" \"${lambda[$qL]}\" \"${mu[$qL]}\" \"${sigma[$qS]}\" \"${nIter}\" \"${nSteps}\" \"${timePerLik}\" \"${lastLogLik}\" \"\"" >> ${outputFile}

            # otherwise no result
            else

              echo "\"${t}\" \"${s}\" \"${lambda[$pL]}\" \"${mu[$pL]}\" \"${sigma[$pS]}\" \"${lambda[$qL]}\" \"${mu[$qL]}\" \"${sigma[$qS]}\" \"${nIter}\" \"${nSteps}\" \"NA\" \"NA\" \"timeout ${timeout} seconds\"" >> ${outputFile}

            fi
          done
        done
      done
    done
  done
done
