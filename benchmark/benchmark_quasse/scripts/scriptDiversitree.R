#!/usr/bin/env Rscript

suppressPackageStartupMessages(library(ape))
suppressPackageStartupMessages(library(diversitree))
suppressPackageStartupMessages(library(microbenchmark))

source("../../validation/utils.R")
source("../../validation/fbd_equations.R")

args = commandArgs(trailingOnly=TRUE)

s           = as.numeric(args[1])
t           = as.numeric(args[2])
lambdaSim   = as.numeric(args[3])
muSim       = as.numeric(args[4])
sigmaSim    = as.numeric(args[5])
lambdaInf   = as.numeric(args[6])
muInf       = as.numeric(args[7])
sigmaInf    = as.numeric(args[8])
M           = as.numeric(args[9])
treeFile    = as.character(args[10])
dataFile    = as.character(args[11])
outFile     = as.character(args[12])

# read the tree
tree = read.nexus(treeFile)

# read thed ata
data = read.table(dataFile, sep="\t")
traits = data[,2]
names(traits) = data[,1]

## Benchmark diversitree
nSteps=1000
pars = c(lambdaInf, muInf, 0, sigmaInf*sigmaInf)
control = list(dt.max=1/nSteps, method="fftC", nx=s, r=1, xr.mult=5)

sd = 0.1*sigmaSim
rho = 1.0
model = make.quasse(tree, traits, sd, constant.x, constant.x, control, sampling.f=rho)

# compute the dx
fit_env = environment(model)
cache   = get("cache", envir=fit_env)
dx      = cache$control$dx

# Default value if the likelihood evaluation fails
meanTime = NA
cLL = NA
error = ""

ll = try(model(pars, condition.surv=FALSE, root=ROOT.FLAT, intermediate=TRUE))
if ( class(ll) == "try-error" || is.na(ll) == TRUE ) {
  error = "Invalid lik"
} else {
  cLL = log(mean(attributes(ll)$intermediates$vals[-1 * 1 : s])) + sum(attributes(ll)$intermediates$lq) + factor(tree)

  benchDiver = microbenchmark(model(pars, condition.surv=FALSE, root=ROOT.FLAT, intermediate=TRUE), times = M, unit = "s")
  summaryD = summary(benchDiver)

  meanTime = summaryD$mean
}


resD = c(t, s, lambdaSim, muSim, sigmaSim, lambdaInf, muInf, sigmaInf, M, nSteps, meanTime, cLL, error)
write.table(t(resD), outFile, row.names = FALSE, col.names = FALSE, append = TRUE)
