#!/usr/bin/python
import os
import sys
import dendropy
import pandas as pd
import numpy as np
from scipy.optimize import nnls
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from palettable.colorbrewer.diverging import RdBu_8
from palettable.colorbrewer.qualitative import Set1_8, Set2_8

DPI = 100
LINE_WIDTH=2.5

font = {'family' : 'DejaVu Sans',
    'weight' : 'normal',
    'size'   : 8}

plt.rc('font', **font)
plt.rcParams['lines.linewidth'] = LINE_WIDTH


def readSettings(filename):
    inFile = file(filename, 'r')

    runName = ''
    nIter = timeout = 0
    nTips = nStates = vecLambda = vecMu = []

    settings = {}

    line = inFile.readline().strip()
    while line:
        nextLine = inFile.readline().strip()
        if line == "runName":
            settings[line] = nextLine
        elif line == "nIter":
            settings[line] = int(nextLine)
        elif line == "nTips":
            words = nextLine.split(' ')
            settings[line] = [int(w) for w in words]
        elif line == "nStates":
            words = nextLine.split(' ')
            settings[line] = [int(w) for w in words]
        elif line == "lambda":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "mu":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "timeout":
            settings[line] = int(nextLine)

        #Read next line:
        line = inFile.readline().strip()

    return settings


def readBenchmark(impl, filename):
    dFrame = pd.read_csv(filename, sep=' ')
    dFrame = dFrame.drop(['error'], axis=1)
    timeImpl = 'time{}'.format(impl)
    llImpl = 'll{}'.format(impl)
    dFrame = dFrame.rename(columns={'meanTime' : timeImpl, 'loglik' : llImpl})

    return dFrame

def mergeDFrame(impls, dFrames):

    if len(impls) == 1:
        return dFrames[impls[0]]
    else:
        df = pd.merge(dFrames[impls[0]], dFrames[impls[1]], on=['nTips', 'nStates', 'lambdaSim', 'muSim', 'sigmaSim', 'lambdaLL', 'muLL', 'sigmaLL'], how = 'inner')

        for i in range(2, len(impls)):
            df = pd.merge(df, dFrames[impls[i]], on=['nTips', 'nStates', 'lambdaSim', 'muSim', 'sigmaSim', 'lambdaLL', 'muLL', 'sigmaLL'], how = 'inner')

    return df


def modelFitQuadratic(implementation, nTips, nStates, df, showFigures):

    group = df.groupby(['nTips', 'nStates'] , as_index = False).mean()

    K = []
    N = []
    T = []

    timeImpl = 'time{}'.format(implementation)
    pivotedDF = group.pivot(index='nTips', columns='nStates', values=timeImpl)
    val = pivotedDF.to_numpy().transpose()

    #val = np.ndarray((len(nStates), len(nTips)))

    rangeK = range(np.min([val.shape[0], len(nStates)]))
    rangeN = range(np.min([val.shape[1], len(nTips)]))
    for iS, s in zip(rangeK, nStates):
        for iT, t in zip(rangeN, nTips) :
            K.append(s)
            N.append(t)
            T.append(val[iS, iT])

    K = np.array(K)
    N = np.array(N)
    T = np.array(T)

    T_model = None
    # General
    coeffLabel = ['C', 'tips', 'states', 'tips*states', 'tips^2*states', 'tips*states^2']
    A = np.array([K*0+1, N, K, N*K, (K)*(N**2), (K**2)*(N)]).T
    #A = np.array([K*0+1, N, K, N*K, (K)*(N**2), (K**2)*(N), (K**2)*(N**2),  (K**2)*(N**1.5)]).T
    B = (T.flatten())
    coeff, r = nnls(A, B)
    #coeff, r, rank, s = np.linalg.lstsq(A, B, rcond=None)
    #r = r[0]
    print '-----------------------------------------------------------------------------------------'
    print implementation
    for cLabel, c in zip(coeffLabel, coeff):
        print 'Coeff[ {} ] = {} seconds => {} cycles'.format(cLabel, c, 3.8*1e9*c)
    print 'Sum of residuals = ' + '{0:.2E}'.format(r)
    T_model = (np.dot(A, coeff))

    ############################ PLOT ##############################
    if showFigures:
        fig = plt.figure()
        ax = fig.gca(projection='3d')

        #fig.colorbar(p)
        XG, YG = np.meshgrid(nStates, nTips)
        ZG = val[:len(nStates), :len(nTips)].transpose()

        ax.plot_wireframe(XG, YG, ZG, alpha=1, linewidth=2, colors='k')
        s = ax.plot_surface(XG, YG, ZG, alpha=0.5, linewidth=0, cmap=cm.viridis, antialiased=False, shade=False)

        ax.scatter(K, N, T_model, 's', s=250, color='r', alpha=1, marker='x')

        #surf = ax.plot_surface(X, Y, Z, linewidth=0, antialiased=False, cmap=cm.cividis)
        fig.colorbar(s, shrink=0.5, aspect=5)
        ax.set_title('Implementation times: {}'.format(implementation))
        ax.set_xlabel('States number')
        ax.set_ylabel('Species number')
        ax.set_zlabel('Second')
        plt.tight_layout()

        fig = plt.figure()
        ax = fig.gca(projection='3d')
        ax.set_title('Implementation rel error: {}'.format(implementation))
        ax.set_xlabel('States number')
        ax.set_ylabel('Species number')
        ax.scatter(K, N, (T_model-T)/T, s=250, cmap=cm.viridis, alpha=1, marker='x')

    #plt.show()
    return coeff

def checkPrediction(implementations, settings, df, dicCoeffSmall, dicCoeffLarge, threshold):

    cntSuccess = 0
    cntTotal = 0


    group = df.groupby(['nTips', 'nStates'] , as_index = False).mean()

    countSuccessPerImpl = {}
    for idx, row in group.iterrows():
        #print row
        sigma = row.sigmaLL
        K = row.nStates
        N = row.nTips
        A = np.array([K*0+1, N, K, N*K, (K)*(N**2), (K**2)*(N)]).T
        #A = np.array([K*0+1, N, K, N*K, (K)*(N**2), (K**2)*(N), (K**2)*(N**2),  (K**2)*(N**1.5)]).T

        bestImpl = ''
        minTime = 1e10
        bestPredImpl = ''
        minPredictedTime = 1e10
        minRealTime = 0
        for impl in implementations:
            coeff = None
            if N <= threshold[0] and K <= threshold[1]:
                coeff = dicCoeffSmall[impl]
            else:
                coeff = dicCoeffLarge[impl]

            time = row['time{}'.format(impl)]
            if time < minTime:
                minTime = time
                bestImpl = impl

            #if len(implementations) == 2 and not 'BW' in impl: continue
            #if len(implementations) > 2 and not 'BW' in impl and K <= 64: continue
            #if len(implementations) == 2 and K >= 128 and N <= 64 and 'BW' in impl: continue
            #if len(implementations) == 2 and 'BW' in impl and K >= 128: continue
            #if len(implementations) == 2 and 'RB_TP' == impl: continue

            #if N <= 128 and sigma >= 1.6 and 'BW' in impl: continue
            #if N <= 128 and sigma <= 0.6 and not 'BW' in impl: continue

            if len(implementations) > 2 and N <= 64 and K <= 64 and ("2T" in impl or not "BW" in impl): continue
            if len(implementations) > 2 and N > 64 and K > 64 and not "2T" in impl: continue


            predTime = A.dot(coeff)
            if predTime < minPredictedTime:
                minPredictedTime = predTime
                bestPredImpl = impl
                minRealTime = row['time{}'.format(impl)]



        if  bestImpl in countSuccessPerImpl:
            countSuccessPerImpl[bestImpl] += 1
        else:
            countSuccessPerImpl[bestImpl] = 1

        if bestImpl == bestPredImpl or np.abs(1-minTime/minRealTime) < 0.1:
            cntSuccess += 1
            #if not "BW" in bestImpl:
            #    print '[CORRECT]{} tips x {} states: best predicted [{}] - true best [{}] : slowdown = {}'.format(N, K, bestPredImpl, bestImpl, minTime/minRealTime)
        elif len(implementations) >= 2 and minTime/minRealTime < 0.5:
            print row.lambdaLL, "  ", row.sigmaLL
            print '[WRONG]{} tips x {} states: best predicted [{}] - true best [{}] : slowdown = {}'.format(N, K, bestPredImpl, bestImpl, minTime/minRealTime)

        cntTotal += 1

    for impl in implementations:
        pct = 0.
        if impl in countSuccessPerImpl:
            pct = 100.*countSuccessPerImpl[impl]/cntTotal
        print '{} was {}% of the time the best implementation'.format(impl, pct)

    print '{}% correct predictions'.format(100.0*cntSuccess/cntTotal)

def generateCPP(implementations, df, dicCoeffSmall, dicCoeffLarge, threshold):
    myClass = 'AutoTuningApproximator'

    dicImpl = {'RB_TP' : 1, 'RB_TP_BW' : 2, 'RB_TP_2T_opt' : 3 , 'RB_TP_2T_bw' : 4}
    dicThreads = {'RB_TP' : 1 , 'RB_TP_BW' : 1, 'RB_TP_2T_opt' : 2, 'RB_TP_2T_bw' : 2}

    header = 'static const size_t thresholdNTips_quasse, thresholdNStates_quasse;\n'
    body = 'const size_t {}::thresholdNTips_quasse = {};\n'.format(myClass, threshold[0])
    body += 'const size_t {}::thresholdNStates_quasse = {};\n\n'.format(myClass, threshold[1])
    body += '//coefficients : ([K*0+1, N, K, N*K, (K)*(N**2), (K**2)*(N)])\n'

    headerSmall = 'static const std::vector<double>'
    headerLarge = 'static const std::vector<double>'
    suffixes = []
    for impl in implementations:
        suffix = '{}A_{}T'.format(dicImpl[impl], dicThreads[impl])
        suffixes.append(suffix)

        coeffStr = ''.join(['({})'.format(x) for x in dicCoeffSmall[impl]])
        headerSmall += ' coeffsSmall{}_quasse,'.format(suffix)
        body += 'const std::vector<double> {}::coeffsSmall{}_quasse = boost::assign::list_of{};\n'.format(myClass, suffix, coeffStr)

        coeffStr = ''.join(['({})'.format(x) for x in dicCoeffLarge[impl]])
        headerLarge += ' coeffsLarge{}_quasse,'.format(suffix)
        body += 'const std::vector<double> {}::coeffsLarge{}_quasse = boost::assign::list_of{};\n\n'.format(myClass, suffix, coeffStr)

    header += '{};\n{};\n'.format(headerSmall[:-1], headerLarge[:-1])
    header += 'static const std::map< std::pair<size_t, size_t>, std::vector<double> > coeffsSmall_quasse, coeffsLarge_quasse;\n\n'

    body += '// {idApprox, nThreads} => [coeffs]\n'
    body += 'const std::map< std::pair<size_t, size_t>, std::vector<double> > {}::coeffsSmall_quasse = boost::assign::map_list_of'.format(myClass)
    for suffixe in suffixes:
        words = suffixe.split('_')
        body += '(std::make_pair({},{}), {}::coeffsSmall{}_quasse)'.format(words[0][:-1], words[1][:-1], myClass, suffixe)
    body += ';\n\n'

    body += 'const std::map< std::pair<size_t, size_t>, std::vector<double> > {}::coeffsLarge_quasse = boost::assign::map_list_of'.format(myClass)
    for suffixe in suffixes:
        words = suffixe.split('_')
        body += '(std::make_pair({},{}), {}::coeffsLarge{}_quasse)'.format(words[0][:-1], words[1][:-1], myClass, suffixe)
    body += ';\n\n'

    return [header, body]

settingFile = sys.argv[1]
settings = readSettings(settingFile)

showFigures = (sys.argv[2].lower() != 'false')

implementations = ['RB_TP', 'RB_TP_BW', 'RB_TP_2T_opt', 'RB_TP_2T_bw']
#implementations = ['Diversitree', 'TensorPhylo', 'RevBayes', 'RevBayesWithTP', 'Castor']

dFrames = {}
for impl in implementations:
    resultFile = './results/' + settings["runName"] + '/time' + impl + '.txt'
    dFrames[impl] = readBenchmark(impl, resultFile)

df = mergeDFrame(implementations, dFrames)

threshold = [128, 64]
# Small
dicCoeffSmall = {}
nTipsSmall = [t for t in settings['nTips'] if t <= threshold[0]]
nStatesSmall = [s for s in settings['nStates'] if s <= threshold[1]]
for impl in implementations:
    dicCoeffSmall[impl] = modelFitQuadratic(impl, nTipsSmall, nStatesSmall, df, showFigures)

threshold = [512, 256]
# large
dicCoeffLarge = {}
nTipsLarge = [t for t in settings['nTips'] if t <= threshold[0]]
nStatesLarge = [s for s in settings['nStates'] if s <= threshold[1]]
for impl in implementations:
    dicCoeffLarge[impl] = modelFitQuadratic(impl, nTipsLarge, nStatesLarge, df, showFigures)

if showFigures:
    plt.show()

print 'Sequential predictions:'
implementationsSeq = ['RB_TP', 'RB_TP_BW']
checkPrediction(implementationsSeq, settings, df, dicCoeffSmall, dicCoeffLarge, threshold)

print 'Parallel predictions:'
implementationsAll = ['RB_TP', 'RB_TP_BW', 'RB_TP_2T_opt', 'RB_TP_2T_bw']
checkPrediction(implementationsAll, settings, df, dicCoeffSmall, dicCoeffLarge, threshold)


thresholdSmall = [128, 64]
implementationsAll = ['RB_TP', 'RB_TP_BW', 'RB_TP_2T_opt', 'RB_TP_2T_bw']
cppCode = generateCPP(implementationsAll, df, dicCoeffSmall, dicCoeffLarge, thresholdSmall)

print 'Writing CPP header stub in stub.h'
oFile = file('stub.h', 'w')
oFile.write(cppCode[0])
oFile.close()

print 'Writing CPP header stub in stub.cpp'
oFile = file('stub.cpp', 'w')
oFile.write(cppCode[1])
oFile.close()

#plt.show()
