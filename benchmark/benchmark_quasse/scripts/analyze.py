#!/usr/bin/python
import os
import sys
import dendropy
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from palettable.colorbrewer.diverging import RdBu_8
from palettable.colorbrewer.qualitative import Set2_8, Dark2_8, Set3_12


myPalette = Dark2_8

REF_IMPL_LL = 'Analytical'

DPI = 100
LINE_WIDTH=2.5

font = {'family' : 'DejaVu Sans',
    'weight' : 'normal',
    'size'   : 8}

plt.rc('font', **font)
#plt.rc('text', usetex=True)
plt.rcParams['svg.fonttype'] = 'none'
plt.rcParams['lines.linewidth'] = LINE_WIDTH


def readSettings(filename):
    inFile = file(filename, 'r')

    runName = ''
    nIter = timeout = 0
    nTips = nStates = vecLambda = vecMu = []

    settings = {}

    line = inFile.readline().strip()
    while line:
        nextLine = inFile.readline().strip()
        if line == "runName":
            settings[line] = nextLine
        elif line == "nIter":
            settings[line] = int(nextLine)
        elif line == "nTips":
            words = nextLine.split(' ')
            settings[line] = [int(w) for w in words]
        elif line == "nStates":
            words = nextLine.split(' ')
            settings[line] = [int(w) for w in words]
        elif line == "lambda":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "mu":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "sigma":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "timeout":
            settings[line] = int(nextLine)

        #Read next line:
        line = inFile.readline().strip()

    return settings


def readBenchmark(impl, filename):
    dFrame = pd.read_csv(filename, sep=' ')
    dFrame = dFrame.drop(['error'], axis=1)
    dFrame = dFrame.drop(['nIter'], axis=1)
    nStepsImpl = 'nSteps{}'.format(impl)
    timeImpl = 'time{}'.format(impl)
    llImpl = 'll{}'.format(impl)
    dFrame = dFrame.rename(columns={'meanTime' : timeImpl, 'loglik' : llImpl, 'nStepsInt' : nStepsImpl})

    return dFrame

def mergeDFrame(impls, dFrames):

    if len(impls) == 1:
        return dFrames[impls[0]]
    else:
        df = pd.merge(dFrames[impls[0]], dFrames[impls[1]], on=['nTips', 'nStates', 'lambdaSim', 'muSim', 'sigmaSim', 'lambdaLL', 'muLL', 'sigmaLL'], how = 'outer')

        for i in range(2, len(impls)):
            df = pd.merge(df, dFrames[impls[i]], on=['nTips', 'nStates', 'lambdaSim', 'muSim', 'sigmaSim', 'lambdaLL', 'muLL', 'sigmaLL'], how = 'outer')

    return df

def plotConvergenceQuasse(settings, impls, df, figureFolder):

    nPlots = len(impls) - 1

    ''''
    X_WIDTH = 400*nPlots
    Y_WIDTH = 400
    fig, ax = plt.subplots(1, nPlots, figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=True, sharex=False, sharey=True)
    '''

    for t in settings['nTips']:
        #if t >= 32: continue

        X_WIDTH = 300*len(settings['sigma'])
        Y_WIDTH = 300*len(settings['lambda'])
        fig, ax = plt.subplots(len(settings['lambda']), len(settings['sigma']), figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=True, sharex=True, sharey=False)

        for iP in range(len(settings['lambda'])):
            for iS in range(len(settings['sigma'])):

                data = {}
                for impl in impls:
                    data[impl] = []

                #for s in settings['nStates']:
                res = df.loc[(df['nTips'] == t) & (df['lambdaSim'] == settings['lambda'][iP]) & (df['sigmaSim'] == settings['sigma'][iS]) & (df['lambdaLL'] == settings['lambda'][iP]) & (df['sigmaLL'] == settings['sigma'][iS]) ]
                #print t, " ", settings['lambda'][iP], " ", settings['sigma'][iS]
                #print "RES: ", res

                for impl in impls:
                    llImpl = 'll{}'.format(impl)
                    data[impl].append(res['nStates'])
                    data[impl].append(res[llImpl])

                styles = ['-', '--x', 'o', 's', 'd']
                for iI, impl in zip(range(len(impls)), impls):

                    ax[iP, iS].plot(np.log2(data[impl][0]), data[impl][1], styles[iI], color=myPalette.hex_colors[iI])


                refLik = data[REF_IMPL_LL][1].to_numpy()[0]
                #print refLik

                ax[iP, iS].set_ylim((refLik*1.5, refLik*0.75))


                if iP == len(settings['lambda'])-1:
                    ax[iP, iS].set_xlabel('Number of states (log2)')
                if iS == 0:
                    ax[iP, iS].set_ylabel('Log likelihood')

        plt.suptitle('Trees with {} taxa'.format(t))
        plt.legend(impls)
        plt.savefig('{}/conv_{}_{}T.png'.format(figureFolder, settings['runName'], t ))
        plt.savefig('{}/conv_{}_{}T.svg'.format(figureFolder, settings['runName'], t ))

def plotErrorQuasseSingle(settings, impl, df, vecSigma, figureFolder):

    llImpl = 'll{}'.format(impl)
    llRef = 'll{}'.format(REF_IMPL_LL)

    for t in settings['nTips']:
        #if t >= 64: continue

        X_WIDTH = 250*len(vecSigma)
        Y_WIDTH = 250*len(settings['lambda'])
        fig, ax = plt.subplots(len(settings['lambda']), len(vecSigma), figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=False, sharex=True, sharey=True)

        for iP in range(len(settings['lambda'])):
            for iS in range(len(vecSigma)):

                res = df.loc[(df['nTips'] == t) & (df['lambdaSim'] == settings['lambda'][iP]) & (df['sigmaSim'] == vecSigma[iS])]

                #print res

                for iS2 in range(len(vecSigma)):
                    resTmp = res.loc[(res['sigmaLL'] == vecSigma[iS2])]
                    relativeErr = -(resTmp[llImpl]-resTmp[llRef])/resTmp[llRef]

                    myCol = myPalette.hex_colors[iS2]
                    ax[iP, iS].scatter(np.log2(resTmp['nStates']), relativeErr, color=myCol, alpha=0.75)

                #refLik = data[REF_IMPL_LL][1].to_numpy()[0]
                #print refLik

                ax[iP, iS].axhline([0.], linestyle='-.', color='k', alpha=0.5)
                ax[iP, iS].set_ylim([-1., 1])
                #ax[iS].axhline([0.], linestyle='-.', color='k', alpha=0.5)


                if iP == len(settings['lambda'])-1:
                    #ax[iS].set_xlabel('Number of states (log2)\n Sigma={}'.format(vecSigma[iS]))
                    ax[iP, iS].set_xlabel('Number of states (log2)\n SigmaSim={}'.format(vecSigma[iS]))
                if iS == 0:
                    #ax[iS].set_ylabel('Rel. LL error\nlamba={}, mu={}'.format(settings['lambda'][iP], settings['mu'][iP]))
                    ax[iP, iS].set_ylabel('Rel. LL error\nlamba={}, mu={}'.format(settings['lambda'][iP], settings['mu'][iP]))

        #plt.suptitle('Trees with {} taxa.'.format(t))
        plt.legend(labels=['ref'] + ['sigmaLL=' + repr(s2) for s2 in vecSigma])
        plt.tight_layout()
        plt.savefig('{}/Error_{}_{}_{}T.png'.format(figureFolder, settings['runName'], impl, t ))
        plt.savefig('{}/Error_{}_{}_{}T.svg'.format(figureFolder, settings['runName'], impl, t ))
        plt.close(fig)

def plotErrorQuasse(settings, impls, df, vecSigma, figureFolder):

    whichOnes = ['simulation', 'all', 'extreme']

    for which in whichOnes:

        nPlots = len(impls) - 1

        for t in settings['nTips']:
            #if t >= 64: continue

            X_WIDTH = 250*len(vecSigma)
            Y_WIDTH = 250*len(settings['lambda'])
            fig, ax = plt.subplots(len(settings['lambda']), len(vecSigma), figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=False, sharex=True, sharey=True)

            for iP in range(len(settings['lambda'])):
                for iS in range(len(vecSigma)):

                    data = {}
                    for impl in impls:
                        data[impl] = []

                    #for s in settings['nStates']:
                    res = None
                    if which == 'simulation':
                        res = df.loc[(df['nTips'] == t) & (df['lambdaSim'] == settings['lambda'][iP]) & (df['sigmaSim'] == vecSigma[iS]) & (df['lambdaLL'] == settings['lambda'][iP]) & (df['sigmaLL'] == vecSigma[iS]) ]
                    elif which == 'all':
                        res = df.loc[(df['nTips'] == t) & (df['lambdaSim'] == settings['lambda'][iP]) & (df['sigmaSim'] == vecSigma[iS])]
                    elif which == 'extreme':
                        res = df.loc[(df['nTips'] == t) & (df['lambdaSim'] == settings['lambda'][iP]) & (df['sigmaSim'] == vecSigma[iS]) & ((df['sigmaLL'] == vecSigma[0]) | (df['sigmaLL'] == vecSigma[-1]) )]
                    #print res

                    for impl in impls:
                        llImpl = 'll{}'.format(impl)

                        x = res['nStates'].to_numpy()
                        y = res[llImpl].to_numpy()
                        iSort = x.argsort()
                        x = x[iSort]
                        y = y[iSort]
                        data[impl].append(x)
                        data[impl].append(y)

                    styles = None
                    if which == "simulation" and 'Diversitree' in impls:
                        styles = ['-', '--x', 'o', 's', 'd', 'v']
                    else:
                        styles = ['-', '^', 'o', 's', 'd', 'v']

                    for iI, impl in zip(range(len(impls)), impls):
                        if impl == REF_IMPL_LL: continue


                        #ax[iS].plot(np.log2(data[impl][0])+offset, (relativeErr), styles[iI], color=myPalette.hex_colors[iI], alpha=0.75)
                        relativeErr = -(data[impl][1]-data[REF_IMPL_LL][1])/data[REF_IMPL_LL][1]

                        offset = 0.7*(iI*1./len(impls)-0.5)
                        myCol = myPalette.hex_colors[iI]
                        ax[iP, iS].plot(np.log2(data[impl][0])+offset, (relativeErr), styles[iI], color=myCol, alpha=0.75)


                    #refLik = data[REF_IMPL_LL][1].to_numpy()[0]
                    #print refLik

                    ax[iP, iS].axhline([0.], linestyle='-.', color='k', alpha=0.5)
                    #ax[iS].axhline([0.], linestyle='-.', color='k', alpha=0.5)



                    if which == "simulation":
                        ax[iP, iS].set_ylim((-.2, .2))
                        #ax[iS].set_ylim((-.2, .2))


                    if iP == len(settings['lambda'])-1:
                        #ax[iS].set_xlabel('Number of states (log2)\n Sigma={}'.format(vecSigma[iS]))
                        ax[iP, iS].set_xlabel('Number of states (log2)\n SigmaSim={}'.format(vecSigma[iS]))
                    if iS == 0:
                        #ax[iS].set_ylabel('Rel. LL error\nlamba={}, mu={}'.format(settings['lambda'][iP], settings['mu'][iP]))
                        ax[iP, iS].set_ylabel('Rel. LL error\nlamba={}, mu={}'.format(settings['lambda'][iP], settings['mu'][iP]))

            suffix = 'all'
            #plt.suptitle('Trees with {} taxa for "{}" parameters.'.format(t, which))
            plt.tight_layout()
            plt.legend(impls[1:])
            plt.savefig('{}/Error_{}_{}_{}T_{}.png'.format(figureFolder, settings['runName'], suffix, t, which ))
            plt.savefig('{}/Error_{}_{}_{}T_{}.svg'.format(figureFolder, settings['runName'], suffix, t, which ))
            plt.close(fig)

def plotAvgErrorQuasse(settings, impls, df, vecSigma, figureFolder):

    for t in settings['nTips']:
        #if t >= 64: continue

        X_WIDTH = 250*len(vecSigma)
        Y_WIDTH = 250*len(settings['lambda'])
        fig, ax = plt.subplots(len(settings['lambda']), len(vecSigma), figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=False, sharex=True, sharey=True)

        for iP in range(len(settings['lambda'])):
            for iS in range(len(vecSigma)):

                data = {}
                for impl in impls:
                    data[impl] = []

                #for s in settings['nStates']:

                res = df.loc[(df['nTips'] == t) & (df['lambdaSim'] == settings['lambda'][iP]) & (df['sigmaSim'] == vecSigma[iS])]

                for s in settings['nStates']:
                    tmp = res.loc[(df['nStates'] == s)]

                    for impl in impls:
                        llImpl = 'll{}'.format(impl)
                        data[impl].append(tmp[llImpl].to_numpy())


                styles = None
                styles = ['-', '^', 'o', 's', 'd', 'v']

                #for iD in range(len(settings['nStates'])):


                for iI, impl in zip(range(len(impls)), impls):
                    if impl == REF_IMPL_LL: continue

                    avgRelrr = []
                    stdRelErr = []
                    for iD in range(len(settings['nStates'])):
                        vals = np.divide(-(data[impl][iD]-data[REF_IMPL_LL][iD]), data[REF_IMPL_LL][iD] )
                        avgRelrr.append( np.average( vals ) )
                        stdRelErr.append( np.std( vals ) )

                        cntNotConv = np.count_nonzero(np.isnan(data[impl][iD]))
                        if cntNotConv and cntNotConv < len(data[impl][iD]):
                            print "{} did not converge {} times for iLamb={}, iSig={}, k={}, n={}".format(impl, cntNotConv, iP, iS, iD, t)


                    offset = 0.7*(iI*1./len(impls)-0.5)
                    myCol = myPalette.hex_colors[iI]
                    ax[iP, iS].errorbar(np.log2([ int(x) for x in settings['nStates']])+offset, avgRelrr, yerr=stdRelErr, marker=styles[iI], color=myCol, alpha=0.75, ls=":", lw=2.)



                #refLik = data[REF_IMPL_LL][1].to_numpy()[0]
                #print refLik

                ax[iP, iS].axhline([0.], linestyle='-.', color='k', alpha=0.5)
                #ax[iS].axhline([0.], linestyle='-.', color='k', alpha=0.5)


                if iP == len(settings['lambda'])-1:
                    #ax[iS].set_xlabel('Number of states (log2)\n Sigma={}'.format(vecSigma[iS]))
                    ax[iP, iS].set_xlabel('Number of states (log2)\n SigmaSim={}'.format(vecSigma[iS]))
                if iS == 0:
                    #ax[iS].set_ylabel('Rel. LL error\nlamba={}, mu={}'.format(settings['lambda'][iP], settings['mu'][iP]))
                    ax[iP, iS].set_ylabel('Rel. LL error\nlamba={}, mu={}'.format(settings['lambda'][iP], settings['mu'][iP]))

        ax[0,0].set_ylim([-2, 0.5])

        suffix = 'all'
        #plt.suptitle('Trees with {} taxa for "{}" parameters.'.format(t, which))
        plt.tight_layout()
        plt.legend(['ref']+impls[1:])
        #plt.show()
        plt.savefig('{}/AvgError_{}_{}_{}T.png'.format(figureFolder, settings['runName'], suffix, t ))
        plt.savefig('{}/AvgError_{}_{}_{}T.svg'.format(figureFolder, settings['runName'], suffix, t ))
        plt.close(fig)

def plotAvgSpeedupsSummary(settings, impls, df, dataLabel, figureFolder):

    nPlots = len(impls)

    X_WIDTH = 350*nPlots
    Y_WIDTH = 300*nPlots
    fig, ax = plt.subplots(nPlots, nPlots, figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=False, sharex=False , sharey='row')

    #group.add_suffix('_avg').reset_index()
    #print group.mean()

    #dfMod = df.apply(pd.to_numeric, errors='coerce')
    dfMod = df

    for i, implA in zip(range(len(impls)), impls):
        localRef = implA

        for j, implB in zip(range(len(impls)), impls):

            if j == 0 :  ax[i, j].set_ylabel('Speedup (log)')
            if i == len(impls)-1: ax[i, j].set_xlabel('Number of states (log2)')

            if i == j: continue

            speedup = ''
            if dataLabel != 'both':
                timeA = dfMod['{}{}'.format(dataLabel, implA)]
                timeB = dfMod['{}{}'.format(dataLabel, implB)]
                speedup = 'sp{}to{}'.format(implB, implA)
                dfMod[speedup] = np.log10(timeB / timeA)
            else:
                timeA = dfMod['time{}'.format(implA)]/dfMod['nSteps{}'.format(implA)]
                timeB = dfMod['time{}'.format(implB)]/dfMod['nSteps{}'.format(implB)]
                speedup = 'sp{}to{}'.format(implB, implA)
                dfMod[speedup] = np.log10(timeB / timeA)

            dfLocal = dfMod[['nTips', 'nStates', speedup]]
            dfLocal = dfLocal.dropna(subset=[speedup])
            group = dfLocal.groupby(['nTips', 'nStates'] , as_index = False).mean()

            pivotedDF = group.pivot(index='nTips', columns='nStates', values=speedup)
            #print pivotedDF
            rangeNTips = pivotedDF.index #nTips
            rangeNStates = pivotedDF.columns #nStates
            val = pivotedDF.to_numpy().transpose()
            #print val.shape
            #print "--------------------------------------------"

            #rangeY = flatY[:val.shape[0]]
            ax[i, j].plot(np.log2(rangeNStates), [0. for y in rangeNStates], '--', color='grey')

            for iX, t in zip(range(len(rangeNTips)), rangeNTips):
                #if iX >= val.shape[1]: continue
                ax[i, j].plot(np.log2(rangeNStates), val[:,iX], '-x', color=myPalette.hex_colors[iX])

            #ax[i, j].set_yscale('log')
            ax[i, j].title.set_text('Sp. {} over {}'.format(implA, implB))
            #ax[i, j].get_yaxis().set_major_formatter(mpl.ticker.ScalarFormatter())

    for i, implA in zip(range(len(impls)), impls):
        localRef = implA
        labels = []
        speedups = []
        colors = []

        for j, implB in zip(range(len(impls)), impls):
            if i==j: continue

            labels.append(implB)
            speedup = 'sp{}to{}'.format(implB, implA)
            speedups.append(dfMod[speedup].to_numpy())
            speedups[-1] = speedups[-1][np.logical_not(np.isnan(speedups[-1]))]
            colors.append(myPalette.hex_colors[j])

        bplot = ax[i,i].boxplot(speedups, patch_artist=True, labels=labels)
        ax[i, i].title.set_text(implA)
        ax[i, i].title.set_color(myPalette.hex_colors[i])
        ax[i, i].axhline(y=0., linestyle='--', color='grey')
        #ax[i, i].set_yscale('log')
        ax[i, i].set_xticklabels(ax[i, i].get_xticklabels(),rotation=20)

        for patch, color in zip(bplot['boxes'], colors):
            patch.set_facecolor(color)



    #handles, labels = ax[0,1].get_legend_handles_labels()
    labels = ['1x'] + ['{} taxa'.format(l) for l in settings["nTips"]]
    ax[0, 1].legend(labels=labels)
    ax[1, 0].legend(labels=labels)

    plt.tight_layout()

    if dataLabel == 'time':
        plt.suptitle('Speedup f(time)')
    elif dataLabel == 'nSteps':
        plt.suptitle('Speedup f(integration steps)')
    else:
        plt.suptitle('Speedup f(time/steps)')

    plt.tight_layout(rect=[0, 0.03, 1, 0.95])

    plt.savefig('{}/Speedup.png'.format(figureFolder))
    plt.savefig('{}/Speedup.svg'.format(figureFolder))
    plt.close(fig)


def plotSpeedupVsAccuracy(settings, impls, df, thresholdE, figureFolder):

    if len(impls) > 2:
        print '"plotSpeedupVsAccuracy" only works for 2 impls'
        return

    font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 10}

    plt.rc('font', **font)

    medianprops = dict(linestyle='-', linewidth=3, color='k')

    # Compute relative errors
    localDF = df.copy()
    for impl in impls:
        llImpl = 'll{}'.format(impl)
        llImplRef = 'll{}'.format(REF_IMPL_LL)
        relativeErr = np.abs((localDF[llImpl]-localDF[llImplRef])/localDF[llImplRef])
        relEImpl = 'err{}'.format(impl)
        localDF[relEImpl] = relativeErr

    heights = []
    for t in settings['nTips']:
        heights.append(3)
        heights.append(1.5)

    X_WIDTH = 600*(1+len(impls))
    Y_WIDTH = 200*2*len(settings['nTips'])
    fig, ax = plt.subplots(2*len(settings['nTips']), (1+len(impls)), figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=True, sharex=True, sharey=False, gridspec_kw={'height_ratios': heights})

    for iT, t in zip(range(len(settings['nTips'])), settings['nTips']):
        #if t > 128: continue
        convTotal = {}
        toMerge = {}
        for iI, impl in zip(range(len(impls)), impls):
            data = []
            convPct = []
            convTotal[impl] = 0
            toMerge[impl] = []
            timeImpl = 'time{}'.format(impl)
            relEImpl = 'err{}'.format(impl)
            for iS in range(len(settings['sigma'])):
                res = localDF.loc[(localDF['nTips'] == t) & (localDF['sigmaSim'] == settings['sigma'][iS]) & (localDF[relEImpl] < thresholdE) ]
                #res = localDF.loc[(localDF['nTips'] == t) & (localDF['sigmaSim'] == settings['sigma'][iS]) & (localDF[relEImpl] < thresholdE) & (localDF['nStates'] >= 128) ]
                res = res.loc[res.groupby(['sigmaLL', 'lambdaSim', 'lambdaLL', 'muSim', 'muLL'], as_index=False)['nStates'].idxmin()]
                data.append(res['nStates'].to_numpy())
                toMerge[impl].append(res[['sigmaSim', 'sigmaLL', 'lambdaSim', 'lambdaLL', 'muSim', 'muLL', timeImpl]])

                nCases = (len(settings['lambda'])*len(settings['lambda'])*len(settings['sigma']))
                convTotal[impl] += len(data[-1])
                convPct.append(100.*len(data[-1])/nCases)


            ticksPos = range(1,1+len(convPct))
            data = [np.log2(d) for d in data]

            bPlots = ax[2*iT, iI].boxplot(data, medianprops=medianprops, patch_artist=True)
            for patch in bPlots['boxes']:
                patch.set_facecolor(Set2_8.hex_colors[iI])

            ax[2*iT, iI].set_yticks(np.log2(settings['nStates']))
            ax[2*iT, iI].set_yticklabels([repr(s) for s in settings['nStates']])
            ax[2*iT, iI].set_ylabel(r'min. K for rel err<{}'.format(thresholdE))

            ax[2*iT+1, iI].bar(ticksPos, convPct, color=Set2_8.hex_colors[iI], edgecolor='k')
            ax[2*iT+1, iI].set_ylabel('Converged (%)')

        speedups = []
        for iS in range(len(settings['sigma'])):

            innerDF = toMerge[impls[0]][iS]
            for iI in range(1, len(impls)):
                innerDF = pd.merge(innerDF, toMerge[impls[iI]][iS], on=['lambdaSim', 'muSim', 'sigmaSim', 'lambdaLL', 'muLL', 'sigmaLL'], how = 'inner')
                #print innerDF

            timeDiv = innerDF['time{}'.format(impls[1])]
            bPlots = ax[2*iT+1, 2].boxplot(timeDiv, positions=[iS+1-0.2], widths=0.3, medianprops=medianprops, patch_artist=True)
            for patch in bPlots['boxes']:
                patch.set_facecolor(Set2_8.hex_colors[1])

            timeOther = innerDF['time{}'.format(impls[0])]
            bPlots = ax[2*iT+1, 2].boxplot(timeOther, positions=[iS+1+0.2], widths=0.3, medianprops=medianprops, patch_artist=True)
            for patch in bPlots['boxes']:
                patch.set_facecolor(Set2_8.hex_colors[0])

            speedup = (timeDiv / timeOther)
            speedups.append(speedup)

        ax[2*iT+1, 2].set_yscale('log')
        ax[2*iT+1, 2].set_ylabel('Time')

        bPlots = ax[2*iT, 2].boxplot(speedups, medianprops=medianprops, patch_artist=True)
        for patch in bPlots['boxes']:
            patch.set_facecolor(Set2_8.hex_colors[2])
        ax[2*iT, 2].set_yscale('log')
        ax[2*iT, 2].set_ylabel('Speedup')

    for iI, impl in zip(range(len(impls)), impls):
        ax[-1, iI].set_xlabel('Sigma ({})'.format(impl))
        ax[-1, iI].set_xticklabels([repr(s) for s in settings['sigma']])

    plt.suptitle('Speedup of {} over {}\n with smallest K such that rel err. < {}'.format(impls[0], impls[1], thresholdE))

    plt.tight_layout(rect=[0, 0.00, 1, 0.95])
    plt.savefig('{}/SpeedupAcc_{}_{}.png'.format(figureFolder, impls[0], impls[1]))
    plt.savefig('{}/SpeedupAcc_{}_{}.svg'.format(figureFolder, impls[0], impls[1]))


def plotSpeedupFixed(settings, impls, df, fixedK, figureFolder):

    if len(impls) > 2:
        print '"plotSpeedupVsAccuracy" only works for 2 impls'
        return

    font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 10}

    plt.rc('font', **font)

    medianprops = dict(linestyle='-', linewidth=3, color='k')

    # Compute relative errors
    localDF = df.copy()
    for impl in impls:
        llImpl = 'll{}'.format(impl)
        llImplRef = 'll{}'.format(REF_IMPL_LL)
        relativeErr = np.abs((localDF[llImpl]-localDF[llImplRef])/localDF[llImplRef])
        relEImpl = 'err{}'.format(impl)
        localDF[relEImpl] = relativeErr

    heights = []
    for t in settings['nTips']:
        heights.append(3)
        heights.append(1.5)

    X_WIDTH = 600*(1+len(impls))
    Y_WIDTH = 200*2*len(settings['nTips'])
    fig, ax = plt.subplots(2*len(settings['nTips']), (1+len(impls)), figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=True, sharex=True, sharey=False, gridspec_kw={'height_ratios': heights})

    speedupsPerTips = []
    for iT, t in zip(range(len(settings['nTips'])), settings['nTips']):
        speedupsPerTips.append([])
        #if t > 128: continue
        convTotal = {}
        toMerge = {}
        for iI, impl in zip(range(len(impls)), impls):
            data = []
            convPct = []
            convTotal[impl] = 0
            toMerge[impl] = []
            timeImpl = 'time{}'.format(impl)
            relEImpl = 'err{}'.format(impl)
            for iS in range(len(settings['sigma'])):
                res = localDF.loc[(localDF['nTips'] == t) & (localDF['sigmaSim'] == settings['sigma'][iS]) & (localDF['nStates'] == fixedK[iI]) ]
                res = res.loc[res.groupby(['sigmaLL', 'lambdaSim', 'lambdaLL', 'muSim', 'muLL'], as_index=False)['nStates'].idxmin()]
                data.append(res['nStates'].to_numpy())
                toMerge[impl].append(res[['sigmaSim', 'sigmaLL', 'lambdaSim', 'lambdaLL', 'muSim', 'muLL', timeImpl]])

                nCases = (len(settings['lambda'])*len(settings['lambda'])*len(settings['sigma']))
                convTotal[impl] += len(data[-1])
                convPct.append(100.*len(data[-1])/nCases)


            ticksPos = range(1,1+len(convPct))
            data = [np.log2(d) for d in data]

            bPlots = ax[2*iT, iI].boxplot(data, medianprops=medianprops, patch_artist=True)
            for patch in bPlots['boxes']:
                patch.set_facecolor(Set2_8.hex_colors[iI])

            ax[2*iT, iI].set_yticks(np.log2(settings['nStates']))
            ax[2*iT, iI].set_yticklabels([repr(s) for s in settings['nStates']])
            ax[2*iT, iI].set_ylabel(r'K')

            ax[2*iT+1, iI].bar(ticksPos, convPct, color=Set2_8.hex_colors[iI], edgecolor='k')
            ax[2*iT+1, iI].set_ylabel('Converged (%)')

        speedups = []
        for iS in range(len(settings['sigma'])):

            innerDF = toMerge[impls[0]][iS]
            for iI in range(1, len(impls)):
                innerDF = pd.merge(innerDF, toMerge[impls[iI]][iS], on=['lambdaSim', 'muSim', 'sigmaSim', 'lambdaLL', 'muLL', 'sigmaLL'], how = 'inner')
                #print innerDF

            timeDiv = innerDF['time{}'.format(impls[1])]
            bPlots = ax[2*iT+1, 2].boxplot(timeDiv, positions=[iS+1-0.2], widths=0.3, medianprops=medianprops, patch_artist=True)
            for patch in bPlots['boxes']:
                patch.set_facecolor(Set2_8.hex_colors[1])

            timeOther = innerDF['time{}'.format(impls[0])]
            bPlots = ax[2*iT+1, 2].boxplot(timeOther, positions=[iS+1+0.2], widths=0.3, medianprops=medianprops, patch_artist=True)
            for patch in bPlots['boxes']:
                patch.set_facecolor(Set2_8.hex_colors[0])

            speedup = (timeDiv / timeOther)
            speedups.append(speedup)
            for s in speedup:
                speedupsPerTips[-1].append(s)

        ax[2*iT+1, 2].set_yscale('log')
        ax[2*iT+1, 2].set_ylabel('Time')

        bPlots = ax[2*iT, 2].boxplot(speedups, medianprops=medianprops, patch_artist=True)
        for patch in bPlots['boxes']:
            patch.set_facecolor(Set2_8.hex_colors[2])
        ax[2*iT, 2].set_yscale('log')
        ax[2*iT, 2].set_ylabel('Speedup')

    for iI, impl in zip(range(len(impls)), impls):
        ax[-1, iI].set_xlabel('Sigma ({})'.format(impl))
        ax[-1, iI].set_xticklabels([repr(s) for s in settings['sigma']])

    plt.suptitle('Speedup of {} over {}\n'.format(impls[0], impls[1]))

    plt.tight_layout(rect=[0, 0.00, 1, 0.95])
    plt.savefig('{}/SpeedupFixed_{}_{}_{}_{}.png'.format(figureFolder, impls[0], impls[1], fixedK[0], fixedK[1]))
    plt.savefig('{}/SpeedupFixed_{}_{}_{}_{}.svg'.format(figureFolder, impls[0], impls[1], fixedK[0], fixedK[1]))

    X_WIDTH = 400
    Y_WIDTH = 300
    fig, ax = plt.subplots(1, 1, figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=True, sharex=False, sharey=False)

    bPlots = ax.boxplot(speedupsPerTips, patch_artist=True)
    for col, patch in zip(Set2_8.hex_colors, bPlots['boxes']):
        patch.set_facecolor(col)

    ax.set_yscale('log')
    ax.set_ylabel('Speedup')
    ax.set_xticklabels(settings['nTips'])
    ax.set_xlabel('nTips')

    plt.suptitle('Speedup at fixed K=[{},{}]'.format(fixedK[0], fixedK[1]))

    plt.tight_layout(rect=[0, 0.00, 1, 0.95])
    plt.savefig('{}/OverallSpeedupFixed_{}_{}_{}_{}.png'.format(figureFolder, impls[0], impls[1], fixedK[0], fixedK[1]))
    plt.savefig('{}/OverallSpeedupFixed_{}_{}_{}_{}.svg'.format(figureFolder, impls[0], impls[1], fixedK[0], fixedK[1]))

def analyseErrorQuasse(settings, impl, df):

    whichOnes = ['simulation', 'extreme']

    localDF = df.copy()
    localDF = localDF.loc[(localDF['nStates'] == 1024)]
    llImpl = 'll{}'.format(impl)
    llImplRef = 'll{}'.format(REF_IMPL_LL)
    absoluteErr = ((localDF[llImpl]-localDF[llImplRef]))
    absEImpl = 'aErr{}'.format(impl)
    localDF[absEImpl] = absoluteErr
    relativeErr = np.abs((localDF[llImpl]-localDF[llImplRef])/localDF[llImplRef])
    relEImpl = 'rErr{}'.format(impl)
    localDF[relEImpl] = relativeErr

    for which in whichOnes:

        res = None
        if which == 'simulation':
            res = localDF.loc[(localDF['lambdaSim'] == localDF['lambdaLL']) & (localDF['sigmaSim'] == localDF['sigmaLL'])]
        elif which == 'extreme':
            res = localDF.loc[(localDF['lambdaSim'] == localDF['lambdaLL']) & (localDF['sigmaSim'] == settings['sigma'][3]) & (localDF['sigmaLL'] == settings['sigma'][0])]


        X_WIDTH = 1200
        Y_WIDTH = 600
        fig, ax = plt.subplots(2, 4, figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=True, sharex=False, sharey=False)

        errType = absEImpl
        ax[0,0].scatter(res['lambdaLL'], res[errType])
        ax[0,1].scatter(res['sigmaLL'], res[errType])
        ax[0,2].scatter(res['nTips'], res[errType])
        ax[0,3].scatter(res[llImplRef], res[errType])
        ax[0,0].set_ylabel('Absolute error')

        errType = relEImpl
        ax[1,0].scatter(res['lambdaLL'], res[errType])
        ax[1,1].scatter(res['sigmaLL'], res[errType])
        ax[1,2].scatter(res['nTips'], res[errType])
        ax[1,3].scatter(res['treeHeight'], res[errType])
        ax[1,0].set_ylabel('Relative error')
        ax[1,0].set_xlabel('Lambda and mu')
        ax[1,1].set_xlabel('Sigma')
        ax[1,2].set_xlabel('nTips')
        ax[1,3].set_xlabel('treeHeight')


settingFile = sys.argv[1]
figureFolder = sys.argv[2]
settings = readSettings(settingFile)

figureFolder += '/' + settings['runName']

if not os.path.isdir(figureFolder) :
    os.makedirs(figureFolder)

#plotConvergenceQuasse(settings, implementations, df, figureFolder)

if settings['runName'] == 'benchmarkFinal':

    implementations =     implementations = ['Analytical', 'Diversitree', 'RB_TP_1T_AT', 'RB_TP_1T_AT3',  'RB_TP_4T_AT2', 'RB_TP_4T_AT3', 'TP_0F_1Proc']
    #implementations = ['Analytical', 'Diversitree', 'RB_TP_BW']

    dFrames = {}
    for impl in implementations:
        resultFile = './results/' + settings["runName"] + '/time' + impl + '.txt'
        dFrames[impl] = readBenchmark(impl, resultFile)

    df = mergeDFrame(implementations, dFrames)

    vecSigma = settings['sigma']#[::2]
    plotErrorQuasseSingle(settings, 'RB_TP_1T_AT', df, vecSigma, figureFolder)
    plotErrorQuasseSingle(settings, 'TP_0F_1Proc', df, vecSigma, figureFolder)
    plotErrorQuasseSingle(settings, 'Diversitree', df, vecSigma, figureFolder)
    implNoPar = ['Analytical', 'Diversitree', 'RB_TP_1T_AT3', 'TP_0F_1Proc']
    plotErrorQuasse(settings, implNoPar, df, vecSigma, figureFolder)
    plotAvgErrorQuasse(settings, implNoPar, df, vecSigma, figureFolder)

    implsNoAnal = implementations[:]
    implsNoAnal.remove('Analytical')
    plotAvgSpeedupsSummary(settings, implsNoAnal, df, 'time', figureFolder)

    impls = ['RB_TP_1T_AT', 'Diversitree']
    plotSpeedupVsAccuracy(settings, impls, df, 0.1, figureFolder)
    plotSpeedupFixed(settings, impls, df, [128, 1024], figureFolder)

    impls = ['RB_TP_1T_AT3', 'Diversitree']
    plotSpeedupVsAccuracy(settings, impls, df, 0.1, figureFolder)
    plotSpeedupFixed(settings, impls, df, [128, 1024], figureFolder)

    impls = ['RB_TP_1T_AT', 'TP_0F_1Proc']
    plotSpeedupVsAccuracy(settings, impls, df, 0.1, figureFolder)
    plotSpeedupFixed(settings, impls, df, [128, 1024], figureFolder)

    impls = ['TP_0F_1Proc', 'Diversitree']
    plotSpeedupVsAccuracy(settings, impls, df, 0.1, figureFolder)
    plotSpeedupFixed(settings, impls, df, [128, 1024], figureFolder)

    #impls = ['TP_1F_1Proc', 'Diversitree']
    #plotSpeedupVsAccuracy(settings, impls, df, 0.1, figureFolder)
    #plotSpeedupFixed(settings, impls, df, [128, 1024], figureFolder)

    impls = ['RB_TP_4T_AT2', 'RB_TP_1T_AT']
    plotSpeedupVsAccuracy(settings, impls, df, 0.1, figureFolder)
    plotSpeedupFixed(settings, impls, df, [128, 128], figureFolder)


    impls = ['RB_TP_1T_AT3', 'RB_TP_1T_AT']
    plotSpeedupVsAccuracy(settings, impls, df, 0.1, figureFolder)
    plotSpeedupFixed(settings, impls, df, [128, 128], figureFolder)

    impls = ['RB_TP_4T_AT2', 'RB_TP_1T_AT3']
    plotSpeedupVsAccuracy(settings, impls, df, 0.1, figureFolder)
    plotSpeedupFixed(settings, impls, df, [128, 128], figureFolder)

    impls = ['RB_TP_4T_AT3', 'RB_TP_1T_AT3']
    plotSpeedupVsAccuracy(settings, impls, df, 0.1, figureFolder)
    plotSpeedupFixed(settings, impls, df, [128, 128], figureFolder)

else:

    implementations = ['Analytical', 'Diversitree', 'RB_TP_BW']

    dFrames = {}
    for impl in implementations:
        resultFile = './results/' + settings["runName"] + '/time' + impl + '.txt'
        dFrames[impl] = readBenchmark(impl, resultFile)

    df = mergeDFrame(implementations, dFrames)

    vecSigma = settings['sigma'][::2]
    plotErrorQuasseSingle(settings, 'Diversitree', df, vecSigma, figureFolder)
    plotErrorQuasseSingle(settings, 'RB_TP_BW', df, vecSigma, figureFolder)

    plotErrorQuasse(settings, implementations, df, vecSigma, figureFolder)
    plotAvgErrorQuasse(settings, implementations, df, vecSigma, figureFolder)

    implsNoAnal = implementations[:]
    implsNoAnal.remove('Analytical')
    plotAvgSpeedupsSummary(settings, implsNoAnal, df, 'time', figureFolder)

    impls = ['RB_TP_BW', 'Diversitree']
    plotSpeedupVsAccuracy(settings, impls, df, 0.1, figureFolder)
    plotSpeedupFixed(settings, impls, df, [128, 1024], figureFolder)
