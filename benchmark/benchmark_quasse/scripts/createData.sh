#!/bin/bash

SETTING_FILE=${1}
SIM_FOLDER=${2}

# benchmark setting
source ../utils/readSettings.sh

# use it like Rscript validation/simulate_trees.r f n s r (edited)
# where f is the output directory, n is the number of tips in the trees, s is the number of states, and r is the number of replicates

for t in "${nTips[@]}"
do
  for pS in `seq 0 $((${#sigma[@]}-1))`
  do
    for pL in `seq 0 $((${#lambda[@]}-1))`
    do

      OUT_FOLDER="${SIM_FOLDER}/SIM_${t}T_${pL}LSIM_${pS}SSIM"
      mkdir -p ${OUT_FOLDER}

      Rscript ./scripts/simulateData.R ${OUT_FOLDER} ${t} ${sigma[$pS]} ${lambda[$pL]}

      #for qS in `seq 0 $((${#sigma[@]}-1))`
      #do

        #treeFile="${SIM_FOLDER}/SIM_${t}T_${pL}LSIM_${pS}SSIM/tree.nex"
        #rescaledTreeFile="${SIM_FOLDER}/SIM_${t}T_${pL}LSIM_${pS}SSIM/tree_rescaled_${qS}SINF.nex"

        #Rscript ./scripts/rescaleTreeAndData.R ${treeFile} ${sigma[$qS]} ${rescaledTreeFile}

      #done

    done
  done
done
