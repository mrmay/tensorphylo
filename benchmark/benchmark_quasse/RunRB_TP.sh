#!/bin/bash
calc(){ awk "BEGIN { print $* }"; }

# Read the setting file given as argument
SETTING_FILE=${1}
SIM_FOLDER=${2}
RB_EXEC=${3}
TP_LIB_PATH=${4}
N_THREAD=${5}

source ../utils/readSettings.sh

# folders required
IFS='_' read -ra SUFFIX <<< "${TP_LIB_PATH}"

MY_NAME=RB_TP_${N_THREAD}T_${SUFFIX[-1]}
echo $MY_NAME
TEMP_FOLDER=./tmp/${runName}
mkdir -p ${TEMP_FOLDER}
TP_LOCK_FILE=${TEMP_FOLDER}/LOCK_${MY_NAME}_${runName}.txt

PARAM_FOLDER=./data/params${MY_NAME}/${runName}
BASE_RB_CONVERTER_SCRIPT=scripts/baseScriptRB_TP_Par.Rev
mkdir -p ${PARAM_FOLDER}

RESULT_FOLDER=./results/${MY_NAME}/${runName}
mkdir -p ${RESULT_FOLDER}

OUTPUT_FOLDER=./results/${runName}
mkdir -p ${OUTPUT_FOLDER}
outputFile=${OUTPUT_FOLDER}/time${MY_NAME}.txt

N_TEST_TIMEOUT=5
TIME_FOR_TEST=$(( 15 + ${N_TEST_TIMEOUT} * ${timeout} ))

# loop through settings
echo "nTips nStates lambdaSim muSim sigmaSim lambdaLL muLL sigmaLL nIter nStepsInt meanTime loglik error" > ${outputFile}

for t in "${nTips[@]}"
do

  for s in "${nStates[@]}"
  do

    if [ "$s" -ge "512" ]
    then
      continue
    fi

    for pL in `seq 0 $((${#lambda[@]}-1))`
    do
      for qL in `seq 0 $((${#lambda[@]}-1))`
      do
        for pS in `seq 0 $((${#sigma[@]}-1))`
        do
          for qS in `seq 0 $((${#sigma[@]}-1))`
          do

            if [ -f ${TP_LOCK_FILE} ]
            then
              rm ${TP_LOCK_FILE}
            fi

            name="${runName}_${t}T_${s}S_${pL}LSIM_${qL}LINF_${pS}SSIM_${qS}SINF_${nIter}M_"
            echo $name


            if [ ! -f ${TP_LOCK_FILE} ]
            then

              treeFile="${SIM_FOLDER}/SIM_${t}T_${pL}LSIM_${pS}SSIM/tree.nex"
              dataFile="${SIM_FOLDER}/SIM_${t}T_${pL}LSIM_${pS}SSIM/data.nex"
              outFile=${TEMP_FOLDER}/dummyTP.txt

              # Use RB to dump the parameters.dat file
              RB_SCRIPT=${PARAM_FOLDER}/${name}.Rev
              cp ${BASE_RB_CONVERTER_SCRIPT} ${RB_SCRIPT}

              sed -i -e "s|TP_LIB_PATH|${TP_LIB_PATH}|g" ${RB_SCRIPT}
              sed -i -e "s|TREE_FILE|${treeFile}|g" ${RB_SCRIPT}
              sed -i -e "s|DATA_FILE|${dataFile}|g" ${RB_SCRIPT}
              sed -i -e "s|N_STATE|${s}|g" ${RB_SCRIPT}
              sed -i -e "s|PARAM_LAMBDA|${lambda[$qL]}|g" ${RB_SCRIPT}
              sed -i -e "s|PARAM_MU|${mu[$qL]}|g" ${RB_SCRIPT}
              sed -i -e "s|PARAM_SIGMA|${sigma[$qS]}|g" ${RB_SCRIPT}
              sed -i -e "s|SIGMA_SD|${sigma[$pS]}|g" ${RB_SCRIPT}
              sed -i -e "s|N_ITER|${N_TEST_TIMEOUT}|g" ${RB_SCRIPT}
              sed -i -e "s|N_PROC|${N_THREAD}|g" ${RB_SCRIPT}
              sed -i -e "s|OUT_FILE|${outFile}|g" ${RB_SCRIPT}

              # First we try with 5 liks for timeout
              # If the lock is not there after this test we benchmark
              # The lock stay active until when change of tree
              timeout ${TIME_FOR_TEST}s bash <<EOT
                touch ${TP_LOCK_FILE}
                ${RB_EXEC} ${RB_SCRIPT} 1> /dev/null
                rm ${TP_LOCK_FILE}
EOT

            fi

            # If the lock is not there: benchmark
            if [ ! -f ${TP_LOCK_FILE} ]
            then

              treeFile="${SIM_FOLDER}/SIM_${t}T_${pL}LSIM_${pS}SSIM/tree.nex"
              dataFile="${SIM_FOLDER}/SIM_${t}T_${pL}LSIM_${pS}SSIM/data.nex"
              outFile="${RESULT_FOLDER}/${name}.txt"

              # Use RB to dump the parameters.dat file
              RB_SCRIPT=${PARAM_FOLDER}/${name}.Rev
              cp ${BASE_RB_CONVERTER_SCRIPT} ${RB_SCRIPT}

              sed -i -e "s|TP_LIB_PATH|${TP_LIB_PATH}|g" ${RB_SCRIPT}
              sed -i -e "s|TREE_FILE|${treeFile}|g" ${RB_SCRIPT}
              sed -i -e "s|DATA_FILE|${dataFile}|g" ${RB_SCRIPT}
              sed -i -e "s|N_STATE|${s}|g" ${RB_SCRIPT}
              sed -i -e "s|PARAM_LAMBDA|${lambda[$qL]}|g" ${RB_SCRIPT}
              sed -i -e "s|PARAM_MU|${mu[$qL]}|g" ${RB_SCRIPT}
              sed -i -e "s|PARAM_SIGMA|${sigma[$qS]}|g" ${RB_SCRIPT}
              sed -i -e "s|SIGMA_SD|${sigma[$pS]}|g" ${RB_SCRIPT}
              sed -i -e "s|N_ITER|${nIter}|g" ${RB_SCRIPT}
              sed -i -e "s|N_PROC|${N_THREAD}|g" ${RB_SCRIPT}
              sed -i -e "s|OUT_FILE|${outFile}|g" ${RB_SCRIPT}

              ${RB_EXEC} ${RB_SCRIPT} 1> /dev/null

              # approximator timeInit timeComp  lik int steps
              line=`cat ${outFile}`
              words=($line)

              #echo "calc ${words[3]} / ${nIter}"
              timePerLik=${words[0]}
              lastLogLik=${words[1]}
              nSteps=-1

              echo "\"${t}\" \"${s}\" \"${lambda[$pL]}\" \"${mu[$pL]}\" \"${sigma[$pS]}\" \"${lambda[$qL]}\" \"${mu[$qL]}\" \"${sigma[$qS]}\" \"${nIter}\" \"${nSteps}\" \"${timePerLik}\" \"${lastLogLik}\" \"\"" >> ${outputFile}

            # otherwise no result
            else

              echo "\"${t}\" \"${s}\" \"${lambda[$pL]}\" \"${mu[$pL]}\" \"${sigma[$pS]}\" \"${lambda[$qL]}\" \"${mu[$qL]}\" \"${sigma[$qS]}\" \"${nIter}\" \"${nSteps}\" \"NA\" \"NA\" \"timeout ${timeout} seconds\"" >> ${outputFile}

            fi
          done
        done
      done
    done
  done
done
