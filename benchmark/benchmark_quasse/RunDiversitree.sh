#!/bin/bash
calc(){ awk "BEGIN { print $* }"; }

# Read the setting file given as argument
SETTING_FILE=${1}
SIM_FOLDER=${2}

source ../utils/readSettings.sh

# folders required

MY_NAME=Diversitree

TEMP_FOLDER=./tmp/${runName}
mkdir -p ${TEMP_FOLDER}
DIV_LOCK_FILE=${TEMP_FOLDER}/LOCK_${MY_NAME}_${runName}.txt
DUMMY_OUT_FILE=${TEMP_FOLDER}/dummyOut${MY_NAME}.txt

OUTPUT_FOLDER=./results/${runName}
mkdir -p ${OUTPUT_FOLDER}
outputFile=${OUTPUT_FOLDER}/time${MY_NAME}.txt

N_TEST_TIMEOUT=5
TIME_FOR_TEST=$(( 15 + ${N_TEST_TIMEOUT} * ${timeout} ))

# loop through settings
echo "nTips nStates lambdaSim muSim sigmaSim lambdaLL muLL sigmaLL nIter nStepsInt meanTime loglik error" > ${outputFile}

for t in "${nTips[@]}"
do

  for s in "${nStates[@]}"
  do

    if [ "$s" -lt "128" ]
    then
      continue
    fi

    for pL in `seq 0 $((${#lambda[@]}-1))`
    do
      for qL in `seq 0 $((${#lambda[@]}-1))`
      do
        for pS in `seq 0 $((${#sigma[@]}-1))`
        do
          for qS in `seq 0 $((${#sigma[@]}-1))`
          do

            if [ -f ${DIV_LOCK_FILE} ]
            then
              rm ${DIV_LOCK_FILE}
            fi

            name="${runName}_${t}T_${s}S_${pL}LSIM_${qL}LINF_${pS}SSIM_${qS}SINF_${nIter}M"
            echo $name

            treeFile="${SIM_FOLDER}/SIM_${t}T_${pL}LSIM_${pS}SSIM/tree.nex"
            #dataFile="${SIM_FOLDER}/SIM_${t}T_${pL}LSIM_${pS}SSIM/data.tsv"
            dataFile="${SIM_FOLDER}/SIM_${t}T_${pL}LSIM_${pS}SSIM/data.tsv"

            # First we try with 5 liks for timeout
            # If the lock is not there after this test we benchmark
            # The lock stay active until when change of tree
            if [ ! -f ${DIV_LOCK_FILE} ]
            then

              timeout ${TIME_FOR_TEST}s bash <<EOT
                touch ${DIV_LOCK_FILE}
                Rscript scripts/scriptDiversitree.R ${s} ${t} ${lambda[$pL]} ${mu[$pL]} ${sigma[$pS]} ${lambda[$qL]} ${mu[$qL]} ${sigma[$qS]} ${N_TEST_TIMEOUT} ${treeFile} ${dataFile} ${DUMMY_OUT_FILE}
                rm ${DIV_LOCK_FILE}
EOT

            fi

            # If the lock is not there: benchmark
            if [ ! -f ${DIV_LOCK_FILE} ]
            then

              Rscript scripts/scriptDiversitree.R ${s} ${t} ${lambda[$pL]} ${mu[$pL]} ${sigma[$pS]} ${lambda[$qL]} ${mu[$qL]} ${sigma[$qS]} ${nIter} ${treeFile} ${dataFile} ${outputFile}

            # otherwise no result
            else

              echo "\"${t}\" \"${s}\" \"${lambda[$pL]}\" \"${mu[$pL]}\" \"${sigma[$pS]}\" \"${lambda[$qL]}\" \"${mu[$qL]}\" \"${sigma[$qS]}\" \"${nIter}\" \"1\" \"NA\" \"NA\" \"timeout ${timeout} seconds\"" >> ${outputFile}

            fi

          done
        done
      done
    done
  done
done
