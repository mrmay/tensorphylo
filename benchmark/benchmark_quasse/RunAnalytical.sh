#!/bin/bash
calc(){ awk "BEGIN { print $* }"; }

# Read the setting file given as argument
SETTING_FILE=${1}
SIM_FOLDER=${2}
RB_EXEC=${3}

source ../utils/readSettings.sh

# folders required
TEMP_FOLDER=./tmp/${runName}
mkdir -p ${TEMP_FOLDER}

MY_NAME="Analytical"

RESULT_FOLDER=./results/${MY_NAME}/${runName}
mkdir -p ${RESULT_FOLDER}

OUTPUT_FOLDER=./results/${runName}
mkdir -p ${OUTPUT_FOLDER}
outputFile=${OUTPUT_FOLDER}/time${MY_NAME}.txt

SCRIPT_FOLDER=./data/params${MY_NAME}/${runName}
BASE_SCRIPT_RB=./scripts/baseAnalytical_prob.Rev
mkdir -p ${SCRIPT_FOLDER}

# loop through settings
echo "nTips nStates lambdaSim muSim sigmaSim lambdaLL muLL sigmaLL nIter nStepsInt meanTime loglik error" > ${outputFile}

for t in "${nTips[@]}"
do
  for s in "${nStates[@]}"
  do
    for pL in `seq 0 $((${#lambda[@]}-1))`
    do
      for qL in `seq 0 $((${#lambda[@]}-1))`
      do
        for pS in `seq 0 $((${#sigma[@]}-1))`
        do
          for qS in `seq 0 $((${#sigma[@]}-1))`
          do

            name="${runName}_${t}T_${s}S_${pL}LSIM_${qL}LINF_${pS}SSIM_${qS}SINF_${nIter}M"
            echo $name

            treeFile="${SIM_FOLDER}/SIM_${t}T_${pL}LSIM_${pS}SSIM/tree.nex"
            dataFile="${SIM_FOLDER}/SIM_${t}T_${pL}LSIM_${pS}SSIM/data.nex"

            l=${lambda[$qL]}
            m=${mu[$qL]}
            sig=${sigma[$qS]}

            MY_SCRIPT=${SCRIPT_FOLDER}/${name}.rev
            cp ${BASE_SCRIPT_RB} ${MY_SCRIPT}

            OUT_FILE="${RESULT_FOLDER}/${name}.txt"

            sed -i -e "s|TREE_FILE|${treeFile}|g" ${MY_SCRIPT}
            sed -i -e "s|DATA_NEX_FILE|${dataFile}|g" ${MY_SCRIPT}
            sed -i -e "s|LAMBDA_PARAM|${l}|g" ${MY_SCRIPT}
            sed -i -e "s|MU_PARAM|${m}|g" ${MY_SCRIPT}
            sed -i -e "s|SIGMA_PARAM|${sig}|g" ${MY_SCRIPT}
            sed -i -e "s|SIGMA_SD|${sigma[$pS]}|g" ${MY_SCRIPT}
            sed -i -e "s|OUT_FILE|${OUT_FILE}|g" ${MY_SCRIPT}

            ${RB_EXEC} ${MY_SCRIPT}

            avgTime="1e-12"
            loglik=`cat ${OUT_FILE} | cut -f 2 -d$'\t'`

            echo "\"${t}\" \"${s}\" \"${lambda[$pL]}\" \"${mu[$pL]}\" \"${sigma[$pS]}\" \"${lambda[$qL]}\" \"${mu[$qL]}\" \"${sigma[$qS]}\" \"${nIter}\" \"1\" \"${avgTime}\" \"${loglik}\" \"\"" >> ${outputFile}

          done
        done
      done
    done
  done
done
