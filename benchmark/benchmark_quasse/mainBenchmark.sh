#!/bin/bash
SETTING_FILE=${1}

REVBAYES_BIN=rb_current
#N_PROC=1
#TENSOR_PHYLO_BIN=./bin/TensorPhyloBW
TENSOR_PHYLO_LIB_FOLDER=/home/meyerx/Projets/TensorPhylo/benchmark/benchmark_quasse/lib_AT

source ../utils/readSettings.sh

echo "Starting benchmark '${runName}' with settings:"
echo "-----------------------------------------------"
# settings
echo "Number of iterations: ${nIter}"
echo "Number of tips: ${nTips[@]}"
echo "Number of states: ${nStates[@]}"
echo "Lambda values: ${lambda[@]}"
echo "Mu values: ${mu[@]}"
echo "Sigma values: ${sigma[@]}"
echo "Timeout: ${timeout}"

SIM_FOLDER="`pwd`/SimulatedData/${runName}"

if [ ! -d ${SIM_FOLDER} ]; then
  echo "-----------------------------------------------"
  echo "Simulating the datasets"
  bash scripts/createData.sh ${SETTING_FILE} ${SIM_FOLDER}
  echo "Done"
fi

echo "-----------------------------------------------"

echo "Benchmarking analytical"
bash ./RunAnalytical.sh ${SETTING_FILE} ${SIM_FOLDER} ${REVBAYES_BIN} > log/${runName}_anal.log &

echo "Benchmarking diversitree"
bash ./RunDiversitree.sh ${SETTING_FILE} ${SIM_FOLDER} > log/${runName}_div.log &

echo "Benchmarking ReBayes + TensorPhylo SEQUENTIAL"
N_PROC=1
bash ./RunRB_TP.sh ${SETTING_FILE} ${SIM_FOLDER} ${REVBAYES_BIN} ${TENSOR_PHYLO_LIB_FOLDER} ${N_PROC} > log/${runName}_RB_TP_${N_PROC}T.log

echo "Benchmarking ReBayes + TensorPhylo PARALLEL"
N_PROC=6
bash ./RunRB_TP.sh ${SETTING_FILE} ${SIM_FOLDER} ${REVBAYES_BIN} ${TENSOR_PHYLO_LIB_FOLDER} ${N_PROC} > log/${runName}_RB_TP_${N_PROC}T.log
