#!/bin/bash

nIter=$1
rootDir=$2

TEMP_FOLDER=${rootDir}/Temp
mkdir -p ${TEMP_FOLDER}

RESULT_FOLDER=${rootDir}/benchmark/Results/Results_1
mkdir -p ${RESULT_FOLDER}
rm ${RESULT_FOLDER}/*

BENCHMARK_FOLDER=${rootDir}/benchmark

##################     CLADO      ####################
#--param parameters/clado.dat --nexus data/example_clado_tree.nex --tsv data/example_clado_data.tsv -v 1 --benchmark 50
param=${TEMP_FOLDER}/clado.dat
nexus=${rootDir}/data/example_clado_tree.nex
tsv=${rootDir}/data/example_clado_data.tsv

for myEx in TensorPhyloMaster TensorPhyloSeqOMP TensorPhyloSeq2OMP TensorPhyloFirstPar TensorPhyloImprovPar TensorPhyloParOMP TensorPhyloPar2OMP
do
  for iA in 0 1 2
  do
    for iT in 1
    do
      cp ${BENCHMARK_FOLDER}/BenchmarkPhyloDB/clado.dat ${param}
      sed -i "s/#A/${iA}/g" ${TEMP_FOLDER}/clado.dat
      sed -i "s/#T/${iT}/g" ${TEMP_FOLDER}/clado.dat

      echo "-------------------------------------------------------"
      echo "CLADO : ${myEx}_${nIter}_${iA}A_${iT}T"
      echo "-------------------------------------------------------"

      outFile=${RESULT_FOLDER}/out_clado_${myEx}_${nIter}_${iA}A_${iT}T.txt
      timeFile=${RESULT_FOLDER}/time_clado_${myEx}_${nIter}_${iA}A_${iT}T.txt
      (time ${rootDir}/${myEx} --param ${param} --nexus ${nexus} --tsv ${tsv} --benchmark ${nIter} -v 1) 2> ${timeFile}
    done
  done
done

for myEx in TensorPhyloFirstPar TensorPhyloImprovPar TensorPhyloParOMP TensorPhyloPar2OMP
do
  for iA in 3
  do
    for iT in 1 2 4 6
    do
      cp ${BENCHMARK_FOLDER}/BenchmarkPhyloDB/clado.dat ${param}
      sed -i "s/#A/${iA}/g" ${TEMP_FOLDER}/clado.dat
      sed -i "s/#T/${iT}/g" ${TEMP_FOLDER}/clado.dat

      echo "-------------------------------------------------------"
      echo "CLADO : ${myEx}_${nIter}_${iA}A_${iT}T"
      echo "-------------------------------------------------------"

      outFile=${RESULT_FOLDER}/out_clado_${myEx}_${nIter}_${iA}A_${iT}T.txt
      timeFile=${RESULT_FOLDER}/time_clado_${myEx}_${nIter}_${iA}A_${iT}T.txt
      (time ${rootDir}/${myEx} --param ${param} --nexus ${nexus} --tsv ${tsv} --benchmark ${nIter} -v 1) 2> ${timeFile}
    done
  done
done

##################     HUGE      ####################
#--param parameters/huge_params.dat --nexus data/huge_tree.nex --tsv data/huge_data.tsv --benchmark 50 -v 0
param=${TEMP_FOLDER}/huge.dat
nexus=${rootDir}/data/huge_tree.nex
tsv=${rootDir}/data/huge_data.tsv

for myEx in TensorPhyloMaster TensorPhyloSeqOMP TensorPhyloSeq2OMP TensorPhyloFirstPar TensorPhyloImprovPar TensorPhyloParOMP TensorPhyloPar2OMP
do
  for iA in 0 1 2
  do
    for iT in 1
    do
      cp ${BENCHMARK_FOLDER}/BenchmarkPhyloDB/huge.dat ${param}
      sed -i "s/#A/${iA}/g" ${TEMP_FOLDER}/huge.dat
      sed -i "s/#T/${iT}/g" ${TEMP_FOLDER}/huge.dat

      echo "-------------------------------------------------------"
      echo "HUGE : ${myEx}_${nIter}_${iA}A_${iT}T"
      echo "-------------------------------------------------------"

      outFile=${RESULT_FOLDER}/out_huge_${myEx}_${nIter}_${iA}A_${iT}T.txt
      timeFile=${RESULT_FOLDER}/time_huge_${myEx}_${nIter}_${iA}A_${iT}T.txt
      (time ${rootDir}/${myEx} --param ${param} --nexus ${nexus} --tsv ${tsv} --benchmark ${nIter} -v 1) 2> ${timeFile}
    done
  done
done

for myEx in TensorPhyloFirstPar TensorPhyloImprovPar TensorPhyloParOMP TensorPhyloPar2OMP
do
  for iA in 3
  do
    for iT in 1 2 4 6
    do
      cp ${BENCHMARK_FOLDER}/BenchmarkPhyloDB/huge.dat ${param}
      sed -i "s/#A/${iA}/g" ${TEMP_FOLDER}/huge.dat
      sed -i "s/#T/${iT}/g" ${TEMP_FOLDER}/huge.dat

      echo "-------------------------------------------------------"
      echo "HUGE : ${myEx}_${nIter}_${iA}A_${iT}T"
      echo "-------------------------------------------------------"

      outFile=${RESULT_FOLDER}/out_huge_${myEx}_${nIter}_${iA}A_${iT}T.txt
      timeFile=${RESULT_FOLDER}/time_huge_${myEx}_${nIter}_${iA}A_${iT}T.txt
      (time ${rootDir}/${myEx} --param ${param} --nexus ${nexus} --tsv ${tsv} --benchmark ${nIter} -v 1) 2> ${timeFile}
    done
  done
done
