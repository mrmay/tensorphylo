#!/usr/bin/python
import os
import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from scipy import stats

def readBenchmark(filename):

    seqNames = []
    seqTimes = []

    parNames = []
    parNThread = []
    parTimes = []

    inFile = open(filename, 'r')

    for line in inFile:
        words = line.split()
        if len(words) == 0: continue
        execName = words[0]
        nT = float(words[1])
        initTime = float(words[2])
        runTime = float(words[3])

        if 'SEQ' in words[0]:
            seqNames.append(execName)
            seqTimes.append(runTime)
        else:
            parNames.append(execName)
            parNThread.append(nT)
            parTimes.append(runTime)

    seqTimes = np.array(seqTimes)
    parNThread = np.array(parNThread)
    parTimes = np.array(parTimes)

    bestSeqTime = np.min(seqTimes)
    speedup = bestSeqTime/parTimes

    return [seqNames, seqTimes, parNames, parNThread, parTimes, speedup]

def readBenchmarkRB(filename):

    seqTimes = []

    inFile = open(filename, 'r')

    iLine = 0
    for line in inFile:

        if iLine < 3:
            iLine += 1
            continue
        else:
            iLine += 1

        words = line.split()
        if len(words) == 0 or words[0] == 'Summary': break
        seqTimes.append(float(words[1]))

    return [seqTimes, np.average(seqTimes)]

def estimateAlpha(nThreads, speedup):
    alphas = (nThreads[1:]/speedup[1:]-1.)/(nThreads[1:]-1.)
    #print alphas
    #print nThreads[1:]
    avg = np.average(alphas)
    slope, intercept, r_value, p_value, std_err = stats.linregress(nThreads[1:], alphas)
    return [avg, intercept, slope, r_value, p_value, std_err, alphas]

def estimateAlphaGustafson(nThreads, speedup):
    alphas = (nThreads[1:]-speedup[1:])/(nThreads[1:]-1.)
    #print alphas
    #print speedup[1:]
    #print nThreads[1:]
    avg = np.average(alphas)
    slope, intercept, r_value, p_value, std_err = stats.linregress(nThreads[1:], alphas)
    return [avg, intercept, slope, r_value, p_value, std_err, alphas]

def benchmarkRes2(folderRes2):
    datasets = ['clado','huge']
    execNames = ['ImprovPar', 'SeqOMP', 'ParOMP', 'Seq2OMP', 'Par2OMP', 'Seq3OMP', 'Par3OMP' ]

    minTime = {}
    results = {}

    # read
    for ds in datasets:
        results[ds] = {}
        minTime[ds] = 1e10
        for ex in execNames:
            filename = folderRes2 + 'out_' + ds + '_TensorPhylo' + ex + 'Bench_200_0A_6T.txt'
            res = readBenchmark(filename)
            results[ds][ex] = res

            minTime[ds] = min(np.min(res[1]), minTime[ds])
            #if len(res[4]) > 0:
            #    minTime[ds] = min(np.min(res[4]), minTime[ds])

    for ds in datasets:
        fig, ax = plt.subplots(1,5, figsize=(16,4))
        for iE, ex in zip(range(len(execNames)), execNames):

            if len(results[ds][ex][2]) > 0:
                tmp = (minTime[ds]/results[ds][ex][1]).tolist()
                tmp.append(minTime[ds]/results[ds][ex][4][0])
                ax[0].plot([1, 2, 3, 4], tmp, '.', label=ex)
            else:
                ax[0].plot([1, 2, 3], minTime[ds]/results[ds][ex][1], '.', label=ex)


            if len(results[ds][ex][3]) > 1:
                print ds + ' - ' + ex
                resAlpha = estimateAlpha(results[ds][ex][3], minTime[ds]/results[ds][ex][4])

                ax[1].plot(results[ds][ex][3], minTime[ds]/results[ds][ex][4], '-x', label=ex)
                ax[2].plot(iE, resAlpha[0], 'x', label=ex)
                ax[3].plot(iE, resAlpha[1], 'x', label=ex)
                ax[4].plot(iE, resAlpha[2], 'x', label=ex)

        ax[1].set_title(ds)

        ax[0].legend()
        ax[1].legend()
        ax[2].legend()

        plt.tight_layout()

    fig, ax = plt.subplots(2, 4, figsize=(16,4), sharey=True)
    for iD, ds in zip(range(len(datasets)), datasets):
        iPar = 0
        for iE, ex in zip(range(len(execNames)), execNames):
            if len(results[ds][ex][3]) > 1:
                print ds + ' - ' + ex
                resAlpha = estimateAlpha(results[ds][ex][3], minTime[ds]/results[ds][ex][4])

                ax[iD][iPar].plot(results[ds][ex][3][1:], resAlpha[-1], '-x', label=ex)
                ax[iD][iPar].set_title(ex)
                iPar += 1

    plt.tight_layout()


def fancySpeedup3D(results):
    nIter = '100'
    nTips = [ 50, 100, 250, 500, 750]
    nStates = [ 100, 250, 500, 750]
    isLog = True

    # plot times
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    Z=[]
    X2 = []
    Y2 = []
    Z2 = []
    for iT in nTips:
        Z.append([])
        for iS in nStates:
            key = repr(iT) + 'T_' + repr(iS) + 'S'
            X2.append(iT)
            Y2.append(iS)
            if key in results:
                Z2.append(results[key][1][-1]/int(nIter))
                Z[-1].append(results[key][1][-1]/int(nIter))
            else:
                Z2.append(None)
                Z[-1].append(None)

    #fig.colorbar(p)
    X, Y = np.meshgrid(nTips, nStates)
    Z = np.array(Z).transpose()

    if isLog:
        X = np.log10(X)
        Y = np.log10(Y)
        Z = np.log10(Z)

    ax.plot_wireframe(X, Y, Z, alpha=1, linewidth=2, colors='k')
    s = ax.plot_surface(X, Y, Z, alpha=0.5, linewidth=0, cmap=cm.viridis, antialiased=False, shade=False)
    #p = ax.scatter(X2, Y2, Z2, s=150, c=Z2, cmap=cm.viridis, alpha=1)

    for i in range(1,4):
        Z=[]
        #X2 = []
        #Y2 = []
        #Z2 = []
        for iT in nTips:
            Z.append([])
            for iS in nStates:
                key = repr(iT) + 'T_' + repr(iS) + 'S'
                #X2.append(iT)
                #Y2.append(iS)
                if key in results:
                    #Z2.append(results[key][4][i]/int(nIter))
                    Z[-1].append(results[key][4][i]/int(nIter))
                else:
                    #Z2.append(None)
                    Z[-1].append(None)

        X, Y = np.meshgrid(nTips, nStates)
        Z = np.array(Z).transpose()

        if isLog:
            X = np.log10(X)
            Y = np.log10(Y)
            Z = np.log10(Z)
            #X2 = np.log10(X2)
            #Y2 = np.log10(Y2)
            #Z2 = np.log10(Z2)
        c = plt.get_cmap('Set1')(i)
        ax.plot_wireframe(X, Y, Z, alpha=1, linewidth=2, colors='k')
        ax.plot_surface(X, Y, Z, alpha=0.6, linewidth=0, color=c, antialiased=False, shade=False)
    #p = ax.scatter(X2, Y2, Z2, s=150, c=Z2, cmap=cm.viridis, alpha=1)


    #surf = ax.plot_surface(X, Y, Z, linewidth=0, antialiased=False, cmap=cm.cividis)
    fig.colorbar(s, shrink=0.5, aspect=5)
    ax.set_title('Runtime for 1 lik')
    ax.set_xlabel('Species number')
    ax.set_ylabel('States number')
    ax.set_zlabel('Second')
    plt.tight_layout()


def modelFitQuadratic(name, X2, Y2, Z2):

    #X2.append(0)
    #Y2.append(0)
    #Z2.append(0)
    X3 = np.array(X2)
    Y3 = np.array(Y2)
    Z3 = np.array(Z2)
    for iZ in range(len(Z3)):
        Z3[iZ] = Z3[iZ]

    A = np.array([X3*0+1, (X3**2)*(Y3**2)]).T
    B = Z3.flatten()

    coeff, r, rank, s = np.linalg.lstsq(A, B, rcond=None)
    print '-----------------------------------------------------------------------------------------'
    print 'Benchmark with a 2.4GHz proc - approximately 2 instructions per cycle profiled'
    print 'Least-square for measuredTime - (c+a*nTips^2 * nState^2)'
    print 'Coefficient c = ' + '{0:.2E}'.format(coeff[0]) + ' seconds - ' + '{0:.2E}'.format(coeff[0]*2.4*1e9) + ' clocks'
    print 'Coefficient a = ' + '{0:.2E}'.format(coeff[1]) + ' seconds - ' + '{0:.2E}'.format(coeff[1]*2.4*1e9)  + ' cycles'
    print 'Sum of residuals = ' + '{0:.2E}'.format(r[0])

    #Z4 = np.dot(A, coeff)
    #print Z4
    #ax.scatter(X3, Y3, Z4, 's', s=500, color='k', alpha=1, marker='x')

def benchmarkRes3(name, limX, limY, execName, folderRes3):
    nIter = '100'

    nTips = limX
    nStates = limY

    minTime = {}
    results = {}
    alphas = {}
    alphas2 = {}

    # read
    for iT in nTips:
        for iS in nStates:
            key = repr(iT) + 'T_' + repr(iS) + 'S'
            filename = folderRes3 + 'out_TensorPhylo' + execName + 'Bench_' + nIter + '_' + key + '.txt'
            if not os.path.isfile(filename): continue
            res = readBenchmark(filename)
            results[key] = res
            minTime[key] = np.min(res[1])
            alphas[key] = estimateAlpha(res[3], minTime[key]/res[4])
            alphas2[key] = estimateAlphaGustafson(res[3], minTime[key]/res[4])

    nTips = limX
    nStates = limY

    # plot times
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    Z=[]
    X2 = []
    Y2 = []
    Z2 = []
    for iT in nTips:
        Z.append([])
        for iS in nStates:
            key = repr(iT) + 'T_' + repr(iS) + 'S'
            X2.append(iT)
            Y2.append(iS)
            if key in results:
                Z2.append(results[key][1][-1]/int(nIter))
                Z[-1].append(results[key][1][-1]/int(nIter))
            else:
                Z2.append(None)
                Z[-1].append(None)

    #fig.colorbar(p)
    X, Y = np.meshgrid(nTips, nStates)
    Z = np.array(Z).transpose()

    ax.plot_wireframe(X, Y, Z, alpha=1, linewidth=2, colors='k')
    ax.plot_surface(X, Y, Z, alpha=0.5, linewidth=0, cmap=cm.viridis, antialiased=False, shade=True)
    p = ax.scatter(X2, Y2, Z2, s=150, c=Z2, cmap=cm.viridis, alpha=1)

    #surf = ax.plot_surface(X, Y, Z, linewidth=0, antialiased=False, cmap=cm.cividis)
    fig.colorbar(p, shrink=0.5, aspect=5)
    ax.set_title(name + ' : Runtime for 1 lik')
    ax.set_xlabel('Species number')
    ax.set_ylabel('States number')
    ax.set_zlabel('Second')
    plt.tight_layout()

    modelFitQuadratic(name, X2, Y2, Z2)

    #plot alpha
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    Z=[]
    X2 = []
    Y2 = []
    Z2 = []
    for iT in nTips:
        Z.append([])
        for iS in nStates:
            key = repr(iT) + 'T_' + repr(iS) + 'S'
            X2.append(iT)
            Y2.append(iS)
            if key in results:
                Z2.append(alphas[key][1])
                Z[-1].append(alphas[key][1])
            else:
                Z2.append(None)
                Z[-1].append(None)

    #fig.colorbar(p)
    X, Y = np.meshgrid(nTips, nStates)
    Z = np.array(Z).transpose()
    ax.plot_wireframe(X, Y, Z, alpha=1, linewidth=2, colors='k')
    ax.plot_surface(X, Y, Z, alpha=0.5, linewidth=0, cmap=cm.viridis, antialiased=False, shade=True)
    p = ax.scatter(X2, Y2, Z2, s=150, c=Z2, cmap=cm.viridis, alpha=1)
    #surf = ax.plot_surface(X, Y, Z, linewidth=0, antialiased=False, cmap=cm.cividis)
    fig.colorbar(p, shrink=0.5, aspect=5)
    ax.set_title(name + ' : Sequential code ratio')
    ax.set_xlabel('Species number')
    ax.set_ylabel('States number')
    ax.set_zlabel('Alphas')
    plt.tight_layout()

    # Plot all speedups
    fig, ax = plt.subplots(len(nTips), len(nStates), figsize=(len(nStates)*2,len(nTips)*2), sharey=True)
    plt.title(name + ' : Speedups')
    for i, iT in zip(range(len(nTips)), nTips):
        for j, iS in zip(range(len(nStates)), nStates):
            key = repr(iT) + 'T_' + repr(iS) + 'S'
            if not key in results: continue
            ax[i, j].plot(results[key][3], results[key][3], '-', label='ideal')
            S= 1./(0.2+(0.8)/results[key][3])
            ax[i, j].plot(results[key][3], S, '-', label='Amdhal(0.2)')
            S= 0.5+0.5*results[key][3]
            ax[i, j].plot(results[key][3], S, '--', label='Gufstafson(0.5)')
            ax[i, j].plot(results[key][3], minTime[key]/results[key][4], '-x', label='measured')
            ax[i, j].set_title(key)
    ax[-1, -1].legend()
    plt.tight_layout()

    # Plot all alphas
    fig, ax = plt.subplots(len(nTips), len(nStates), figsize=(len(nStates)*2,len(nTips)*2), sharey=True)
    plt.title(name + ' : Alphas')
    for i, iT in zip(range(len(nTips)), nTips):
        for j, iS in zip(range(len(nStates)), nStates):
            key = repr(iT) + 'T_' + repr(iS) + 'S'
            if not key in results: continue
            ax[i, j].plot(results[key][3][1:], alphas[key][-1], '-x', label='Amdhal')
            ax[i, j].plot(results[key][3][1:], alphas2[key][-1], '--x', label='Gustafson')
            ax[i, j].set_title(key)
            ax[i, j].set_ylim([0., 1.])

    ax[-1, -1].legend()
    plt.tight_layout()

def benchmarkResRB(name, limX, limY, execName, folderResTP, folderResRB):
    nIter = '100'

    nTips = limX
    nStates = limY

    minTime = {}
    results = {}
    alphas = {}
    alphas2 = {}
    resultsRB = {}

    # read TensorPhylo
    for iT in nTips:
        for iS in nStates:
            key = repr(iT) + 'T_' + repr(iS) + 'S'
            filename = folderResTP + 'out_TensorPhylo' + execName + 'Bench_' + nIter + '_' + key + '.txt'
            if not os.path.isfile(filename): continue
            res = readBenchmark(filename)
            results[key] = res

    # read RevBayes
    for iT in nTips:
        for iS in nStates:
            key = repr(iT) + 'T_' + repr(iS) + 'S'
            filename = folderResRB + 'res_' + key + '.txt'
            print filename
            if not os.path.isfile(filename): continue
            res = readBenchmarkRB(filename)
            resultsRB[key] = res

    nTips = limX
    nStates = limY

    # plot times
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    Z=[]
    X2 = []
    Y2 = []
    Z2 = []
    for iT in nTips:
        Z.append([])
        for iS in nStates:
            key = repr(iT) + 'T_' + repr(iS) + 'S'
            X2.append(iT)
            Y2.append(iS)
            if key in results:
                # results[key][1][-1]/int(nIter) ==> is Time(APPROXIMATOR_SEQ_OPTIMIZED)/nIter
                speedup = resultsRB[key][1]/(results[key][1][-1]/int(nIter))


                Z2.append(speedup)
                Z[-1].append(speedup)
            else:
                Z2.append(None)
                Z[-1].append(None)

    #fig.colorbar(p)
    X, Y = np.meshgrid(nTips, nStates)
    Z = np.array(Z).transpose()

    ax.plot_wireframe(X, Y, Z, alpha=1, linewidth=2, colors='k')
    ax.plot_surface(X, Y, Z, alpha=0.5, linewidth=0, cmap=cm.viridis, antialiased=False, shade=True)
    p = ax.scatter(X2, Y2, Z2, s=150, c=Z2, cmap=cm.viridis, alpha=1)

    #surf = ax.plot_surface(X, Y, Z, linewidth=0, antialiased=False, cmap=cm.cividis)
    fig.colorbar(p, shrink=0.5, aspect=5)
    ax.set_title('Speedup over RevBayes with cladogenetic events')
    ax.set_xlabel('Species number')
    ax.set_ylabel('States number')
    ax.set_zlabel('Speedup')
    plt.tight_layout()


if len(sys.argv) != 2:
    print "This script should be called as:\n>python analyze.py ${TensorPhyloRootFolder}"
    exit()

rootFolder = sys.argv[1]

# nTips = [ 50, 100, 250, 500, 750]
# nStates = [ 100, 250, 500, 750]
#
# folderRes3 = rootFolder + '/benchmark/BenchmarkPhyloDB/ResWithClado/'
# name = 'With Clado'
# execName = 'Final'
# benchmarkRes3(name, nTips, nStates, execName, folderRes3)
#
# nTips = [ 50, 100, 250, 500, 750]
# nStates = [ 100, 250, 500, 750]
#
# folderRes5 = rootFolder + '/benchmark/BenchmarkPhyloDB/ResWithoutClado/'
# name = 'Without Clado'
# execName = 'FinalNoClado'
# benchmarkRes3(name, nTips, nStates, execName, folderRes5)

nTips = [ 50, 100, 250, 500, 750]
nStates = [ 100, 250, 500, 750]

folderResTP = rootFolder + '/benchmark/BenchmarkPhyloDB/ResWithClado/'
folderResRB = rootFolder + '/benchmark/BenchmarkPhyloDB/ResWithClado_RB/'
name = 'With Clado'
execName = 'Final'
benchmarkResRB(name, nTips, nStates, execName, folderResTP, folderResRB)


plt.tight_layout()
plt.show()
