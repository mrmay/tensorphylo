#!/bin/bash

nIter=$1
rootDir=$2

TEMP_FOLDER=${rootDir}/Temp
mkdir -p ${TEMP_FOLDER}

RESULT_FOLDER=${rootDir}/benchmark/Results/Results_3
mkdir -p ${RESULT_FOLDER}
rm ${RESULT_FOLDER}/*

BENCHMARK_FOLDER=${rootDir}/benchmark

##################     TEST      ####################
# time ./build_local_omp/TensorPhyloBench --nThread 4 --nexus Benchmark/SimulatedTrees/SIM_500T_250S/tree_1.nex --tsv Benchmark/SimulatedTrees/SIM_500T_250S/data_1.tsv --benchmark 50 --log benchTest.txt
nThread=6
replica=1

for myEx in TensorPhyloFinalBench
do
  for iT in 50 100 250 500 750
  do
    for iS in 100 250 500 750
    do

      nexus=${rootDir}/benchmark/SimulatedTrees/SIM_${iT}T_${iS}S/tree_${replica}.nex
      tsv=${rootDir}/benchmark/SimulatedTrees/SIM_${iT}T_${iS}S/data_${replica}.tsv

      echo "nTips = ${iT} -- nStates = ${iS}"
      echo "Nexus file = ${nexus}"
      echo "TSV file = ${tsv}"

      logFile=${RESULT_FOLDER}/out_${myEx}_${nIter}_${iT}T_${iS}S.txt
      ${rootDir}/${myEx} --nThread ${nThread} --nexus ${nexus} --tsv ${tsv} --benchmark ${nIter} --log ${logFile}
    done
  done
done
