#!/bin/bash


N_ITER=$1
TENSORPHYLO_ROOT=$2

bash ${TENSORPHYLO_ROOT}/benchmark/BenchmarkPhyloDB/RunTest1.sh ${N_ITER} ${TENSORPHYLO_ROOT}
bash ${TENSORPHYLO_ROOT}/benchmark/BenchmarkPhyloDB/RunTest2.sh ${N_ITER} ${TENSORPHYLO_ROOT}
bash ${TENSORPHYLO_ROOT}/benchmark/BenchmarkPhyloDB/RunTest3.sh ${N_ITER} ${TENSORPHYLO_ROOT}
