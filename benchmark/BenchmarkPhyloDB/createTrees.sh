#!/bin/bash

SIM_FOLDER=../SimulatedTrees
mkdir -p ${SIM_FOLDER}

# use it like Rscript validation/simulate_trees.r f n s r (edited)
# where f is the output directory, n is the number of tips in the trees, s is the number of states, and r is the number of replicates

for nTip in 50 100 250 500 750 1000
do
  for nState in 100 250 500 750 1000
  do

    OUT_FOLDER=${SIM_FOLDER}/SIM_${nTip}T_${nState}S
    mkdir -p ${OUT_FOLDER}

    Rscript ../validation/simulate_trees.R ${OUT_FOLDER} ${nTip} ${nState} 5

  done
done
