#!/bin/bash

rootDir=$1

TEMP_FOLDER=${rootDir}/Temp
mkdir -p ${TEMP_FOLDER}

RESULT_FOLDER=${rootDir}/benchmark/Results/Results_RB
mkdir -p ${RESULT_FOLDER}

SCRIPT_FOLDER=${rootDir}/benchmark/Scripts/Scripts_RB
mkdir -p ${SCRIPT_FOLDER}
rm ${SCRIPT_FOLDER}


BENCHMARK_FOLDER=${rootDir}/benchmark

##################     TEST      ####################
# time ./build_local_omp/TensorPhyloBench --nThread 4 --nexus Benchmark/SimulatedTrees/SIM_500T_250S/tree_1.nex --tsv Benchmark/SimulatedTrees/SIM_500T_250S/data_1.tsv --benchmark 50 --log benchTest.txt
replica=1

for iT in 50 100 250 500 750
do
  for iS in 100 250 500 750
  do

    nexus=${rootDir}/benchmark/SimulatedTrees/SIM_${iT}T_${iS}S/tree_${replica}.nex
    tsv=${rootDir}/benchmark/SimulatedTrees/SIM_${iT}T_${iS}S/data_${replica}.tsv
    tsv_rb=${rootDir}/benchmark/SimulatedTrees/SIM_${iT}T_${iS}S/data_${replica}_rb.tsv

    tail -n +2 ${tsv} > ${tsv_rb}

    echo "nTips = ${iT} -- nStates = ${iS}"
    echo "Nexus file = ${nexus}"
    echo "TSV file = ${tsv_rb}"

    MY_SCRIPT=${SCRIPT_FOLDER}/rb_${iT}T_${iS}S.rev
    cp ${rootDir}/benchmark/BenchmarkPhyloDB/script.rev ${MY_SCRIPT}

    sed -i -e "s|MY_TSV|\"${tsv_rb}\"|g" ${MY_SCRIPT}
    sed -i -e "s|MY_NEX|\"${nexus}\"|g" ${MY_SCRIPT}
    sed -i -e "s|MY_NSTATE|${iS}|g" ${MY_SCRIPT}

    ${rootDir}/rb ${MY_SCRIPT}

    cp time.txt ${RESULT_FOLDER}/res_${iT}T_${iS}S.txt

  done
done
