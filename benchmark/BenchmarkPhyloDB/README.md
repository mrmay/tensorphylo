# Benchmark ran on PhyloDB

## Benchmarks
This is the first benchmark conducted to assess TensorPhylo w/ openmp performance with:
* _ResWithClado_ folder contains times measurements over 100 lik. computations for a model with a cladogenetic tensor omega (identity tensor)
* _ResWIthoutClado_ ffolder contains times measurements over 100 lik. computations for a model without cladogenetic tensor omega

## Visualizing the results
> python analyze.py ROOT_FOLDER

where _ROOT_FOLDER_ is the *TensorPhylo* root folder (e.g., _/home/usr/TensorPhylo_)
