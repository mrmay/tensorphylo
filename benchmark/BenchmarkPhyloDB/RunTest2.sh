#!/bin/bash

nIter=$1
rootDir=$2

TEMP_FOLDER=${rootDir}/Temp
mkdir -p ${TEMP_FOLDER}

RESULT_FOLDER=${rootDir}/benchmark/Results/Results_2
mkdir -p ${RESULT_FOLDER}
rm ${RESULT_FOLDER}/*

BENCHMARK_FOLDER=${rootDir}/benchmark

##################     CLADO      ####################
#--param parameters/clado.dat --nexus data/example_clado_tree.nex --tsv data/example_clado_data.tsv -v 1 --benchmark 50
param=${TEMP_FOLDER}/clado.dat
nexus=${rootDir}/data/example_clado_tree.nex
tsv=${rootDir}/data/example_clado_data.tsv

for myEx in TensorPhyloImprovParBench TensorPhyloSeqOMPBench TensorPhyloParOMPBench TensorPhyloSeq2OMPBench TensorPhyloPar2OMPBench
do
  for iA in 0
  do
    for iT in 6
    do

      cp ${BENCHMARK_FOLDER}/BenchmarkPhyloDB/clado.dat ${param}
      sed -i "s/#A/${iA}/g" ${TEMP_FOLDER}/clado.dat
      sed -i "s/#T/${iT}/g" ${TEMP_FOLDER}/clado.dat

      echo "CLADO : ${myEx}_${nIter}_${iA}A_${iT}T"

      logFile=${RESULT_FOLDER}/out_clado_${myEx}_${nIter}_${iA}A_${iT}T.txt
      ${rootDir}/${myEx} --param ${param} --nexus ${nexus} --tsv ${tsv} --benchmark ${nIter} --log ${logFile}
    done
  done
done

##################     HUGE      ####################
#--param parameters/huge_params.dat --nexus data/huge_tree.nex --tsv data/huge_data.tsv --benchmark 50 -v 0
param=${TEMP_FOLDER}/huge.dat
nexus=${rootDir}/data/huge_tree.nex
tsv=${rootDir}/data/huge_data.tsv

for myEx in TensorPhyloImprovParBench TensorPhyloSeqOMPBench TensorPhyloParOMPBench TensorPhyloSeq2OMPBench TensorPhyloPar2OMPBench
do
  for iA in 0
  do
    for iT in 6
    do
      cp ${BENCHMARK_FOLDER}/BenchmarkPhyloDB/huge.dat ${param}
      sed -i "s/#A/${iA}/g" ${TEMP_FOLDER}/huge.dat
      sed -i "s/#T/${iT}/g" ${TEMP_FOLDER}/huge.dat

      echo "HUGE : ${myEx}_${nIter}_${iA}A_${iT}T"

      logFile=${RESULT_FOLDER}/out_huge_${myEx}_${nIter}_${iA}A_${iT}T.txt
      ${rootDir}/${myEx} --param ${param} --nexus ${nexus} --tsv ${tsv} --benchmark ${nIter} --log ${logFile}
    done
  done
done
