#!/bin/bash
calc(){ awk "BEGIN { print $* }"; }

# Read the setting file given as argument
SETTING_FILE=${1}
SIM_FOLDER=${2}
RB_EXEC=${3}
TP_LIB_PATH=${4}
N_THREAD=${5}
BASE_RB_CONVERTER_SCRIPT=${6}
echo ${BASE_RB_CONVERTER_SCRIPT}
SUFFIX=${7}
echo ${SUFFIX}

source ./scripts/readSettings.sh

# folders required
#IFS='_' read -ra SUFFIX <<< "${TP_LIB_PATH}"

MY_NAME=RB_TP_${N_THREAD}T_${SUFFIX}
echo $MY_NAME
TEMP_FOLDER=./tmp/${runName}
mkdir -p ${TEMP_FOLDER}
TP_LOCK_FILE=${TEMP_FOLDER}/LOCK_${MY_NAME}_${runName}.txt

PARAM_FOLDER=./data/params${MY_NAME}/${runName}
#BASE_RB_CONVERTER_SCRIPT=scripts/baseScriptRB_TP_Par_absorbing.Rev
mkdir -p ${PARAM_FOLDER}

RESULT_FOLDER=./results/${MY_NAME}/${runName}
mkdir -p ${RESULT_FOLDER}

OUTPUT_FOLDER=./results/${runName}
mkdir -p ${OUTPUT_FOLDER}
outputFile=${OUTPUT_FOLDER}/time${MY_NAME}.txt

N_TEST_TIMEOUT=2
TIME_FOR_TEST=$(( 15 + ${N_TEST_TIMEOUT} * ${timeout} ))

DS_K=64

# loop through settings
echo "nTips nStates iParam nIter avgChromNb loglik error" > ${outputFile}

for t in "${nTips[@]}"
do

  for p in `seq 0 $((${#cladoNoChange[@]}-1))`
  do

    dataFile="${SIM_FOLDER}/SIM_${t}T_${DS_K}S/data_${p}P.nex"
    maxNChromosomes=`python scripts/defineMaxNChromosome.py ${dataFile}`

    for s in "$(( ${maxNChromosomes} + 10 )) ${nStates[@]}"
    do
      if [ -f ${TP_LOCK_FILE} ]
      then
        rm ${TP_LOCK_FILE}
      fi

      name="${runName}_${t}T_${s}S_${p}SIM_${nIter}M"
      echo $name

: << "END"
      if [ ! -f ${TP_LOCK_FILE} ]
      then

        treeFile="${SIM_FOLDER}/SIM_${t}T_${DS_K}S_0SIM/tree.nex"
        dataFile="${SIM_FOLDER}/SIM_${t}T_${DS_K}S_0SIM/data.nex"
        outFile=${TEMP_FOLDER}/dummyTP.txt

        # Use RB to dump the parameters.dat file
        RB_SCRIPT=${PARAM_FOLDER}/${name}.Rev
        cp ${BASE_RB_CONVERTER_SCRIPT} ${RB_SCRIPT}

        sed -i -e "s|TP_LIB_PATH|${TP_LIB_PATH}|g" ${RB_SCRIPT}
        sed -i -e "s|TREE_FILE|${treeFile}|g" ${RB_SCRIPT}
        sed -i -e "s|DATA_FILE|${dataFile}|g" ${RB_SCRIPT}
        sed -i -e "s|N_STATE|${s}|g" ${RB_SCRIPT}
        sed -i -e "s|PARAM_ETA_GAMMA|${etaGamma[$p]}|g" ${RB_SCRIPT}
        sed -i -e "s|PARAM_ETA_DELTA|${etaDelta[$p]}|g" ${RB_SCRIPT}
        sed -i -e "s|PARAM_ETA_RHO|${etaRho[$p]}|g" ${RB_SCRIPT}
        sed -i -e "s|PARAM_CLADO_NO_CHANGE|${cladoNoChange[$p]}|g" ${RB_SCRIPT}
        sed -i -e "s|PARAM_CLADO_FISSION|${cladoFission[$p]}|g" ${RB_SCRIPT}
        sed -i -e "s|PARAM_CLADO_FUSION|${cladoFusion[$p]}|g" ${RB_SCRIPT}
        sed -i -e "s|PARAM_CLADO_POLYPLOID|${cladoPolyploid[$p]}|g" ${RB_SCRIPT}
        sed -i -e "s|PARAM_MU|${mu[${p}]}|g" ${RB_SCRIPT}
        sed -i -e "s|N_ITER|${N_TEST_TIMEOUT}|g" ${RB_SCRIPT}
        sed -i -e "s|K_SIMU|${DS_K}|g" ${RB_SCRIPT}
        sed -i -e "s|N_PROC|${N_THREAD}|g" ${RB_SCRIPT}
        sed -i -e "s|OUT_FILE|${outFile}|g" ${RB_SCRIPT}

        # First we try with 5 liks for timeout
        # If the lock is not there after this test we benchmark
        # The lock stay active until when change of tree
        timeout ${TIME_FOR_TEST}s bash <<EOT
          touch ${TP_LOCK_FILE}
          ${RB_EXEC} ${RB_SCRIPT} 1> /dev/null
          rm ${TP_LOCK_FILE}
EOT

      fi
END

      # If the lock is not there: benchmark
      if [ ! -f ${TP_LOCK_FILE} ]
      then

        treeFile="${SIM_FOLDER}/SIM_${t}T_${DS_K}S/tree.nex"
        dataFile="${SIM_FOLDER}/SIM_${t}T_${DS_K}S/data_${p}P.nex"
        echo $dataFile

        outFile="${RESULT_FOLDER}/${name}.txt"

        # Use RB to dump the parameters.dat file
        RB_SCRIPT=${PARAM_FOLDER}/${name}.Rev
        cp ${BASE_RB_CONVERTER_SCRIPT} ${RB_SCRIPT}

        sed -i -e "s|TP_LIB_PATH|${TP_LIB_PATH}|g" ${RB_SCRIPT}
        sed -i -e "s|TREE_FILE|${treeFile}|g" ${RB_SCRIPT}
        sed -i -e "s|DATA_FILE|${dataFile}|g" ${RB_SCRIPT}
        sed -i -e "s|N_STATE|${s}|g" ${RB_SCRIPT}
        sed -i -e "s|PARAM_ETA_GAMMA|${etaGamma[$p]}|g" ${RB_SCRIPT}
        sed -i -e "s|PARAM_ETA_DELTA|${etaDelta[$p]}|g" ${RB_SCRIPT}
        sed -i -e "s|PARAM_ETA_RHO|${etaRho[$p]}|g" ${RB_SCRIPT}
        sed -i -e "s|PARAM_CLADO_NO_CHANGE|${cladoNoChange[$p]}|g" ${RB_SCRIPT}
        sed -i -e "s|PARAM_CLADO_FISSION|${cladoFission[$p]}|g" ${RB_SCRIPT}
        sed -i -e "s|PARAM_CLADO_FUSION|${cladoFusion[$p]}|g" ${RB_SCRIPT}
        sed -i -e "s|PARAM_CLADO_POLYPLOID|${cladoPolyploid[$p]}|g" ${RB_SCRIPT}
        sed -i -e "s|PARAM_MU|${mu[${p}]}|g" ${RB_SCRIPT}
        sed -i -e "s|N_ITER|${nIter}|g" ${RB_SCRIPT}
        sed -i -e "s|K_SIMU|${DS_K}|g" ${RB_SCRIPT}
        sed -i -e "s|N_PROC|${N_THREAD}|g" ${RB_SCRIPT}
        sed -i -e "s|OUT_FILE|${outFile}|g" ${RB_SCRIPT}

        ${RB_EXEC} ${RB_SCRIPT} # 1> /dev/null

        # approximator timeInit timeComp  lik int steps
        line=`cat ${outFile}`
        words=($line)

        #echo "calc ${words[3]} / ${nIter}"
        avgChrom=${words[0]}
        lastLogLik=${words[1]}

        echo "\"${t}\" \"${s}\" \"${p}\" \"${nIter}\" \"${avgChrom}\" \"${lastLogLik}\" \"\"" >> ${outputFile}

      # otherwise no result
      else

        echo "\"${t}\" \"${s}\" \"${p}\" \"${nIter}\" \"NA\" \"NA\" \"timeout ${timeout} seconds\"" >> ${outputFile}

      fi

    done
  done
done
