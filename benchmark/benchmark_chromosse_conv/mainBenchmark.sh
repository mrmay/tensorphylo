#!/bin/bash
SETTING_FILE=${1}

REVBAYES_BIN=rb_current
BASE_LIB_PATH=/home/meyerx/Projets/TensorPhyloProto/benchmark/benchmark_chromosse_conv

source ./scripts/readSettings.sh

echo "Starting benchmark '${runName}' with settings:"
echo "-----------------------------------------------"
# settings
echo "Number of iterations: ${nIter}"
echo "Number of tips: ${nTips[@]}"
echo "Number of states: ${nStates[@]}"
echo "etaGamma values: ${etaGamma[@]}"
echo "etaDelta values: ${etaDelta[@]}"
echo "etaRho values: ${etaRho[@]}"
echo "cladoNoChange values: ${cladoNoChange[@]}"
echo "cladoFission values: ${cladoFission[@]}"
echo "cladoFusion values: ${cladoFusion[@]}"
echo "cladoPolyploid values: ${cladoPolyploid[@]}"
echo "mu values: ${mu[@]}"
echo "Timeout: ${timeout}"

SIM_FOLDER="`pwd`/SimulatedData/${runName}"

if [ ! -d ${SIM_FOLDER} ]; then
  echo "-----------------------------------------------"
  echo "Simulating the datasets"
  bash scripts/createData.sh ${SETTING_FILE} ${SIM_FOLDER} ${REVBAYES_BIN}
  echo "Done"
fi

echo "-----------------------------------------------"

N_PROC=4
echo "Benchmarking RB + TP with ${N_PROC} threads and approximator ${MY_LIB}"
SUFFIX=withA
SCRIPT=scripts/baseScriptRB_TP_Par_absorbing.Rev
bash ./RunRB_TP.sh ${SETTING_FILE} ${SIM_FOLDER} ${REVBAYES_BIN} ${BASE_LIB_PATH}/lib_AT ${N_PROC} ${SCRIPT} ${SUFFIX} > log/${runName}_RB_TP_AT_WA.log

N_PROC=4
echo "Benchmarking RB + TP with ${N_PROC} threads and approximator ${MY_LIB}"
SUFFIX=withoutA
SCRIPT=scripts/baseScriptRB_TP_Par.Rev
bash ./RunRB_TP.sh ${SETTING_FILE} ${SIM_FOLDER} ${REVBAYES_BIN} ${BASE_LIB_PATH}/lib_AT ${N_PROC} ${SCRIPT} ${SUFFIX} > log/${runName}_RB_TP_AT_WOA.log
