#!/usr/bin/python
import os
import sys
import numpy as np

dataFile = sys.argv[1]

iFile = open(dataFile, 'r')
data = []
for line in iFile:
    words = line.split('\t')
    data.append(int(words[1]))

print np.max(data)
