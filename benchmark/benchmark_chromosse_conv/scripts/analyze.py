#!/usr/bin/python
import os
import sys
import dendropy
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from palettable.colorbrewer.diverging import RdBu_8
from palettable.colorbrewer.qualitative import Set1_8, Dark2_8


myPalette = Dark2_8

REF_IMPL_LL = 'Analytical'

DPI = 100
LINE_WIDTH=2.5

font = {'family' : 'DejaVu Sans',
    'weight' : 'normal',
    'size'   : 10}

plt.rc('font', **font)
#plt.rc('text', usetex=True)
plt.rcParams['svg.fonttype'] = 'none'
plt.rcParams['lines.linewidth'] = LINE_WIDTH


def readSettings(filename):
    inFile = file(filename, 'r')

    runName = ''
    nIter = timeout = 0
    nTips = nStates = vecLambda = vecMu = []

    settings = {}

    line = inFile.readline().strip()
    while line:
        nextLine = inFile.readline().strip()
        if line == "runName":
            settings[line] = nextLine
        elif line == "nIter":
            settings[line] = int(nextLine)
        elif line == "nTips":
            words = nextLine.split(' ')
            settings[line] = [int(w) for w in words]
        elif line == "nStates":
            words = nextLine.split(' ')
            settings[line] = [int(w) for w in words]
        elif line == "etaGamma":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "etaRho":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "etaDelta":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "cladoNoChange":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "cladoFission":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "cladoFusion":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "cladoPolyploid":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "mu":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "timeout":
            settings[line] = int(nextLine)

        #Read next line:
        line = inFile.readline().strip()

    return settings


def readBenchmark(impl, filename):
    dFrame = pd.read_csv(filename, sep=' ')
    dFrame = dFrame.drop(['error'], axis=1)
    dFrame = dFrame.drop(['nIter'], axis=1)
    nStepsImpl = 'nSteps{}'.format(impl)
    timeImpl = 'time{}'.format(impl)
    llImpl = 'll{}'.format(impl)
    dFrame = dFrame.rename(columns={'meanTime' : timeImpl, 'loglik' : llImpl, 'nStepsInt' : nStepsImpl})

    return dFrame

def mergeDFrame(impls, dFrames):

    if len(impls) == 1:
        return dFrames[impls[0]]
    else:
        df = pd.merge(dFrames[impls[0]], dFrames[impls[1]], on=['nTips', 'nStates', 'iParam', 'avgChromNb'], how = 'outer')

        for i in range(2, len(impls)):
            df = pd.merge(df, dFrames[impls[i]], on=['nTips', 'nStates', 'iParam', 'avgChromNb'], how = 'outer')

    return df

def readData(filename):
    iFile = open(filename, 'r')
    data = []
    for line in iFile:
        words = line.split('\t')
        data.append(int(words[1]))

    return data

def plotConvergence(settings, implementations, df, figureFolder, isLog, isZoom):

    X_WIDTH = 300*len(settings['mu'])
    Y_WIDTH = 250*len(settings['nTips'])


    if not isLog:
        fig, ax = plt.subplots(len(settings['nTips']), len(settings['mu']), figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=False, sharex=True, sharey=False)
    else:
        fig, ax = plt.subplots(len(settings['nTips']), len(settings['mu']), figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=False, sharex=False, sharey=False)

    for iT in range(len(settings['nTips'])):
        for iP in range(len(settings['mu'])):

            dataFile = 'SimulatedData/{}/SIM_{}T_64S/data_{}P.nex'.format(settings['runName'], settings['nTips'][iT], iP)
            data = readData(dataFile)

            if not isLog:
                #ax[iT, iP].axvline(8, color=Set2_8.hex_colors[0], lw=1.5, ls='-.', alpha=1)
                ax[iT, iP].axvline(np.max(data), color=Set1_8.hex_colors[0], lw=1.5, ls='-.', alpha=1)
                ax[iT, iP].axvline(np.max(data)+10, color=Set1_8.hex_colors[1], lw=1.5, ls='-', alpha=1)
                ax[iT, iP].axvline((np.max(data))*2, color=Set1_8.hex_colors[2], lw=1.5, ls='dashed', alpha=1)
                ax[iT, iP].axvline((np.max(data))*4, color=Set1_8.hex_colors[3], lw=1.5, ls='dashed', alpha=1)
            else:
                #ax[iT, iP].axvline(np.log2(8), color=Set2_8.hex_colors[0], lw=1.5, ls='-.', alpha=1)
                ax[iT, iP].axvline(np.log2(np.max(data)), color=Set1_8.hex_colors[0], lw=1.5, ls='-.', alpha=1)
                ax[iT, iP].axvline(np.log2(np.max(data)+10), color=Set1_8.hex_colors[1], lw=1.5, ls='-', alpha=1)
                ax[iT, iP].axvline(np.log2((np.max(data))*2), color=Set1_8.hex_colors[2], lw=1.5, ls='dashed', alpha=1)
                ax[iT, iP].axvline(np.log2((np.max(data))*4), color=Set1_8.hex_colors[3], lw=1.5, ls='dashed', alpha=1)
            #print mean, ", ", std
            #ax[iT, iP].axvline(mean, color='k', ls='dotted')


            #ax[iT, iP].axvline(mean+std)
            minLogLik = 0
            for iI, impl in zip(range(len(implementations)), implementations):

                #for s in settings['nStates']:
                res = df.loc[(df['nTips'] == settings['nTips'][iT]) & (df['iParam'] == iP)]
                res = res.sort_values(by=['nStates'])
                #print t, " ", settings['lambda'][iP], " ", settings['sigma'][iS]
                #print "RES: ", res

                styles = ['-', '--x', 'o', 's', 'd']

                x = (res['nStates'])
                if isLog:
                    x = np.log2(x)
                ax[iT, iP].plot(x, res['ll{}'.format(impl)], '-x', color=Dark2_8.hex_colors[iI])
                minLogLik = np.min(res['ll{}'.format(impl)])


                if iT == len(settings['nTips'])-1:
                    ax[iT, iP].set_xlabel('Number of states\n Parameters = {}'.format(iP))
                if iP == 0:
                    ax[iT, iP].set_ylabel('Log likelihood\nTips = {}'.format(settings['nTips'][iT]))

            if isZoom:
                ax[iT, iP].set_ylim([(1/0.9999)*minLogLik, 0.9995*minLogLik])

    plt.legend(['max Nb chrom', 'max NB chrom + 10', 'max NB chrom x 2', 'max NB chrom x 4'] + implementations)

    suffix = 'def'
    if isLog:
        suffix = 'log'

    if isZoom:
        suffix += '_z'

    plt.tight_layout()

    plt.savefig('{}/conv_{}_{}.png'.format(figureFolder, settings['runName'], suffix))
    plt.savefig('{}/conv_{}_{}.svg'.format(figureFolder, settings['runName'], suffix))



settingFile = sys.argv[1]
figureFolder = sys.argv[2]
settings = readSettings(settingFile)

figureFolder += '/' + settings['runName']

if not os.path.isdir(figureFolder) :
    os.makedirs(figureFolder)


implementations = ['WithAbs_NBin', 'WithoutAbs_NBin']
#implementations = ['Diversitree', 'TensorPhylo', 'RevBayes', 'RevBayesWithTP', 'Castor']

dFrames = {}
for impl in implementations:
    resultFile = './results/' + settings["runName"] + '/time' + impl + '.txt'
    print resultFile
    dFrames[impl] = readBenchmark(impl, resultFile)

df = mergeDFrame(implementations, dFrames)

plotConvergence(settings, implementations, df, figureFolder, False, False)
plotConvergence(settings, implementations, df, figureFolder, True, False)
plotConvergence(settings, implementations, df, figureFolder, True, True)


plt.show()
