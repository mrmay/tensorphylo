
#################
# read the data #
#################


tree = readTrees("TREE_FILE")[1]
age  = tree.rootAge()
taxa = tree.taxa()

k    = N_STATE
###############
# rate matrix #
###############

gamma <- PARAM_ETA_GAMMA #0.2 # gains
delta <- PARAM_ETA_DELTA #0.2 # losses
rho   <- PARAM_ETA_RHO # 0.1 # duplication

# this matrix includes 0 in the state-space
Q_chromo := fnChromosomes(k, gamma, delta, rho)
#Q_chromo
########################
# cladogenetic changes #
########################

clado_no_change <- abs(PARAM_CLADO_NO_CHANGE) # 1.0)
clado_fission   <- abs(PARAM_CLADO_FISSION) # 0.1)
clado_fusion    <- abs(PARAM_CLADO_FUSION) # 0.2)
clado_polyploid <- abs(PARAM_CLADO_POLYPLOID) # 0.5)
clado_demipoly  <- abs(0.) # 0.0)

# set a vector to hold the speciation rates
speciation_rates := [clado_no_change, clado_fission, clado_fusion, clado_polyploid, clado_demipoly]
total_speciation := sum(speciation_rates)

ne = 0
clado_events[++ne] := [1,1,1]
clado_probs[ne]    := Probability(1.0)

# also specify the speciation rates
lambda[1] := 0

idx = 0

# right now no demipolyploidy
for(i in 1:k) { # this index is the number of chromosomes

	idx = i # because tensorphylo will drop the indexing by one

	if (i == 1) {

		# compute the total rate
		lambda[idx] := abs(clado_no_change + clado_fission + clado_polyploid)

		# no change
		clado_events[++ne] := [idx, idx, idx]
		clado_probs[ne]    := Probability(clado_no_change / lambda[idx])

		# increase by one
		clado_events[++ne] := [idx, idx + 1, idx]
		clado_probs[ne]    := Probability(0.5 * (clado_fission + clado_polyploid) / lambda[idx])
		clado_events[++ne] := [idx, idx, idx + 1]
		clado_probs[ne]    := Probability(0.5 * (clado_fission + clado_polyploid) / lambda[idx])

	} else if ( i + i <= k ) { # polyploidization allowed

		# compute the total rate
		lambda[idx] := abs(clado_no_change + clado_fission + clado_fusion + clado_polyploid)

		# no change
		clado_events[++ne] := [idx, idx, idx]
		clado_probs[ne]    := Probability(clado_no_change / lambda[idx])

		# increase by one
		clado_events[++ne] := [idx, idx + 1, idx]
		clado_probs[ne]    := Probability(0.5 * clado_fission / lambda[idx])
		clado_events[++ne] := [idx, idx, idx + 1]
		clado_probs[ne]    := Probability(0.5 * clado_fission / lambda[idx])

		# decrease by one
		clado_events[++ne] := [idx, idx - 1, idx]
		clado_probs[ne]    := Probability(0.5 * clado_fusion / lambda[idx])
		clado_events[++ne] := [idx, idx, idx - 1]
		clado_probs[ne]    := Probability(0.5 * clado_fusion / lambda[idx])

		# polyploidization
		clado_events[++ne] := [idx, i + i , idx]
		clado_probs[ne]    := Probability(0.5 * clado_polyploid / lambda[idx])
		clado_events[++ne] := [idx, idx, i + i]
		clado_probs[ne]    := Probability(0.5 * clado_polyploid / lambda[idx])

	} else if ( i < k ) { # fissuion but no polyploidy

		# compute the total rate
		lambda[idx] := abs(clado_no_change + clado_fission + clado_fusion)

		# no change
		clado_events[++ne] := [idx, idx, idx]
		clado_probs[ne]    := Probability(clado_no_change / lambda[idx])

		# increase by one
		clado_events[++ne] := [idx, idx + 1, idx]
		clado_probs[ne]    := Probability(0.5 * clado_fission / lambda[idx])
		clado_events[++ne] := [idx, idx, idx + 1]
		clado_probs[ne]    := Probability(0.5 * clado_fission / lambda[idx])

		# decrease by one
		clado_events[++ne] := [idx, idx - 1, idx]
		clado_probs[ne]    := Probability(0.5 * clado_fusion / lambda[idx])
		clado_events[++ne] := [idx, idx, idx - 1]
		clado_probs[ne]    := Probability(0.5 * clado_fusion / lambda[idx])

	} else { # maximum state -- no fission or polyploidy

		# compute the total rate
		lambda[idx] := abs(clado_no_change + clado_fusion)

		# no change
		clado_events[++ne] := [idx, idx, idx]
		clado_probs[ne]    := Probability(clado_no_change / lambda[idx])

		# decrease by one
		clado_events[++ne] := [idx, idx - 1, idx]
		clado_probs[ne]    := Probability(0.5 * clado_fusion / lambda[idx])
		clado_events[++ne] := [idx, idx, idx - 1]
		clado_probs[ne]    := Probability(0.5 * clado_fusion / lambda[idx])

	}

}

omega := fnCladogeneticProbabilityMatrix(clado_events, clado_probs, k+1)
#omega
####################
# other parameters #
####################
paramRF <- rep(0, k + 1)
paramRF[8] <- 1.0
#paramRF

root_frequencies <- simplex(paramRF)
root_frequencies
#########
# CDBDP #
#########

x ~ dnPhyloCTMCClado(
            tree               = tree,
            Q                  = Q_chromo,
            cladoProbs         = omega,
            rootFrequencies    = root_frequencies,
            branchRates        = 1.0,
            pInv               = 0.0,
            nSites             = 1,
            type               = "NaturalNumbers"
            )

while(x.getEmpiricalBaseFrequencies()[1] > 0.) {
	root_frequencies
	x.redraw()
}
x.getEmpiricalBaseFrequencies()

writeCharacterDataDelimited(x, filename="DATA_FILE")

q()
