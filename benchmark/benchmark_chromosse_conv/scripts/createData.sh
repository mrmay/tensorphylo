#!/bin/bash

SETTING_FILE=${1}
SIM_FOLDER=${2}
RB_EXEC=${3}

# benchmark setting
source ./scripts/readSettings.sh

p=0

s="64"

for t in "${nTips[@]}"
do

  OUT_FOLDER="${SIM_FOLDER}/SIM_${t}T_${s}S"
  treeFile="${OUT_FOLDER}/tree.nex"

  mkdir -p ${OUT_FOLDER}

  lambda=`bc <<< "${cladoFission[$p]} + ${cladoFusion[$p]} + ${cladoPolyploid[$p]}"`

  echo "Rscript ./scripts/simulate_trees.R ${OUT_FOLDER} ${t} ${s} ${lambda}"
  Rscript ./scripts/simulate_trees.R ${OUT_FOLDER} ${t} ${s} ${lambda}
  mv ${OUT_FOLDER}/tree_1.nex $treeFile

  for p in `seq 0 $((${#etaGamma[@]}-1))`
  do

    if [ $t -ge "128" ] && [ $s -gt "128" ]
    then
      continue
    fi

    dataFile="${OUT_FOLDER}/data_${p}P.nex"

    RB_SCRIPT=${OUT_FOLDER}/simu.Rev
    cp ./scripts/simulateData.Rev ${RB_SCRIPT}

    sed -i -e "s|N_TIPS|${t}|g" ${RB_SCRIPT}
    sed -i -e "s|N_STATE|${s}|g" ${RB_SCRIPT}
    sed -i -e "s|TREE_FILE|${treeFile}|g" ${RB_SCRIPT}
    sed -i -e "s|DATA_FILE|${dataFile}|g" ${RB_SCRIPT}
    sed -i -e "s|PARAM_ETA_GAMMA|${etaGamma[$p]}|g" ${RB_SCRIPT}
    sed -i -e "s|PARAM_ETA_DELTA|${etaDelta[$p]}|g" ${RB_SCRIPT}
    sed -i -e "s|PARAM_ETA_RHO|${etaRho[$p]}|g" ${RB_SCRIPT}
    sed -i -e "s|PARAM_CLADO_NO_CHANGE|${cladoNoChange[$p]}|g" ${RB_SCRIPT}
    sed -i -e "s|PARAM_CLADO_FISSION|${cladoFission[$p]}|g" ${RB_SCRIPT}
    sed -i -e "s|PARAM_CLADO_FUSION|${cladoFusion[$p]}|g" ${RB_SCRIPT}
    sed -i -e "s|PARAM_CLADO_POLYPLOID|${cladoPolyploid[$p]}|g" ${RB_SCRIPT}
    sed -i -e "s|PARAM_MU|${mu[$p]}|g" ${RB_SCRIPT}

    ${RB_EXEC} ${RB_SCRIPT} # 1> /dev/null

  done

done
