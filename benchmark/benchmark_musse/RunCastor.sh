#!/bin/bash
calc(){ awk "BEGIN { print $* }"; }

# Read the setting file given as argument
SETTING_FILE=${1}
SIM_FOLDER=${2}

source ../utils/readSettings.sh

# folders required
TEMP_FOLDER=./tmp/${runName}
mkdir -p ${TEMP_FOLDER}
CAS_LOCK_FILE=${TEMP_FOLDER}/LOCK_CAS_${runName}.txt
DUMMY_OUT_FILE=${TEMP_FOLDER}/dummyOutCastor.txt

OUTPUT_FOLDER=./results/${runName}
mkdir -p ${OUTPUT_FOLDER}
outputFile=${OUTPUT_FOLDER}/timeCastor.txt

N_TEST_TIMEOUT=5
TIME_FOR_TEST=$(( 15 + ${N_TEST_TIMEOUT} * ${timeout} ))

# loop through settings
echo "nTips nStates maxLambda maxMu nIter meanTime loglik error" > ${outputFile}

for t in "${nTips[@]}"
do

  if [ -f ${CAS_LOCK_FILE} ]
  then
    rm ${CAS_LOCK_FILE}
  fi

  for s in "${nStates[@]}"
  do
    for p in `seq 0 $((${#lambda[@]}-1))`
    do

      name="${runName}_${t}T_${s}S_${p}P_${nIter}M"
      echo $name

      treeFile="${SIM_FOLDER}/SIM_${t}T_${s}S_${p}P/tree_1.nex"
      dataFile="${SIM_FOLDER}/SIM_${t}T_${s}S_${p}P/data_1.tsv"

      l=${lambda[$p]}
      m=${mu[$p]}

      # First we try with 5 liks for timeout
      # If the lock is not there after this test we benchmark
      # The lock stay active until when change of tree
      if [ ! -f ${CAS_LOCK_FILE} ]
      then

        timeout ${TIME_FOR_TEST}s bash <<EOT
          touch ${CAS_LOCK_FILE}
          Rscript scripts/scriptCastor.R ${s} ${t} ${l} ${m} ${N_TEST_TIMEOUT} ${treeFile} ${dataFile} ${DUMMY_OUT_FILE}
          rm ${CAS_LOCK_FILE}
EOT

      fi

      # If the lock is not there: benchmark
      if [ ! -f ${CAS_LOCK_FILE} ]
      then

        Rscript scripts/scriptCastor.R ${s} ${t} ${l} ${m} ${nIter} ${treeFile} ${dataFile} ${outputFile}

      # otherwise no result
      else

        echo "\"${t}\" \"${s}\" \"${l}\" \"${m}\" \"${nIter}\" \"NA\" \"NA\" \"timeout ${timeout} seconds\"" >> ${outputFile}

      fi

    done
  done
done
