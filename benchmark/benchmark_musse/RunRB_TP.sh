#!/bin/bash
calc(){ awk "BEGIN { print $* }"; }

# Read the setting file given as argument
SETTING_FILE=${1}
REVBAYES_BIN=${2}
TENSOR_PHYLO_LIB_FOLDER=${3}
SIM_FOLDER=${4}

source ../utils/readSettings.sh

# folders required
TEMP_FOLDER=./tmp/${runName}
mkdir -p ${TEMP_FOLDER}

RB_TP_LOCK_FILE=${TEMP_FOLDER}/LOCK_RB_WITH_TP_${runName}.txt

RESULT_FOLDER=./results/RB_TP/${runName}
mkdir -p ${RESULT_FOLDER}

SCRIPT_FOLDER=./data/parametersRB_TP/${runName}
BASE_SCRIPT_RB_TP=./scripts/scriptTP.rev
mkdir -p ${SCRIPT_FOLDER}

OUTPUT_FOLDER=./results/${runName}
mkdir -p ${OUTPUT_FOLDER}
outputFile=${OUTPUT_FOLDER}/timeRevBayesWithTP.txt

N_TEST_TIMEOUT=5
TIME_FOR_TEST=$(( 15 + ${N_TEST_TIMEOUT} * ${timeout} ))

# loop through settings
echo "nTips nStates maxLambda maxMu nIter meanTime loglik error" > ${outputFile}

for t in "${nTips[@]}"
do

  if [ -f ${RB_TP_LOCK_FILE} ]
  then
    rm ${RB_TP_LOCK_FILE}
  fi

  for s in "${nStates[@]}"
  do
    for p in `seq 0 $((${#lambda[@]}-1))`
    do

      name="${runName}_${t}T_${s}S_${p}P_${nIter}M"
      echo $name

      treeFile="${SIM_FOLDER}/SIM_${t}T_${s}S_${p}P/tree_1.nex"

      dataFileTP="${SIM_FOLDER}/SIM_${t}T_${s}S_${p}P/data_1_tp.tsv"
      dataFileRB="${SIM_FOLDER}/SIM_${t}T_${s}S_${p}P/data_1_rb.tsv"

      tail -n +2 ${dataFileTP} > ${dataFileRB}

      # If the lock is not there we run the benchmark
      # If the benchmark is interrupted the lock stays active
      # The lock is only cleand when we move to a new tree
      if [ ! -f ${RB_TP_LOCK_FILE} ]
      then

        MY_SCRIPT=${TEMP_FOLDER}/script_RB_TP.rev
        cp ${BASE_SCRIPT_RB_TP} ${MY_SCRIPT}

        OUT_FILE=${TEMP_FOLDER}/oufile_RB_TP.txt

        l=${lambda[$p]}
        m=${mu[$p]}

        sed -i -e "s|TREE_FILE|${treeFile}|g" ${MY_SCRIPT}
        sed -i -e "s|DATA_FILE|${dataFileRB}|g" ${MY_SCRIPT}
        sed -i -e "s|MAX_LAMBDA|${l}|g" ${MY_SCRIPT}
        sed -i -e "s|MAX_MU|${m}|g" ${MY_SCRIPT}
        sed -i -e "s|N_ITER|${N_TEST_TIMEOUT}|g" ${MY_SCRIPT}
        sed -i -e "s|MY_NSTATE|${s}|g" ${MY_SCRIPT}
        sed -i -e "s|OUT_FILE|${OUT_FILE}|g" ${MY_SCRIPT}
        sed -i -e "s|LIBRARY_PATH|${TENSOR_PHYLO_LIB_FOLDER}|g" ${MY_SCRIPT}

        timeout ${TIME_FOR_TEST}s bash <<EOT
          touch ${RB_TP_LOCK_FILE}
          ${REVBAYES_BIN} ${MY_SCRIPT}
          rm ${RB_TP_LOCK_FILE}
EOT

      fi

      # If the lock is not there: benchmark
      if [ ! -f ${RB_TP_LOCK_FILE} ]
      then

        MY_SCRIPT=${SCRIPT_FOLDER}/${name}.rev
        cp ${BASE_SCRIPT_RB_TP} ${MY_SCRIPT}

        OUT_FILE="${RESULT_FOLDER}/${name}.txt"

        l=${lambda[$p]}
        m=${mu[$p]}

        sed -i -e "s|TREE_FILE|${treeFile}|g" ${MY_SCRIPT}
        sed -i -e "s|DATA_FILE|${dataFileRB}|g" ${MY_SCRIPT}
        sed -i -e "s|MAX_LAMBDA|${l}|g" ${MY_SCRIPT}
        sed -i -e "s|MAX_MU|${m}|g" ${MY_SCRIPT}
        sed -i -e "s|N_ITER|${nIter}|g" ${MY_SCRIPT}
        sed -i -e "s|MY_NSTATE|${s}|g" ${MY_SCRIPT}
        sed -i -e "s|OUT_FILE|${OUT_FILE}|g" ${MY_SCRIPT}
        sed -i -e "s|LIBRARY_PATH|${TENSOR_PHYLO_LIB_FOLDER}|g" ${MY_SCRIPT}

        ${REVBAYES_BIN} ${MY_SCRIPT}

        avgTime=`cat ${OUT_FILE}`

        echo "\"${t}\" \"${s}\" \"${l}\" \"${m}\" \"${nIter}\" \"${avgTime}\" \"NA\" \"\"" >> ${outputFile}

      # otherwise no result
      else

        echo "\"${t}\" \"${s}\" \"${l}\" \"${m}\" \"${nIter}\" \"NA\" \"NA\" \"timeout ${timeout} seconds\"" >> ${outputFile}

      fi

    done
  done
done
