#!/bin/bash
SETTING_FILE=${1}
METHODS=${2}

REVBAYES_BIN=rb_current
N_PROC=1

TENSOR_PHYLO_BIN=./bin/TensorPhyloFinal
TENSOR_PHYLO_LIB_FOLDER=/home/meyerx/Projets/TensorPhylo/build_local/Lib

source ../utils/readSettings.sh

echo "Starting benchmark '${runName}' with settings:"
echo "-----------------------------------------------"
# settings
echo "Number of iterations: ${nIter}"
echo "Number of tips: ${nTips[@]}"
echo "Number of states: ${nStates[@]}"
echo "Lambda values: ${lambda[@]}"
echo "Mu values: ${mu[@]}"
echo "Timeout: ${timeout}"


SIM_FOLDER="`pwd`/SimulatedTrees/${runName}"
if [ ! -d ${SIM_FOLDER} ]; then
  echo "-----------------------------------------------"
  echo "Simulating the datasets"
  bash scripts/createTrees.sh ${SETTING_FILE} ${SIM_FOLDER}
  echo "Done"
fi

echo "-----------------------------------------------"

# launch the benchmarks
if [ "$METHODS" == "all" ]; then
  echo "All methods will be benchmarked..."
  echo "Benchmarking diversitree"
  bash ./RunDiversitree.sh ${SETTING_FILE} ${SIM_FOLDER} > log/Diversitree.txt &

  N_THREADS=1
  I_APPROX=2
  echo "Benchmarking TensorPhylo ${I_APPROX}A ${N_THREADS}T"
  bash ./RunTP.sh ${SETTING_FILE} ${TENSOR_PHYLO_BIN} ${SIM_FOLDER} ${N_THREADS} ${I_APPROX} > log/TP_${I_APPROX}_${N_THREAD}T.txt &

  N_THREADS=1
  I_APPROX=1
  echo "Benchmarking TensorPhylo ${I_APPROX}A ${N_THREADS}T"
  bash ./RunTP.sh ${SETTING_FILE} ${TENSOR_PHYLO_BIN} ${SIM_FOLDER} ${N_THREADS} ${I_APPROX} > log/TP_${I_APPROX}_${N_THREAD}T.txt

#############################

  N_THREADS=2
  I_APPROX=4
  echo "Benchmarking TensorPhylo ${I_APPROX}A ${N_THREADS}T"
  bash ./RunTP.sh ${SETTING_FILE} ${TENSOR_PHYLO_BIN} ${SIM_FOLDER} ${N_THREADS} ${I_APPROX} > log/TP_${I_APPROX}_${N_THREAD}T.txt &

  N_THREADS=2
  I_APPROX=3
  echo "Benchmarking TensorPhylo ${I_APPROX}A ${N_THREADS}T"
  bash ./RunTP.sh ${SETTING_FILE} ${TENSOR_PHYLO_BIN} ${SIM_FOLDER} ${N_THREADS} ${I_APPROX} > log/TP_${I_APPROX}_${N_THREAD}T.txt

########################################

  N_THREADS=4
  I_APPROX=3
  echo "Benchmarking TensorPhylo ${I_APPROX}A ${N_THREADS}T"
  bash ./RunTP.sh ${SETTING_FILE} ${TENSOR_PHYLO_BIN} ${SIM_FOLDER} ${N_THREADS} ${I_APPROX} > log/TP_${I_APPROX}_${N_THREAD}T.txt

##########################################

  N_THREADS=4
  I_APPROX=4
  echo "Benchmarking TensorPhylo ${I_APPROX}A ${N_THREADS}T"
  bash ./RunTP.sh ${SETTING_FILE} ${TENSOR_PHYLO_BIN} ${SIM_FOLDER} ${N_THREADS} ${I_APPROX} > log/TP_${I_APPROX}_${N_THREAD}T.txt

##########################################

  echo "Benchmarking Castor"
  bash ./RunCastor.sh ${SETTING_FILE} ${SIM_FOLDER} > log/logCastor.txt &

  echo "Benchmarking RevBayes"
  bash ./RunRB.sh ${SETTING_FILE} ${REVBAYES_BIN} ${SIM_FOLDER} > log/logRB.txt &

elif [ "$METHODS" == "diversitree" ]; then
  echo "Benchmarking diversitree"
  bash ./RunDiversitree.sh ${SETTING_FILE} ${SIM_FOLDER}
elif [ "$METHODS" == "castor" ]; then
  echo "Benchmarking Castor"
  bash ./RunCastor.sh ${SETTING_FILE} ${SIM_FOLDER}
elif [ "$METHODS" == "TensorPhylo" ]; then
  echo "Benchmarking TensorPhylo"
  bash ./RunTP.sh ${SETTING_FILE} ${TENSOR_PHYLO_BIN} ${SIM_FOLDER} ${N_PROC}
elif [ "$METHODS" == "RevBayes" ]; then
  echo "Benchmarking RevBayes"
  bash ./RunRB.sh ${SETTING_FILE} ${REVBAYES_BIN} ${SIM_FOLDER}
elif [ "$METHODS" == "RevBayesTP" ]; then
  echo "Benchmarking RevBayes + TensorPhylo"
  bash ./RunRB_TP.sh ${SETTING_FILE} ${REVBAYES_BIN} ${TENSOR_PHYLO_LIB_FOLDER} ${SIM_FOLDER}
fi
