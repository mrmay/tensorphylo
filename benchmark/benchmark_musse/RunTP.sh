#!/bin/bash
calc(){ awk "BEGIN { print $* }"; }

# Read the setting file given as argument
SETTING_FILE=${1}
TENSOR_PHYLO_BIN=${2}
SIM_FOLDER=${3}
N_PROC=${4}
I_APPROX=${5}

source ../utils/readSettings.sh

# "Unique name"
EXEC_NAME=`basename ${TENSOR_PHYLO_BIN}`
UNIQUE_NAME=${EXEC_NAME}_${I_APPROX}A_${N_PROC}T

# folders required
TEMP_FOLDER=./tmp/${runName}
mkdir -p ${TEMP_FOLDER}
TP_LOCK_FILE=${TEMP_FOLDER}/LOCK_TP_${runName}_${UNIQUE_NAME}.txt

PARAM_FOLDER=./data/parameters${UNIQUE_NAME}/${runName}
mkdir -p ${PARAM_FOLDER}

RESULT_FOLDER=./results/${UNIQUE_NAME}/${runName}
mkdir -p ${RESULT_FOLDER}

OUTPUT_FOLDER=./results/${runName}
mkdir -p ${OUTPUT_FOLDER}
outputFile=${OUTPUT_FOLDER}/time${UNIQUE_NAME}.txt

N_TEST_TIMEOUT=3
TIME_FOR_TEST=$(( 15 + ${N_TEST_TIMEOUT} * ${timeout} ))

# create parameter files
nProc=${N_PROC}
for s in "${nStates[@]}"
do
  for p in `seq 0 $((${#lambda[@]}-1))`
  do

    l=${lambda[$p]}
    m=${mu[$p]}

    paramsFile=${PARAM_FOLDER}/params_${s}S_${p}P_${nProc}T.dat

    #maxLambda, maxMu, nStates, nThreads, outputFilename"
    python scripts/generateMusseForTP.py ${l} ${m} ${s} ${nProc} ${I_APPROX} ${paramsFile}

  done
done


# loop through settings
echo "nTips nStates maxLambda maxMu nIter meanTime loglik error" > ${outputFile}

for t in "${nTips[@]}"
do

  for s in "${nStates[@]}"
  do
    for p in `seq 0 $((${#lambda[@]}-1))`
    do

      if [ -f ${TP_LOCK_FILE} ]
      then
        rm ${TP_LOCK_FILE}
      fi

      name="${runName}_${t}T_${s}S_${p}P_${nIter}M_${nProc}Proc"
      echo $name

      treeFile="${SIM_FOLDER}/SIM_${t}T_${s}S_${p}P/tree_1.nex"
      dataFile="${SIM_FOLDER}/SIM_${t}T_${s}S_${p}P/data_1_tp.tsv"
      paramsFile="./${PARAM_FOLDER}/params_${s}S_${p}P_${nProc}T.dat"
      outFile="${RESULT_FOLDER}/${name}.txt"

      if [ ! -f ${TP_LOCK_FILE} ]
      then

        dummyOut=${TEMP_FOLDER}/dummyTP.txt

        # First we try with 5 liks for timeout
        # If the lock is not there after this test we benchmark
        # The lock stay active until when change of tree
        #echo "Time for timeout : ${TIME_FOR_TEST} for ${N_TEST_TIMEOUT} try"
        timeout ${TIME_FOR_TEST}s bash <<EOT
          touch ${TP_LOCK_FILE}
          ${TENSOR_PHYLO_BIN} --param ${paramsFile} --nexus ${treeFile} --tsv ${dataFile} --benchmark ${N_TEST_TIMEOUT} --log ${dummyOut} --legacy 1
          rm ${TP_LOCK_FILE}
EOT

      fi

      # If the lock is not there: benchmark
      if [ ! -f ${TP_LOCK_FILE} ]
      then

        ${TENSOR_PHYLO_BIN} --param ${paramsFile} --nexus ${treeFile} --tsv ${dataFile} --benchmark ${nIter} --log ${outFile} --legacy 1

        # approximator nThread timeInit timeComp
        line=`cat ${outFile}`
        words=($line)

        l=${lambda[$p]}
        m=${mu[$p]}

        #echo "calc ${words[3]} / ${nIter}"
        timePerLik=`calc ${words[3]} / ${nIter}`
        lastLogLik=${words[4]}

        echo "\"${t}\" \"${s}\" \"${l}\" \"${m}\" \"${nIter}\" \"${timePerLik}\" \"${lastLogLik}\" \"\"" >> ${outputFile}

      # otherwise no result
      else

        l=${lambda[$p]}
        m=${mu[$p]}

        echo "\"${t}\" \"${s}\" \"${l}\" \"${m}\" \"${nIter}\" \"NA\" \"NA\" \"timeout ${timeout} seconds\"" >> ${outputFile}

      fi

    done
  done
done
