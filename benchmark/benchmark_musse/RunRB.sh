#!/bin/bash
calc(){ awk "BEGIN { print $* }"; }

# Read the setting file given as argument
SETTING_FILE=${1}
REVBAYES_BIN=${2}
SIM_FOLDER=${3}

source ../utils/readSettings.sh

# folders required
TEMP_FOLDER=./tmp/${runName}
mkdir -p ${TEMP_FOLDER}

RB_LOCK_FILE=${TEMP_FOLDER}/LOCK_RB_${runName}.txt

RESULT_FOLDER=./results/RB/${runName}
mkdir -p ${RESULT_FOLDER}

SCRIPT_FOLDER=./data/parametersRB/${runName}
BASE_SCRIPT_RB=./scripts/scriptRB.rev
mkdir -p ${SCRIPT_FOLDER}

OUTPUT_FOLDER=./results/${runName}
mkdir -p ${OUTPUT_FOLDER}
outputFile=${OUTPUT_FOLDER}/timeRevBayes.txt

N_TEST_TIMEOUT=5
TIME_FOR_TEST=$(( 15 + ${N_TEST_TIMEOUT} * ${timeout} ))

# loop through settings
echo "nTips nStates maxLambda maxMu nIter meanTime loglik error" > ${outputFile}

for t in "${nTips[@]}"
do

  if [ -f ${RB_LOCK_FILE} ]
  then
    rm ${RB_LOCK_FILE}
  fi

  for s in "${nStates[@]}"
  do
    for p in `seq 0 $((${#lambda[@]}-1))`
    do

      name="${runName}_${t}T_${s}S_${p}P_${nIter}M"
      echo $name

      treeFile="${SIM_FOLDER}/SIM_${t}T_${s}S_${p}P/tree_1.nex"

      dataFileTP="${SIM_FOLDER}/SIM_${t}T_${s}S_${p}P/data_1_tp.tsv"
      dataFileRB="${SIM_FOLDER}/SIM_${t}T_${s}S_${p}P/data_1_rb.tsv"

      tail -n +2 ${dataFileTP} > ${dataFileRB}

      # First we try with 5 liks for timeout
      # If the lock is not there after this test we benchmark
      # The lock stay active until when change of tree
      if [ ! -f ${RB_LOCK_FILE} ]
      then

        MY_SCRIPT=${TEMP_FOLDER}/script_RB.rev
        cp ${BASE_SCRIPT_RB} ${MY_SCRIPT}

        OUT_FILE=${TEMP_FOLDER}/outfile_RB.txt

        l=${lambda[$p]}
        m=${mu[$p]}

        sed -i -e "s|TREE_FILE|${treeFile}|g" ${MY_SCRIPT}
        sed -i -e "s|DATA_FILE|${dataFileRB}|g" ${MY_SCRIPT}
        sed -i -e "s|MAX_LAMBDA|${l}|g" ${MY_SCRIPT}
        sed -i -e "s|MAX_MU|${m}|g" ${MY_SCRIPT}
        sed -i -e "s|N_ITER|${N_TEST_TIMEOUT}|g" ${MY_SCRIPT}
        sed -i -e "s|MY_NSTATE|${s}|g" ${MY_SCRIPT}
        sed -i -e "s|OUT_FILE|${OUT_FILE}|g" ${MY_SCRIPT}

        timeout ${TIME_FOR_TEST}s bash <<EOT
          touch ${RB_LOCK_FILE}
          ${REVBAYES_BIN} ${MY_SCRIPT}
          rm ${RB_LOCK_FILE}
EOT

      fi

      # If the lock is not there: benchmark
      if [ ! -f ${RB_LOCK_FILE} ]
      then

        MY_SCRIPT=${SCRIPT_FOLDER}/${name}.rev
        cp ${BASE_SCRIPT_RB} ${MY_SCRIPT}

        OUT_FILE="${RESULT_FOLDER}/${name}.txt"

        l=${lambda[$p]}
        m=${mu[$p]}

        sed -i -e "s|TREE_FILE|${treeFile}|g" ${MY_SCRIPT}
        sed -i -e "s|DATA_FILE|${dataFileRB}|g" ${MY_SCRIPT}
        sed -i -e "s|MAX_LAMBDA|${l}|g" ${MY_SCRIPT}
        sed -i -e "s|MAX_MU|${m}|g" ${MY_SCRIPT}
        sed -i -e "s|N_ITER|${nIter}|g" ${MY_SCRIPT}
        sed -i -e "s|MY_NSTATE|${s}|g" ${MY_SCRIPT}
        sed -i -e "s|OUT_FILE|${OUT_FILE}|g" ${MY_SCRIPT}

        ${REVBAYES_BIN} ${MY_SCRIPT}

        avgTime=`cat ${OUT_FILE}`

        echo "\"${t}\" \"${s}\" \"${l}\" \"${m}\" \"${nIter}\" \"${avgTime}\" \"NA\" \"\"" >> ${outputFile}

      # otherwise no result
      else

        echo "\"${t}\" \"${s}\" \"${l}\" \"${m}\" \"${nIter}\" \"NA\" \"NA\" \"timeout ${timeout} seconds\"" >> ${outputFile}

      fi

    done
  done
done
