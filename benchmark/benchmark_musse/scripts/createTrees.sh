#!/bin/bash

SETTING_FILE=${1}
SIM_FOLDER=${2}

# benchmark setting
runName="test"
method="all"
nIter=0
nTips=(0)
nStates=(0)
lambda=(0.)
lambdaSim=(0.)
mu=(0.)
timeout=0

# Read the setting file given as argument
while IFS= read -r key
do
  if [ "$key" == "nIter" ]; then
    read -r value
    nIter=${value}
  elif [ "$key" == "nTips" ]; then
    read -r value
    nTips=(${value})
  elif [ "$key" == "nStates" ]; then
    read -r value
    nStates=(${value})
  elif [ "$key" == "lambda" ]; then
    read -r value
    lambda=(${value})
  elif [ "$key" == "mu" ]; then
    read -r value
    mu=(${value})
  elif [ "$key" == "timeout" ]; then
    read -r value
    timeout=${value}
  elif [ "$key" == "runName" ]; then
    read -r value
    runName=${value}
  else
    echo "Format problem in the setting file"
    exit 1
  fi
done < "${SETTING_FILE}"

# use it like Rscript validation/simulate_trees.r f n s r (edited)
# where f is the output directory, n is the number of tips in the trees, s is the number of states, and r is the number of replicates

for t in "${nTips[@]}"
do
  for s in "${nStates[@]}"
  do
    for p in `seq 0 $((${#lambda[@]}-1))`
    do

      OUT_FOLDER="${SIM_FOLDER}/SIM_${t}T_${s}S_${p}P"
      mkdir -p ${OUT_FOLDER}

      Rscript ./scripts/simulate_trees.R ${OUT_FOLDER} ${t} ${s} ${lambda[$p]}

    done
  done
done
