#!/usr/bin/python
import os
import sys
import math
import dendropy
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from palettable.colorbrewer.diverging import RdBu_8
from palettable.colorbrewer.qualitative import Set1_8, Set2_8

DPI = 100
LINE_WIDTH=2.5

font = {'family' : 'DejaVu Sans',
    'weight' : 'normal',
    'size'   : 8}

plt.rc('font', **font)
plt.rcParams['svg.fonttype'] = 'none'
plt.rcParams['lines.linewidth'] = LINE_WIDTH


def readSettings(filename):
    inFile = file(filename, 'r')

    runName = ''
    nIter = timeout = 0
    nTips = nStates = vecLambda = vecMu = []

    settings = {}

    line = inFile.readline().strip()
    while line:
        nextLine = inFile.readline().strip()
        if line == "runName":
            settings[line] = nextLine
        elif line == "nIter":
            settings[line] = int(nextLine)
        elif line == "nTips":
            words = nextLine.split(' ')
            settings[line] = [int(w) for w in words]
        elif line == "nStates":
            words = nextLine.split(' ')
            settings[line] = [int(w) for w in words]
        elif line == "lambda":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "mu":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "timeout":
            settings[line] = int(nextLine)

        #Read next line:
        line = inFile.readline().strip()

    return settings


def readBenchmark(impl, filename):
    dFrame = pd.read_csv(filename, sep=' ')
    dFrame = dFrame.drop(['error'], axis=1)
    timeImpl = 'time{}'.format(impl)
    llImpl = 'll{}'.format(impl)
    dFrame = dFrame.rename(columns={'meanTime' : timeImpl, 'loglik' : llImpl})

    return dFrame

def mergeDFrame(impls, dFrames):

    if len(impls) == 1:
        return dFrames[impls[0]]
    else:
        df = pd.merge(dFrames[impls[0]], dFrames[impls[1]], on=['nTips', 'nStates', 'maxLambda', 'maxMu', 'nIter'], how='outer')

        for i in range(2, len(impls)):
            df = pd.merge(df, dFrames[impls[i]], on=['nTips', 'nStates', 'maxLambda', 'maxMu', 'nIter'], how='outer')

    return df

def defineTreeHeights(settings, df):
    baseFilePath = './SimulatedTrees/{}/'.format(settings["runName"])

    headers = ["nTips", "nStates", "maxLambda", "maxMu", "treeHeight"]
    rows = []
    for t in settings["nTips"]:
        for s in settings["nStates"]:
            for iP in range(len(settings["lambda"])):
                filename = '{}SIM_{}T_{}S_{}P/tree_1.nex'.format(baseFilePath, t, s, iP)
                tree = dendropy.Tree.get(path=filename, schema="nexus")
                height = tree.max_distance_from_root()
                rows.append([t, s, settings["lambda"][iP], settings["mu"][iP], height])

    tmpDataFrame = pd.DataFrame(rows, columns=headers)
    df = pd.merge(df, tmpDataFrame, on=['nTips', 'nStates', 'maxLambda', 'maxMu'])
    return df


def compareImpls(settings, impls, df):

    #timeRef = 'time{}'.format(REF_IMPL)
    #llRef = 'll{}'.format(REF_IMPL)

    for impl in impls:
        speedup = 'sp{}'.format(impl)
        llDiff = 'llDiff{}'.format(impl)

        time = 'time{}'.format(impl)
        ll = 'll{}'.format(impl)

        #df[speedup] = df[time] / df[timeRef]
        #df[llDiff] = df[ll] - df[llRef]

    return df


def plotAvgSpeedups(settings, impls, df):

    nPlots = len(impls) - 1

    ''''
    X_WIDTH = 400*nPlots
    Y_WIDTH = 400
    fig, ax = plt.subplots(1, nPlots, figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=True, sharex=False, sharey=False)
    '''

    group = df.groupby(['nTips', 'nStates'] , as_index = False).mean()
    #group.add_suffix('_avg').reset_index()
    #print group.mean()

    for i, impl in zip(range(len(impls)), impls):

        speedup = 'sp{}'.format(impl)

        fig = plt.figure()
        ax = fig.gca(projection='3d')

        # get the grid
        flatX = settings["nTips"]
        flatY = settings["nStates"]
        gridX, gridY = np.meshgrid(flatX, flatY)

        # create the grid of value. pivote nStates from index to columns
        pivotedDF = group.pivot(index='nTips', columns='nStates', values=speedup)
        val = pivotedDF.to_numpy().transpose()
        val3d = val.copy()

        if True:
            gridX = np.log2(gridX)
            gridY = np.log2(gridY)
            val3d = np.log10(val)

        minVal = np.min(val.flatten())
        maxVal = np.max(val.flatten())
        extrema = np.max([np.abs([minVal, maxVal])])

        myMap = RdBu_8.mpl_colormap
        ax.plot_wireframe(gridX, gridY, val3d, alpha=1, linewidth=2, colors='k')
        s = ax.plot_surface(gridX, gridY, val3d, alpha=0.8, linewidth=0, cmap=myMap, antialiased=False, shade=False, vmin=-extrema, vmax=extrema)

        fig.colorbar(s, shrink=0.5, aspect=5)
        ax.set_title('Speedup over {} (averaged over lambdas/mus)'.format(impl))
        ax.set_xlabel('Number of taxa (log2)')
        ax.set_ylabel('Number of states (log2)')
        ax.set_zlabel('Speedup')
        plt.tight_layout()

        fig, ax = plt.subplots()
        for i, t in zip(range(len(flatX)), flatX):
            ax.plot(np.log2(flatY), val[:,i], '-x', color=Set2_8.hex_colors[i])

        ax.set_yscale('log')
        ax.get_yaxis().set_major_formatter(mpl.ticker.ScalarFormatter())

        ax.set_title('Speedup over {} (averaged over lambdas/mus)'.format(impl))
        ax.set_xlabel('Number of states (log2)')
        ax.set_ylabel('Speedup')
        plt.legend(['{} taxa'.format(l) for l in flatX])
        plt.tight_layout()

def plotAvgSpeedupsSummary(settings, impls, df, thresholdAcc, figureFolder):

    nPlots = len(impls)

    X_WIDTH = 350*nPlots
    Y_WIDTH = 300*nPlots
    fig, ax = plt.subplots(nPlots, nPlots, figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=True, sharex=False , sharey='row')

    #group.add_suffix('_avg').reset_index()
    #print group.mean()

    for i, implA in zip(range(len(impls)), impls):
        localRef = implA

        for j, implB in zip(range(len(impls)), impls):

            if j == 0 :  ax[i, j].set_ylabel('Speedup (log)')
            if i == len(impls)-1: ax[i, j].set_xlabel('Number of states (log2)')

            if i == j: continue

            timeA = df['time{}'.format(implA)]
            timeB = df['time{}'.format(implB)]
            speedup = 'sp{}to{}'.format(implB, implA)
            df[speedup] = timeB / timeA

            dfMod = df.apply(pd.to_numeric, errors='coerce')
            dfMod = dfMod.dropna(subset=[speedup])

            if thresholdAcc != None:
                llA = dfMod['ll{}'.format(implA)]
                llB = dfMod['ll{}'.format(implB)]
                relError = 'relErr{}to{}'.format(implB, implA)
                dfMod[relError] = np.fabs((llB - llA)/llA)

                dfMod = dfMod.loc[(dfMod[relError] < thresholdAcc)]

            if len(dfMod) == 0: continue

            #print dfMod
            group = dfMod.groupby(['nTips', 'nStates'] , as_index = False).mean()

            flatX = settings["nTips"]
            flatY = settings["nStates"]

            pivotedDF = group.pivot(index='nTips', columns='nStates', values=speedup)
            print pivotedDF
            val = pivotedDF.to_numpy().transpose()

            rangeY = flatY[:val.shape[0]]
            ax[i, j].plot(np.log2(flatY), [1. for y in flatY], '--', color='grey')

            for iX, t in zip(range(len(flatX)), flatX):
                print iX, " ", t, " ", val.shape
                if iX >= val.shape[1]: continue
                ax[i, j].plot(np.log2(rangeY), val[:,iX], '-x', color=Set2_8.hex_colors[iX])

            ax[i, j].set_yscale('log')
            ax[i, j].title.set_text('Sp. {} over {}'.format(implA, implB))
            #ax[i, i].yaxis.set_major_locator(mpl.ticker.LogLocator(base=10.0))
            #ax[i, j].get_yaxis().set_major_formatter(mpl.ticker.LogFormatter())

    for i, implA in zip(range(len(impls)), impls):
        localRef = implA
        labels = []
        speedups = []
        colors = []

        for j, implB in zip(range(len(impls)), impls):
            if i==j: continue

            labels.append(implB)
            speedup = 'sp{}to{}'.format(implB, implA)
            speedups.append(dfMod[speedup].to_numpy())
            speedups[-1][np.logical_not(np.isnan(speedups[-1]))]
            colors.append(Set1_8.hex_colors[j])

        bplot = ax[i,i].boxplot(speedups, patch_artist=True, labels=labels)
        ax[i, i].title.set_text(implA)
        ax[i, i].title.set_color(Set1_8.hex_colors[i])
        ax[i, i].axhline(y=1., linestyle='--', color='grey')
        ax[i, i].set_yscale('log')
        ax[i, i].set_xticklabels(ax[i, i].get_xticklabels(),rotation=20)
        #ax[i, i].yaxis.set_major_locator(mpl.ticker.LogLocator(base=10.0))

        for patch, color in zip(bplot['boxes'], colors):
            patch.set_facecolor(color)



    #handles, labels = ax[0,1].get_legend_handles_labels()
    labels = ['1x'] + ['{} taxa'.format(l) for l in flatX]
    ax[0, 1].legend(labels=labels)
    ax[1, 0].legend(labels=labels)

    plt.tight_layout()
    plt.savefig('{}/avgSpeedups.svg'.format(figureFolder))


def plotTreeHeight(settings, impls, df):

    nPlots = len(impls) - 1

    ''''
    X_WIDTH = 400*nPlots
    Y_WIDTH = 400
    fig, ax = plt.subplots(1, nPlots, figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=True, sharex=False, sharey=False)
    '''

    markers = [ '+', '*', '^', 'o', 'x', 'd', 'p']
    colors = Set2_8
    #group = df.groupby(['nTips', 'nStates'] , as_index = False).mean()
    #group.add_suffix('_avg').reset_index()
    #print group.mean()

    for i, impl in zip(range(len(impls)), impls):

        speedup = 'sp{}'.format(impl)

        fig, ax = plt.subplots()

        for index, row in df.iterrows():

            x = row['treeHeight']
            y = row[speedup]
            marker = markers[settings['nTips'].index(row['nTips'])]
            color = colors.hex_colors[settings['nStates'].index(row['nStates'])]

            ax.plot(x, y, marker=marker, color=color)


        ax.set_title('Speedup over {} as a function of the tree length'.format(impl))
        ax.set_xlabel('Tree height (~lambda)')
        ax.set_ylabel('Speedup')

        ax.set_yscale('log')
        ax.get_yaxis().set_major_formatter(mpl.ticker.ScalarFormatter())

        f = lambda m,c: plt.plot([],[],marker=m, color=c, ls="none")[0]
        handles = [f("s", colors.hex_colors[i]) for i in range(len(settings['nStates']))]
        handles += [f(markers[i], "k") for i in range(len(settings['nTips']))]

        labels = ['{} states'.format(l) for l in settings['nStates']]
        labels += ['{} taxa'.format(l) for l in settings['nTips']]

        plt.legend(handles, labels, loc=1, framealpha=0.5, ncol=2)

        plt.tight_layout()



settingFile = sys.argv[1]
figureFolder = sys.argv[2]
settings = readSettings(settingFile)

#implementations = ['Diversitree', 'TP_2A_1T', 'TP_3A_1T', 'TP_4A_2T', 'TP_4A_4T', 'TP_5A_2T', 'TP_5A_4T', 'RevBayes']
#implementations = ['Diversitree', 'TP_AutoTuning', 'TP_Compact', 'TP_Branchwise', 'RevBayes', 'Castor']
#implementations = ['Diversitree', 'TensorPhyloFinal_1A_1T', 'TensorPhyloFinal_2A_1T', 'TensorPhyloFinal_3A_2T', 'TensorPhyloFinal_4A_2T', 'TensorPhyloFinal_3A_4T']
#implementations = ['TensorPhyloFinal_1A_1T', 'TensorPhyloFinal_2A_1T', 'timeTP_AT_SEQ', 'timeTP_AT_PAR']
implementations = ['Diversitree', 'TP_AT_SEQ', 'TP_AT_PAR', 'RevBayes', 'Castor']

#implementations = ['Diversitree', 'TensorPhylo', 'RevBayes', 'RevBayesWithTP', 'Castor']

dFrames = {}
for impl in implementations:
    resultFile = './results/' + settings["runName"] + '/time' + impl + '.txt'
    dFrames[impl] = readBenchmark(impl, resultFile)

df = mergeDFrame(implementations, dFrames)

#df = addBestTP(implTP, df)

#df = defineTreeHeights(settings, df)

#if not REF_IMPL in implementations:
#    sys.exit("This script requires that '{}' has been part of the benchmark.".format(REF_IMPL))
#implsNoRef = implementations[:]
#implsNoRef.remove(REF_IMPL)

df = compareImpls(settings, implementations, df)
#print df

#plotAvgSpeedups(settings, implsNoRef, df)

#plotTreeHeight(settings, implsNoRef, df)

plotAvgSpeedupsSummary(settings, implementations, df, None, figureFolder)
# plotAvgSpeedupsSummary(settings, implementations, df, 0.01)

plt.show()
