#!/bin/bash
calc(){ awk "BEGIN { print $* }"; }

# Read the setting file given as argument
SETTING_FILE=${1}
TENSOR_PHYLO_BIN=${2}
SIM_FOLDER=${3}

EXEC_NAME=`basename ${TENSOR_PHYLO_BIN}`

source ../utils/readSettings.sh

# folders required
TEMP_FOLDER=./tmp/${runName}
mkdir -p ${TEMP_FOLDER}
TP_LOCK_FILE=${TEMP_FOLDER}/LOCK_TP_${runName}_${EXEC_NAME}.txt

PARAM_FOLDER=./data/parametersTP/${runName}_${EXEC_NAME}
mkdir -p ${PARAM_FOLDER}

RESULT_FOLDER=./results/TP/${runName}_${EXEC_NAME}
mkdir -p ${RESULT_FOLDER}

OUTPUT_FOLDER=./results/${runName}
mkdir -p ${OUTPUT_FOLDER}
outputFile=${OUTPUT_FOLDER}/time${EXEC_NAME}.txt

N_TEST_TIMEOUT=1
TIME_FOR_TEST=$(( 15 + ${N_TEST_TIMEOUT} * ${timeout} ))

# create parameter files
nThreads=(1)
for t in "${nThreads[@]}"
do
  for s in "${nStates[@]}"
  do
    for p in `seq 0 $((${#lambda[@]}-1))`
    do

      l=${lambda[$p]}
      m=${mu[$p]}

      paramsFile=${PARAM_FOLDER}/params_${s}S_${p}P_${t}T.dat

      #maxLambda, maxMu, nStates, nThreads, outputFilename"
      python scripts/generateFBDForTP.py ${l} ${m} ${s} ${t} ${paramsFile}

    done
  done
done


# loop through settings
echo "nTips nStates maxLambdaSim maxMuSim maxLambdaLL maxMuLL nIter meanTime loglik error" > ${outputFile}

nProc=1
for t in "${nTips[@]}"
do
  for s in "${nStates[@]}"
  do
    for p in `seq 0 $((${#lambda[@]}-1))`
    do
      for q in `seq 0 $((${#lambda[@]}-1))`
      do

        if [ -f ${TP_LOCK_FILE} ]
        then
          rm ${TP_LOCK_FILE}
        fi


        name="${runName}_${t}T_${s}S_${p}P_${q}Q_${nIter}M_${nProc}Proc"
        echo $name

        treeFile="${SIM_FOLDER}/SIM_${t}T_${s}S_${p}P/tree_1.nex"
        dataFile="${SIM_FOLDER}/SIM_${t}T_${s}S_${p}P/data_1_tp.tsv"
        paramsFile="./${PARAM_FOLDER}/params_${s}S_${q}P_${nProc}T.dat"
        outFile="${RESULT_FOLDER}/${name}.txt"

        if [ ! -f ${TP_LOCK_FILE} ]
        then

          timeout ${TIME_FOR_TEST}s bash <<EOT
            touch ${TP_LOCK_FILE}
            ${TENSOR_PHYLO_BIN} --param ${paramsFile} --nexus ${treeFile} --tsv ${dataFile} --benchmark ${nIter} --log ${outFile} --legacy 1
            rm ${TP_LOCK_FILE}
EOT

        fi

        l=${lambda[$q]}
        m=${mu[$q]}

        # If the lock is not there: benchmark
        if [ ! -f ${TP_LOCK_FILE} ]
        then

          # approximator nThread timeInit timeComp lik
          line=`cat ${outFile}`
          words=($line)


          #echo "calc ${words[3]} / ${nIter}"
          timePerLik=`calc ${words[3]} / ${nIter}`
          lastLogLik=${words[4]}

          echo "\"${t}\" \"${s}\" \"${lambda[$p]}\" \"${mu[$p]}\" \"${l}\" \"${m}\" \"${nIter}\" \"${timePerLik}\" \"${lastLogLik}\" \"\"" >> ${outputFile}

        # otherwise no result
        else

          echo "\"${t}\" \"${s}\" \"${lambda[$p]}\" \"${mu[$p]}\" \"${l}\" \"${m}\" \"${nIter}\" \"NA\" \"NA\" \"timeout ${timeout} seconds\"" >> ${outputFile}

        fi
      done
    done
  done
done
