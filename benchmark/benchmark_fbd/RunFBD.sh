#!/bin/bash
calc(){ awk "BEGIN { print $* }"; }

# Read the setting file given as argument
SETTING_FILE=${1}
SIM_FOLDER=${2}


source ../utils/readSettings.sh

# folders required
TEMP_FOLDER=./tmp/${runName}
mkdir -p ${TEMP_FOLDER}
FBD_LOCK_FILE=${TEMP_FOLDER}/LOCK_FBD_${runName}.txt
DUMMY_OUT_FILE=${TEMP_FOLDER}/dummyOutDiv.txt

OUTPUT_FOLDER=./results/${runName}
mkdir -p ${OUTPUT_FOLDER}
outputFile=${OUTPUT_FOLDER}/timeFBD.txt

# loop through settings
echo "nTips nStates maxLambdaSim maxMuSim maxLambdaLL maxMuLL nIter meanTime loglik error" > ${outputFile}

for t in "${nTips[@]}"
do
  for s in "${nStates[@]}"
  do
    for p in `seq 0 $((${#lambda[@]}-1))`
    do
      for q in `seq 0 $((${#lambda[@]}-1))`
      do

        name="${runName}_${t}T_${s}S_${p}P_${q}Q_${nIter}M"
        echo $name

        treeFile="${SIM_FOLDER}/SIM_${t}T_${s}S_${p}P/tree_1.nex"
        dataFile="${SIM_FOLDER}/SIM_${t}T_${s}S_${p}P/data_1.tsv"

        l=${lambda[$q]}
        m=${mu[$q]}

        Rscript scripts/scriptFBD.R ${s} ${t} ${lambda[$p]} ${mu[$p]} ${l} ${m} ${nIter} ${treeFile} ${dataFile} ${outputFile}

      done
    done
  done
done
