#!/usr/bin/python
import os
import sys
import numpy as np


def writeParametersToFile(lbda, mu, nStates, nThreads, filename):

    outFile = open(filename, 'w')

    outFile.write("applyTreeLikCorrection\n")
    outFile.write("true\n\n")

    outFile.write("intLikApproximator\n")
    outFile.write("0\n\n")

    outFile.write("nThreads\n")
    outFile.write(repr(nThreads)+"\n\n")

    outFile.write("intScheme\n")
    outFile.write("2\n\n")

    outFile.write("condType\n")
    outFile.write("0\n\n")

    outFile.write("deltaT\n")
    outFile.write("0.5\n\n")

    outFile.write("rootPrior\n")
    outFile.write(repr(nStates)+"\n")
    for i in range(nStates):
        outFile.write(repr(1./nStates)+ " ")
    outFile.write("\n\n")

    outFile.write("lambda\n")
    outFile.write(repr(nStates)+"\n")
    vecLambda = [lbda for x in range(nStates)]
    for l in vecLambda:
        outFile.write(repr(l)+ " ")
    outFile.write("\n\n")

    outFile.write("mu\n")
    outFile.write(repr(nStates)+"\n")
    vecMu = [mu for x in range(nStates)]
    for m in vecMu:
        outFile.write(repr(m)+ " ")
    outFile.write("\n\n")

    outFile.write("phi\n")
    outFile.write(repr(nStates)+"\n")
    for i in range(nStates):
        outFile.write(repr(0.)+ " ")
    outFile.write("\n\n")

    outFile.write("delta\n")
    outFile.write(repr(nStates)+"\n")
    for i in range(nStates):
        outFile.write(repr(0.)+ " ")
    outFile.write("\n\n")

    outFile.write("eta\n")
    outFile.write(repr(nStates) + " " + repr(nStates) + "\n")
    for i in range(nStates):
        for j in range(nStates):
            if i != j:
                outFile.write(repr(0.1) + " ")
            else:
                outFile.write(repr(-0.1*(nStates-1)) + " ")
        outFile.write("\n")
    outFile.write("\n\n")

    outFile.write("omega\n")
    outFile.write("0\n\n")

    outFile.write("massSpeciationTimes\n")
    outFile.write("0\n\n")

    outFile.write("massSpeciationProb\n")
    outFile.write("0\n\n")

    outFile.write("massExtinctionTimes\n")
    outFile.write("0\n\n")

    outFile.write("massExtinctionProb\n")
    outFile.write("0\n\n")

    outFile.write("massExtinctionStateChangeProb\n")
    outFile.write("0\n\n")

    outFile.write("massSamplingTimes\n")
    outFile.write("1\n")
    outFile.write("0\n\n")

    outFile.write("massSamplingProb\n")
    outFile.write("1\n")
    outFile.write(repr(nStates)+"\n")
    for i in range(nStates):
        outFile.write(repr(1.)+ " ")
    outFile.write("\n\n")

    outFile.write("massDestrSamplingTimes\n")
    outFile.write("0\n\n")

    outFile.write("massDestrSamplingProb\n")
    outFile.write("0\n\n")

    outFile.write("synchMonitoring\n")
    outFile.write("0\n\n")

#mu, lambda, nStates, nThreads, filename
#print len(sys.argv)
#print sys.argv

if len(sys.argv) != 6:
    print "This script should be called as:\n>python generateMusseForTP.py maxLambda, maxMu, nStates, nThreads, outputFilename"
    exit()

maxLambda = float(sys.argv[1])
maxMu = float(sys.argv[2])
nStates = int(sys.argv[3])
nThreads = int(sys.argv[4])
outputFilename = sys.argv[5]

writeParametersToFile(maxLambda, maxMu, nStates, nThreads, outputFilename)
