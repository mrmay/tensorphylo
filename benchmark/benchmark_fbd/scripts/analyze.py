#!/usr/bin/python
import os
import sys
import dendropy
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from palettable.colorbrewer.diverging import RdBu_8
from palettable.colorbrewer.qualitative import Set2_8
import seaborn as sns



REF_IMPL = 'FBD'

DPI = 100
LINE_WIDTH=2.5

font = {'family' : 'DejaVu Sans',
    'weight' : 'normal',
    'size'   : 12}

plt.rc('font', **font)
plt.rcParams['svg.fonttype'] = 'none'
plt.rcParams['lines.linewidth'] = LINE_WIDTH
sns.set(style="whitegrid")


def readSettings(filename):
    inFile = file(filename, 'r')

    runName = ''
    nIter = timeout = 0
    nTips = nStates = vecLambda = vecMu = []

    settings = {}

    line = inFile.readline().strip()
    while line:
        nextLine = inFile.readline().strip()
        if line == "runName":
            settings[line] = nextLine
        elif line == "nIter":
            settings[line] = int(nextLine)
        elif line == "nTips":
            words = nextLine.split(' ')
            settings[line] = [int(w) for w in words]
        elif line == "nStates":
            words = nextLine.split(' ')
            settings[line] = [int(w) for w in words]
        elif line == "lambda":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "mu":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "timeout":
            settings[line] = int(nextLine)

        #Read next line:
        line = inFile.readline().strip()

    return settings


def readBenchmark(impl, filename):
    dFrame = pd.read_csv(filename, sep=' ')
    dFrame = dFrame.drop(['error'], axis=1)
    timeImpl = 'time{}'.format(impl)
    llImpl = 'll{}'.format(impl)
    dFrame = dFrame.rename(columns={'meanTime' : timeImpl, 'loglik' : llImpl})

    return dFrame

def mergeDFrame(impls, dFrames):

    if len(impls) == 1:
        return dFrames[impls[0]]
    else:
        df = pd.merge(dFrames[impls[0]], dFrames[impls[1]], on=['nTips', 'nStates', 'maxLambdaSim', 'maxMuSim', 'maxLambdaLL', 'maxMuLL', 'nIter'], how = 'outer')

        for i in range(2, len(impls)):
            df = pd.merge(df, dFrames[impls[i]], on=['nTips', 'nStates', 'maxLambdaSim', 'maxMuSim', 'maxLambdaLL', 'maxMuLL', 'nIter'], how = 'outer')

    return df

def defineTreeHeights(settings, df):
    baseFilePath = './SimulatedTrees/{}/'.format(settings["runName"])

    headers = ["nTips", "nStates", 'maxLambdaSim', 'maxMuSim', "treeHeight"]
    rows = []
    for t in settings["nTips"]:
        for s in settings["nStates"]:
            for iP in range(len(settings["lambda"])):
                filename = '{}SIM_{}T_{}S_{}P/tree_1.nex'.format(baseFilePath, t, s, iP)
                tree = dendropy.Tree.get(path=filename, schema="nexus")
                height = tree.max_distance_from_root()
                rows.append([t, s, settings["lambda"][iP], settings["mu"][iP], height])

    tmpDataFrame = pd.DataFrame(rows, columns=headers)
    df = pd.merge(df, tmpDataFrame, on=['nTips', 'nStates', 'maxLambdaSim', 'maxMuSim'])
    return df


def compareImpls(settings, impls, df):

    llRef = 'll{}'.format(REF_IMPL)

    for impl in impls:
        llDiff = 'llDiff{}'.format(impl)

        ll = 'll{}'.format(impl)

        df[llDiff] = df[ll] - df[llRef]

    return df

def plotLogLikDiff(settings, impls, df, figureFolder):

    dfMod = df.apply(pd.to_numeric, errors='coerce')
    #dfMod = dfMod.dropna()

    markers = [ '+', '*', '^', 'o', 'x', 'd', 'p']
    colors = Set2_8

    data = []
    cols = []
    labels = []
    cntNAN = []
    for i, impl in zip(range(len(impls)), impls):
        llDiff = 'llDiff{}'.format(impl)
        labels.append(impl)
        cols.append(llDiff)
        data.append(dfMod[llDiff].to_numpy())
        cntNAN.append(df[llDiff].isnull().sum(axis = 0))

    for col, lab in zip(cols, labels):
        dfMod = dfMod.rename(columns = {col : lab})

    dfSNS = pd.melt(dfMod[labels])
    dfSNS = dfSNS.rename(columns={"variable": "Implementations", "value": "Diff. LL"})
    dfSNS["Log of abs diff. LL"] = np.log(np.abs(dfSNS["Diff. LL"]))

    #dfAbsLogSNS = pd.melt(np.log(np.abs(dfMod[cols])))
    #dfAbsLogSNS = dfAbsLogSNS.rename(columns={"variable": "Implementations", "value": "Log of abs diff. LL"})

    X_WIDTH = 600*2
    Y_WIDTH = 400*2
    fig, ax = plt.subplots(2, 2, figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=False, sharex=False, sharey=False)

    sns.set_palette(colors.mpl_colors)
    sns.boxplot(x="Implementations", y="Diff. LL", data=dfSNS, ax=ax[0,0], width=0.8)
    sns.boxplot(x="Implementations", y="Log of abs diff. LL", data=dfSNS, ax=ax[0,1], width=0.8)
    sns.boxplot(x="Implementations", y="Diff. LL", data=dfSNS, ax=ax[1,0], showfliers = False, width=0.8)
    sns.boxplot(x="Implementations", y="Log of abs diff. LL", data=dfSNS, ax=ax[1,1], showfliers = False, width=0.8)

    plt.tight_layout()
    plt.savefig('{}/overallDiff.svg'.format(figureFolder))

    X_WIDTH = len(labels)*450
    Y_WIDTH = 3*300
    fig, ax = plt.subplots(3,len(labels),figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=False, sharex=False, sharey=True)

    dfMod['Sim: Lambda, Mu'] = '[' + dfMod['maxLambdaSim'].astype(str) + ',' + dfMod['maxMuSim'].astype(str) + ']'

    for iL, label in zip(range(len(labels)), labels):

        dfMod['absLog_'+label]= np.log(np.abs(dfMod[label]))

        sns.set_palette(colors.mpl_colors)
        sns.boxplot(x='nTips', y='absLog_'+label, data=dfMod, ax=ax[0, iL])
        sns.boxplot(x='nStates', y='absLog_'+label, data=dfMod, ax=ax[1, iL])
        sns.boxplot(x='Sim: Lambda, Mu', y='absLog_'+label, data=dfMod, ax=ax[2, iL])

    plt.tight_layout()
    plt.savefig('{}/detailedDiff.svg'.format(figureFolder))


'''
    dfNAN = df[df.isna().any(axis=1)]
    dfNAN = dfNAN[['nTips', 'nStates', 'maxLambdaSim', 'maxMuSim', 'maxLambdaLL', 'maxMuLL', 'treeHeight', 'llDiffDiversitree', 'llDiffTensorPhylo']]
    dfNAN = dfNAN.rename(columns={'maxLambdaSim' : 'lambdaSim', 'maxMuSim' : 'muSim', 'maxLambdaLL' : 'lambdaLL', 'maxMuLL' : 'muLL', 'llDiffDiversitree' : 'diversitree', 'llDiffTensorPhylo' : 'TensorPhylo'})
    print dfNAN

    print 'Nan count:', cntNAN
    for i, implA in zip(range(len(impls)), impls):
        llA = 'll{}'.format(implA)
        for j, implB in zip(range(i+1, len(impls)), impls[i+1:]):
            llB = 'll{}'.format(implB)
            print df.isnull().groupby([llA, llB]).size()
'''


settingFile = sys.argv[1]
figureFolder = sys.argv[2]
settings = readSettings(settingFile)
#, 'TensorPhylo'
implementations = ['FBD', 'Diversitree', 'TensorPhyloFinal', 'RevBayes', 'Castor'  ]
#implementations = ['FBD', 'Diversitree',  'TensorPhyloFancy1_0', 'TensorPhyloBW2', 'TensorPhyloFC', 'RevBayes', 'Castor' ]
# 'Castor', 'RevBayes']
#implementations = ['Diversitree', 'TensorPhylo', 'RevBayes', 'RevBayesWithTP', 'Castor']

dFrames = {}
for impl in implementations:
    resultFile = './results/' + settings["runName"] + '/time' + impl + '.txt'
    dFrames[impl] = readBenchmark(impl, resultFile)

df = mergeDFrame(implementations, dFrames)

#df = defineTreeHeights(settings, df)

if not REF_IMPL in implementations:
    sys.exit("This script requires that '{}' has been part of the benchmark.".format(REF_IMPL))
implsNoRef = implementations[:]
implsNoRef.remove(REF_IMPL)

df = compareImpls(settings, implsNoRef, df)
#print df

#plotAvgSpeedups(settings, implsNoRef, df)

#plotTreeHeight(settings, implsNoRef, df)

plotLogLikDiff(settings, implsNoRef, df, figureFolder)




plt.show()



#selDF = df[ (np.abs(df['llDiffTensorPhyloBWAcc']) > 1e-3) ]

'''
for impl in implsNoRef:
    print "Problematic cases for ", impl

    llDiff = 'llDiff{}'.format(impl)
    selDF = df[ np.abs(df[llDiff]) > 1e-3 ]

    print selDF[['nTips', 'nStates', 'maxLambdaSim', 'maxMuSim', 'maxLambdaLL', 'maxMuLL', llDiff]]
'''
