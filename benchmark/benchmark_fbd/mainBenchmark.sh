#!/bin/bash
SETTING_FILE=${1}
METHODS=${2}

REVBAYES_BIN=rb_current
TENSOR_PHYLO_BIN=./bin/TensorPhyloFinal
TENSOR_PHYLO_LIB_FOLDER=/home/meyerx/Projets/TensorPhylo/build_local/Lib

source ../utils/readSettings.sh

echo "Starting benchmark '${runName}' with settings:"
echo "-----------------------------------------------"
# settings
echo "Number of iterations: ${nIter}"
echo "Number of tips: ${nTips[@]}"
echo "Number of states: ${nStates[@]}"
echo "Lambda values: ${lambda[@]}"
echo "Mu values: ${mu[@]}"
echo "Timeout: ${timeout}"


SIM_FOLDER="`pwd`/SimulatedTrees/${runName}"
if [ ! -d ${SIM_FOLDER} ]; then
  echo "-----------------------------------------------"
  echo "Simulating the datasets"
  bash scripts/createTrees.sh ${SETTING_FILE} ${SIM_FOLDER}
  echo "Done"
fi

echo "-----------------------------------------------"

# launch the benchmarks
: <<'END'
echo "All methods will be benchmarked..."

echo "Benchmarking FBD"
bash ./RunFBD.sh ${SETTING_FILE} ${SIM_FOLDER}

echo "Benchmarking diversitree"
bash ./RunDiversitree.sh ${SETTING_FILE} ${SIM_FOLDER}

echo "Benchmarking TensorPhylo"
bash ./RunTP.sh ${SETTING_FILE} ${TENSOR_PHYLO_BIN} ${SIM_FOLDER}

echo "Benchmarking TensorPhyloDOPRI"
bash ./RunTPDOPRI.sh ${SETTING_FILE} ${TENSOR_PHYLO_BIN} ${SIM_FOLDER}

echo "Benchmarking RevBayes + TensorPhylo"
bash ./RunRB_TP.sh ${SETTING_FILE} ${REVBAYES_BIN} ${TENSOR_PHYLO_LIB_FOLDER} ${SIM_FOLDER}

echo "Benchmarking RevBayes"
bash ./RunRB.sh ${SETTING_FILE} ${REVBAYES_BIN} ${SIM_FOLDER}

echo "Benchmarking Castor"
bash ./RunCastor.sh ${SETTING_FILE} ${SIM_FOLDER}
END

TENSOR_PHYLO_BIN=./bin/TensorPhyloFinal
echo "Benchmarking ${TENSOR_PHYLO_BIN}"
bash ./RunTP.sh ${SETTING_FILE} ${TENSOR_PHYLO_BIN} ${SIM_FOLDER}
