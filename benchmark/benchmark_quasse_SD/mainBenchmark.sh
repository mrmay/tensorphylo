#!/bin/bash
SETTING_FILE=${1}
METHODS=${2}

REVBAYES_BIN=rb_current
N_PROC=1
TENSOR_PHYLO_BIN=./bin/TensorPhyloBench
TENSOR_PHYLO_LIB_FOLDER=/home/meyerx/Projets/TensorPhylo/build_local/Lib

source ../utils/readSettings.sh

echo "Starting benchmark '${runName}' with settings:"
echo "-----------------------------------------------"
# settings
echo "Number of iterations: ${nIter}"
echo "Number of tips: ${nTips[@]}"
echo "Number of states: ${nStates[@]}"
echo "Lambda values: ${lambda[@]}"
echo "Mu values: ${mu[@]}"
echo "Sigma values: ${sigma[@]}"
echo "Timeout: ${timeout}"


SIM_FOLDER="`pwd`/SimulatedData/${runName}"
if [ ! -d ${SIM_FOLDER} ]; then
  echo "-----------------------------------------------"
  echo "Simulating the datasets"
  bash scripts/createData.sh ${SETTING_FILE} ${SIM_FOLDER}
  echo "Done"
fi

echo "-----------------------------------------------"

echo "Benchmarking diversitree"
bash ./RunDiversitree.sh ${SETTING_FILE} ${SIM_FOLDER}

echo "Benchmarking TensorPhylo through RB"
bash ./RunTP.sh ${SETTING_FILE} ${SIM_FOLDER} ${REVBAYES_BIN} ${TENSOR_PHYLO_LIB_FOLDER}
