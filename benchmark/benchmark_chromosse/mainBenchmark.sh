#!/bin/bash
SETTING_FILE=${1}

REVBAYES_BIN=rb_current
BASE_LIB_PATH=/home/meyerx/Projets/TensorPhyloProto/benchmark/benchmark_chromosse

source ./scripts/readSettings.sh

echo "Starting benchmark '${runName}' with settings:"
echo "-----------------------------------------------"
# settings
echo "Number of iterations: ${nIter}"
echo "Number of tips: ${nTips[@]}"
echo "Number of states: ${nStates[@]}"
echo "etaGamma values: ${etaGamma[@]}"
echo "etaDelta values: ${etaDelta[@]}"
echo "etaRho values: ${etaRho[@]}"
echo "cladoNoChange values: ${cladoNoChange[@]}"
echo "cladoFission values: ${cladoFission[@]}"
echo "cladoFusion values: ${cladoFusion[@]}"
echo "cladoPolyploid values: ${cladoPolyploid[@]}"
echo "mu values: ${mu[@]}"
echo "Timeout: ${timeout}"

SIM_FOLDER="`pwd`/SimulatedData/${runName}"

if [ ! -d ${SIM_FOLDER} ]; then
  echo "-----------------------------------------------"
  echo "Simulating the datasets"
  bash scripts/createData.sh ${SETTING_FILE} ${SIM_FOLDER} ${REVBAYES_BIN}
  echo "Done"
fi

echo "-----------------------------------------------"

echo "Benchmarking RB "
bash ./RunRB.sh ${SETTING_FILE} ${SIM_FOLDER} ${REVBAYES_BIN}  > log/${runName}_RB_TP_OPTI.log &

N_PROC=1
MY_LIB="opti"
echo "Benchmarking RB + TP with ${N_PROC} threads and approximator ${MY_LIB}"
bash ./RunRB_TP.sh ${SETTING_FILE} ${SIM_FOLDER} ${REVBAYES_BIN} ${BASE_LIB_PATH}/lib_${MY_LIB} ${N_PROC} > log/${runName}_RB_TP_OPTI.log &

N_PROC=1
MY_LIB="bw2"
echo "Benchmarking RB + TP with ${N_PROC} threads and approximator ${MY_LIB}"
bash ./RunRB_TP.sh ${SETTING_FILE} ${SIM_FOLDER} ${REVBAYES_BIN} ${BASE_LIB_PATH}/lib_${MY_LIB} ${N_PROC} > log/${runName}_RB_TP_BW.log & # > log/${runName}_RB_TP_${N_PROC}T.log
