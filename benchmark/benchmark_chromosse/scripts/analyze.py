#!/usr/bin/python
import os
import sys
import dendropy
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from palettable.colorbrewer.diverging import RdBu_8
from palettable.colorbrewer.qualitative import Set2_8, Dark2_8, Set1_9


myPalette = Set1_9

REF_IMPL_LL = 'Analytical'

DPI = 100
LINE_WIDTH=2.5

font = {'family' : 'DejaVu Sans',
    'weight' : 'normal',
    'size'   : 10}

plt.rc('font', **font)
#plt.rc('text', usetex=True)
plt.rcParams['svg.fonttype'] = 'none'
plt.rcParams['lines.linewidth'] = LINE_WIDTH
plt.rcParams.update({'errorbar.capsize': LINE_WIDTH})


def readSettings(filename):
    inFile = file(filename, 'r')

    runName = ''
    nIter = timeout = 0
    nTips = nStates = vecLambda = vecMu = []

    settings = {}

    line = inFile.readline().strip()
    while line:
        nextLine = inFile.readline().strip()
        if line == "runName":
            settings[line] = nextLine
        elif line == "nIter":
            settings[line] = int(nextLine)
        elif line == "nTips":
            words = nextLine.split(' ')
            settings[line] = [int(w) for w in words]
        elif line == "nStates":
            words = nextLine.split(' ')
            settings[line] = [int(w) for w in words]
        elif line == "etaGamma":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "etaRho":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "etaDelta":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "cladoNoChange":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "cladoFission":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "cladoFusion":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "cladoPolyploid":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "mu":
            words = nextLine.split(' ')
            settings[line] = [float(w) for w in words]
        elif line == "timeout":
            settings[line] = int(nextLine)

        #Read next line:
        line = inFile.readline().strip()

    return settings


def readBenchmark(impl, filename):
    dFrame = pd.read_csv(filename, sep=' ')
    dFrame = dFrame.drop(['error'], axis=1)
    dFrame = dFrame.drop(['nIter'], axis=1)
    nStepsImpl = 'nSteps{}'.format(impl)
    timeImpl = 'time{}'.format(impl)
    llImpl = 'll{}'.format(impl)
    dFrame = dFrame.rename(columns={'meanTime' : timeImpl, 'loglik' : llImpl, 'nStepsInt' : nStepsImpl})

    return dFrame

def mergeDFrame(impls, dFrames):

    if len(impls) == 1:
        return dFrames[impls[0]]
    else:
        df = pd.merge(dFrames[impls[0]], dFrames[impls[1]], on=['nTips', 'nStates', 'iParam'], how = 'outer')

        for i in range(2, len(impls)):
            df = pd.merge(df, dFrames[impls[i]], on=['nTips', 'nStates', 'iParam'], how = 'outer')

    return df


def plotAvgSpeedupsSummary(settings, impls, df, figureFolder):

    nPlots = len(impls)

    X_WIDTH = 350*nPlots
    Y_WIDTH = 300*nPlots
    fig, ax = plt.subplots(nPlots, nPlots, figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=False, sharex=False , sharey='row')

    dfMod = df

    for i, implA in zip(range(len(impls)), impls):
        localRef = implA

        for j, implB in zip(range(len(impls)), impls):

            if j == 0 :  ax[i, j].set_ylabel('Speedup (log)')
            if i == len(impls)-1: ax[i, j].set_xlabel('Number of states (log2)')

            if i == j: continue

            timeA = dfMod['{}{}'.format('time', implA)]
            timeB = dfMod['{}{}'.format('time', implB)]
            speedup = 'sp{}to{}'.format(implB, implA)
            dfMod[speedup] = np.log10(timeB / timeA)

            dfLocal = dfMod[['nTips', 'nStates', speedup]]
            dfLocal = dfLocal.dropna(subset=[speedup])
            group = dfLocal.groupby(['nTips', 'nStates'] , as_index = False).mean()

            pivotedDF = group.pivot(index='nTips', columns='nStates', values=speedup)
            #print pivotedDF
            rangeNTips = pivotedDF.index #nTips
            rangeNStates = pivotedDF.columns #nStates
            val = pivotedDF.to_numpy().transpose()
            #print val.shape
            #print "--------------------------------------------"

            #rangeY = flatY[:val.shape[0]]
            ax[i, j].plot(np.log2(rangeNStates), [1. for y in rangeNStates], '--', color='grey')

            for iX, t in zip(range(len(rangeNTips)), rangeNTips):
                #if iX >= val.shape[1]: continue
                ax[i, j].plot(np.log2(rangeNStates), np.power(10,val[:,iX]), '-x', color=myPalette.hex_colors[iX])

            ax[i, j].set_yscale('log')
            ax[i, j].title.set_text('Sp. {} over {}'.format(implA, implB))
            #ax[i, j].get_yaxis().set_major_formatter(mpl.ticker.ScalarFormatter())

    for i, implA in zip(range(len(impls)), impls):
        localRef = implA
        labels = []
        speedups = []
        colors = []

        for j, implB in zip(range(len(impls)), impls):
            if i==j: continue

            labels.append(implB)
            speedup = 'sp{}to{}'.format(implB, implA)
            speedups.append(dfMod[speedup].to_numpy())
            speedups[-1] = speedups[-1][np.logical_not(np.isnan(speedups[-1]))]
            speedups[-1] = np.power(10, speedups[-1])
            colors.append(myPalette.hex_colors[j])

        bplot = ax[i,i].boxplot(speedups, patch_artist=True, labels=labels)
        ax[i, i].title.set_text(implA)
        ax[i, i].title.set_color(myPalette.hex_colors[i])
        ax[i, i].axhline(y=1., linestyle='--', color='grey')
        ax[i, i].set_xticklabels(ax[i, i].get_xticklabels(),rotation=20)
        ax[i, i].set_yscale('log')

        for patch, color in zip(bplot['boxes'], colors):
            patch.set_facecolor(color)



    labels = ['1x'] + ['{} taxa'.format(l) for l in settings["nTips"]]
    ax[0, 1].legend(labels=labels)
    ax[1, 0].legend(labels=labels)

    #plt.suptitle('Speedup f(time)')

    plt.tight_layout()

    plt.savefig('{}/Speedup.png'.format(figureFolder))
    plt.savefig('{}/Speedup.svg'.format(figureFolder))
    plt.close(fig)

def plotAvgTimes(settings, impls, df, figureFolder):

    X_WIDTH = 600
    Y_WIDTH = 400
    fig, ax = plt.subplots(1, 1, figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=False, sharex=False , sharey='row')

    dfMod = df

    K = dfMod['nStates']
    N = dfMod['nTips']
    complexity = np.power(N*N*K*K, 1/4.)
    dfMod['complexity'] = complexity

    dfMod = dfMod.sort_values(by=['complexity'])

    ax.axhline(0.01, color='k', ls='dotted', alpha=0.5)
    ax.axhline(1., color='k', ls='dotted', alpha=0.5)
    ax.axhline(30., color='k', ls='dotted', alpha=0.5)
    ax.axhline(60., color='k', ls='dotted', alpha=0.5)

    lines = []
    for i, impl in zip(range(len(impls)), impls):

        strTime = '{}{}'.format('time', impl)

        dfLocal = dfMod.dropna(subset=[strTime])
        dfLocal = dfLocal[['complexity', strTime]]
        groupAvg = dfLocal.groupby(['complexity'], as_index = False).mean()
        groupStd = dfLocal.groupby(['complexity'], as_index = False).std()

        eBar = ax.errorbar(x=groupAvg['complexity'].to_numpy(), y=groupAvg[strTime].to_numpy(), yerr=groupStd[strTime].to_numpy(), color=myPalette.hex_colors[i], solid_capstyle='butt')
        lines.append(eBar[0])
    ax.set_yscale('log')

    axR = ax.twinx()
    axR.set_yscale('log')
    axR.set_ylim(ax.get_ylim())
    axR.set_yticks([0.01, 1, 30, 60])
    axR.set_yticklabels(['0.01 sec.', '1 sec.', '30 sec.', '1 min.'])

    ax.set_ylabel('Time per likelihoods (seconds)')

    ax.set_xlabel('Analyses complexity (f(nStates, nTips))')

    N=[32, 128, 512, 2048]
    K=[64, 256, 256, 256]
    xTicks=np.power([i*i*j*j for i, j in zip(N,K)], 1./4.)

    ax.set_xticks(xTicks)
    ax.set_xticklabels(['{} x {}'.format(i,j) for i,j in zip(N,K)])

    axT = ax.twiny()
    axT.set_xlim(ax.get_xlim())

    N=[128, 512, 2048, 256]
    #K=[2**4, 2**8, 2**10, 2**12]
    kPow=[5, 7, 8, 10]
    xTicks=np.power([i*i*(2**j)*(2**j) for i, j in zip(N,kPow)], 1./4.)
    axT.set_xticks(xTicks)
    axT.set_xticklabels(['{} x {} ({} areas)'.format(i,2**j, j) for i,j in zip(N,kPow)])

    ax.legend(handles=lines, labels=impls)


    plt.tight_layout()

    plt.savefig('{}/times.png'.format(figureFolder))
    plt.savefig('{}/times.svg'.format(figureFolder))
    plt.close(fig)


def plotAvgSpeedup(settings, impls, df, figureFolder):

    X_WIDTH = 600
    Y_WIDTH = 400
    fig, ax = plt.subplots(1, 1, figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=False, sharex=False , sharey='row')

    dfMod = df

    K = dfMod['nStates']
    N = dfMod['nTips']
    complexity = np.power(N*N*K*K, 1/4.)
    dfMod['complexity'] = complexity

    dfMod = dfMod.sort_values(by=['complexity'])

    lines = []
    implRef = impls[0]
    strTimeRef = '{}{}'.format('time', implRef)
    for i, impl in zip(range(len(impls[1:])), impls[1:]):


        strTime = '{}{}'.format('time', impl)
        strSpeedup = 'sp{}To{}'.format(implRef, impl)

        dfMod[strSpeedup] = dfMod[strTimeRef]/dfMod[strTime]

        dfLocal = dfMod.dropna(subset=[strSpeedup])

        dfLocal = dfLocal[['complexity', strSpeedup]]
        groupAvg = dfLocal.groupby(['complexity'], as_index = False).mean()
        groupStd = dfLocal.groupby(['complexity'], as_index = False).std()

        eBar = ax.errorbar(x=groupAvg['complexity'].to_numpy(), y=groupAvg[strSpeedup].to_numpy(), yerr=groupStd[strSpeedup].to_numpy(), color=myPalette.hex_colors[i], solid_capstyle='butt')
        lines.append(eBar[0])

    #ax.set_yscale('log')

    ax.set_ylabel('Speedup')

    ax.set_xlabel('Analyses complexity (f(nStates, nTips))')

    N=[32, 128, 512, 2048]
    K=[64, 256, 256, 256]
    xTicks=np.power([i*i*j*j for i, j in zip(N,K)], 1./4.)

    ax.set_xticks(xTicks)
    ax.set_xticklabels(['{} x {}'.format(i,j) for i,j in zip(N,K)])

    axT = ax.twiny()
    axT.set_xlim(ax.get_xlim())

    N=[128, 512, 2048, 256]
    #K=[2**4, 2**8, 2**10, 2**12]
    kPow=[5, 7, 8, 10]
    xTicks=np.power([i*i*(2**j)*(2**j) for i, j in zip(N,kPow)], 1./4.)
    axT.set_xticks(xTicks)
    axT.set_xticklabels(['{} x {} ({} areas)'.format(i,2**j, j) for i,j in zip(N,kPow)])

    ax.legend(handles=lines, labels=impls[1:])


    plt.tight_layout()

    plt.savefig('{}/speedupTP.png'.format(figureFolder))
    plt.savefig('{}/speedupTP.svg'.format(figureFolder))
    plt.close(fig)



settingFile = sys.argv[1]
figureFolder = sys.argv[2]
settings = readSettings(settingFile)

figureFolder += '/' + settings['runName']

if not os.path.isdir(figureFolder) :
    os.makedirs(figureFolder)


#implementations = ['RB', 'RB_TP_1T_bw', 'RB_TP_1T_opti', 'RB_TP_2T_bwPar', 'RB_TP_2T_optiPar','RB_TP_4T_bwPar', 'RB_TP_4T_optiPar', 'RB_TP_1T_AT', 'RB_TP_6T_AT']
implementations = ['RB', 'TP_1T', 'TP_2T', 'TP_4T', 'TP_6T'] #'RB',
#implementations = ['Diversitree', 'TensorPhylo', 'RevBayes', 'RevBayesWithTP', 'Castor']

dFrames = {}
for impl in implementations:
    resultFile = './results/' + settings["runName"] + '/time' + impl + '.txt'
    print resultFile
    dFrames[impl] = readBenchmark(impl, resultFile)

df = mergeDFrame(implementations, dFrames)

plotAvgSpeedupsSummary(settings, implementations, df, figureFolder)



implementations = ['RB', 'TP_1T', 'TP_6T'] #'RB',
plotAvgTimes(settings, implementations, df, figureFolder)
#implementations = ['RB', 'TP_1T', 'TP_2T', 'TP_4T', 'TP_6T']
#plotAvgSpeedup(settings, implementations, df, figureFolder)

implementations = ['TP_1T', 'TP_2T', 'TP_4T', 'TP_6T']
plotAvgSpeedup(settings, implementations, df, figureFolder)

plt.show()
