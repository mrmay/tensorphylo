#!/bin/bash

# benchmark setting
runName="test"
nIter=0
nTips=(0)
nStates=(0)
etaGamma=(0)
etaDelta=(0)
etaRho=(0)
cladoNoChange=(0)
cladoFission=(0)
cladoFusion=(0)
cladoPolyploid=(0)
mu=(0)
timeout=0

# Read the setting file given as argument
while IFS= read -r key
do
  if [ "$key" == "nIter" ]; then
    read -r value
    nIter=${value}
  elif [ "$key" == "nTips" ]; then
    read -r value
    nTips=(${value})
  elif [ "$key" == "nStates" ]; then
    read -r value
    nStates=(${value})
  elif [ "$key" == "etaGamma" ]; then
    read -r value
    etaGamma=(${value})
  elif [ "$key" == "etaDelta" ]; then
    read -r value
    etaDelta=(${value})
  elif [ "$key" == "etaRho" ]; then
    read -r value
    etaRho=(${value})
  elif [ "$key" == "cladoNoChange" ]; then
    read -r value
    cladoNoChange=(${value})
  elif [ "$key" == "cladoFission" ]; then
    read -r value
    cladoFission=(${value})
  elif [ "$key" == "cladoFusion" ]; then
    read -r value
    cladoFusion=(${value})
  elif [ "$key" == "cladoPolyploid" ]; then
    read -r value
    cladoPolyploid=(${value})
  elif [ "$key" == "mu" ]; then
    read -r value
    etaRho=(${value})
    mu=(${value})
  elif [ "$key" == "timeout" ]; then
    read -r value
    timeout=${value}
  elif [ "$key" == "runName" ]; then
    read -r value
    runName=${value}
  else
    echo "Format problem in the setting file"
    exit 1
  fi
done < "${SETTING_FILE}"
