#!/usr/bin/python
import os
import sys
import math
import dendropy
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from palettable.colorbrewer.diverging import RdBu_8
from palettable.colorbrewer.qualitative import Set1_6, Set2_8

REF_IMPL_LL = 'Analytical'
REF_IMPL_TIME = 'Diversitree'

DPI = 100
LINE_WIDTH=2.5

font = {'family' : 'DejaVu Sans',
    'weight' : 'normal',
    'size'   : 8}

plt.rc('font', **font)
plt.rcParams['lines.linewidth'] = LINE_WIDTH

class Probe:
  def __init__(self):
    self.time = 0.
    self.nStates = 0
    self.nEdges = 0


def readDumpFile(filename):
    inFile = file(filename, 'r')

    edges = set()
    data = []
    intTimes = []
    vecSR = []
    vecLN = []

    probe = None
    line = "fakeLine"
    while line and (len(data) < 5000000) :
        line = inFile.readline().strip()
        words = line.split()
        if len(words) == 0: continue

        if words[0] == 'time':
            probe = Probe()
            nextLine = inFile.readline().strip()
            probe.time = float(nextLine)
            print "Time : ", probe.time
        elif words[0] == 'nStates':
            nextLine = inFile.readline().strip()
            probe.nStates = int(nextLine)
        elif words[0] == 'nEdges':
            nextLine = inFile.readline().strip()
            probe.nEdges = int(nextLine)
        elif words[0] == 'U':
            nextLine = inFile.readline().strip()
            words = nextLine.split()
            for iS in range(probe.nStates):
                scaler = float(words[-1])
                scaledValue = float(words[iS])
                rawValue = scaledValue * math.exp(scaler)
                if scaledValue == 0.:
                    logValue = None
                else:
                    logValue = math.log(scaledValue)+scaler
                row = [probe.time, 'U', iS, rawValue, scaledValue, logValue, scaler]
                data.append(row)
            edges.add('U')
        elif words[0] == 'P':
            for iE in range(probe.nEdges):
                nextLine = inFile.readline().strip()
                words = nextLine.split()
                edges.add('Edge{}'.format(words[0]))
                scaler = float(words[-1])
                for iS in range(probe.nStates):
                    scaledValue = float(words[1+iS])
                    rawValue = scaledValue * math.exp(scaler)
                    if scaledValue == 0.:
                        logValue = None
                    else:
                        logValue = math.log(scaledValue)+scaler
                    row = [probe.time, 'Edge{}'.format(words[0]), iS, rawValue, scaledValue, logValue, scaler]
                    data.append(row)
        elif line == 'Integration times':
            nextLine = inFile.readline().strip()
            words = nextLine.split()
            intTimes = [float(w) for w in words]
        elif line == 'SR':
            nextLine = inFile.readline().strip()
            words = nextLine.split()
            sr = float(words[0])
            if sr > 0.:
                vecSR.append([sr])
                vecLN.append(int(words[1]))

    df = pd.DataFrame(data, columns=['time', 'edge', 'state', 'rawV', 'scaledV', 'logV', 'scaler'])

    if len(intTimes) == 0:
        intTimes = df.time.unique()
        print "Times : ", intTimes

    return [probe.nStates, list(edges), df, intTimes, vecSR, vecLN]

def plotODE(nStates, valTypes, edges, df, outputFolder):

    N_LINES=5
    myPal = sns.diverging_palette(220, 20, n=nStates, center='dark')

    for edge in edges:

        print 'Treating edge {}'.format(edge)

        Y_WIDTH = (N_LINES+1)*400
        X_WIDTH = len(valTypes)*400
        fig, ax = plt.subplots(N_LINES+1, len(valTypes), figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=True, sharex=True, sharey=False)

        subsetDF = df.loc[(df['edge'] == edge)]
        for iV, valType in zip(range(len(valTypes)), valTypes):
            subsetDF_VT = subsetDF[['time', 'edge', 'state', valType]]

            #myRange = np.linspace(0, (nStates)-(nStates)/N_LINES, N_LINES)+N_LINES/2
            myRange = np.linspace(0, nStates, N_LINES)
            myRange[1:] = [x-1 for x in myRange[1:]]
            for iL, iS in zip(range(N_LINES), myRange):
                tmpDF = subsetDF_VT.loc[(subsetDF_VT['state'] == iS)]
                sns.lineplot(x='time', y=valType, palette=myPal, data=tmpDF, ax=ax[iL,iV], legend=False)
                ax[iL,0].set_ylabel('Prob at k={}'.format(iS))

            sns.lineplot(x='time', y=valType, data=subsetDF_VT, ci="sd", dashes=False, markers=True, ax=ax[N_LINES, iV], legend=False)
            ax[N_LINES,0].set_ylabel('Avg+sd prb')

        plt.suptitle('Edge: {}'.format(edge))
        plt.savefig('{}/def_{}.png'.format(outputFolder, edge))
        plt.close('all')
        #g = sns.FacetGrid(subsetDF, col='state', col_wrap=4)

        '''
        myRange = np.linspace(0, nStates, N_LINES)
        myRange[1:] = [x-1 for x in myRange[1:]]
        for iR in myRange:
            subsetDF_VT = subsetDF[['time', 'edge', 'state', valTypes[0]]]
            tmpDF = subsetDF_VT.loc[(subsetDF_VT['state'] == iR)]

            X_WIDTH = 1200
            Y_WIDTH = 800
            fig, ax = plt.subplots(1, 1, figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=True)
            print tmpDF['time'].to_numpy()
            print tmpDF[valTypes[0]].to_numpy()
            plt.plot(tmpDF['time'].to_numpy(), tmpDF[valTypes[0]].to_numpy(), 'x-')
            plt.show()
        '''


def plotExtremumODE(nStates, valTypes, edges, df, outputFolder):

    for edge in edges:

        print 'Treating extremum - edge {}'.format(edge)

        X_WIDTH = 3*600
        Y_WIDTH = len(valTypes)*400
        fig, ax = plt.subplots(len(valTypes), 3, figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=True, sharex=True, sharey=False)

        subsetDF = df.loc[(df['edge'] == edge)]
        for iV, valType in zip(range(len(valTypes)), valTypes):
            subsetDF_VT = subsetDF[['time', 'edge', 'state', valType]]

            nExt = 3
            lowerDF = subsetDF_VT.loc[(subsetDF_VT['state'] <= nExt)]
            #lowerDF['type'] = ['low' for x in range(len(lowerDF))]
            upperDF = subsetDF_VT.loc[(subsetDF_VT['state'] >= (nStates-(nExt+1)))]
            #upperDF['type'] = ['high' for x in range(len(upperDF))]
            midDF = subsetDF_VT.loc[(subsetDF_VT['state'] >= (nStates/2-(nExt))) & (subsetDF_VT['state'] <= (nStates/2+(nExt)))]

            #myPal = sns.diverging_palette(220, 20, n=2, center='dark')
            sns.lineplot(x='time', y=valType, data=upperDF, ci="sd", ax=ax[iV,0], legend='brief')
            ax[1,0].set_xlabel('Time (low range K)')
            sns.lineplot(x='time', y=valType, data=midDF, ci="sd", ax=ax[iV,1], legend='brief')
            ax[1,1].set_xlabel('Time (mid range K)')
            sns.lineplot(x='time', y=valType, data=lowerDF, ci="sd", ax=ax[iV,2], legend='brief')
            ax[1,2].set_xlabel('Time (high range K)')
            #sns.lineplot(x='time', y=valType, hue='state', palette="cubehelix", data=mergedDF, ax=ax[1,iV])


        plt.suptitle('Edge: {}'.format(edge))
        plt.savefig('{}/ext_{}.png'.format(outputFolder, edge))
        plt.close('all')
        #g = sns.FacetGrid(subsetDF, col='state', col_wrap=4)

def plotAllEdges(nStates, valType, edges, df, outputFolder, intTimes, vecSR, vecLN):

    print 'Plotting all'

    X_WIDTH = 1200
    Y_WIDTH = 800


    if len(vecSR) > 0:
        heights = [4, 1, 1]
        fig, ax = plt.subplots(3, 1, figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=True, sharex=True, sharey=False, gridspec_kw={'height_ratios': heights})

        subsetDF_VT = df[['time', 'edge', 'state', valType]]
        subsetDF_VT = df.loc[(subsetDF_VT['edge'] != 'U')]

        sns.lineplot(x='time', y=valType, hue='edge', data=subsetDF_VT, ax=ax[0], palette=sns.color_palette("Set1", len(edges)-1), ci=None, legend=False)

        probTimes = subsetDF_VT['time'].unique()
        subsetDF_VT = subsetDF_VT.loc[(subsetDF_VT['time'] > 1.0)]
        #print subsetDF_VT[[valType]].min()
        [min, max] = ax[0].get_ylim()
        #ax[0].set_ylim((subsetDF_VT[[valType]].min()[0],max))

        ax[1].hist(intTimes, bins=20, density=False)

        ax[0].set_ylabel('Log avg. prob')
        ax[1].set_ylabel('Int. step cnt')

        ax[1].set_xlabel('Time')

        ax[2].plot(probTimes, vecSR)

        good = [[], []]
        bad = [[], []]
        for iT in range(len(probTimes)):
            if vecLN[iT] == 0:
                good[0].append(probTimes[iT])
                good[1].append(vecSR[iT])
            else:
                bad[0].append(probTimes[iT])
                bad[1].append(vecSR[iT])

        ax[2].scatter(x=good[0], y=good[1], s=50, marker='o', color='g')
        ax[2].scatter(x=bad[0], y=bad[1], s=50, marker='x', color='r')

        ax[2].set_yscale('log')
        ax[2].set_xlabel('Time')
        ax[2].set_ylabel('SR')
        yticks = mpl.ticker.MaxNLocator(3)
        ax[2].yaxis.set_major_locator(yticks)
        yticks = mpl.ticker.LogLocator(3)
        ax[2].yaxis.set_minor_locator(yticks)

        plt.suptitle('All edges')
        plt.savefig('{}/all.png'.format(outputFolder))
        plt.close('all')
    else:
        heights = [4, 1]
        fig, ax = plt.subplots(2, 1, figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=True, sharex=True, sharey=False, gridspec_kw={'height_ratios': heights})

        subsetDF_VT = df[['time', 'edge', 'state', valType]]
        subsetDF_VT = df.loc[(subsetDF_VT['edge'] != 'U')]

        sns.lineplot(x='time', y=valType, hue='edge', data=subsetDF_VT, ax=ax[0], palette=sns.color_palette("Set1", len(edges)-1), ci=None, legend=False)

        subsetDF_VT = subsetDF_VT.loc[(subsetDF_VT['time'] > 1.0)]
        #print subsetDF_VT[[valType]].min()
        [min, max] = ax[0].get_ylim()
        #ax[0].set_ylim((subsetDF_VT[[valType]].min()[0],max))

        ax[1].hist(intTimes, bins=20, density=False)

        ax[0].set_ylabel('Log avg. prob')
        ax[1].set_ylabel('Int. step cnt')


        ax[1].set_xlabel('Time')

        plt.suptitle('All edges')
        plt.savefig('{}/all.png'.format(outputFolder))
        plt.close('all')

    ######################### with CI #######################

    X_WIDTH = 1200
    Y_WIDTH = 800
    heights = [4, 1]

    fig, ax = plt.subplots(2, 1, figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=True, sharex=True, sharey=False, gridspec_kw={'height_ratios': heights})

    subsetDF_VT = df[['time', 'edge', 'state', valType]]
    subsetDF_VT = df.loc[(subsetDF_VT['edge'] != 'U')]

    sns.lineplot(x='time', y=valType, hue='edge', data=subsetDF_VT, ax=ax[0], palette=sns.color_palette("Set1", len(edges)-1), ci='sd', legend=False)

    subsetDF_VT = subsetDF_VT.loc[(subsetDF_VT['time'] > 1.0)]
    #print subsetDF_VT[[valType]].min()
    [min, max] = ax[0].get_ylim()
    #ax[0].set_ylim((subsetDF_VT[[valType]].min()[0],max))

    ax[1].hist(intTimes, bins=20, density=False)

    ax[0].set_ylabel('Log avg. prob')
    ax[1].set_ylabel('Int. step cnt')

    ax[1].set_xlabel('Time')

    plt.suptitle('All edges')
    plt.savefig('{}/all_CI.png'.format(outputFolder))
    plt.close('all')

    ######################### figures per slices #######################
    '''
    # time slices
    maxTime = df[['time']].max()
    step = 1.0
    nFig = int(maxTime/step)+1

    fig, ax = plt.subplots(nFig, 1, figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=True, sharex=False, sharey=False)

    subsetDF_VT = df[['time', 'edge', 'state', valType]]
    subsetDF_VT = df.loc[(subsetDF_VT['edge'] != 'U')]

    for i in range((nFig)):
        plotDF = subsetDF_VT.loc[(subsetDF_VT['time'] >= i*step) & (subsetDF_VT['time'] <= (i+1)*step)]
        nColors = plotDF[['edge']].nunique()[0]
        print nColors
        sns.lineplot(x='time', y=valType, hue='edge', data=plotDF, ax=ax[i], palette=sns.color_palette("Set1", nColors), ci=None, legend=False)

        #ax.set_ylim((-75,0))

    plt.suptitle('All edges - sliced')
    plt.savefig('{}/allSliced.png'.format(outputFolder))

    #g = sns.FacetGrid(subsetDF, col='state', col_wrap=4)
    '''

dumpFile = sys.argv[1]
outputFolder = sys.argv[2]

[nStates, edges, df, intTimes, vecSR, vecLN] = readDumpFile(dumpFile)

valTypes = ['rawV', 'logV']

plotODE(nStates, valTypes, edges, df, outputFolder)

plotExtremumODE(nStates, valTypes, edges, df, outputFolder)

plotAllEdges(nStates, 'logV', edges, df, outputFolder, intTimes, vecSR, vecLN)


#plt.show()
