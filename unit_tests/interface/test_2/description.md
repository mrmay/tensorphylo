# Description
Time-homogeneous, state-homogeneous birth-death process on a tree with sequential CPU implementation

# Tags
[Interface][Sequential][CPU][TimeHomogeneous][StateHomogeneous][Yule][NoBDD]
