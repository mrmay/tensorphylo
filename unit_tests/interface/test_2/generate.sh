#!/bin/bash

# step 0: prepare parameters 
rb prepare_data.Rev;

# step 1: compute ll using external program
Rscript compute_ll.R
