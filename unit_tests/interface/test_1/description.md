# Description
Time-homogeneous, state-homogeneous Yule process on a tree with sequential CPU implementation

# Tags
[Interface][Sequential][CPU][TimeHomogeneous][StateHomogeneous][Yule][NoBDD]
