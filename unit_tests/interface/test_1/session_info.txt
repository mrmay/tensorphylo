R version 3.6.2 (2019-12-12)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: Ubuntu 18.04.3 LTS

Matrix products: default
BLAS:   /usr/lib/x86_64-linux-gnu/blas/libblas.so.3.7.1
LAPACK: /usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3.7.1

locale:
 [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
 [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
 [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
 [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
[1] dispRity_1.3.5 ape_5.3       

loaded via a namespace (and not attached):
 [1] Rcpp_1.0.3              compiler_3.6.2          tools_3.6.2            
 [4] magic_1.5-9             phytools_0.6-99         phyclust_0.1-28        
 [7] digest_0.6.24           nlme_3.1-143            lattice_0.20-38        
[10] mgcv_1.8-31             pkgconfig_2.0.3         Matrix_1.2-18          
[13] fastmatch_1.1-0         igraph_1.2.4.2          parallel_3.6.2         
[16] expm_0.999-4            mvtnorm_1.0-12          coda_0.19-3            
[19] cluster_2.1.0           geiger_2.0.6.4          gtools_3.8.1           
[22] maps_3.3.0              combinat_0.0-8          ade4_1.7-15            
[25] grid_3.6.2              scatterplot3d_0.3-41    deSolve_1.27.1         
[28] plotrix_3.7-7           animation_2.6           phangorn_2.5.5         
[31] strap_1.4               gdata_2.18.0            clipr_0.7.0            
[34] magrittr_1.5            splines_3.6.2           Claddis_0.3.4          
[37] MASS_7.3-51.5           abind_1.4-5             mnormt_1.5-6           
[40] permute_0.9-5           geoscale_2.0            numDeriv_2016.8-1.1    
[43] quadprog_1.5-8          subplex_1.5-4           geometry_0.4.5         
[46] vegan_2.5-6             clusterGeneration_1.3.4
