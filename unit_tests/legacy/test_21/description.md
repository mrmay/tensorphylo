# Description
State-dependent birth-death process on a tree (crown age) with sequential CPU implementation

# Tags
[Sequential][CPU][SSE][Tree][NoBDD][Validation]
