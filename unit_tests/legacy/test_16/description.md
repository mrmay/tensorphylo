# Description
Conditional birth-death process on a tree starting at the stem with sequential CPU implementation

# Tags
[Sequential][CPU][StateIndependent][BDP][Condition][Tree][Stem][NoBDD][Validation]
