library(TESS)
source("validation/fbd_equations.R")

# settings
lambda  = 1.0
mu      = 0.5
time    = 1.0
rho     = 1.0
me_time = time / 2
me_prob = c(0.25, 0.5, 0.75)

format(sapply(me_prob, function(x) {
  TESS:::tess.equations.p1.constant(lambda, mu, me_time, 1 - x, rho, 0, 1, log=FALSE)
}))

