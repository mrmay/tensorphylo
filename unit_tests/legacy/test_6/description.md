# Description
Birth-death-mass-extinction process on a single branch with sequential CPU with high-precision integration

# Tags
[Sequential][CPU][StateIndependent][BDP][SingleBranch][NoBDD][MassExtinction][Validation]
