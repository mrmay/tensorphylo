# Description
Birth-death process on a tree with sequential CPU implementation

# Tags
[Sequential][CPU][StateIndependent][BDP][Tree][NoBDD]
