library(TESS)
source("validation/fbd_equations.R")

# read the tree
tree  = read.nexus("unit_tests/test_11/example_yule_tree.nex")
times = branching.times(tree)

# settings
lambda  = 1.0
mu      = 0.5
rho     = 1.0
me_time = max(branching.times(tree)) - 0.5
me_prob = c(0.25, 0.5, 0.75)

format(sapply(me_prob, function(x) {
  TESS::tess.likelihood(times=times, lambda=lambda, mu=mu, massExtinctionTimes=me_time, massExtinctionSurvivalProbabilities=1 - x, samplingProbability=rho, CONDITION="time", log=TRUE) + log(lambda)
}))
