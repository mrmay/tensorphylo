# Description
Birth-death process on a tree with extinct tips with sequential CPU implementation

# Tags
[Sequential][CPU][StateIndependent][BDP][Tree][Fossils][NoBDD][Validation]
