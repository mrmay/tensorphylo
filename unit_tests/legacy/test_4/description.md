# Description
Birth-death process on a single branch with sequential CPU implementation with high-precision integration.

# Tags
[Sequential][CPU][StateIndependent][BDP][SingleBranch][NoBDD][Validation]
