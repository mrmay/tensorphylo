# Description
Conditional birth-death process on a tree starting at the crown with sequential CPU implementation

# Tags
[Sequential][CPU][StateIndependent][BDP][Condition][Tree][Crown][NoBDD][Validation]
