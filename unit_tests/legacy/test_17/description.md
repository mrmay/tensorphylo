# Description
Conditional fossilized birth-death process on a tree starting at the crown with sequential CPU implementation

# Tags
[Sequential][CPU][StateIndependent][FBD][Condition][Tree][Crown][NoBDD][Validation]
