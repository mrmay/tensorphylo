# Description
Birth-death-fossilization process on a tree (stem age) with sequential CPU implementation

# Tags
[Sequential][CPU][StateIndependent][FBD][Tree][NoBDD][Validation]
