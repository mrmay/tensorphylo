# Description
Conditional fossilized birth-death process on a tree starting at the stem with sequential CPU implementation

# Tags
[Sequential][CPU][StateIndependent][FBD][Condition][Tree][Stem][NoBDD][Validation]
