# Description
Yule process on a single branch with sequential CPU implementation.

# Tags
[Sequential][CPU][StateIndependent][Yule][SingleBranch][NoBDD]
