# Description
Birth-death-mass-extinction process on a single branch with sequential CPU

# Tags
[Sequential][CPU][StateIndependent][BDP][SingleBranch][NoBDD][MassExtinction]
