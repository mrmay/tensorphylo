# Description
Birth-death mass-extinction process on a tree with sequential CPU implementation

# Tags
[Sequential][CPU][StateIndependent][BDP][MassExtinction][Tree][NoBDD][Validation]
