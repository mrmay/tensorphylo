# Description
Birth-death process on a single branch with sequential CPU implementation.

# Tags
[Sequential][CPU][StateIndependent][BDP][SingleBranch][NoBDD]
