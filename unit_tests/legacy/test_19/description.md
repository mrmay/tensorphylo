# Description
Yule process on a tree with extinct tips with sequential CPU implementation

# Tags
[Sequential][CPU][StateIndependent][Yule][Tree][Fossils][NoBDD][Validation]
