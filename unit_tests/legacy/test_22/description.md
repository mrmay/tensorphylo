# Description
Cladogenetic state-dependent birth-death process on a tree (crown age) with sequential CPU implementation

# Tags
[Sequential][CPU][SSE][Cladogenetic][Tree][NoBDD][Validation]
