# Description
Yule process on a single branch with sequential CPU implementation with high-precision integration.

# Tags
[Sequential][CPU][StateIndependent][Yule][SingleBranch][NoBDD][Validation]
