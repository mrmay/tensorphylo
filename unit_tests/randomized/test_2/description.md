# Description
Time-homogeneous, state-homogeneous sampled birth-death process on a tree with sequential CPU implementation

# Tags
[Randomized][Sequential][CPU][TimeHomogeneous][StateHomogeneous][Yule][NoBDD]
