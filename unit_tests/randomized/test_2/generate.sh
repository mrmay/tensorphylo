#!/bin/bash

# step 1: prepare parameters 
rb prepare_data.Rev > rev_log.out;

# step 2: compute ll using external program
Rscript compute_ll.R > r_log.out;
